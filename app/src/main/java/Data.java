public class Data{
	private String address;
	private double distance;
	private Object message;
	private Object disid;
	private double latitude;
	private int rId;
	private String timedate;
	private String datetime;
	private String regName;
	private double langtitude;
	private Object updateddate;
	private int id;
	private Object regid;
	private int trackId;

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setDistance(double distance){
		this.distance = distance;
	}

	public double getDistance(){
		return distance;
	}

	public void setMessage(Object message){
		this.message = message;
	}

	public Object getMessage(){
		return message;
	}

	public void setDisid(Object disid){
		this.disid = disid;
	}

	public Object getDisid(){
		return disid;
	}

	public void setLatitude(double latitude){
		this.latitude = latitude;
	}

	public double getLatitude(){
		return latitude;
	}

	public void setRId(int rId){
		this.rId = rId;
	}

	public int getRId(){
		return rId;
	}

	public void setTimedate(String timedate){
		this.timedate = timedate;
	}

	public String getTimedate(){
		return timedate;
	}

	public void setDatetime(String datetime){
		this.datetime = datetime;
	}

	public String getDatetime(){
		return datetime;
	}

	public void setRegName(String regName){
		this.regName = regName;
	}

	public String getRegName(){
		return regName;
	}

	public void setLangtitude(double langtitude){
		this.langtitude = langtitude;
	}

	public double getLangtitude(){
		return langtitude;
	}

	public void setUpdateddate(Object updateddate){
		this.updateddate = updateddate;
	}

	public Object getUpdateddate(){
		return updateddate;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setRegid(Object regid){
		this.regid = regid;
	}

	public Object getRegid(){
		return regid;
	}

	public void setTrackId(int trackId){
		this.trackId = trackId;
	}

	public int getTrackId(){
		return trackId;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"address = '" + address + '\'' + 
			",distance = '" + distance + '\'' + 
			",message = '" + message + '\'' + 
			",disid = '" + disid + '\'' + 
			",latitude = '" + latitude + '\'' + 
			",r_id = '" + rId + '\'' + 
			",timedate = '" + timedate + '\'' + 
			",datetime = '" + datetime + '\'' + 
			",regName = '" + regName + '\'' + 
			",langtitude = '" + langtitude + '\'' + 
			",updateddate = '" + updateddate + '\'' + 
			",id = '" + id + '\'' + 
			",regid = '" + regid + '\'' + 
			",trackId = '" + trackId + '\'' + 
			"}";
		}
}
