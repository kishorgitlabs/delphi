package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainmagic.delphitvs.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import model.filters.details.FilterDetailList;

public class CommonRailProductsAdapter extends ArrayAdapter {
    private List<FilterDetailList> commonRailData,dupItems;
    private Context context;


    public CommonRailProductsAdapter(Context context,List<FilterDetailList> commonRailData) {
        super(context, R.layout.common_rail_products);
        this.commonRailData=commonRailData;
        this.context=context;
        dupItems=new ArrayList<>(commonRailData);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view= LayoutInflater.from(context).inflate(R.layout.common_rail_products,null);
//        TextView sNo=view.findViewById(R.id.common_rail_sno);
        TextView app=view.findViewById(R.id.common_rail_app);
//        TextView pNo=view.findViewById(R.id.common_rail_pump);
//        ImageView commom_service=view.findViewById(R.id.commom_service);

//        commom_service.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent commonservice=new Intent(context, CommonPumpServiceList.class);
//                context.startActivity(commonservice);
//            }
//        });

//        sNo.setText(position+1+"");
        app.setText(commonRailData.get(position).getVehicleApplication());
//        pNo.setText(commonRailData.get(position).getPartNumber());
        return view;
    }

    @Override
    public int getCount() {
        return commonRailData.size();
    }

    public void filter(String s) {

        s = s.toLowerCase(Locale.getDefault()).trim();
        commonRailData.clear();

        if (s.length() == 0) {
            commonRailData.addAll(dupItems);

        } else {
            for (int i=0;i<dupItems.size();i++) {
                String wp = dupItems.get(i).getPartNumber();

                if (wp.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())) {
                    commonRailData.add(dupItems.get(i));

                }
            }
        }
        notifyDataSetChanged();
    }
}
