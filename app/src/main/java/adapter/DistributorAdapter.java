package adapter;

import android.content.Context;


import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.brainmagic.delphitvs.R;

import java.util.List;

import model.distributorlist.DistributorData;

public class DistributorAdapter extends ArrayAdapter {

    private Context context;
    private List<DistributorData> data;

    public DistributorAdapter(@NonNull Context context, List<DistributorData> distributorData) {
        super(context, R.layout.distributoradapter);
        this.context=context;
        this.data=distributorData;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        convertView=null;
        if (convertView==null){

            convertView= LayoutInflater.from(context).inflate(R.layout.distributoradapter,null);

            TextView distributorno=convertView.findViewById(R.id.distributorno);
            TextView salescode=convertView.findViewById(R.id.salescode);
            TextView salesname=convertView.findViewById(R.id.salesname);
            TextView salesaddresses=convertView.findViewById(R.id.salesaddresses);

            distributorno.setText(data.get(position).getCustomerCode());
            salescode.setText(data.get(position).getSalesOffice());
            salesname.setText(data.get(position).getName());
            salesaddresses.setText(data.get(position).getStreet3()+","+data.get(position).getStreet2()+","+data.get(position).getStreet());
        }
        return convertView;
    }

    @Override
    public int getCount() {
        return data.size();
    }
}

