package adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.ServicepartList;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import alert.Alert;
import model.PartnumberSearch;
import model.filters.details.FilterDetailList;
import model.inlinefields.Inline;
import model.productdetail.ProductDetails;

public class FilterListAdapter extends ArrayAdapter {

    private Context context;
    private List<Inline> data;
    private LinearLayout popup_layout;
    private String Dealer_tex,State_tex;
    private Alert box;
    private CardView cv;
    private int count_position;
    private String value_change,data_value;
    Animation scaleup,popup;
    private List<FilterDetailList> items,dupItems;

    public FilterListAdapter(Context context, List<FilterDetailList> items) {
        super(context, R.layout.productdetailsadapter);
        this.context = context;
        this.items=items;
        box =new Alert(context);
        dupItems=new ArrayList<>(items);
        scaleup = AnimationUtils.loadAnimation(context, R.anim.animation);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @SuppressLint("WrongConstant")
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        FilterListAdapter.ComplaintsHolder holder;

        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.productdetailsadapter, parent, false);

            holder = new FilterListAdapter.ComplaintsHolder();


            holder.tex1 = (TextView) convertView.findViewById(R.id.dtvspartno);
            // holder.tex2 = (TextView) convertView.findViewById(R.id.ed_text2);
            holder.tex2 = (TextView) convertView.findViewById(R.id.oepartno);
            holder.tex3 = (TextView) convertView.findViewById(R.id.brand);
            //  holder.tex5 = (TextView) convertView.findViewById(R.id.text5);
            holder.tex4 = (TextView) convertView.findViewById(R.id.vehicleapplication);
            holder.tex5 = (TextView) convertView.findViewById(R.id.description);
            holder.mrp = (TextView) convertView.findViewById(R.id.mrp_filter);
            holder.filterImages =  convertView.findViewById(R.id.filter_image);

            holder.product_image = (ImageView) convertView.findViewById(R.id.lionlogo);
            holder.cardView = (CardView)convertView.findViewById(R.id.card);
           /* holder.linearLayout_pooup = (LinearLayout) convertView.findViewById(R.id.cart_item_layout);
            holder.linearLayout_application = (LinearLayout) convertView.findViewById(R.id.application);
            holder.linearLayout_alpha = (LinearLayout) convertView.findViewById(R.id.alpha);
*/

//            holder.cardView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent i= new Intent(context, ServicepartList.class);
////                    i.putExtra("DTVSPARTNO",items.get(position).getDTVSPartNo());
//                    context.startActivity(i);
//                }
//
//            });
           /* if(position ==0){
                holder.cardView.setCardBackgroundColor(Color.parseColor("#8bd8bd"));
            }

            else if(position %2 == 0){
                holder.cardView.setCardBackgroundColor(Color.parseColor("#df678c"));
            }else if(position %3 == 0){
                holder.cardView.setBackgroundColor(Color.parseColor("#ffe67c"));
            }else{
                holder.cardView.setCardBackgroundColor(Color.parseColor("#8aaae5"));
            }*/
            holder.tex1.setText(items.get(position).getPartNumber());
            holder.tex2.setText(items.get(position).getOEPartNumber());
            holder.tex3.setText(items.get(position).getSegment());
            holder.tex4.setText(items.get(position).getVehicleApplication());
            holder.tex5.setText(items.get(position).getDescription());
            holder.mrp.setText("₹ "+items.get(position).getMRP());

//            CircularProgressDrawable circularProgressDrawable=new CircularProgressDrawable(context);
//            circularProgressDrawable.setStrokeWidth(5f);
//            circularProgressDrawable.setCenterRadius(30f);
//
//            circularProgressDrawable.start();
            Picasso.with(context).load(items.get(position).getmImage()).error(R.drawable.noimageavail_seven_five).placeholder( R.drawable.progress_animation ).into(holder.filterImages);

           /* if(data1!= null){
                data_value = data1.get(position).getDescription();
                if (data_value.equals("DELIVERY VALVE")) {
                    holder.product_image.setImageResource(R.drawable.deliveryvalve);
                } else if (data_value.equals("ELEMENT")) {
                    holder.product_image.setImageResource(R.drawable.elemnt);

                } else {
                    holder.product_image.setImageResource(R.drawable.nozzle);
                    holder.linearLayout_application.setVisibility(View.GONE);
                    holder.linearLayout_alpha.setVisibility(View.VISIBLE);
                }
            }else {
                if (value_change.equals("DELIVERY VALVE")) {
                    holder.product_image.setImageResource(R.drawable.deliveryvalve);
                } else if (value_change.equals("ELEMENT")) {
                    holder.product_image.setImageResource(R.drawable.elemnt);

                } else {
                    holder.product_image.setImageResource(R.drawable.nozzle);
                    holder.linearLayout_application.setVisibility(View.GONE);
                    holder.linearLayout_alpha.setVisibility(View.VISIBLE);
                }
            }
            holder.linearLayout_pooup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (bool == true)
                        viewPopup(data,position);
                    else
                        viewpopuptwo(data1,position);
                }
            });

            if(bool == true) {
                holder.tex1.setText(data.get(position).getDTVSPartno());
                holder.tex2.setText(data.get(position).getEquivalentPartNo());
                holder.tex3.setText("Rs:" +data.get(position).getMRP());
                holder.tex4.setText(data.get(position).getCustomerApplication());
                if(data.get(position).getHSNCODE() != null)

                    holder.tex5.setText(data.get(position).getHSNCODE());
                else
                    holder.tex5.setText("");
            }
            else {
                holder.tex1.setText(data1.get(position).getDTVSPartno());
                holder.tex2.setText(data1.get(position).getEquivalentPartNo());
                holder.tex3.setText("Rs:" +data1.get(position).getMRP());
                holder.tex4.setText(data1.get(position).getCustomerApplication());
                if(data1.get(position).getHSNCODE() != null)
                    holder.tex5.setText(data1.get(position).getHSNCODE());
                else
                    holder.tex5.setText("");
            }*/

            convertView.setTag(holder);
        } else {
            holder = (FilterListAdapter.ComplaintsHolder) convertView.getTag();
        }
        cv = (CardView) convertView.findViewById(R.id.card);//Change this to your view
//        cv.startAnimation(scaleup);
        return convertView;
    }


    private void viewPopup(List<Inline> data, int position) {
        try {
            final AlertDialog alertDialog;
            alertDialog = new AlertDialog.Builder(context).create();
            alertDialog.setIcon(R.drawable.ic_launcher_delphi);
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.popupadapter, null);
            popup = AnimationUtils.loadAnimation(context, R.anim.bounce);
            dialogView.setAnimation(popup);
            alertDialog.setView(dialogView);

            ImageView Back = (ImageView) dialogView.findViewById(R.id.back);
            /*TextView tex1=(TextView)dialogView.findViewById(R.id.tantext1) ;
            tex1.setText(String.valueOf(data.get(position).getUptodateequip()));*/
            ((TextView) dialogView.findViewById(R.id.tantext1)).setText(String.valueOf(data.get(position).getDTVSPartno()));
            ((TextView) dialogView.findViewById(R.id.tantext2)).setText(String.valueOf(data.get(position).getEquivalentPartNo()));
            ((TextView) dialogView.findViewById(R.id.tantext3)).setText(String.valueOf(data.get(position).getDescription()));
            ((TextView) dialogView.findViewById(R.id.tantext4)).setText("Rs:"+String.valueOf(data.get(position).getMRP()));
            ((TextView) dialogView.findViewById(R.id.tantext5)).setText(String.valueOf(data.get(position).getCustomerApplication()));
            Back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //progressDialog.dismiss();
                    alertDialog.dismiss();

                }
            });
            alertDialog.show();

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void viewpopuptwo(List<PartnumberSearch> data1, int position) {
        try {
            final AlertDialog alertDialog;
            alertDialog = new AlertDialog.Builder(context).create();
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.popupadapter, null);
            popup = AnimationUtils.loadAnimation(context, R.anim.bounce);
            dialogView.setAnimation(popup);
            alertDialog.setView(dialogView);

            ImageView Back = (ImageView) dialogView.findViewById(R.id.back);
            /*TextView tex1=(TextView)dialogView.findViewById(R.id.tantext1) ;
            tex1.setText(String.valueOf(data.get(position).getUptodateequip()));*/
            ((TextView) dialogView.findViewById(R.id.tantext1)).setText(String.valueOf(data1.get(position).getDTVSPartno()));
            ((TextView) dialogView.findViewById(R.id.tantext2)).setText(String.valueOf(data1.get(position).getEquivalentPartNo()));
            ((TextView) dialogView.findViewById(R.id.tantext3)).setText(String.valueOf(data1.get(position).getDescription()));
            ((TextView) dialogView.findViewById(R.id.tantext4)).setText("Rs:"+String.valueOf(data1.get(position).getMRP()));
            ((TextView) dialogView.findViewById(R.id.tantext5)).setText(String.valueOf(data1.get(position).getCustomerApplication()));
            Back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //progressDialog.dismiss();
                    alertDialog.dismiss();
                }
            });


            alertDialog.show();

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void filter(String s) {
        s = s.toLowerCase(Locale.getDefault());
        items.clear();

        if (s.length() == 0) {
            items.addAll(dupItems);

        } else {
            for (int i=0;i<dupItems.size();i++) {
                String wp = dupItems.get(i).getPartNumber();
                String app = dupItems.get(i).getVehicleApplication();

                if (wp.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())||app.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())) {
                    items.add(dupItems.get(i));
                }
            }
        }
        notifyDataSetChanged();
    }

    private class ComplaintsHolder {

        public TextView tex1;
        public TextView tex2;
        /*   public ImageView feedback;*/
        public TextView tex3;
        public TextView tex4;

        public ImageView product_image,filterImages;
        public TextView tex5,mrp;

        public CardView cardView;
        public TextView tex6;
        public TextView tex7;
        public LinearLayout linearLayout_pooup;
        public  LinearLayout linearLayout_application;
        public  LinearLayout linearLayout_alpha;
    }

}
