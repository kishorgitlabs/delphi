package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brainmagic.delphitvs.R;

import java.util.List;

public class FiltersSegmentAdpater extends ArrayAdapter {
    private Context context;
    private List<String>filters;
    private boolean isOemFlow=false;
    public FiltersSegmentAdpater(Context context, List<String>filters) {
        super(context, R.layout.filters_segment_list);
        this.context=context;
        this.filters=filters;
    }


    @Override
    public View getView(int position,  View convertView, ViewGroup parent) {
        View view=LayoutInflater.from(context).inflate(R.layout.filters_segment_list,null);
        TextView filter=view.findViewById(R.id.segment_id);
        filter.setText(filters.get(position));

        return view;
    }

    @Override
    public int getCount() {
        return filters.size();
    }
}
