package adapter;

public class Getter {

   private String sno;
    private String serviceparts;
    private String partdesc;
    private String qtyfip;

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getServiceparts() {
        return serviceparts;
    }

    public void setServiceparts(String serviceparts) {
        this.serviceparts = serviceparts;
    }

    public String getPartdesc() {
        return partdesc;
    }

    public void setPartdesc(String partdesc) {
        this.partdesc = partdesc;
    }

    public String getQtyfip() {
        return qtyfip;
    }

    public void setQtyfip(String qtyfip) {
        this.qtyfip = qtyfip;
    }
}
