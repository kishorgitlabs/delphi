package adapter;

import android.content.Context;
import android.os.Parcelable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.viewpager.widget.PagerAdapter;

import com.brainmagic.delphitvs.Aboutus;
import com.brainmagic.delphitvs.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyCustomPageAdapter extends PagerAdapter {
    private List<Integer> homeImageList;
    LayoutInflater layoutInflater;
    Aboutus context;
    String[] cityResourseList;
    String[] hotelNameResourseList;
    private int position;



    public MyCustomPageAdapter(Aboutus aboutus) {
        this.context = aboutus;
        // Image Resources
        homeImageList = new ArrayList<Integer>();
//        Integer[] webServicesImageResourcesList = {R.drawable.album5,
//                R.drawable.delphiabout, R.drawable.album4jpg};
        Integer[] webServicesImageResourcesList = {R.drawable.mannur,
                R.drawable.orugadam};
        homeImageList = Arrays.asList(webServicesImageResourcesList);
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return homeImageList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.viewpager_item,container, false);

        ImageView image = (ImageView) view.findViewById(R.id.imageView);
        image.setImageResource(homeImageList.get(position));
        image.setTag(position);
        container.addView(view);

        return view;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}
