package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brainmagic.delphitvs.R;

import java.util.List;

import model.network.dealer.dealerdetails.DealerDetails;

public class NetworkAdapter  extends ArrayAdapter {
    private Context context;
    private List<DealerDetails> networkList;

    public NetworkAdapter(Context context, List<DealerDetails> networkList) {
        super(context, R.layout.adapter_network_view);
        this.context=context;
        this.networkList=networkList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view= LayoutInflater.from(context).inflate(R.layout.adapter_network_view,null);
        TextView shopName=view.findViewById(R.id.shopname);
        TextView addressOne=view.findViewById(R.id.del_address_one);
        TextView addressTwo=view.findViewById(R.id.del_address_two);
        TextView addressThree=view.findViewById(R.id.del_address_three);
        TextView id=view.findViewById(R.id.del_code);
        TextView contactPerson=view.findViewById(R.id.contact_name);
        TextView mobileNo=view.findViewById(R.id.phone_no);
        TextView emailId=view.findViewById(R.id.email);
        shopName.setText(networkList.get(position).getNameOfServiceProvider());
        addressOne.setText(networkList.get(position).getAddressline1()+", "+networkList.get(position).getAddressline2()+","+networkList.get(position).getAddressline3());
        addressTwo.setText(networkList.get(position).getAddressline2());
        addressThree.setText(networkList.get(position).getAddressline3());
        id.setText(networkList.get(position).getCustomerCode()+"");
        contactPerson.setText(networkList.get(position).getContactperson());
        mobileNo.setText(networkList.get(position).getContactNumber());
        emailId.setText(networkList.get(position).getEmail());

        return view;
    }

    @Override
    public int getCount() {
        return networkList.size();
    }
}
