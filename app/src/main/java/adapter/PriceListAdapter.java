package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brainmagic.delphitvs.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import model.pricelist.PriceListData;

public class PriceListAdapter extends ArrayAdapter {
    private Context context;
    private List<List<PriceListData>> data;
    private List<List<PriceListData>>dupData;
    public PriceListAdapter(Context context, List<List<PriceListData>> data) {
        super(context, R.layout.price_adapter);
        this.context=context;
        this.data=data;
        dupData=new ArrayList<>(data);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view=LayoutInflater.from(context).inflate(R.layout.price_adapter,null);
        TextView sno=view.findViewById(R.id.price_sno);
        TextView partNo=view.findViewById(R.id.price_part_no);
        TextView desc=view.findViewById(R.id.price_desc);
        TextView mrp=view.findViewById(R.id.price_mrp);


        sno.setText(position+1+"");
        partNo.setText(data.get(position).get(0).getPartNo()+"");
        desc.setText(data.get(position).get(0).getDescription()+"");
        mrp.setText("₹ "+data.get(position).get(0).getMRP()+"");
        return view;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    public void filter(String s) {

        s = s.toLowerCase(Locale.getDefault()).trim();
        data.clear();

        if (s.length() == 0) {
            data.addAll(dupData);

        } else {
            for (int i=0;i<dupData.size();i++) {
                String wp = dupData.get(i).get(0).getPartNo();

                if (wp.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())) {
                    data.add(dupData.get(i));

                }
            }
        }
        notifyDataSetChanged();
    }
}


