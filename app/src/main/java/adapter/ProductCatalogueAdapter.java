package adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;

import com.brainmagic.delphitvs.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import alert.Alert;
import model.PartnumberSearch;
import model.allmakeproducts.AllMakeSparesData;
import model.inlinefields.Inline;

public class ProductCatalogueAdapter extends ArrayAdapter {
    private Context context;
    private List<AllMakeSparesData> data,dupItems;
    private LinearLayout popup_layout;
    private List<PartnumberSearch> data1,dupItem;
    private String Dealer_tex,State_tex;
    private Alert box;
    private CardView cv;
    private int count_position;
    private String value_change,data_value;
    private Animation scaleup,popup;
    private boolean bool = false;
    private int resource;

    public ProductCatalogueAdapter(@NonNull Context context, List<AllMakeSparesData> data, boolean bool, String value_change) {
        super(context, R.layout.productcatalogueadapter,data);
        this.context = context;
        this.data = data;
        dupItems=new ArrayList<>(data);
        this.value_change = value_change;
        this.bool = bool;
        box =new Alert(context);
        scaleup = AnimationUtils.loadAnimation(context, R.anim.animation);
    }

      public ProductCatalogueAdapter(@NonNull Context context, List<PartnumberSearch> data1) {
            super(context, R.layout.productcatalogueadapter,data1);
            this.context = context;
            this.data1 = data1;
            dupItem=new ArrayList<>(data1);
            box =new Alert(context);
          scaleup = AnimationUtils.loadAnimation(context, R.anim.animation);
        }

    @SuppressLint("WrongConstant")
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ProductCatalogueAdapter.ComplaintsHolder holder;

        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.productcatalogueadapter, parent, false);

            holder = new ProductCatalogueAdapter.ComplaintsHolder();

            holder.tex1 = (TextView) convertView.findViewById(R.id.txx1);
            // holder.tex2 = (TextView) convertView.findViewById(R.id.ed_text2);
            holder.tex2 = (TextView) convertView.findViewById(R.id.txx2);
            holder.tex3 = (TextView) convertView.findViewById(R.id.txx3);
            //  holder.tex5 = (TextView) convertView.findViewById(R.id.text5);
            holder.tex4 = (TextView) convertView.findViewById(R.id.tex4);
//            holder.tex5 = (TextView) convertView.findViewById(R.id.txx5);
            holder.text5=(TextView)convertView.findViewById(R.id.text5);
            holder.product_image = (ImageView) convertView.findViewById(R.id.part_image);
            holder.cardView = (CardView)convertView.findViewById(R.id.card);
            holder.linearLayout_pooup = (LinearLayout) convertView.findViewById(R.id.cart_item_layout);
            holder.linearLayout_application = convertView.findViewById(R.id.application);
            holder.alpha_no=convertView.findViewById(R.id.alpha_no);
//            holder.linearLayout_alpha =convertView.findViewById(R.id.alpha);

          /*  if(position ==0){
                holder.cardView.setCardBackgroundColor(Color.parseColor("#8bd8bd"));
            }

            else if(position %2 == 0){
                holder.cardView.setCardBackgroundColor(Color.parseColor("#df678c"));
            }else if(position %3 == 0){
                holder.cardView.setBackgroundColor(Color.parseColor("#ffe67c"));
            }else if(position %5 == 0){
                holder.cardView.setBackgroundColor(Color.parseColor("#f0a07c"));
            }else{
                holder.cardView.setCardBackgroundColor(Color.parseColor("#8aaae5"));
            }*/


            if(data1!= null){
                data_value = data1.get(position).getDescription();
                if (data_value.equals("DELIVERY VALVE")||data_value.equals("ELEMENT")||value_change.equals("SERVICE KIT")) {
//                    holder.product_image.setImageResource(R.drawable.delivery_valve_se);
                    Picasso.with(context).load(data1.get(position).getImage()).error(R.drawable.noimageavail_seven_five).placeholder( R.drawable.progress_animation ).into(holder.product_image);
                }
//                else if (data_value.equals("ELEMENT")) {
////                    holder.product_image.setImageResource(R.drawable.element_hun);
//                }
//                else if(value_change.equals("SERVICE KIT"))
//                {
////                    holder.product_image.setImageResource(R.drawable.noimageavail_seven_five);
//                }
                else {
                    Picasso.with(context).load(data1.get(position).getImage()).error(R.drawable.noimageavail_seven_five).placeholder( R.drawable.progress_animation ).into(holder.product_image);
//                    holder.linearLayout_application.setVisibility(View.GONE);
//                    holder.linearLayout_alpha.setVisibility(View.VISIBLE);
                }
                Picasso.with(context).load(data1.get(position).getImage()).error(R.drawable.noimageavail_seven_five).placeholder( R.drawable.progress_animation ).into(holder.product_image);
            }else {
                if (value_change.equals("DELIVERY VALVE")||value_change.equals("ELEMENT")||value_change.equals("SERVICE KIT")) {
//                    holder.product_image.setImageResource(R.drawable.delivery_valve_se);
                    Picasso.with(context).load(data.get(position).getImage()).error(R.drawable.noimageavail_seven_five).placeholder(R.drawable.progress_animation ).into(holder.product_image);
                }
//                else if (value_change.equals("ELEMENT")) {
////                    holder.product_image.setImageResource(R.drawable.element_hun);
//                }
//                else if(value_change.equals("SERVICE KIT"))
//                {
////                    holder.product_image.setImageResource(R.drawable.noimageavail_seven_five);
//                }
                else{
//                    holder.product_image.setImageResource(R.drawable.nozzle_hun);
                    Picasso.with(context).load(data.get(position).getImage()).error(R.drawable.noimageavail_seven_five).placeholder(R.drawable.progress_animation ).into(holder.product_image);
//                    holder.linearLayout_application.setVisibility(View.GONE);
//                    holder.linearLayout_alpha.setVisibility(View.VISIBLE);
                    holder.alpha_no.setVisibility(View.VISIBLE);
                }
            }

//            holder.linearLayout_pooup.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (bool == true)
//                        viewPopup(data,position);
//                    else
//                        viewpopuptwo(data1,position);
//                }
//            });

        if(bool == true) {
            holder.tex1.setText(data.get(position).getPartNumber());
            holder.tex2.setText(data.get(position).getOEPartNumber());
            holder.tex3.setText("₹ " +data.get(position).getMRP());
            holder.tex4.setText(data.get(position).getVehicleApplication());
            holder.text5.setText(data.get(position).getAlphaNumericalNumber());
//            holder.tex5.setText(data.get(position).getAlphaNumericalNumber());
///)/            if(data.get(position).getHSNCODE() != null)
////
////                holder.tex5.setText(data.get(position).getHSNCODE());
////            else
////                holder.tex5.setText("";
        }
        else {
            holder.tex1.setText(data1.get(position).getDTVSPartno());
            holder.tex2.setText(data1.get(position).getEquivalentPartNo());
            holder.tex3.setText("₹ " +data1.get(position).getMRP());
            holder.tex4.setText(data1.get(position).getCustomerApplication());
            if(data1.get(position).getHSNCODE() != null)
            holder.tex5.setText(data1.get(position).getHSNCODE());
            else
              holder.tex5.setText("");
        }

            convertView.setTag(holder);
        } else {
            holder = (ProductCatalogueAdapter.ComplaintsHolder) convertView.getTag();
        }
        cv = (CardView) convertView.findViewById(R.id.card);//Change this to your view
//        cv.startAnimation(scaleup);
        return convertView;
    }


    public void filter(String s) {

        s = s.toLowerCase(Locale.getDefault()).trim();
        data.clear();

        if (s.length() == 0) {
            data.addAll(dupItems);

        } else {
            for (int i=0;i<dupItems.size();i++) {
                String wp = dupItems.get(i).getPartNumber();
                String app=dupItems.get(i).getOEPartNumber();

                if (wp.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())||app.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())) {
                    data.add(dupItems.get(i));
                }
            }
        }
        notifyDataSetChanged();
    }

    public void filterByPartValue(String s) {

        s = s.toLowerCase(Locale.getDefault()).trim();
        data1.clear();

        if (s.length() == 0) {
            data1.addAll(dupItem);

        } else {
            for (int i=0;i<dupItem.size();i++) {
                String wp = dupItem.get(i).getDTVSPartno();
                String app=dupItem.get(i).getEquivalentPartNo();

                if (wp.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())||app.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())) {
                    data1.add(dupItem.get(i));

                }
            }
        }
        notifyDataSetChanged();
    }
    private void viewPopup(List<AllMakeSparesData> data, int position) {
        try {
            final AlertDialog alertDialog;
            alertDialog = new AlertDialog.Builder(context).create();
            alertDialog.setIcon(R.drawable.ic_launcher_delphi);
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.popupadapter, null);
            popup = AnimationUtils.loadAnimation(context, R.anim.bounce);
            dialogView.setAnimation(popup);
            alertDialog.setView(dialogView);

            ImageView Back = (ImageView) dialogView.findViewById(R.id.back);
            /*TextView tex1=(TextView)dialogView.findViewById(R.id.tantext1) ;
            tex1.setText(String.valueOf(data.get(position).getUptodateequip()));*/
            ((TextView) dialogView.findViewById(R.id.tantext1)).setText(String.valueOf(data.get(position).getPartNumber()));
            ((TextView) dialogView.findViewById(R.id.tantext2)).setText(String.valueOf(data.get(position).getOEPartNumber()));
            ((TextView) dialogView.findViewById(R.id.tantext3)).setText(String.valueOf(data.get(position).getDescription()));
            ((TextView) dialogView.findViewById(R.id.tantext4)).setText("₹ "+String.valueOf(data.get(position).getMRP()));
            ((TextView) dialogView.findViewById(R.id.tantext5)).setText(String.valueOf(data.get(position).getVehicleApplication()));

            Back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //progressDialog.dismiss();
                    alertDialog.dismiss();

                }
            });


            alertDialog.show();

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void viewpopuptwo(List<PartnumberSearch> data1,int position) {
        try {
            final AlertDialog alertDialog;
            alertDialog = new AlertDialog.Builder(context).create();
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.popupadapter, null);
            popup = AnimationUtils.loadAnimation(context, R.anim.bounce);
            dialogView.setAnimation(popup);
            alertDialog.setView(dialogView);

            ImageView Back = (ImageView) dialogView.findViewById(R.id.back);
            /*TextView tex1=(TextView)dialogView.findViewById(R.id.tantext1) ;
            tex1.setText(String.valueOf(data.get(position).getUptodateequip()));*/
            ((TextView) dialogView.findViewById(R.id.tantext1)).setText(String.valueOf(data1.get(position).getDTVSPartno()));
            ((TextView) dialogView.findViewById(R.id.tantext2)).setText(String.valueOf(data1.get(position).getEquivalentPartNo()));
            ((TextView) dialogView.findViewById(R.id.tantext3)).setText(String.valueOf(data1.get(position).getDescription()));
            ((TextView) dialogView.findViewById(R.id.tantext4)).setText("₹ "+String.valueOf(data1.get(position).getMRP()));
            ((TextView) dialogView.findViewById(R.id.tantext5)).setText(String.valueOf(data1.get(position).getCustomerApplication()));
            Back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //progressDialog.dismiss();
                    alertDialog.dismiss();
                }
            });


            alertDialog.show();

        }
        catch (Exception e){
            e.printStackTrace();
        }
        }

    private class ComplaintsHolder {

        public TextView tex1;
         public TextView tex2;
        /*   public ImageView feedback;*/
        public TextView tex3;
        public TextView tex4;
        public TextView text5;
        public ImageView product_image;
         public TextView tex5;

         public CardView cardView;
        public TextView tex6;
        public TextView tex7;
        public  LinearLayout linearLayout_pooup;
        public RelativeLayout linearLayout_application,linearLayout_alpha;
        public RelativeLayout alpha_no;
    }


}

