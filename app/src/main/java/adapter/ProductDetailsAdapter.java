package adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;

import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.ServicepartList;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import alert.Alert;
import model.PartnumberSearch;
import model.api.APIService;
import model.api.RetroClient;
import model.filters.details.FilterDetail;
import model.filters.details.FilterDetailList;
import model.inlinefields.Inline;
import model.productdetail.ProductDetails;
import model.productdetail.ProductDetailsData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailsAdapter extends ArrayAdapter {
    private Context context;
    private List<Inline> data;
    private LinearLayout popup_layout;
    private List<FilterDetailList> productList;
    private List<FilterDetailList> dupItems;
    private String Dealer_tex,State_tex;
    private Alert box;
    private CardView cv;
    private int count_position;
    private String value_change,data_value;
    private Animation scaleup,popup;

    public ProductDetailsAdapter(@NonNull Context context, List<FilterDetailList> productList) {
        super(context, R.layout.productdetailsadapter,productList);
        this.context = context;
        this.productList = productList;
        box =new Alert(context);
        scaleup = AnimationUtils.loadAnimation(context, R.anim.animation);
        dupItems=new ArrayList<>(productList);
    }

    @SuppressLint("WrongConstant")
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ProductDetailsAdapter.ComplaintsHolder holder;

        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.productdetailsadapter, parent, false);

            holder = new ProductDetailsAdapter.ComplaintsHolder();


            holder.tex1 = (TextView) convertView.findViewById(R.id.dtvspartno);
            // holder.tex2 = (TextView) convertView.findViewById(R.id.ed_text2);
            holder.tex2 = (TextView) convertView.findViewById(R.id.oepartno);
            holder.tex3 = (TextView) convertView.findViewById(R.id.brand);
            //  holder.tex5 = (TextView) convertView.findViewById(R.id.text5);
            holder.tex4 = (TextView) convertView.findViewById(R.id.vehicleapplication);
            holder.tex5 = (TextView) convertView.findViewById(R.id.description);
            holder.mrp=convertView.findViewById(R.id.mrp_filter);
            holder.product_image = (ImageView) convertView.findViewById(R.id.filter_image);
            holder.cardView = (CardView)convertView.findViewById(R.id.card);
           /* holder.linearLayout_pooup = (LinearLayout) convertView.findViewById(R.id.cart_item_layout);
            holder.linearLayout_application = (LinearLayout) convertView.findViewById(R.id.application);
            holder.linearLayout_alpha = (LinearLayout) convertView.findViewById(R.id.alpha);
*/

//           holder.cardView.setOnClickListener(new View.OnClickListener() {
//               @Override
//               public void onClick(View view) {
//                   Intent i= new Intent(context, ServicepartList.class);
//                   i.putExtra("DTVSPARTNO",productList.get(position).getPartNumber());
//                   context.startActivity(i);
//               }
//
//           });
           /* if(position ==0){
                holder.cardView.setCardBackgroundColor(Color.parseColor("#8bd8bd"));
            }

            else if(position %2 == 0){
                holder.cardView.setCardBackgroundColor(Color.parseColor("#df678c"));
            }else if(position %3 == 0){
                holder.cardView.setBackgroundColor(Color.parseColor("#ffe67c"));
            }else{
                holder.cardView.setCardBackgroundColor(Color.parseColor("#8aaae5"));
            }*/
            holder.tex1.setText(productList.get(position).getPartNumber());
            holder.tex2.setText(productList.get(position).getOEPartNumber());
            holder.tex3.setText(productList.get(position).getSegment());
            holder.tex4.setText(productList.get(position).getVehicleApplication());
            holder.tex5.setText(productList.get(position).getDescription());
            holder.mrp.setText(productList.get(position).getMRP());


            Picasso.with(context).load(productList.get(position).getmImage()).error(R.drawable.noimageavail_seven_five).placeholder(R.drawable.progress_animation ).into(holder.product_image);
           /* if(productList!= null){
                data_value = productList.get(position).getDescription();
                if (data_value.equals("DELIVERY VALVE")) {
                    holder.product_image.setImageResource(R.drawable.deliveryvalve);
                } else if (data_value.equals("ELEMENT")) {
                    holder.product_image.setImageResource(R.drawable.elemnt);

                } else {
                    holder.product_image.setImageResource(R.drawable.nozzle);
                    holder.linearLayout_application.setVisibility(View.GONE);
                    holder.linearLayout_alpha.setVisibility(View.VISIBLE);
                }
            }else {
                if (value_change.equals("DELIVERY VALVE")) {
                    holder.product_image.setImageResource(R.drawable.deliveryvalve);
                } else if (value_change.equals("ELEMENT")) {
                    holder.product_image.setImageResource(R.drawable.elemnt);

                } else {
                    holder.product_image.setImageResource(R.drawable.nozzle);
                    holder.linearLayout_application.setVisibility(View.GONE);
                    holder.linearLayout_alpha.setVisibility(View.VISIBLE);
                }
            }
            holder.linearLayout_pooup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (bool == true)
                        viewPopup(data,position);
                    else
                        viewpopuptwo(productList,position);
                }
            });

            if(bool == true) {
                holder.tex1.setText(data.get(position).getDTVSPartno());
                holder.tex2.setText(data.get(position).getEquivalentPartNo());
                holder.tex3.setText("Rs:" +data.get(position).getMRP());
                holder.tex4.setText(data.get(position).getCustomerApplication());
                if(data.get(position).getHSNCODE() != null)

                    holder.tex5.setText(data.get(position).getHSNCODE());
                else
                    holder.tex5.setText("");
            }
            else {
                holder.tex1.setText(productList.get(position).getDTVSPartno());
                holder.tex2.setText(productList.get(position).getEquivalentPartNo());
                holder.tex3.setText("Rs:" +productList.get(position).getMRP());
                holder.tex4.setText(productList.get(position).getCustomerApplication());
                if(productList.get(position).getHSNCODE() != null)
                    holder.tex5.setText(productList.get(position).getHSNCODE());
                else
                    holder.tex5.setText("");
            }*/

            convertView.setTag(holder);
        } else {
            holder = (ProductDetailsAdapter.ComplaintsHolder) convertView.getTag();
        }
        cv = (CardView) convertView.findViewById(R.id.card);//Change this to your view
        cv.startAnimation(scaleup);
        return convertView;
    }


    private void viewPopup(List<Inline> data,int position) {
        try {
            final AlertDialog alertDialog;
            alertDialog = new AlertDialog.Builder(context).create();
            alertDialog.setIcon(R.drawable.ic_launcher_delphi);
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.popupadapter, null);
            popup = AnimationUtils.loadAnimation(context, R.anim.bounce);
            dialogView.setAnimation(popup);
            alertDialog.setView(dialogView);

            ImageView Back = (ImageView) dialogView.findViewById(R.id.back);
            /*TextView tex1=(TextView)dialogView.findViewById(R.id.tantext1) ;
            tex1.setText(String.valueOf(data.get(position).getUptodateequip()));*/
            ((TextView) dialogView.findViewById(R.id.tantext1)).setText(String.valueOf(data.get(position).getDTVSPartno()));
            ((TextView) dialogView.findViewById(R.id.tantext2)).setText(String.valueOf(data.get(position).getEquivalentPartNo()));
            ((TextView) dialogView.findViewById(R.id.tantext3)).setText(String.valueOf(data.get(position).getDescription()));
            ((TextView) dialogView.findViewById(R.id.tantext4)).setText("Rs:"+String.valueOf(data.get(position).getMRP()));
            ((TextView) dialogView.findViewById(R.id.tantext5)).setText(String.valueOf(data.get(position).getCustomerApplication()));
            Back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //progressDialog.dismiss();
                    alertDialog.dismiss();

                }
            });


            alertDialog.show();

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void viewpopuptwo(List<PartnumberSearch> productList,int position) {
        try {
            final AlertDialog alertDialog;
            alertDialog = new AlertDialog.Builder(context).create();
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.popupadapter, null);
            popup = AnimationUtils.loadAnimation(context, R.anim.bounce);
            dialogView.setAnimation(popup);
            alertDialog.setView(dialogView);

            ImageView Back = (ImageView) dialogView.findViewById(R.id.back);
            /*TextView tex1=(TextView)dialogView.findViewById(R.id.tantext1) ;
            tex1.setText(String.valueOf(data.get(position).getUptodateequip()));*/
            ((TextView) dialogView.findViewById(R.id.tantext1)).setText(String.valueOf(productList.get(position).getDTVSPartno()));
            ((TextView) dialogView.findViewById(R.id.tantext2)).setText(String.valueOf(productList.get(position).getEquivalentPartNo()));
            ((TextView) dialogView.findViewById(R.id.tantext3)).setText(String.valueOf(productList.get(position).getDescription()));
            ((TextView) dialogView.findViewById(R.id.tantext4)).setText("Rs:"+String.valueOf(productList.get(position).getMRP()));
            ((TextView) dialogView.findViewById(R.id.tantext5)).setText(String.valueOf(productList.get(position).getCustomerApplication()));
            Back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //progressDialog.dismiss();
                    alertDialog.dismiss();
                }
            });


            alertDialog.show();

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private class ComplaintsHolder {

        public TextView tex1;
        public TextView tex2;
        /*   public ImageView feedback;*/
        public TextView tex3;
        public TextView tex4;

        public ImageView product_image;
        public TextView tex5;

        public CardView cardView;
        public TextView mrp;
        public TextView tex7;
        public  LinearLayout linearLayout_pooup;
        public  LinearLayout linearLayout_application;
        public  LinearLayout linearLayout_alpha;
    }

    public void filter(String s) {

        s = s.toLowerCase(Locale.getDefault());
        productList.clear();

        if (s.length() == 0) {
            productList.addAll(dupItems);

        } else {
            for (int i=0;i<dupItems.size();i++) {
                String wp = dupItems.get(i).getPartNumber();
                String app = dupItems.get(i).getVehicleApplication();

                if (wp.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())||app.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())) {
                    productList.add(dupItems.get(i));

                }
            }
        }
        notifyDataSetChanged();
    }
}
