package adapter;


import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.ServiceListActivity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import model.rotaryoldparts.OldNewServiceList;
import model.rotaryoldparts.OldServiceList;

public class RotaryProductAdapter extends ArrayAdapter {
    private List<OldNewServiceList> productDetails,dupItems;
    private Set<OldNewServiceList> dupSet;
    private Context context;
    private static final String TAG = "RotaryProductAdapter";
    private String mainCat,subCat;

    public RotaryProductAdapter(Context context, List<OldNewServiceList> productDetails, String mainCat, String subCat) {
        super(context, R.layout.rotary_product_adapter);
        this.context=context;
        this.productDetails=productDetails;
        dupSet=new HashSet<>(productDetails);
        dupItems=new ArrayList<>(productDetails);
        this.mainCat=mainCat;
        this.subCat=subCat;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView==null)
        {
            convertView= LayoutInflater.from(context).inflate(R.layout.rotary_product_adapter,null);
        }

        TextView sNo=convertView.findViewById(R.id.rotary_sno);
        TextView app=convertView.findViewById(R.id.rotary_app);
        TextView pump=convertView.findViewById(R.id.rotary_pump);
        ImageView service=convertView.findViewById(R.id.rotary_service);

        sNo.setText(position+1+"");
//        app.setText(productDetails.get(position).getEngine());
//        pump.setText(productDetails.get(position).getPartNumber());
//        Log.d(TAG, "getView: "+productDetails.get(position).getEngine());
//        service.setText(productDetails.get(position).get);

        app.setText(productDetails.get(position).getPartTableData().getEngine());
        pump.setText(productDetails.get(position).getPartTableData().getPartNumber());
        Log.d(TAG, "getView: "+productDetails.get(position).getPartTableData().getEngine());

        service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    context.startActivity(new Intent(context, ServiceListActivity.class)
                            .putExtra("MainCatagory",mainCat)
                            .putExtra("SubCatagory",subCat)
                            .putExtra("partNo",productDetails.get(position).getPartTableData().getPartNumber())
                    );
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        return convertView;
    }

    @Override
    public int getCount() {

        return productDetails.size();
    }

    public void filter(String s) {

        s = s.toLowerCase(Locale.getDefault());
        productDetails.clear();
        dupSet.clear();

        if (s.length() == 0) {
            productDetails.addAll(dupItems);
            dupSet.addAll(dupItems);

        } else {
            for (int i=0;i<dupItems.size();i++) {
//                boolean isAdded=false;
                /**
                 * old superseded parts are in other table, so dupItem has getOldServiceList and getPartTableData
                 * If user search for old parts, new parts only viewed for old parts
                 * In order to do this and hashset is used to eliminate duplicate items
                 * or Other boolean logic is used but commented
                 */
                for(OldServiceList list:dupItems.get(i).getOldServiceList()){
                    Log.d(TAG, "filter: "+list);

                    if(list.getSupersededNo().toLowerCase().contains(s.toLowerCase()))
                    {
//                        isAdded=true;
                        dupSet.add(dupItems.get(i));
//                        lists.add(dupItems.get(i));
                    }
                }

                if(dupItems.get(i).getPartTableData().getPartNumber().toLowerCase().contains(s.toLowerCase())||dupItems.get(i).getPartTableData().getEngine().toLowerCase().contains(s.toLowerCase())){
//                    isAdded=true;
                        dupSet.add(dupItems.get(i));
                }


//                if(isAdded)
//                    productDetails.add((dupItems.get(i)));

//                String wp = dupItems.get(i).getOldServiceList().get(0).getPartNumber();
//
//                if (wp.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())) {
//                    productDetails.add(dupItems.get(i));
//
//                }
            }
            productDetails.clear();
            productDetails.addAll(dupSet);
        }

        notifyDataSetChanged();
    }
}
