package adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.ServiceListActivity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import model.commonexpandablemodel.Child;
import model.commonexpandablemodel.OldServiceList;
import model.commonexpandablemodel.ParentClass;

public class RotaryPumpAdapter extends BaseExpandableListAdapter {
    private Context context;
    private List<ParentClass> expandableLists,dupItems;
    private String mainCat,subCat;

    private Set<ParentClass> dupSet;

    public RotaryPumpAdapter(Context context, List<ParentClass> expandableLists,String mainCat,String subCat) {
        this.context = context;
        this.expandableLists = expandableLists;
        dupSet=new HashSet<>(expandableLists);
        dupItems=new ArrayList<>(expandableLists);
        this.mainCat=mainCat;
        this.subCat=subCat;
    }

    public List<ParentClass> getExpandableLists() {
        return expandableLists;
    }

    public void setExpandableLists(List<ParentClass> expandableLists) {
        this.expandableLists = expandableLists;
    }

    @Override
    public int getGroupCount() {
        return expandableLists.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return expandableLists.get(groupPosition).getChild().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return expandableLists.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return expandableLists.get(groupPosition).getChild().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        final ParentClass parentList=expandableLists.get(groupPosition);
        if(convertView==null)
        {
            convertView= LayoutInflater.from(context).inflate(R.layout.rotary_product_adapter,null);
        }
        try {

            TextView sNo = convertView.findViewById(R.id.rotary_sno);
            TextView eng = convertView.findViewById(R.id.rotary_app);
            TextView partNo = convertView.findViewById(R.id.rotary_pump);
            ImageView service = convertView.findViewById(R.id.rotary_service);

            sNo.setText(groupPosition+1+"");
            eng.setText(parentList.getPartTableData().getEngine());
            partNo.setText(parentList.getPartTableData().getPartNumber());

//        Log.d(TAG, "getView: "+productDetails.get(groupPosition).getPartTableData().getEngine());

            service.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        context.startActivity(new Intent(context, ServiceListActivity.class)
                                .putExtra("MainCatagory", mainCat)
                                .putExtra("SubCatagory", subCat)
                                .putExtra("partNo", parentList.getPartTableData().getPartNumber())
                        );
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final Child child=expandableLists.get(groupPosition).getChild().get(childPosition);

        if(convertView==null)
        {
            convertView=LayoutInflater.from(context).inflate(R.layout.common_child_view,null);
        }

        try {
            TextView rail = convertView.findViewById(R.id.rail_service);
            TextView injector = convertView.findViewById(R.id.injector_service);
            TextView main_filter = convertView.findViewById(R.id.mail_Filter);
            TextView mainFilterTxt = convertView.findViewById(R.id.main_filter_txt);

            mainFilterTxt.setText("Filter");
            TextView pre_filter = convertView.findViewById(R.id.rail_pre_Filter);
            LinearLayout preFilterLayout = convertView.findViewById(R.id.pre_filter_layout);
            LinearLayout railLayout = convertView.findViewById(R.id.rail_layout);
            LinearLayout ecuLayout = convertView.findViewById(R.id.ecu_layout);
            LinearLayout pump = convertView.findViewById(R.id.pump_no);

//            pump.setVisibility(View);
            preFilterLayout.setVisibility(View.GONE);
            pump.setVisibility(View.GONE);

            railLayout.setVisibility(View.GONE);
            ecuLayout.setVisibility(View.GONE);
            TextView ecu = convertView.findViewById(R.id.ecu_filter);

            rail.setText(child.getRail());
            injector.setText(child.getInjector());
            main_filter.setText(child.getMainfilter());
            main_filter.setTextColor(context.getResources().getColor(R.color.white));
            pre_filter.setText(child.getPrefilter());
            ecu.setText(child.getEcu());


//            main_filter.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    context.startActivity(new Intent(context, CommonRailFilterList.class)
//                            .putExtra("main_cat", mainCat)
//                            .putExtra("sub_cat", subCat)
//                            .putExtra("service", "Service Parts")
//                            .putExtra("type", "Main Filter")
//                            .putExtra("partNo", child.getMainfilter())
//                    );
//                }
//            });
//
//            pre_filter.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    context.startActivity(new Intent(context, CommonRailFilterList.class)
//                            .putExtra("main_cat", mainCat)
//                            .putExtra("sub_cat", subCat)
//                            .putExtra("service", "Service Parts")
//                            .putExtra("type", "Pre-Filter")
//                            .putExtra("partNo", child.getPrefilter())
//                    );
//                }
//            });

        }catch (Exception e)
        {
            e.printStackTrace();
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

//    public void filter(String s) {
//
//        s = s.toLowerCase(Locale.getDefault()).trim();
//        expandableLists.clear();
//
//        if (s.length() == 0) {
//            expandableLists.addAll(dupList);
//
//        } else {
//            for (int i=0;i<dupList.size();i++) {
//                String eng = dupList.get(i).getPartTableData().getEngine();
//                String partNo = dupList.get(i).getPartTableData().getPartNumber();
//                List<OldServiceList> oldServiceLists=dupList.get(i).getOldServiceList();
//
//                for(OldServiceList serviceList:oldServiceLists) {
//                    if (eng.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())
//                            || partNo.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())
//                            || partNo.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())
//                            || partNo.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())
//                    ) {
//                        expandableLists.add(dupList.get(i));
//
//                    }
//                }
//            }
//        }
//        notifyDataSetChanged();
//    }

    public void filter(String s) {

        s = s.toLowerCase(Locale.getDefault());
        expandableLists.clear();
        dupSet.clear();

        if (s.length() == 0) {
            expandableLists.addAll(dupItems);
            dupSet.addAll(dupItems);

        } else {
            for (int i=0;i<dupItems.size();i++) {
//                boolean isAdded=false;
                /**
                 * old superseded parts are in other table, so dupItem has getOldServiceList and getPartTableData
                 * If user search for old parts, new parts only viewed for old parts
                 * In order to do this and hashset is used to eliminate duplicate items
                 * or Other boolean logic is used but commented
                 */
                for(OldServiceList list:dupItems.get(i).getOldServiceList()){
//                    Log.d(TAG, "filter: "+list);

                    if(list.getSupersededNo().toLowerCase().contains(s.toLowerCase()))
                    {
//                        isAdded=true;
                        dupSet.add(dupItems.get(i));
//                        lists.add(dupItems.get(i));
                    }
                }

                if(dupItems.get(i).getPartTableData().getPartNumber().toLowerCase().contains(s.toLowerCase())||dupItems.get(i).getPartTableData().getEngine().toLowerCase().contains(s.toLowerCase())){
//                    isAdded=true;
                    dupSet.add(dupItems.get(i));
                }


//                if(isAdded)
//                    expandableLists.add((dupItems.get(i)));

//                String wp = dupItems.get(i).getOldServiceList().get(0).getPartNumber();
//
//                if (wp.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())) {
//                    expandableLists.add(dupItems.get(i));
//
//                }
            }
            expandableLists.clear();
            expandableLists.addAll(dupSet);
        }

        notifyDataSetChanged();
    }
}
