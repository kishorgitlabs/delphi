package adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brainmagic.delphitvs.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import model.rotaryservicelist.RotaryServiceData;
import model.rotaryservicelist.RotaryServiceList;


public class RotaryServiceListAdapter extends ArrayAdapter {
    private Context context;
    private  List<RotaryServiceList> serviceList,dupItems;
    private static final String TAG = "RotaryServiceListAdapte";

    public RotaryServiceListAdapter(Context context, List<RotaryServiceList> serviceList) {
        super(context, R.layout.rotary_service_adapter);
        this.context=context;
        this.serviceList=serviceList;
        dupItems=new ArrayList<>(serviceList);
    }

    @Override
    public View getView(int position,View convertView,ViewGroup parent) {
        if(convertView==null)
        {
            convertView= LayoutInflater.from(context).inflate(R.layout.rotary_service_adapter,null);
        }

        TextView sNo=convertView.findViewById(R.id.rotary_service_sno);
        TextView illNo=convertView.findViewById(R.id.rotary_service_ill_no);
        TextView seq=convertView.findViewById(R.id.rotary_service_line_seq_no);
        TextView partNo=convertView.findViewById(R.id.rotary_service_part_no);
        TextView dsc=convertView.findViewById(R.id.rotary_service_part_dsc);
        TextView qty=convertView.findViewById(R.id.rotary_service_qty_fip);

        sNo.setText(position+1+"");
        seq.setText(serviceList.get(position).getLINESEQNO());
        illNo.setText(serviceList.get(position).getILLUSTRATIONNO());
        partNo.setText(serviceList.get(position).getSerPARTNO());
        dsc.setText(serviceList.get(position).getPARTDESCRIPTION());
        qty.setText(serviceList.get(position).getQTYFIP());

        return convertView;

    }

    @Override
    public int getCount() {

        return serviceList.size();
    }

    public void filter(String s) {

        s = s.toLowerCase(Locale.getDefault()).trim();
        serviceList.clear();

        if (s.length() == 0) {
            serviceList.addAll(dupItems);

        }
        else {
            for (int i=0;i<dupItems.size();i++) {
                String iNo = dupItems.get(i).getILLUSTRATIONNO();
                String iPNo = dupItems.get(i).getSerPARTNO();

                Log.d(TAG, "filter: "+s);
                if(iNo!=null)
                    if (iNo.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())||iPNo.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())) {
                    serviceList.add(dupItems.get(i));
                }
            }
        }
        notifyDataSetChanged();
    }
}
