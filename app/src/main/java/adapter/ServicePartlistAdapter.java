package adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.brainmagic.delphitvs.R;

import java.util.List;

import alert.Alert;
import io.reactivex.annotations.NonNull;
import model.PartnumberSearch;
import model.inlinefields.Inline;
import model.productdetail.ProductDetails;
import model.servicepart.ServicePartlist;

public class ServicePartlistAdapter extends ArrayAdapter {
    private Context context;
    private List<Inline> data;
    private LinearLayout popup_layout;
    private List<ServicePartlist> data1;
    private String Dealer_tex,State_tex;
    private Alert box;
    private CardView cv;
    private int count_position;
    private String value_change,data_value;
    Animation scaleup,popup;



    public ServicePartlistAdapter(@NonNull Context context, List<ServicePartlist> data1) {
        super(context, R.layout.servicepartlist,data1);
        this.context = context;
        this.data1 = data1;
        box =new Alert(context);
        scaleup = AnimationUtils.loadAnimation(context, R.anim.animation);
    }

    @SuppressLint("WrongConstant")
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ServicePartlistAdapter.ComplaintsHolder holder;

        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.servicepartlist, parent, false);

            holder = new ServicePartlistAdapter.ComplaintsHolder();


            holder.tex1 = (TextView) convertView.findViewById(R.id.dtvspartno);
            holder.tex2 = (TextView) convertView.findViewById(R.id.lineseqno);
            holder.tex3 = (TextView) convertView.findViewById(R.id.illustrationno);

            holder.tex4 = (TextView) convertView.findViewById(R.id.partdescription);
            holder.tex5 = (TextView) convertView.findViewById(R.id.qtyfip);





           /* holder.linearLayout_pooup = (LinearLayout) convertView.findViewById(R.id.cart_item_layout);
            holder.linearLayout_application = (LinearLayout) convertView.findViewById(R.id.application);
            holder.linearLayout_alpha = (LinearLayout) convertView.findViewById(R.id.alpha);
*/

            holder.tex1.setText(data1.get(position).getDTVSPartNo());
            holder.tex2.setText(data1.get(position).getLineSeqNo());
            holder.tex3.setText(data1.get(position).getIllustrationNo());
            holder.tex4.setText(data1.get(position).getPartDescription());
            holder.tex5.setText(data1.get(position).getQtyFip());

           /* if(data1!= null){
                data_value = data1.get(position).getDescription();
                if (data_value.equals("DELIVERY VALVE")) {
                    holder.product_image.setImageResource(R.drawable.deliveryvalve);
                } else if (data_value.equals("ELEMENT")) {
                    holder.product_image.setImageResource(R.drawable.elemnt);

                } else {
                    holder.product_image.setImageResource(R.drawable.nozzle);
                    holder.linearLayout_application.setVisibility(View.GONE);
                    holder.linearLayout_alpha.setVisibility(View.VISIBLE);
                }
            }else {
                if (value_change.equals("DELIVERY VALVE")) {
                    holder.product_image.setImageResource(R.drawable.deliveryvalve);
                } else if (value_change.equals("ELEMENT")) {
                    holder.product_image.setImageResource(R.drawable.elemnt);

                } else {
                    holder.product_image.setImageResource(R.drawable.nozzle);
                    holder.linearLayout_application.setVisibility(View.GONE);
                    holder.linearLayout_alpha.setVisibility(View.VISIBLE);
                }
            }
            holder.linearLayout_pooup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (bool == true)
                        viewPopup(data,position);
                    else
                        viewpopuptwo(data1,position);
                }
            });

            if(bool == true) {
                holder.tex1.setText(data.get(position).getDTVSPartno());
                holder.tex2.setText(data.get(position).getEquivalentPartNo());
                holder.tex3.setText("Rs:" +data.get(position).getMRP());
                holder.tex4.setText(data.get(position).getCustomerApplication());
                if(data.get(position).getHSNCODE() != null)

                    holder.tex5.setText(data.get(position).getHSNCODE());
                else
                    holder.tex5.setText("");
            }
            else {
                holder.tex1.setText(data1.get(position).getDTVSPartno());
                holder.tex2.setText(data1.get(position).getEquivalentPartNo());
                holder.tex3.setText("Rs:" +data1.get(position).getMRP());
                holder.tex4.setText(data1.get(position).getCustomerApplication());
                if(data1.get(position).getHSNCODE() != null)
                    holder.tex5.setText(data1.get(position).getHSNCODE());
                else
                    holder.tex5.setText("");
            }*/

            convertView.setTag(holder);
        } else {
            holder = (ServicePartlistAdapter.ComplaintsHolder) convertView.getTag();
        }

        return convertView;
    }


    private void viewPopup(List<Inline> data,int position) {
        try {
            final AlertDialog alertDialog;
            alertDialog = new AlertDialog.Builder(context).create();
            alertDialog.setIcon(R.drawable.ic_launcher_delphi);
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.popupadapter, null);
            popup = AnimationUtils.loadAnimation(context, R.anim.bounce);
            dialogView.setAnimation(popup);
            alertDialog.setView(dialogView);

            ImageView Back = (ImageView) dialogView.findViewById(R.id.back);
            /*TextView tex1=(TextView)dialogView.findViewById(R.id.tantext1) ;
            tex1.setText(String.valueOf(data.get(position).getUptodateequip()));*/
            ((TextView) dialogView.findViewById(R.id.tantext1)).setText(String.valueOf(data.get(position).getDTVSPartno()));
            ((TextView) dialogView.findViewById(R.id.tantext2)).setText(String.valueOf(data.get(position).getEquivalentPartNo()));
            ((TextView) dialogView.findViewById(R.id.tantext3)).setText(String.valueOf(data.get(position).getDescription()));
            ((TextView) dialogView.findViewById(R.id.tantext4)).setText("Rs:"+String.valueOf(data.get(position).getMRP()));
            ((TextView) dialogView.findViewById(R.id.tantext5)).setText(String.valueOf(data.get(position).getCustomerApplication()));
            Back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //progressDialog.dismiss();
                    alertDialog.dismiss();

                }
            });

            alertDialog.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void viewpopuptwo(List<PartnumberSearch> data1, int position) {
        try {
            final AlertDialog alertDialog;
            alertDialog = new AlertDialog.Builder(context).create();
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.popupadapter, null);
            popup = AnimationUtils.loadAnimation(context, R.anim.bounce);
            dialogView.setAnimation(popup);
            alertDialog.setView(dialogView);

            ImageView Back = (ImageView) dialogView.findViewById(R.id.back);
            /*TextView tex1=(TextView)dialogView.findViewById(R.id.tantext1) ;
            tex1.setText(String.valueOf(data.get(position).getUptodateequip()));*/
            ((TextView) dialogView.findViewById(R.id.tantext1)).setText(String.valueOf(data1.get(position).getDTVSPartno()));
            ((TextView) dialogView.findViewById(R.id.tantext2)).setText(String.valueOf(data1.get(position).getEquivalentPartNo()));
            ((TextView) dialogView.findViewById(R.id.tantext3)).setText(String.valueOf(data1.get(position).getDescription()));
            ((TextView) dialogView.findViewById(R.id.tantext4)).setText("Rs:"+String.valueOf(data1.get(position).getMRP()));
            ((TextView) dialogView.findViewById(R.id.tantext5)).setText(String.valueOf(data1.get(position).getCustomerApplication()));
            Back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //progressDialog.dismiss();
                    alertDialog.dismiss();
                }
            });


            alertDialog.show();

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private class ComplaintsHolder {
        public TextView tex1;
        public TextView tex2;
        public TextView tex3;
        public TextView tex4;
        public ImageView product_image;
        public TextView tex5;
        public CardView cardView;
        public TextView tex6;
        public TextView tex7;
        public  LinearLayout linearLayout_pooup;
        public  LinearLayout linearLayout_application;
        public  LinearLayout linearLayout_alpha;
    }

    {

    }
}
