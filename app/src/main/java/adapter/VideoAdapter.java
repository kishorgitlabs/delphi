package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainmagic.delphitvs.R;

import java.util.List;

import model.general.Categorydata.CategoryItems;
import model.general.categorylist.CategoryData;
import model.general.video.VideoList;

public class VideoAdapter extends ArrayAdapter {
    private Context context;
    private List<CategoryData> data;
    private boolean type=false;
    private List<VideoList> videoItems;
//    public CategoryAdapter(Context context, List<CategoryData> data) {
//        super(context, R.layout.category_adapter);
//        this.context=context;
//        this.data=data;
//    }

    public VideoAdapter(Context context, List<VideoList> videoItems, boolean type) {
        super(context, R.layout.category_adapter);
        this.context=context;
        this.videoItems=videoItems;
        this.type=type;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view= LayoutInflater.from(context).inflate(R.layout.category_adapter,null);

        TextView categoryName=view.findViewById(R.id.category_name);
        TextView categorySno=view.findViewById(R.id.category_sno);
        categorySno.setText(position+1+".");
        ImageView arrow=view.findViewById(R.id.arrow);

        if(type)
        {
            arrow.setVisibility(View.GONE);
            categoryName.setText(videoItems.get(position).getVideoName());
        }else {
            categoryName.setText(data.get(position).getCategory());
        }

        return view;
    }

    @Override
    public int getCount() {
        if(data!=null)
            return data.size();
        else
            return videoItems.size();
    }
}
