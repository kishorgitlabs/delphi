package adapter.regional;

import android.content.Context;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.brainmagic.delphitvs.R;

import java.util.List;

import model.regional.salesnamelist.OrderList;

public class CurrentOrderDetailsRegionlaAdapter extends ArrayAdapter {

    Context context;
    List<OrderList>list;

    public CurrentOrderDetailsRegionlaAdapter(@NonNull Context context, List<OrderList>list) {
        super(context, R.layout.rmvieworderdetailsadapter,list);
        this.context=context;
        this.list=list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.rmvieworderdetailsadapter,parent,false);

        TextView sno=(TextView)view.findViewById(R.id.s_no);
        TextView orderid=(TextView)view.findViewById(R.id.order_id);
        TextView contactperson=(TextView)view.findViewById(R.id.contact_person);
        TextView total=(TextView)view.findViewById(R.id.total);

        orderid.setText(list.get(position).getOrderid());
        contactperson.setText(list.get(position).getContactPerson());
         total.setText("Rs."+list.get(position).getTotal());
        sno.setText(position+1+"");


        return view;


    }

    @Override
    public int getCount() {

      return   list.size();
    }

}
