package adapter.regional;

import android.content.Context;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.brainmagic.delphitvs.R;

import java.util.List;

import io.reactivex.annotations.NonNull;
import model.regional.OrderDetail;

public class CurrentOrderViewRegionalAdapter extends ArrayAdapter {
    Context context;
    List<OrderDetail>datas;

    public CurrentOrderViewRegionalAdapter(@NonNull Context context, List<OrderDetail>datas) {
        super(context, R.layout.rmviewcurrentmonthadapter,datas);
        this.context=context;
        this.datas=datas;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


        View views= LayoutInflater.from(context).inflate(R.layout.rmviewcurrentmonthadapter,parent,false);

        TextView salesname=views.findViewById(R.id.current_sales_name);
        TextView salesdate=views.findViewById(R.id.current_sales_date);
//        TextView salesamount=views.findViewById(R.id.current_sales_amount);

        TextView custcode=views.findViewById(R.id.custcode);
        TextView custname=views.findViewById(R.id.cutname);

        custcode.setText(datas.get(position).getCustomercode());
        custname.setText(datas.get(position).getCustomername());
        salesname.setText(datas.get(position).getExeName());
//        salesamount.setText("Rs."+datas.get(position).getTotal());

        String[] date=datas.get(position).getDate().split("T");
       salesdate.setText(date[0]);



        return views;



    }

    @Override
    public int getCount() {

        return datas.size();
    }
}
