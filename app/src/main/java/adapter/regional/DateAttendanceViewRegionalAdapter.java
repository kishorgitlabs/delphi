package adapter.regional;

import android.content.Context;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.brainmagic.delphitvs.R;

import java.util.List;

import io.reactivex.annotations.NonNull;
import model.regional.salesnamelist.DataRegionalAttendanceView;
import model.regional.salesnamelist.DataRegionalDateWiseView;

public class DateAttendanceViewRegionalAdapter extends ArrayAdapter {

Context context;
List<DataRegionalDateWiseView> regional_date;



    public DateAttendanceViewRegionalAdapter(Context context, List<DataRegionalDateWiseView> data){
        super(context,R.layout.regionalmanagerviewattendance);
        this.context=context;
        this.regional_date=data;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {



        View views=LayoutInflater.from(context).inflate(R.layout.regionalmanagerviewattendance,parent,false);

            TextView sno=views.findViewById(R.id.regional_sno);
            TextView date=views.findViewById(R.id.regional_date);
            TextView day=views.findViewById(R.id.regional_day);

            sno.setText(position+1+"");
            day.setText(regional_date.get(position).getAttendDay());

            String[] regionaldate=regional_date.get(position).getDate().split("T");
            date.setText(regionaldate[0]);



        return views;
    }

    @Override
    public int getCount() {

        return regional_date.size();
    }
}

