package adapter.regional;

import android.content.Context;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.regionalmanager.DateWiseOrderRegional;

import java.util.List;

import io.reactivex.annotations.NonNull;
import model.sales.order.orderhistory.OrderList;

public class DateWiseOrderRegionalAdapter extends ArrayAdapter {

    Context context;
    List<model.partdetails.OrderList> orderList;


    public DateWiseOrderRegionalAdapter(DateWiseOrderRegional context, List<model.partdetails.OrderList> orderList) {
        super(context,R.layout.datewiseorderregionaladapter, orderList);

        this.context=context;
        this.orderList=orderList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View views=LayoutInflater.from(context).inflate(R.layout.datewiseorderregionaladapter,parent,false);

        TextView custcode=(TextView)views.findViewById(R.id.custcode);
        TextView sales_order_date=(TextView)views.findViewById(R.id.sales_order_date);
        TextView distname=(TextView)views.findViewById(R.id.distname);
        TextView executname=(TextView)views.findViewById(R.id.executname);


        String[]orderdate=orderList.get(position).getInsertDate().split("T");
        sales_order_date.setText(orderdate[0]);

        custcode.setText(orderList.get(position).getDiscode());
        executname.setText(orderList.get(position).getExeName());

        distname.setText(orderList.get(position).getDisname());
        if(orderList.get(position).getOrderDate()!=null){
            sales_order_date.setText(orderdate[0]);
        }


        return views;
    }

    @Override
    public int getCount() {

        return orderList.size();
    }

}
