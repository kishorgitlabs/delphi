package adapter.regional;

import android.content.Context;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.brainmagic.delphitvs.R;

import java.util.List;

import io.reactivex.annotations.NonNull;
import model.regional.Data;
import model.regional.OrderDetail;

public class DateWiseOrdersAdapter extends ArrayAdapter {

    Context context;
    List<OrderDetail>data;

    public DateWiseOrdersAdapter(@NonNull Context context, List<OrderDetail>data) {
        super(context, R.layout.datewiseorderregionaladapter,data);
        this.context=context;
        this.data=data;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View views= LayoutInflater.from(context).inflate(R.layout.datewiseorderregionaladapter,parent,false);

//        TextView sales_executive_name=(TextView)views.findViewById(R.id.sales_executive_name);
        TextView sales_order_date=(TextView)views.findViewById(R.id.sales_order_date);
//        TextView sales_order_amount=(TextView)views.findViewById(R.id.sales_order_amount);


//        sales_executive_name.setText(data.get(position).getExeName());

        String[]orderdate=data.get(position).getDate().split("T");
        sales_order_date.setText(orderdate[0]);


        if(data.get(position).getDate()!=null){
            sales_order_date.setText(orderdate[0]);
        }
//        sales_order_amount.setText(data.get(position).getTotal());

        return views;



    }

    @Override
    public int getCount()
    {
        return data.size();
    }
}
