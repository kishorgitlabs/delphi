package adapter.regional;

import android.content.Context;
import android.content.Intent;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.regionalmanager.SalesMapActive;

import java.util.List;

import io.reactivex.annotations.NonNull;
import model.sales.viewattendance.attendancehistory.SalesAttendanceHistoryResult;

public class SalesAttendanceTimeRegionalAdapter extends ArrayAdapter {
    Context context;
    List<SalesAttendanceHistoryResult>data;

    public SalesAttendanceTimeRegionalAdapter(@NonNull Context context, List<SalesAttendanceHistoryResult>data) {
        super(context, R.layout.rmviewsalesexecutivetimingadapter,data);
        this.context=context;
        this.data=data;
    }


    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {


        View views=LayoutInflater.from(context).inflate(R.layout.rmviewsalesexecutivetimingadapter,parent,false);

        TextView sno=(TextView)views.findViewById(R.id.regional_sno);
        TextView intime=(TextView)views.findViewById(R.id.regional_in_time);
        TextView outtime=(TextView)views.findViewById(R.id.regional_out_time);
        ImageView map=(ImageView) views.findViewById(R.id.regional_map);

        sno.setText(position+1+"");
        intime.setText(data.get(position).getInTime());
        outtime.setText(data.get(position).getOutTime());

        final String[] dates=data.get(position).getDate().split("T");

map.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        context.startActivity(new Intent(context, SalesMapActive.class)
                .putExtra("Rdate",dates[0])
                .putExtra("RtrackId",data.get(position).getId())
                .putExtra("RinLat",data.get(position).getInLatitude())
                .putExtra("RinLon",data.get(position).getInLongitude())
                .putExtra("RoutLat",data.get(position).getOutLatitude())
                .putExtra("RoutLon",data.get(position).getOutLongitude())
                .putExtra("RfromAddress",data.get(position).getAddress())
                .putExtra("RtoAddress",data.get(position).getOutAddress())
                .putExtra("Rempid",data.get(position).getEmpId())
        );
    }
});
        return views;

    }

    @Override
    public int getCount() {

        return data.size();
    }
}
