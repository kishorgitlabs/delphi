package adapter.regional;

import android.annotation.SuppressLint;
import android.content.Context;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.brainmagic.delphitvs.R;

import java.util.List;

import io.reactivex.annotations.NonNull;
import model.sales.viewattendance.attendance.SalesAttendanceResult;

public class SalesAttendanceViewRegionalAdapter extends ArrayAdapter {
    Context context;
    List<SalesAttendanceResult> data;

    public SalesAttendanceViewRegionalAdapter(Context context, List<SalesAttendanceResult> data) {
        super(context, R.layout.rmviewsalesexecutiveattendanceadapter);
        this.context = context;
        this.data = data;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View views=LayoutInflater.from(context).inflate(R.layout.rmviewsalesexecutiveattendanceadapter,parent,false);

        TextView sno=(TextView)views.findViewById(R.id.sno);
        TextView salesname=(TextView)views.findViewById(R.id.sales_name);
        TextView saeles_orderdate=(TextView)views.findViewById(R.id.sales_order_date);
        TextView sales_order_day=(TextView)views.findViewById(R.id.sales_order_day);


        sno.setText(position+1+"");
        salesname.setText(data.get(position).getName());
        sales_order_day.setText(data.get(position).getAttendDay() );

        String[] date=data.get(position).getDate().split("T");
        saeles_orderdate.setText(date[0]);

        return views;
    }

    @Override
    public int getCount() {

        return data.size();
    }

}
