package adapter.regional;

import android.content.Context;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.brainmagic.delphitvs.R;

import java.util.List;

import io.reactivex.annotations.NonNull;
import model.regional.salesnamelist.Datum;

public class SalesFullOrderDetilsAdapter extends ArrayAdapter {

    Context context;
    List<Datum> data;

    public SalesFullOrderDetilsAdapter(@NonNull Context context, List<Datum> datas) {
        super(context, R.layout.currentmonthdetailsadapter);
        this.data=datas;
        this.context=context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.currentmonthdetailsadapter,parent,false);
        TextView partno=(TextView)view.findViewById(R.id.partno);
        TextView productname=(TextView)view.findViewById(R.id.product_name);
        TextView productsegment=(TextView)view.findViewById(R.id.product_segment);
        TextView mrp=(TextView)view.findViewById(R.id.mrp);
        TextView qty=(TextView)view.findViewById(R.id.qty);

        partno.setText(data.get(position).getPartNo());
        productname.setText(data.get(position).getProductName());
        productsegment.setText(data.get(position).getSegment());
        mrp.setText("₹ " +data.get(position).getAmountPerUnit());
        qty.setText(data.get(position).getQuantity());

        return view;
    }

    @Override
    public int getCount() {

        return data.size();
    }
}
