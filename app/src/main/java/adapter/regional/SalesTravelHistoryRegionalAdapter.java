package adapter.regional;

import android.content.Context;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.brainmagic.delphitvs.R;

import java.util.List;

import io.reactivex.annotations.NonNull;
import model.sales.viewattendance.salescoordinate.SalesDataList;

public class SalesTravelHistoryRegionalAdapter extends ArrayAdapter {

    Context context;
    List<SalesDataList>data;

    public SalesTravelHistoryRegionalAdapter(@NonNull Context context, List<SalesDataList>data) {
        super(context, R.layout.rmviewsalesexecutivetravelhistoryadapter);
        this.context=context;
        this.data=data;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


        View views=LayoutInflater.from(context).inflate(R.layout.rmviewsalesexecutivetravelhistoryadapter,parent,false);

        TextView sno=(TextView)views.findViewById(R.id.coordinate_sno);
        TextView time=(TextView)views.findViewById(R.id.coordinate_time);
        TextView address=(TextView)views.findViewById(R.id.coordinate_address);

        sno.setText(position+1+"");
        time.setText(data.get(position).getTimedate());
        address.setText(data.get(position).getAddress());

        return views;
    }

    @Override
    public int getCount()
    {
        return data.size();
    }
}
