package adapter.regional;

import android.content.Context;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.Nullable;

import com.brainmagic.delphitvs.R;

import io.reactivex.annotations.NonNull;

public class ViewAttendanceCoordinateAdapterRegional extends ArrayAdapter {

    public ViewAttendanceCoordinateAdapterRegional(@NonNull Context context) {
        super(context, R.layout.view_attendance_coordinate_adapter);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view= LayoutInflater.from(convertView.getContext()).inflate(R.layout.view_attendance_coordinate_adapter,parent,false);
        return view;
    }

    @Override
    public int getCount() {
        return 10;
    }
}
