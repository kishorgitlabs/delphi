package adapter.regional;

import android.content.Context;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.brainmagic.delphitvs.R;

import io.reactivex.annotations.NonNull;

public class ViewAttendanceRegionalAdapter extends ArrayAdapter {
    private Context context;

    public ViewAttendanceRegionalAdapter(@NonNull Context context) {
        super(context, R.layout.view_attendance_adapter);
        this.context=context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view= LayoutInflater.from(convertView.getContext()).inflate(R.layout.view_attendance_adapter,parent,false);
        TextView sNO=view.findViewById(R.id.regional_attendance_sno);
        TextView name=view.findViewById(R.id.regional_attendance_name);
        TextView date=view.findViewById(R.id.regional_attendance_date);
        TextView day=view.findViewById(R.id.regional_attendance_day);
        return view;
    }

    @Override
    public int getCount() {
        return 10;
    }
}
