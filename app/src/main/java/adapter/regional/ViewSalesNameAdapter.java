package adapter.regional;

import android.content.Context;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.regionalmanager.ViewVisitReportRegional;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.reactivex.annotations.NonNull;
import model.regional.ViewVisitResult;

public class ViewSalesNameAdapter extends ArrayAdapter  {
    Context context;
    public List<ViewVisitResult>data;
    public ArrayList<ViewVisitResult> arrayList;



    public ViewSalesNameAdapter(ViewVisitReportRegional context, List<ViewVisitResult> data) {
        super(context, R.layout.viewvisitsalesnameadapter,data);
        this.context=context;
        this.data=data;
        this.arrayList=new ArrayList<ViewVisitResult>();
        this.arrayList.addAll(data);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View views= LayoutInflater.from(context).inflate(R.layout.viewvisitsalesnameadapter,parent,false);
        TextView sales_name=(TextView)views.findViewById(R.id.sales_name);

        sales_name.setText(data.get(position).getName());
        return views;
    }


    @Override
    public int getCount() {

        return data.size();
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return data.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        data.clear();
        if (charText.length() == 0) {
            data.addAll(arrayList);
        } else {
            for (ViewVisitResult wp : arrayList) {
                if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    data.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

}


