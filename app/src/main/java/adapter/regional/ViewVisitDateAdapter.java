package adapter.regional;

import android.content.Context;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.brainmagic.delphitvs.R;

import java.util.List;

import io.reactivex.annotations.NonNull;
import model.regional.DataItem;

public class ViewVisitDateAdapter extends ArrayAdapter {

    Context context;
    List<DataItem>listitems;

    public ViewVisitDateAdapter(@NonNull Context context, List<DataItem> resource) {
        super(context, R.layout.rmviewvisitreportadapter,resource);

        this.context=context;
        this.listitems=resource;
    }

    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.rmviewvisitreportadapter,parent,false);
        TextView name=view.findViewById(R.id.SE_name);
        TextView mobileno=view.findViewById(R.id.SE_mobileno);
        TextView customertype=view.findViewById(R.id.SE_customertype);
        TextView status=view.findViewById(R.id.SE_status);
        TextView visitdate=view.findViewById(R.id.SE_visited_date);
        TextView visitplace=view.findViewById(R.id.SE_visit_place);

        try {
            name.setText(listitems.get(position).getCustomerName());
            mobileno.setText(listitems.get(position).getCustMobileNo());
            customertype.setText(listitems.get(position).getCustomerType());
//            status.setText( listitems.get(position).getRemark());
            visitdate.setText(listitems.get(position).getVisitedDate());
            visitplace.setText(listitems.get(position).getPlaceOfVisit());


        }catch (Exception e)
        {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public int getCount() {

        return listitems.size();
    }


}
