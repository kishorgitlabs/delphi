package adapter.regional;

import android.content.Context;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.brainmagic.delphitvs.R;

import java.util.List;

import io.reactivex.annotations.NonNull;
import model.sales.visitreport.Datum;

public class ViewVisitRegionaSearchlAdapter extends ArrayAdapter  {
    private Context context;
    private List<Datum> viewVisitReportListSE;


    public ViewVisitRegionaSearchlAdapter(@NonNull Context context, List<Datum> viewVisitReportListSE) {
        super(context, R.layout.visit_report_adapter,viewVisitReportListSE);
        this.context=context;
        this.viewVisitReportListSE=viewVisitReportListSE;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View view= LayoutInflater.from(context).inflate(R.layout.visit_report_adapter,null,false);
        TextView salesPerson=view.findViewById(R.id.sales_person_name);
        TextView customerName=view.findViewById(R.id.customer_name);
        TextView mobileNo=view.findViewById(R.id.mobile_no);
        TextView customerType=view.findViewById(R.id.customer_type);
        TextView reportActivity=view.findViewById(R.id.report_activities);
        TextView visitedDate=view.findViewById(R.id.visited_date);
        TextView placeOfVisit=view.findViewById(R.id.place_of_visit);
//            salesPerson.setText(viewVisitReportListSE.get(position).getSalesPersonName());
            customerName.setText(viewVisitReportListSE.get(position).getCustomerName());
            mobileNo.setText(viewVisitReportListSE.get(position).getCustMobileNo());
            customerType.setText(viewVisitReportListSE.get(position).getCustomerType());
            reportActivity.setText((viewVisitReportListSE.get(position).getRemark()));
            visitedDate.setText(viewVisitReportListSE.get(position).getVisitedDate());
            placeOfVisit.setText(viewVisitReportListSE.get(position).getPlaceOfVisit());

        return view;
    }

    @Override
    public int getCount() {

        return viewVisitReportListSE.size();
    }


    }

