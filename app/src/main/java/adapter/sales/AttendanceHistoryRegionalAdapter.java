package adapter.sales;

import android.content.Context;
import android.content.Intent;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.regionalmanager.MapActivityRegional;
import com.brainmagic.delphitvs.regionalmanager.SalesMapActive;

import java.util.List;

import io.reactivex.annotations.NonNull;
import model.regional.salesnamelist.DataRegionalInOutTime;

public class AttendanceHistoryRegionalAdapter extends ArrayAdapter {


    Context context;
     List<DataRegionalInOutTime> data;

public AttendanceHistoryRegionalAdapter(Context context,  List<DataRegionalInOutTime> data){
    super(context, R.layout.rmviewattendancehistory);
    this.context=context;
    this.data=data;
}
    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {


        View views=LayoutInflater.from(context).inflate(R.layout.rmviewattendancehistory,parent,false);

        TextView sno=views.findViewById(R.id.regional_from_sno);
        TextView intime=views.findViewById(R.id.regional_intime);
        TextView outtime=views.findViewById(R.id.regional_outtime);
        ImageView map=views.findViewById(R.id.regional_map);

String a=data.get(position).getId();
sno.setText(position+1+"");
intime.setText(data.get(position).getInTime());
outtime.setText(data.get(position).getOutTime());


final String[] datas=data.get(position).getDate().split("T");
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                context.startActivity(new Intent(context, MapActivityRegional.class)
                        .putExtra("region_date",datas[0])
                        .putExtra("region_trackid",data.get(position).getId())
                        .putExtra("region_inlat",data.get(position).getInLatitude())
                        .putExtra("region_inlong",data.get(position).getInLongitude())
                        .putExtra("region_outlat",data.get(position).getOutLatitude())
                        .putExtra("region_outlong",data.get(position).getOutLongitude())
                        .putExtra("region_fromaddress",data.get(position).getAddress())
                        .putExtra("region_toaddress",data.get(position).getOutAddress())
                            .putExtra("region_empid",data.get(position).getEmpId())
                );
            }
        });



        return views;


    }

    @Override
    public int getCount() {

    return data.size();
    }
}

