package adapter.sales;

import android.content.Context;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.brainmagic.delphitvs.R;

import java.util.List;

import io.reactivex.annotations.NonNull;
import model.regional.salesnamelist.DataRegionalTimeHistoryPojo;

public class AttendanceTravelHistoryRegionalAdapter extends ArrayAdapter {

    Context context;
    private List<DataRegionalTimeHistoryPojo> data;

public AttendanceTravelHistoryRegionalAdapter(Context context, List<DataRegionalTimeHistoryPojo> data){
    super(context, R.layout.rmviewattendancecoordinates);
    this.context=context;
    this.data=data;
}
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {




        View views=LayoutInflater.from(context).inflate(R.layout.rmviewattendancecoordinates,parent,false);

        TextView sno=(TextView)views.findViewById(R.id.regiona_sno);
        TextView time=(TextView)views.findViewById(R.id.region_time);
        TextView address=(TextView)views.findViewById(R.id.region_address);


        sno.setText(position+1+"");
        time.setText(data.get(position).getTimedate());
        address.setText(data.get(position).getAddress());


        return views;

    }

    @Override
    public int getCount() {

    return data.size();
    }
}
