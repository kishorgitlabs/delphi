package adapter.sales;

import android.content.Context;
import android.content.Intent;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.regionalmanager.AttendanceHistoryRegional;
import com.brainmagic.delphitvs.salesactivities.ViewAttendanceHistoryActivitySales;

import java.util.List;

import io.reactivex.annotations.NonNull;
import model.regional.salesnamelist.DataRegionalAttendanceView;

public class AttendanceViewRegionalAdapter extends ArrayAdapter {

Context context;
List<DataRegionalAttendanceView> data;



    public AttendanceViewRegionalAdapter(Context context, List<DataRegionalAttendanceView> data){
        super(context,R.layout.regionalmanagerviewattendance);
        this.context=context;
        this.data=data;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {



        View views=LayoutInflater.from(context).inflate(R.layout.regionalmanagerviewattendance,parent,false);
        LinearLayout adapter=views.findViewById(R.id.adapter_layout);
            TextView sno=views.findViewById(R.id.regional_sno);
            TextView date=views.findViewById(R.id.regional_date);
            TextView day=views.findViewById(R.id.regional_day);

            sno.setText(position+1+"");
            day.setText(data.get(position).getAttendDay());
            date.setText(data.get(position).getDate());



String[] a=data.get(position).getDate().split("T");
date.setText(a[0]);

        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] dateFormats=data.get(position).getDate().split("T");

                context.startActivity(new Intent(context, AttendanceHistoryRegional.class)
                        .putExtra("regional_date",dateFormats[0])
                        .putExtra("regional_day",data.get(position).getAttendDay())
                );
            }
        });



        return views;
    }

    @Override
    public int getCount() {

        return data.size();
    }
}

