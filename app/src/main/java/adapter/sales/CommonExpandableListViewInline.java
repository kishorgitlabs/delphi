package adapter.sales;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.brainmagic.delphitvs.InLinePumpActivity;
import com.brainmagic.delphitvs.InlineGovernor;
import com.brainmagic.delphitvs.InlineInjector;
import com.brainmagic.delphitvs.PumpNo_Inline_Active;
import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.ServiceListActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import model.changepassword.ChildInlines;
import model.changepassword.DatumInline;

public class CommonExpandableListViewInline extends BaseExpandableListAdapter {
    private Context context;
    private List<DatumInline> expandableLists,dupList;
    private String mainCat,subCat;

    public CommonExpandableListViewInline(Context context, List<DatumInline> expandableLists, String mainCat, String subCat) {
        this.context = context;
        this.expandableLists = expandableLists;
        dupList=new ArrayList<>(expandableLists);
        this.mainCat=mainCat;
        this.subCat=subCat;
    }

    public List<DatumInline> getExpandableLists() {
        return expandableLists;
    }

    public void setExpandableLists(List<DatumInline> expandableLists) {
        this.expandableLists = expandableLists;
    }

    @Override
    public int getGroupCount() {

        return expandableLists.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return expandableLists.get(groupPosition).getChild().size();
    }

    @Override
    public Object getGroup(int groupPosition) {

        return expandableLists.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return expandableLists.get(groupPosition).getChild().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {

        return childPosition;
    }

    @Override
    public boolean hasStableIds() {

        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        final DatumInline parentList=expandableLists.get(groupPosition);
        if(convertView==null)
        {
            convertView= LayoutInflater.from(context).inflate(R.layout.common_rail_products,null);
        }

//        TextView sNo=convertView.findViewById(R.id.common_rail_sno);
        TextView app=convertView.findViewById(R.id.common_rail_app);
//        TextView pNo=convertView.findViewById(R.id.common_rail_pump);
//        final ImageView commom_service=convertView.findViewById(R.id.commom_service);

//        commom_service.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent commonservice=new Intent(context, ServiceListActivity.class);
//                commonservice .putExtra("MainCatagory", mainCat);
//                commonservice .putExtra("SubCatagory", subCat);
//                commonservice.putExtra("partNo",parentList.getPartNo());
//                context.startActivity(commonservice);
//            }
//        });
//        sNo.setText(groupPosition+1+"");
        app.setText(parentList.getApplication());
//        pNo.setText(parentList.getPartNo());
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, final ViewGroup parent) {

        final ChildInlines child=expandableLists.get(groupPosition).getChild().get(childPosition);
        final DatumInline parentList=expandableLists.get(groupPosition);

        if(convertView==null)
        {
            convertView=LayoutInflater.from(context).inflate(R.layout.common_child_views,null);
        }
        TextView rail=convertView.findViewById(R.id.rail_pump_no);
        TextView injector=convertView.findViewById(R.id.rail_service);
        TextView main_filter=convertView.findViewById(R.id.injector_service);
        TextView filter=convertView.findViewById(R.id.filter_service);
//        TextView pre_filter=convertView.findViewById(R.id.rail_pre_Filter);
//        TextView ecu=convertView.findViewById(R.id.ecu_filter);
//        TextView pump = convertView.findViewById(R.id.rail_pump_no);

        rail.setText(child.getPumpNo());
        injector.setText(child.getInjector());
        main_filter.setText(child.getGovernor());
        filter.setText(child.getMainfilter());
//        pre_filter.setText(child.getPrefilter());
//        ecu.setText(child.getEcu());
//          pump.setText(child.getmPumpNo());

        rail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent commonservice=new Intent(context, PumpNo_Inline_Active.class);
                commonservice .putExtra("inline_pumps", mainCat);
                commonservice .putExtra("inline_subs", subCat);
                commonservice.putExtra("inline_partno",child.getPumpNo());
                context.startActivity(commonservice);
            }
        });
        injector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, InlineInjector.class)
                        .putExtra("inline_injector_main",mainCat)
                        .putExtra("inline_injector_sub",subCat)
                        .putExtra("services","Service Parts")
                        .putExtra("mainPartNoo",parentList.getPartNo())
                        .putExtra("partNoo",child.getInjector())
                );
            }
        });
        main_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, InlineGovernor.class)
                        .putExtra("inline_governor_main",mainCat)
                        .putExtra("inline_governor_sub",subCat)
                        .putExtra("governor_service","Service Parts")
                        .putExtra("governor_mainpartno",parentList.getPartNo())
                        .putExtra("governor_partNoo",child.getGovernor())
                );
            }
        });



//        pre_filter.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                context.startActivity(new Intent(context,CommonRailFilterList.class)
//                        .putExtra("main_cat",mainCat)
//                        .putExtra("sub_cat",subCat)
//                        .putExtra("service","Service Parts")
//                        .putExtra("type","Pre-Filter")
//                        .putExtra("partNo",child.getPrefilter())
//                );
//            }
//        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    public void filter(String s) {

        s = s.toLowerCase(Locale.getDefault()).trim();
        expandableLists.clear();

        if (s.length() == 0) {
            expandableLists.addAll(dupList);

        } else {
            for (int i=0;i<dupList.size();i++) {
                String application = dupList.get(i).getApplication();
                String partNo = dupList.get(i).getPartNo();

                if (application.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())||partNo.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())) {
                    expandableLists.add(dupList.get(i));

                }
            }
        }
        notifyDataSetChanged();
    }
}
