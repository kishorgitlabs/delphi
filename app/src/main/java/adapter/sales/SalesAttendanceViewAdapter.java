package adapter.sales;

import android.content.Context;
import android.content.Intent;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.salesactivities.ViewAttendanceHistoryActivitySales;

import java.util.List;

import io.reactivex.annotations.NonNull;
import model.sales.viewattendance.attendance.SalesAttendanceResult;

public class SalesAttendanceViewAdapter extends ArrayAdapter {

    Context context;
    List<SalesAttendanceResult>data;

    public SalesAttendanceViewAdapter(@NonNull Context context, List<SalesAttendanceResult>data) {
        super(context, R.layout.view_attendance_adapter);
        this.context=context;
        this.data=data;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {



        View views=LayoutInflater.from(context).inflate(R.layout.view_attendance_adapter,parent,false);

        LinearLayout adapterLayout=views.findViewById(R.id.sales_attendance_adapter_layout);

        TextView sno=(TextView)views.findViewById(R.id.sales_sno);
        TextView date=(TextView)views.findViewById(R.id.sales_date);
        TextView address=(TextView)views.findViewById(R.id.sales_day);

        sno.setText(position+1+"");
          address.setText(data.get(position).getAttendDay());

          String[]salesdate=data.get(position).getDate().split("T");
          date.setText(salesdate[0]);


        adapterLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] dateFormat=data.get(position).getDate().split("T");
                context.startActivity(new Intent(context, ViewAttendanceHistoryActivitySales.class)
                        .putExtra("date",dateFormat[0])
                        .putExtra("day",data.get(position).getAttendDay())
                );
            }
        });

        return views;



    }

    @Override
    public int getCount()
    {

        return data.size();
    }
}
