package adapter.sales;

import android.content.Context;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.salesactivities.SalesOrderHistory;

import java.util.List;

import io.reactivex.annotations.NonNull;
import model.regional.salesnamelist.OrderList;

public class SalesOrderDateWiseAdapter extends ArrayAdapter {

Context context;
    List<model.partdetails.OrderList> orderList;

    public SalesOrderDateWiseAdapter(SalesOrderHistory context, List<model.partdetails.OrderList> orderList) {
        super(context,R.layout.adapter_sales_order_history, orderList);
        this.context=context;
        this.orderList=orderList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_sales_order_history, parent, false);

        TextView sno=(TextView)view.findViewById(R.id.sales_sno);
        TextView date=(TextView)view.findViewById(R.id.sales_date);
        TextView amount=(TextView)view.findViewById(R.id.sales_amount);


        sno.setText(position+1+"");
        amount.setText(orderList.get(position).getTotal());

        String[] dates=orderList.get(position).getInsertDate().split("T");
        date.setText(dates[0]);


        return view;
    }

    @Override
    public int getCount()
    {

        return orderList.size();
    }
}
