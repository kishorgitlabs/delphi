package adapter.sales;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.brainmagic.delphitvs.R;
import java.util.List;

import model.distributorlist.DistributorData;


public class SelectDistributerAdapter extends ArrayAdapter {

    private Context context;
    String distributermobile;
    private List<DistributorData> distributerlist;
    private Disdetails disdetails;
    String disid="",discode="",disaddressselected="";

    public SelectDistributerAdapter(Context context, List<DistributorData> distributorData) {
        super(context, R.layout.distributeradapter);
        this.context=context;
        this.distributerlist=distributorData;
        disdetails=(Disdetails) context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView=null;

        if (convertView==null){
            convertView= LayoutInflater.from(context).inflate(R.layout.distributeradapter,null);
            CheckBox selectdis=convertView.findViewById(R.id.selectdis);
            TextView distributorno=convertView.findViewById(R.id.distributorno);
            TextView salesofficename=convertView.findViewById(R.id.salesofficename);
            final TextView disaddress=convertView.findViewById(R.id.disaddress);
            distributorno.setText(distributerlist.get(position).getCustomerCode());
            salesofficename.setText(distributerlist.get(position).getSalesOffice());
            disaddress.setText(distributerlist.get(position).getStreet()+","+distributerlist.get(position).getStreet2()+","+distributerlist.get(position).getStreet3());

            if(disid.equals(distributerlist.get(position).getId()))
            {
                selectdis.setChecked(true);
            }
            selectdis.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                    if (checked){
                        compoundButton.setChecked(checked);
                        disid=distributerlist.get(position).getId();
                        discode=distributerlist.get(position).getCustomerCode();
                        disaddressselected=distributerlist.get(position).getStreet()+","+distributerlist.get(position).getStreet2()+","+distributerlist.get(position).getStreet3();
                         disdetails.senddistributerid(distributerlist.get(position).getId());
                         disdetails.senddistributername(distributerlist.get(position).getCustomerCode());
                         disdetails.senddistributeraddress(distributerlist.get(position).getStreet()+","+distributerlist.get(position).getStreet2()+","+distributerlist.get(position).getStreet3());
                         notifyDataSetChanged();
                    }else {
                        compoundButton.setChecked(false);
                        disid="";
                        disdetails.senddistributerid("");
                    }
                }
            });
        }
        return convertView;
    }

    @Override
    public int getCount() {
        return distributerlist.size();
    }

    public  interface Disdetails{
        void senddistributerid(String distriid);
        void senddistributername(String distriname);
        void senddistributeraddress(String distriaddress);
    }
}
