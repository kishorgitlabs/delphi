package adapter.sales;

import android.content.Context;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.brainmagic.delphitvs.R;

import java.util.List;

import io.reactivex.annotations.NonNull;
import model.sales.viewattendance.salescoordinate.SalesDataList;

public class ViewAttendanceCoordinateAdapterSales extends ArrayAdapter {

    private Context context;
    List<SalesDataList> data;

    public ViewAttendanceCoordinateAdapterSales(@NonNull Context context, List<SalesDataList> data) {
        super(context, R.layout.view_attendance_coordinate_adapter);
        this.data=data;
        this.context=context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view= LayoutInflater.from(context).inflate(R.layout.view_attendance_coordinate_adapter,parent,false);
        TextView sNo=view.findViewById(R.id.sales_coordinate);
        TextView time=view.findViewById(R.id.sales_coordinate_time);
        TextView address=view.findViewById(R.id.sales_coordinate_address);

        sNo.setText(position+1+"");
        time.setText(data.get(position).getTimedate());
        address.setText(data.get(position).getAddress());

        return view;
    }

    @Override
    public int getCount() {
        return data.size();
    }
}
