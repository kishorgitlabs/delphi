package adapter.sales;

import android.content.Context;
import android.content.Intent;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.map.MapsActivity;
import com.brainmagic.delphitvs.salesactivities.ViewAttendanceCoordinateActivitySales;

import java.util.List;

import io.reactivex.annotations.NonNull;
import model.sales.viewattendance.attendancehistory.SalesAttendanceHistoryResult;

public class ViewAttendanceHistoryAdapterSales extends ArrayAdapter {
    private List<SalesAttendanceHistoryResult> data;
    private String date,day;
    private Context context;

    public ViewAttendanceHistoryAdapterSales(@NonNull Context context, List<SalesAttendanceHistoryResult> data, String date, String day) {
        super(context, R.layout.view_attendance_history_adapter);
        this.data=data;
        this.context=context;
        this.date=date;
        this.day=day;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view= LayoutInflater.from(context).inflate(R.layout.view_attendance_history_adapter,parent,false);
        LinearLayout salesAdapter=view.findViewById(R.id.sales_attendance_history_adapter_layout);
        TextView sNo=view.findViewById(R.id.sales_sno);
        TextView inTime=view.findViewById(R.id.sales_attendance_in_time);
        TextView outTime=view.findViewById(R.id.sales_attendance_out_time);
        ImageView map=view.findViewById(R.id.sales_attendance_navigate_map);
        sNo.setText(position+1+"");
        inTime.setText(data.get(position).getInTime());
        outTime.setText(data.get(position).getOutTime());
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, MapsActivity.class)
                        .putExtra("date",date)
                        .putExtra("trackId",data.get(position).getId())
                        .putExtra("inLat",data.get(position).getInLatitude())
                        .putExtra("inLon",data.get(position).getInLongitude())
                        .putExtra("outLat",data.get(position).getOutLatitude())
                        .putExtra("outLon",data.get(position).getOutLongitude())
                        .putExtra("fromAddress",data.get(position).getAddress())
                        .putExtra("toAddress",data.get(position).getOutAddress())
                );
            }
        });
        salesAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ViewAttendanceCoordinateActivitySales.class)
                        .putExtra("trackId",data.get(position).getId())
                        .putExtra("date",date)
                        .putExtra("day",day)
                        .putExtra("inTime",data.get(position).getInTime())
                        .putExtra("outTime",data.get(position).getOutTime())
                        .putExtra("inAddress",data.get(position).getAddress())
                        .putExtra("outAddress",data.get(position).getOutAddress())
                );
            }
        });
        return view;
    }



    @Override
    public int getCount() {
        return data.size();
    }
}
