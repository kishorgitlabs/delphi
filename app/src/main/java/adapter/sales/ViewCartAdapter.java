package adapter.sales;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.salesactivities.SelectPartDetails;
import com.muddzdev.styleabletoastlibrary.StyleableToast;


import java.util.List;

import model.CartDAO;
import room.AppDatabase;


public class ViewCartAdapter extends ArrayAdapter implements View.OnTouchListener {

    private Context context;
    private List<CartDAO> cartDAOS;
    private AppDatabase appDatabase;
    private Recretatesipelistview recretatesipelistview;


    public ViewCartAdapter(Context context, List<CartDAO> cartDAOS) {
        super(context, R.layout.viewcartadapter);
        this.context=context;
        this.cartDAOS=cartDAOS;
        this.recretatesipelistview=(Recretatesipelistview)context;

    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        convertView=null;

        if (convertView==null){

            appDatabase=AppDatabase.getAppDatabase(context);
            convertView= LayoutInflater.from(context).inflate(R.layout.viewcartadapter,null);
            TextView partnumber=convertView.findViewById(R.id.partnumber);
            TextView qtyyy=convertView.findViewById(R.id.qtyyy);
            TextView amtt=convertView.findViewById(R.id.amtt);
            TextView sino=convertView.findViewById(R.id.sino);
            TextView mrp=convertView.findViewById(R.id.mrp);

            sino.setText(String.format("%s",position+1));
            partnumber.setText(cartDAOS.get(position).getPartnumber());
            qtyyy.setText(cartDAOS.get(position).getQty());
            amtt.setText(cartDAOS.get(position).getTotalamount());
            mrp.setText(cartDAOS.get(position).getMrp());

            qtyyy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showAlertBox(cartDAOS.get(position).getPartnumber(),cartDAOS.get(position).getQty(),cartDAOS.get(position).getTotalamount(),cartDAOS.get(position).getMrp(),cartDAOS.get(position).getDescription());
                }
            });


        }
        return convertView;
    }

    public void showAlertBox(final String partNumber, final String quantity, final String totalAmount, final String partPrice, final String description)
    {
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        View dialogView= LayoutInflater.from(context).inflate(R.layout.cart_quantity_alert,null);
        final TextView partNumbers=dialogView.findViewById(R.id.partnum_details);
        final EditText editText=dialogView.findViewById(R.id.quantity_edit_text);
        TextView save=dialogView.findViewById(R.id.cart_alert_save);
        TextView cancel=dialogView.findViewById(R.id.cart_alert_cancel);
        editText.setText(quantity);
        partNumbers.setText(partNumber);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String qty=editText.getText().toString();
                if(qty.equals("0") || TextUtils.isEmpty(qty)) {
                    alertDialog.dismiss();
                    StyleableToast st = new StyleableToast(context,
                            "Please enter a valid number", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }
                else
                {
                    int pp= Integer.parseInt(String.valueOf(partPrice));
                    int qq= Integer.valueOf(String.valueOf(qty));
                    int tots=pp*qq;
                    String tamt= String.valueOf(tots);
                    appDatabase.ProductsDAO().updatecart(tamt,qty,partNumber);
                    cartDAOS=appDatabase.ProductsDAO().getCartDao();
                    recretatesipelistview.recreted();
                    alertDialog.dismiss();
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        alertDialog.setView(dialogView);
        alertDialog.show();
    }



    public interface Recretatesipelistview{
        void recreted();
    }

    @Override
    public int getCount() {
        return cartDAOS.size();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }
}
