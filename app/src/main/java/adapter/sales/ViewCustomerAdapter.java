package adapter.sales;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.brainmagic.delphitvs.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import model.sales.customerdetails.GetCustomerDetailsResult;


/**
 * Created by system01 on 2/12/2017.
 */

public class ViewCustomerAdapter extends ArrayAdapter {

    private Context context;

    private List<GetCustomerDetailsResult> cusList;


    public ViewCustomerAdapter(Context context, List<GetCustomerDetailsResult> cusList) {
        super(context, R.layout.view_customer_adapter);
        this.context = context;
        this.cusList = cusList;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.view_customer_adapter, parent, false);

            TextView sNo = (TextView) convertView.findViewById(R.id.sno_view_customer);
            TextView customerName = (TextView) convertView.findViewById(R.id.customer_name_city);
            TextView customerMobile = (TextView) convertView.findViewById(R.id.view_customer_mobile);
            TextView customerMail = (TextView) convertView.findViewById(R.id.view_customer_mail);
            LinearLayout viewCustomerLayout =  convertView.findViewById(R.id.view_customer_layout);

            sNo.setText(position+1+"");
            customerMail.setText(cusList.get(position).getDealerEmail());
            customerMobile.setText(cusList.get(position).getDealerMobile());
            customerName.setText(cusList.get(position).getDealerName()+"\n"+cusList.get(position).getDealerCity());
            viewCustomerLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewCustomerDetail(cusList.get(position));
                }
            });

        }

        return convertView;
    }

    private void viewCustomerDetail(GetCustomerDetailsResult getCustomerDetailsResult)
    {

        final AlertDialog alertDialog = new AlertDialog.Builder(
                context).create();

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.view_customer_alert, null);
        ImageView imageView = dialogView.findViewById(R.id.image_view);
        Picasso.with(context).load(getCustomerDetailsResult.getmUploadImage()).error(R.drawable.noimageavail_seven_five).placeholder( R.drawable.progress_animation ).into(imageView);
        TextView customerType = dialogView.findViewById(R.id.customer_type);
        TextView shopName = dialogView.findViewById(R.id.shop_name_view_customer);
        TextView contactPerson = dialogView.findViewById(R.id.contact_person_view_customer);
        TextView mobileNo = dialogView.findViewById(R.id.mobile_no_view_customer);
        TextView emailId = dialogView.findViewById(R.id.email_id_view_customer);
        TextView customerState = dialogView.findViewById(R.id.customer_state);
        TextView customerCity = dialogView.findViewById(R.id.customer_city);
        TextView customerAddress = dialogView.findViewById(R.id.customer_address_one);
        TextView pincode = dialogView.findViewById(R.id.customer_pincode_view_customer);
        TextView gst = dialogView.findViewById(R.id.gst_no_view_customer);

        customerType.setText(getCustomerDetailsResult.getUserType());
        shopName.setText(getCustomerDetailsResult.getShopName());
        contactPerson.setText(getCustomerDetailsResult.getContactPersonname());
        mobileNo.setText(getCustomerDetailsResult.getDealerMobile());
        emailId.setText(getCustomerDetailsResult.getDealerEmail());
        customerState.setText(getCustomerDetailsResult.getDealerState());
        customerCity.setText(getCustomerDetailsResult.getDealerCity());
        customerAddress.setText(getCustomerDetailsResult.getDealerAddress());
        pincode.setText(getCustomerDetailsResult.getmPinCode());
        gst.setText(getCustomerDetailsResult.getGST());

        Button okay = (Button)  dialogView.findViewById(R.id.view_customer_okay);
        alertDialog.setView(dialogView);

        okay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();

            }
        });
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public int getCount() {
        return cusList.size();
    }
}
