package adapter.sales;

import android.content.Context;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.salesactivities.ViewFullSalesOrderHistory;

import java.util.List;

import io.reactivex.annotations.NonNull;

public class ViewSalesOrderFullHistoryAdapter  extends ArrayAdapter {

Context context;
    List<model.regional.salesnamelist.OrderList>data;

    public ViewSalesOrderFullHistoryAdapter(ViewFullSalesOrderHistory context, List<model.regional.salesnamelist.OrderList> orderList) {
        super(context, R.layout.adapter_sales_full_order_history,orderList);
        this.context=context;
        this.data=orderList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View views=LayoutInflater.from(context).inflate(R.layout.adapter_sales_full_order_history,parent,false);

   TextView sno=(TextView)views.findViewById(R.id.s_no_more_order_view);
   TextView orderid =(TextView)views.findViewById(R.id.part_no_more_order_view);
   TextView contactperson=(TextView)views.findViewById(R.id.part_dsc_more_order_view);
   TextView grandtotal=(TextView)views.findViewById(R.id.qty_more_order_view);


   sno.setText(position+1+"");
  orderid.setText(data.get(position).getOrderid());
  contactperson.setText(data.get(position).getContactPerson());
  grandtotal.setText(data.get(position).getTotal());


       return views;
    }

    @Override
    public int getCount() {

        return data.size();
    }
}
