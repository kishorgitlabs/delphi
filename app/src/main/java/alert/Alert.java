package alert;



import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.salesactivities.AttendanceActivitySales;

import model.api.APIService;
import model.api.RetroClient;
import model.changepassword.ChangePasswordModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Systems02 on 22-May-17.
 */

public class Alert {

    public   Context context;
    public AlertDialog alertDialog;
    private onPositiveClickListener onPositiveClickListener;
    private onNegativeClickListener onNegativeClickListener;
    private onButtonClickListener onButtonClickListener;

    public Alert(Context context) {
        this.context = context;
//        onClickListener= (Alert.onClickListener) context;
    }

    public void showAlertbox(String msg) {
        alertDialog = new AlertDialog.Builder(
                context).create();

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertbox, null);
        TextView alert = dialogView.findViewById(R.id.textView1);
        alert.setText(msg);
        Button okay = (Button)  dialogView.findViewById(R.id.okay);
        alertDialog.setView(dialogView);
        okay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();

            }
        });
        //alertDialog.setMessage(msg);
        //alertDialog.setTitle("Turbo Energy Limited");
       /* alertDialog.setIcon(ContextCompat.getDrawable(context,R.mipmap.ic_launcher_square));
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });*/
        //alertDialog.setIcon(R.drawable.logo);
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        /*alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        })*/
    }

    public void showAlertboxnegative(String msg) {
        alertDialog = new AlertDialog.Builder(
                context).create();

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertbox, null);
        TextView alert = dialogView.findViewById(R.id.textView1);
        alert.setText(msg);
        Button okay = (Button)  dialogView.findViewById(R.id.okay);
        alertDialog.setView(dialogView);
        okay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
                ((Activity) context).finish();

            }
        });
        //alertDialog.setMessage(msg);
        //alertDialog.setTitle("Turbo Energy Limited");
       /* alertDialog.setIcon(ContextCompat.getDrawable(context,R.mipmap.ic_launcher_square));
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });*/
        //alertDialog.setIcon(R.drawable.logo);
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        /*alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        })*/
    }

    public void showAlertBoxWithListener(String msg, int isVisible) {
        alertDialog = new AlertDialog.Builder(
                context).create();

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertbox, null);
        TextView alert = dialogView.findViewById(R.id.textView1);
        alert.setText(msg);
        Button okay = (Button)  dialogView.findViewById(R.id.okay);
        Button cancel = (Button)  dialogView.findViewById(R.id.cancel);
        FrameLayout cancelLayout = dialogView.findViewById(R.id.cancel_layout);
        alertDialog.setView(dialogView);
        cancelLayout.setVisibility(isVisible);
        okay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
//                ((Activity) context).finish();

                onPositiveClickListener.onPositiveClick();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
//                ((Activity) context).finish();
                onNegativeClickListener.onPositiveClick();
            }
        });
        //alertDialog.setMessage(msg);
        //alertDialog.setTitle("Turbo Energy Limited");
       /* alertDialog.setIcon(ContextCompat.getDrawable(context,R.mipmap.ic_launcher_square));
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });*/
        //alertDialog.setIcon(R.drawable.logo);
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        /*alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        })*/
    }

    public void setOnPositiveClickListener(onPositiveClickListener onClickListener){
        this.onPositiveClickListener=onClickListener;
    }

    public void setOnNegativeClickListener(onNegativeClickListener onClickListener){
        this.onNegativeClickListener=onClickListener;
    }
    public void setOnButtonClickListener(onButtonClickListener onClickListener){
        this.onButtonClickListener=onClickListener;
    }

    public interface onPositiveClickListener{
        public void onPositiveClick();
    }

    public interface onNegativeClickListener{
        public void onPositiveClick();
    }

    public interface onButtonClickListener{
        void onMessage(String msg);
    }

    public void changePasswordAlert(final String phone) {
        alertDialog = new AlertDialog.Builder(
                context).create();

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.change_password, null);
        final TextView mobileNo = dialogView.findViewById(R.id.mobile_no);
        final TextView oldPass = dialogView.findViewById(R.id.old_password);
        final TextView newPass= dialogView.findViewById(R.id.new_password);
        Button submit = (Button)  dialogView.findViewById(R.id.submit);
        Button cancel = (Button)  dialogView.findViewById(R.id.cancel);
        FrameLayout cancelLayout = dialogView.findViewById(R.id.cancel_layout);
        alertDialog.setView(dialogView);
        submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
//                ((Activity) context).finish();

                String mobile=mobileNo.getText().toString();
                String oldPassword=oldPass.getText().toString();
                String newPassword=newPass.getText().toString();

                changePassword(phone,oldPassword,newPassword);

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
//                ((Activity) context).finish();
            }
        });

        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);

    }

    private void changePassword(String mobileNo, String pass, String newPass)
    {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {
            APIService apiService = RetroClient.getApiService();
            Call<ChangePasswordModel> call=apiService.changePassword(mobileNo,pass,newPass);
            call.enqueue(new Callback<ChangePasswordModel>() {
                @Override
                public void onResponse(Call<ChangePasswordModel> call, Response<ChangePasswordModel> response) {
                    try{
                        progressDialog.dismiss();
                        if(response.isSuccessful())
                        {
                            if(response.body().equals("Success"))
                            {
                                onButtonClickListener.onMessage("Success");
                            }
                            else if(response.body().equals("NotSuccess")){
                                onButtonClickListener.onMessage("Old Password is Incorrect. Please check one more time");
                            }else {
                                onButtonClickListener.onMessage("Cannot Change your Password. Please contact Admin");
                            }
                        }
                        else {
                            onButtonClickListener.onMessage("Something went wrong . Please try again later .");
                        }
                    }
                    catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        onButtonClickListener.onMessage("Bad Response. Please try again Later");
                    }
                }

                @Override
                public void onFailure(Call<ChangePasswordModel> call, Throwable t) {
                    progressDialog.dismiss();
                    onButtonClickListener.onMessage("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
            progressDialog.dismiss();
            onButtonClickListener.onMessage("InValid Data. Please contact Admin");
        }

    }
}
