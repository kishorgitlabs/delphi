package com.brainmagic.delphitvs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.provider.Settings;


import android.os.Bundle;

import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.brainmagic.delphitvs.regionalmanager.RegionalMainmenu;
import com.brainmagic.delphitvs.salesactivities.SalesMainmenu;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.Timer;
import java.util.TimerTask;

import adapter.MyCustomPageAdapter;

public class
Aboutus extends AppCompatActivity {
    private ViewPager myPager = null;
    private ImageView img_back,img_home;
    private RelativeLayout anim_about;
    private static int currentPage = 0;
    private static int NUM_PAGES = 3;
    Animation animation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutus);

        MyCustomPageAdapter adapter = new MyCustomPageAdapter(Aboutus.this);
        myPager = (ViewPager) findViewById(R.id.viewpager);
        anim_about = (RelativeLayout)findViewById(R.id.about);
        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator);
        myPager.setAdapter(adapter);
        myPager.setCurrentItem(0);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bounce);

        final float density = getResources().getDisplayMetrics().density;
        indicator.setViewPager(myPager);
        indicator.setRadius(5 * density);

        img_back =(ImageView)findViewById(R.id.back);
//        img_home =(ImageView)findViewById(R.id.home);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


//        img_home.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(Aboutus.this,MainActivity.class);
//                startActivity(i);
//            }
//        });

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                myPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);


// Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

        final ImageView img_menu = (ImageView)findViewById(R.id.menu);
        img_menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                img_menu.startAnimation(animation);
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(Aboutus.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);


                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {

//                            pop.getMenu().findItem(R.id.menu_aboutus).setVisible(true);
                            case R.id.menu_home:
                                startActivity(new Intent(Aboutus.this, MainActivity.class));
                                break;

                            case R.id.menu_pricelist:
                                startActivity(new Intent(Aboutus.this, PriceList.class));
                                break;
                            case R.id.menu_general:
                                startActivity(new Intent(Aboutus.this, General.class));
                                break;
                            case R.id.menu_productcatalogue:
                                startActivity(new Intent(Aboutus.this, ProductCatalogue.class));
                                break;

                            case R.id.menu_vehiclesegment:
                                startActivity(new Intent(Aboutus.this, VehicleSegment.class));
                                break;

                            case R.id.menu_login:
                                SharedPreferences preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
                                if(preferences.getBoolean("salesLogin",false)) {
                                    if(preferences.getString("userType","").equals("SalesExecutive"))
                                    {
                                        Intent i = new Intent(Aboutus.this, SalesMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                    else {
                                        Intent i = new Intent(Aboutus.this, RegionalMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                }
                                else {
                                    Intent i = new Intent(Aboutus.this, SalesLogin.class);
                                    startActivity(i);
                                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                }


                        }

                        return false;
                    }
                });
                pop.inflate(R.menu.aboutusmenu);
                pop.getMenu().findItem(R.id.menu_aboutus).setVisible(false);

                pop.show();
            }
        });

    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.left_in, R.anim.left_out);

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
