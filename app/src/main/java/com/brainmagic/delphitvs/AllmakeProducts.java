package com.brainmagic.delphitvs;

import android.content.Intent;

import android.os.Bundle;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

public class AllmakeProducts extends AppCompatActivity {
    private LinearLayout layout_spares,layout_filters;
    private ImageView img_back,img_home;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_allmake_products);
       layout_spares = (LinearLayout) findViewById(R.id.cardview_spares);
       layout_filters = (LinearLayout)findViewById(R.id.cardview_filters);

        img_back =(ImageView)findViewById(R.id.back);
        img_home =(ImageView)findViewById(R.id.home);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AllmakeProducts.this,MainActivity.class);
                startActivity(i);
            }
        });
       layout_filters.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent i = new Intent(AllmakeProducts.this,FiltersAllmakeproducts.class);
               startActivity(i);
               overridePendingTransition(R.anim.left_in,R.anim.left_out);
           }
       });

       layout_spares.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent i = new Intent(AllmakeProducts.this,SparesAllmakeproducts.class);
               startActivity(i);
               overridePendingTransition(R.anim.left_in,R.anim.left_out);
           }
       });


    }
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in,R.anim.left_out);
    }

}
