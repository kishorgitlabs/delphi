package com.brainmagic.delphitvs;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;

import android.os.Bundle;

import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.regionalmanager.RegionalMainmenu;
import com.brainmagic.delphitvs.salesactivities.SalesMainmenu;

import java.util.List;

import adapter.CategoryAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.general.Categorydata.CategoryData;
import model.general.Categorydata.CategoryItems;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryDataView extends AppCompatActivity {

    private ImageView back;
    private ListView categoryDataList;
    private Alert alert;
    private TextView categoryType,categoryData;
    private List<CategoryItems> categoryItemsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_data_view);
        categoryData=findViewById(R.id.category_data_header);
        categoryType=findViewById(R.id.category_info_header);
        back=findViewById(R.id.back);
        categoryDataList=findViewById(R.id.category_data_list);

        alert=new Alert(this);
        String categoryDataString=getIntent().getStringExtra("categoryData");
        String categoryTypeString=getIntent().getStringExtra("categoryType");

        categoryData.setText(categoryDataString);
        categoryType.setText(categoryTypeString);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        final ImageView img_menu = (ImageView)findViewById(R.id.menu);
        img_menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(CategoryDataView.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_home:
                                startActivity(new Intent(CategoryDataView.this, MainActivity.class));
                                break;
                            case R.id.menu_aboutus:
                                startActivity(new Intent(CategoryDataView.this, Aboutus.class));
                                break;
                            case R.id.menu_pricelist:
                                startActivity(new Intent(CategoryDataView.this, PriceList.class));
                                break;
                            case R.id.menu_general:
                                startActivity(new Intent(CategoryDataView.this, General.class));
                                break;
                            case R.id.menu_productcatalogue:
                                startActivity(new Intent(CategoryDataView.this, ProductCatalogue.class));
                                break;

                            case R.id.menu_vehiclesegment:
                                startActivity(new Intent(CategoryDataView.this, VehicleSegment.class));
                                break;

                            case R.id.menu_login:
                                SharedPreferences preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
                                if(preferences.getBoolean("salesLogin",false)) {
                                    if(preferences.getString("userType","").equals("SalesExecutive"))
                                    {
                                        Intent i = new Intent(CategoryDataView.this, SalesMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                    else {
                                        Intent i = new Intent(CategoryDataView.this, RegionalMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                }
                                else {
                                    Intent i = new Intent(CategoryDataView.this, SalesLogin.class);
                                    startActivity(i);
                                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                }

                                break;

                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.aboutusmenu);
                pop.show();
            }
        });

        categoryDataList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    if(categoryItemsList.get(position).getUploadbrochureFile()!=null) {
                        Intent searchAddress = new Intent(Intent.ACTION_VIEW, Uri.parse(categoryItemsList.get(position).getUploadbrochureFile()));
                        if (searchAddress != null && !TextUtils.isEmpty(categoryItemsList.get(position).getUploadbrochureFile()))
                            startActivity(searchAddress);
                        else {
                            alert.showAlertbox("No Link Found");
                        }
                    }
                }catch (ActivityNotFoundException e)
                {
                    alert.showAlertbox("No Link found to Open");
//                    Snackbar snackbar1 = Snackbar.make(holder.coordinatorLayout, context.getString(R.string.no_link_is_found), Snackbar.LENGTH_SHORT);
//                    snackbar1.show();
                }
                catch (Exception e)
                {
                    alert.showAlertbox("Invalid Link");
//                    Snackbar snackbar1 = Snackbar.make(holder.coordinatorLayout, context.getString(R.string.invalid_link), Snackbar.LENGTH_SHORT);
//                    snackbar1.show();
//                    Log.d(TAG, "onClick: "+e);
                }

            }
        });

        NetworkConnection connection=new NetworkConnection(this);
        if(connection.CheckInternet())
        {
            getCategoryList();
        }

        else {
            alert.showAlertboxnegative("Please check your network connection and try again!");
        }
    }

    private void getCategoryList()
    {
        final ProgressDialog progressDialog = new ProgressDialog(CategoryDataView.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIService service= RetroClient.getApiService();

        Call<CategoryData> call=service.getMarketCategoryData(getIntent().getStringExtra("categoryData"));
        try {
            call.enqueue(new Callback<CategoryData>() {
                @Override
                public void onResponse(Call<CategoryData> call, Response<CategoryData> response) {
                    progressDialog.dismiss();
                    try{

                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success")) {
                                categoryItemsList=response.body().getData();
                                CategoryAdapter adapter = new CategoryAdapter(CategoryDataView.this, response.body().getData(),true);
                                categoryDataList.setAdapter(adapter);
                            }
                            else
                            {
                                alert.showAlertboxnegative("Invalid Response. Please try again Later");
                            }

                        }else {
                            alert.showAlertboxnegative("Something went wrong . Please try again later .");
                        }

                    }catch (Exception e)
                    {
                        e.printStackTrace();
                        alert.showAlertboxnegative("Bad Response. Please contact Admin");
                    }

                }

                @Override
                public void onFailure(Call<CategoryData> call, Throwable t) {
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
            alert.showAlertboxnegative("Something went wrong . Please try again later .");

        }
    }
}
