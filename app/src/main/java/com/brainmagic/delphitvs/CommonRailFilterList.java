package com.brainmagic.delphitvs;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.regionalmanager.RegionalMainmenu;
import com.brainmagic.delphitvs.salesactivities.SalesMainmenu;
import com.squareup.picasso.Picasso;

import java.util.List;

import adapter.FilterListAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.filters.details.FilterDetail;
import model.filters.details.FilterDetailList;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toaster.Toasts;

public class CommonRailFilterList extends AppCompatActivity {

    private ImageView back,home,menu;
    private TextView textView_one,subCat;
    private String mainCategory_type,subCategory,category,segment;
    private LinearLayout layout_one,layout_two,layout_three;
    private ListView listView;
    private NetworkConnection networkConnection;
    private Animation animation;
    private List<FilterDetailList> Data1;
    private SearchView search_partno;
    private Toasts toasts;
    private FilterListAdapter filterDetailsAdapter;
    private Alert showAlertbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_rail_filter_list);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bounce);
//        subCat = (TextView)findViewById(R.id.sub_cat);
        back = (ImageView)findViewById(R.id.back);
        menu = (ImageView)findViewById(R.id.menu);
        showAlertbox=new Alert(this);
        toasts = new Toasts(this);
//        search_partno = findViewById(R.id.search_partnofilter);
        //layout_one = (LinearLayout)findViewById(R.id.cart_item_layout);
//        listView = (ListView)findViewById(R.id.common_rail_filter_list);
        networkConnection = new NetworkConnection(this);
        TextView mainCatTxt=findViewById(R.id.filter_main);
        TextView subCatTxt=findViewById(R.id.filter_sub);
        TextView moreTxt=findViewById(R.id.filter_more);
        TextView partNumber=findViewById(R.id.common_rail_part_no);
        TextView appTxt=findViewById(R.id.common_rail_app_id);
        TextView partTxt=findViewById(R.id.common_rail_part_id);
        listView=findViewById(R.id.product_list);
//        TextView title=findViewById(R.id.common_filter_header);

        String main=getIntent().getStringExtra("main_cat");
        String sub=getIntent().getStringExtra("sub_cat");
        String service=getIntent().getStringExtra("service");
        String parts=getIntent().getStringExtra("partNo");
        String type=getIntent().getStringExtra("type");

        mainCatTxt.setText(main);
        subCatTxt.setText(sub);
        moreTxt.setText(service);
//        title.setText(parts);
        partNumber.setText(parts);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

//        home.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(CommonRailFilterList.this,MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
//            }
//        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu.startAnimation(animation);
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(CommonRailFilterList.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_home:
                                startActivity(new Intent(CommonRailFilterList.this, MainActivity.class));
                                break;
                            case R.id.menu_aboutus:
                                startActivity(new Intent(CommonRailFilterList.this, Aboutus.class));
                                break;
                            case R.id.menu_pricelist:
                                startActivity(new Intent(CommonRailFilterList.this, PriceList.class));
                                break;
                            case R.id.menu_general:
                                startActivity(new Intent(CommonRailFilterList.this, General.class));
                                break;
                            case R.id.menu_productcatalogue:
                                startActivity(new Intent(CommonRailFilterList.this, ProductCatalogue.class));
                                break;

                            case R.id.menu_vehiclesegment:
                                startActivity(new Intent(CommonRailFilterList.this, VehicleSegment.class));
                                break;

                            case R.id.menu_login:
                                SharedPreferences preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
                                if(preferences.getBoolean("salesLogin",false)) {
                                    if(preferences.getString("userType","").equals("SalesExecutive"))
                                    {
                                        Intent i = new Intent(CommonRailFilterList.this, SalesMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                    else {
                                        Intent i = new Intent(CommonRailFilterList.this, RegionalMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                }
                                else {
                                    Intent i = new Intent(CommonRailFilterList.this, SalesLogin.class);
                                    startActivity(i);
                                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                }


                                break;

                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.aboutusmenu);
                pop.show();
            }
        });

        checkInternet(parts);
    }

    private void checkInternet(String part)
    {
        if(networkConnection.CheckInternet())
        {
            callApi(part);
        }
        else {
            showAlertbox.showAlertboxnegative("Please check your network connection and try again!");
        }
    }

    private void callApi(String part)
    {
        final ProgressDialog progressDialog = new ProgressDialog(CommonRailFilterList.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIService service = RetroClient.getApiService();
//        Call<FilterDetail> call=service.commonFilterList(part,type);
        Call<FilterDetail> call=service.commonFilterList(part,getIntent().getStringExtra("appType"));
        try {
            call.enqueue(new Callback<FilterDetail>() {
                @Override
                public void onResponse(Call<FilterDetail> call, Response<FilterDetail> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.body().getResult().equals("success")){
                            filterDetailsAdapter=new FilterListAdapter(CommonRailFilterList.this,response.body().getData());
                            listView.setAdapter(filterDetailsAdapter);

//                            TextView dtvsPartNo=findViewById(R.id.dtvspartno_filters);
//                            TextView oePartNo=findViewById(R.id.oepartno_filters);
//                            TextView segment=findViewById(R.id.segment_filters);
//                            TextView vehicle_app=findViewById(R.id.vehicleapplication_filters);
//                            TextView dsc=findViewById(R.id.description_filters);
//                            TextView mrp=findViewById(R.id.mrp_filters);
//                            ImageView filterImage=findViewById(R.id.filters_image);
//
//                            dtvsPartNo.setText(response.body().getData().get(0).getPartNumber());
//                            oePartNo.setText(response.body().getData().get(0).getOEPartNumber());
//                            segment.setText(response.body().getData().get(0).getSegment());
//                            vehicle_app.setText(response.body().getData().get(0).getVehicleApplication());
//                            dsc.setText(response.body().getData().get(0).getDescription());
//                            mrp.setText("₹ "+response.body().getData().get(0).getMRP());

//                            Picasso.with(CommonRailFilterList.this).load(response.body().getData().get(0).getmImage()).error(R.drawable.noimageavail_seven_five).placeholder(R.drawable.progress_animation).into(filterImage);

                        }
                        else {
                            showAlertbox.showAlertboxnegative("No data found please try again later");
                        }


                    }catch (Exception e)
                    {
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<FilterDetail> call, Throwable t) {
                    progressDialog.dismiss();
                    showAlertbox.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
            progressDialog.dismiss();
            showAlertbox.showAlertboxnegative("Something went wrong . Please try again later .");
        }
    }
}
