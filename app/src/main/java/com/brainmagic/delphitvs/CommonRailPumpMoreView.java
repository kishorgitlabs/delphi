package com.brainmagic.delphitvs;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.regionalmanager.RegionalMainmenu;
import com.brainmagic.delphitvs.salesactivities.SalesMainmenu;

import java.util.List;

import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.partnumbersearch.PartNumberData;
import model.partnumbersearch.PartNumberSearch;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommonRailPumpMoreView extends AppCompatActivity {
    private ImageView back,home,menu;
    private Animation animation;
    private NetworkConnection networkConnection;
    private TextView rail,injector,main_filter,pre_pre_filter;
    private List<PartNumberData> commonRailData;
    private Alert showAlertBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_rail_pump_more_view);

        networkConnection = new NetworkConnection(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bounce);
        TextView mainCatTxt=findViewById(R.id.main);
        TextView subCatTxt=findViewById(R.id.sub);
        TextView moreTxt=findViewById(R.id.more);
        TextView appTxt=findViewById(R.id.common_rail_app_id);
        TextView partTxt=findViewById(R.id.common_rail_part_id);
        showAlertBox=new Alert(this);

        rail=findViewById(R.id.rail_service);
        injector=findViewById(R.id.injector_service);
        main_filter=findViewById(R.id.mail_Filter);
        pre_pre_filter=findViewById(R.id.rail_pre_Filter);

        final String main=getIntent().getStringExtra("main_cat");
        final String sub=getIntent().getStringExtra("sub_cat");
        final String part=getIntent().getStringExtra("partNo");
        String app=getIntent().getStringExtra("app");

        appTxt.setText(app);
        partTxt.setText(part);
        mainCatTxt.setText(main);
        subCatTxt.setText(sub);
        moreTxt.setText("Service Parts");

        back=findViewById(R.id.back);
        home=findViewById(R.id.home);
        menu=findViewById(R.id.menu);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CommonRailPumpMoreView.this,MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });


        injector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CommonRailPumpMoreView.this, CommonRailInjector.class)
                        .putExtra("main_cat", main)
                        .putExtra("sub_cat", sub)
                        .putExtra("service", "Service Parts")
                        .putExtra("type", "Injector")
                        .putExtra("partNo", commonRailData.get(0).getInjector())

                );
            }
        });

        main_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CommonRailPumpMoreView.this,CommonRailFilterList.class)
                    .putExtra("main_cat",main)
                    .putExtra("sub_cat",sub)
                    .putExtra("service","Service Parts")
                    .putExtra("type","Main Filter")
                    .putExtra("partNo",commonRailData.get(0).getMainFilter())
                );
            }
        });

        pre_pre_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CommonRailPumpMoreView.this,CommonRailFilterList.class)
                        .putExtra("main_cat",main)
                        .putExtra("sub_cat",sub)
                        .putExtra("service","Service Parts")
                        .putExtra("type","Pre-Filter")
                        .putExtra("partNo",commonRailData.get(0).getPreFilter())
                );
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu.startAnimation(animation);
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(CommonRailPumpMoreView.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_home:
                                startActivity(new Intent(CommonRailPumpMoreView.this, MainActivity.class));
                                break;
                            case R.id.menu_aboutus:
                                startActivity(new Intent(CommonRailPumpMoreView.this, Aboutus.class));
                                break;
                            case R.id.menu_pricelist:
                                startActivity(new Intent(CommonRailPumpMoreView.this, PriceList.class));
                                break;
                            case R.id.menu_general:
                                startActivity(new Intent(CommonRailPumpMoreView.this, General.class));
                                break;
                            case R.id.menu_productcatalogue:
                                startActivity(new Intent(CommonRailPumpMoreView.this, ProductCatalogue.class));
                                break;

                            case R.id.menu_vehiclesegment:
                                startActivity(new Intent(CommonRailPumpMoreView.this, VehicleSegment.class));
                                break;

                            case R.id.menu_login:
                                SharedPreferences preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
                                if(preferences.getBoolean("salesLogin",false)) {
                                    if(preferences.getString("userType","").equals("SalesExecutive"))
                                    {
                                        Intent i = new Intent(CommonRailPumpMoreView.this, SalesMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                    else {
                                        Intent i = new Intent(CommonRailPumpMoreView.this, RegionalMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                }
                                else {
                                    Intent i = new Intent(CommonRailPumpMoreView.this, SalesLogin.class);
                                    startActivity(i);
                                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                }


                                break;                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.aboutusmenu);
                pop.show();
            }
        });

        checkInternet();
    }

    private void checkInternet() {
        if(networkConnection.CheckInternet()){
            validatePart();
        }else {
            showAlertBox.showAlertboxnegative("Please check your network connection and try again!");

        }
    }

    private void validatePart()
    {
        final ProgressDialog progressDialog = new ProgressDialog(CommonRailPumpMoreView.this);
        try{

            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            String partNo=getIntent().getStringExtra("partNo");
            APIService service = RetroClient.getApiService();

            Call<PartNumberSearch> call=service.SearchPartnumbers(partNo);

            call.enqueue(new Callback<PartNumberSearch>() {
                @Override
                public void onResponse(Call<PartNumberSearch> call, Response<PartNumberSearch> response) {
                    progressDialog.dismiss();

                    if(response.body().getResult().equals("success")) {
                        commonRailData = response.body().getData();
                        rail.setText(response.body().getData().get(0).getRail());
                        injector.setText(response.body().getData().get(0).getInjector());
                        main_filter.setText(response.body().getData().get(0).getMainFilter());
                        pre_pre_filter.setText(response.body().getData().get(0).getPreFilter());
                    }
                    else {
                        showAlertBox.showAlertboxnegative("No record found please try again later");
                    }
                }

                @Override
                public void onFailure(Call<PartNumberSearch> call, Throwable t) {
                    progressDialog.dismiss();
                    showAlertBox.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });

        }catch (Exception e)
        {
            progressDialog.dismiss();
            showAlertBox.showAlertboxnegative("Something went wrong . Please try again later .");
            e.printStackTrace();
        }
    }
}
