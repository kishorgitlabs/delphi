package com.brainmagic.delphitvs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.regionalmanager.RegionalMainmenu;
import com.brainmagic.delphitvs.salesactivities.SalesMainmenu;

import java.util.List;

import adapter.ProductDetailsAdapter;
import model.productdetail.ProductDetails;
import network.NetworkConnection;

public class CommonrailFiesystem extends AppCompatActivity {
    private ImageView img_back,img_home,menu;
    private LinearLayout layout_filter,layout_pump;
    private NetworkConnection networkConnection;
    private List<ProductDetails> Data1;
    private ProductDetailsAdapter productDetailsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commonrail_fiesystem);
        img_back =(ImageView)findViewById(R.id.back);
//        img_home =(ImageView)findViewById(R.id.home);
        menu =(ImageView)findViewById(R.id.menu);
        networkConnection =new NetworkConnection(this);
        layout_filter = (LinearLayout)findViewById(R.id.commonrailfie_filter);
        layout_pump = (LinearLayout)findViewById(R.id.commonrailfie_hppumps);




        layout_pump.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(CommonrailFiesystem.this, CommonSubCategoryPump.class);
                i.putExtra("MainCatagory","Common Rail System");
                i.putExtra("SubCatagory","Pump");
                startActivity(i);
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });


        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });

        layout_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /*     if(networkConnection.CheckInternet()){
                        ValidatePart();
                    }else {
                        Toast.makeText(getApplicationContext(),"Please check your network connection and try again!",Toast.LENGTH_SHORT).show();
                    }
                }*/
                Intent i = new Intent(CommonrailFiesystem.this, SubCatagoryFilter.class);
                i.putExtra("Category","Main");
                i.putExtra("MainCatagory", "Common Rail System");
                i.putExtra("SubCatagory", "Filter");
                startActivity(i);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });


//        img_home.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(CommonrailFiesystem.this,MainActivity.class);
//                startActivity(i);
//                overridePendingTransition(R.anim.left_in,R.anim.left_out);
//            }
//        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(CommonrailFiesystem.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_home:
                                startActivity(new Intent(CommonrailFiesystem.this, MainActivity.class));
                                break;
                            case R.id.menu_aboutus:
                                startActivity(new Intent(CommonrailFiesystem.this, Aboutus.class));
                                break;
                            case R.id.menu_pricelist:
                                startActivity(new Intent(CommonrailFiesystem.this, PriceList.class));
                                break;
                            case R.id.menu_general:
                                startActivity(new Intent(CommonrailFiesystem.this, General.class));
                                break;
                            case R.id.menu_productcatalogue:
                                startActivity(new Intent(CommonrailFiesystem.this, ProductCatalogue.class));
                                break;

                            case R.id.menu_vehiclesegment:
                                startActivity(new Intent(CommonrailFiesystem.this, VehicleSegment.class));
                                break;

                            case R.id.menu_login:
                                SharedPreferences preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
                                if(preferences.getBoolean("salesLogin",false)) {
                                    if(preferences.getString("userType","").equals("SalesExecutive"))
                                    {
                                        Intent i = new Intent(CommonrailFiesystem.this, SalesMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                    else {
                                        Intent i = new Intent(CommonrailFiesystem.this, RegionalMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                }
                                else {
                                    Intent i = new Intent(CommonrailFiesystem.this, SalesLogin.class);
                                    startActivity(i);
                                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                }


                                break;

                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.aboutusmenu);
                pop.show();
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in,R.anim.left_out);
    }
}
