package com.brainmagic.delphitvs;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.regionalmanager.RegionalMainmenu;
import com.brainmagic.delphitvs.salesactivities.SalesMainmenu;
import com.brainmagic.delphitvs.salesactivities.SelectPartDetails;

import adapter.PriceListAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.general.feedback.FeedBack;
import model.general.feedback.FeedBackModel;
import model.getpartnumber.GetPartNumber;
import model.pricelist.PriceDetails;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedBackForm extends AppCompatActivity {

    private ImageView img_back,img_home;
    private EditText name,email,contactType,mobileNo,city,company,message;
    private AutoCompleteTextView partNo;
    private Button submit;
    private Alert alert;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back_form);

        img_back =(ImageView)findViewById(R.id.back);
        submit =findViewById(R.id.submit_fd);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        alert=new Alert(this);

        name=findViewById(R.id.name_fd);
        email=findViewById(R.id.email_fd);
        contactType=findViewById(R.id.contacttype_fd);
        mobileNo=findViewById(R.id.mobile_no_fd);
        city=findViewById(R.id.city_fd);
        company=findViewById(R.id.company_fd);
        partNo=findViewById(R.id.part_no_fd);
        message=findViewById(R.id.message_fd);


        final ImageView img_menu = (ImageView)findViewById(R.id.menu);
        img_menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                img_menu.startAnimation(animation);
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(FeedBackForm.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_home:
                                startActivity(new Intent(FeedBackForm.this, MainActivity.class));
                                break;
                            case R.id.menu_aboutus:
                                startActivity(new Intent(FeedBackForm.this, Aboutus.class));
                                break;
                            case R.id.menu_pricelist:
                                startActivity(new Intent(FeedBackForm.this, PriceList.class));
                                break;
                            case R.id.menu_general:
                                startActivity(new Intent(FeedBackForm.this, General.class));
                                break;
                            case R.id.menu_productcatalogue:
                                startActivity(new Intent(FeedBackForm.this, ProductCatalogue.class));
                                break;

                            case R.id.menu_vehiclesegment:
                                startActivity(new Intent(FeedBackForm.this, VehicleSegment.class));
                                break;

                            case R.id.menu_login:
                                SharedPreferences preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
                                if(preferences.getBoolean("salesLogin",false)) {
                                    if(preferences.getString("userType","").equals("SalesExecutive"))
                                    {
                                        Intent i = new Intent(FeedBackForm.this, SalesMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                    else {
                                        Intent i = new Intent(FeedBackForm.this, RegionalMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                }
                                else {
                                    Intent i = new Intent(FeedBackForm.this, SalesLogin.class);
                                    startActivity(i);
                                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                }

                                break;


                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.aboutusmenu);
                pop.show();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NetworkConnection connection=new NetworkConnection(FeedBackForm.this);
                if(connection.CheckInternet())
                {
                    String nameString=name.getText().toString();
                    String emailString=email.getText().toString();
                    String contactTypeString=contactType.getText().toString();
                    String mobileString=mobileNo.getText().toString();
                    String cityString=city.getText().toString();
                    String companyString=company.getText().toString();
                    String partNoString=partNo.getText().toString();
                    String msgString=message.getText().toString();

                    if(TextUtils.isEmpty(nameString))
                    {
                        name.setError("Please Enter the Name");
                    }
                    else if(TextUtils.isEmpty(emailString))
                    {
                        email.setError("Please Enter the Email");

                    }
                    else if(TextUtils.isEmpty(mobileString))
                    {
                        mobileNo.setError("Please Enter the Mobile No");

                    }

                    else if(TextUtils.isEmpty(partNoString))
                    {
                        partNo.setError("Please Enter the Part No");

                    }
                    else if(TextUtils.isEmpty(msgString))
                    {
                        message.setError("Please Enter the Message");

                    }
                    else
                    {
                        sendFeedBack(partNoString,nameString,emailString,mobileString,msgString,contactTypeString,cityString,companyString);
                    }
                }
                else
                {
                    alert.showAlertboxnegative("Please check your network connection and try again!");
                }
            }
        });

        checkInternet();

    }

    private void checkInternet()
    {
        NetworkConnection connection = new NetworkConnection(FeedBackForm.this);
        if(connection.CheckInternet())
        {
            getPartNumber();

        }else {
            Toast.makeText(getApplicationContext(),"Please check your network connection and try again!",Toast.LENGTH_SHORT).show();
//            alert.showAlertboxnegative("Please check your Network connection and try again!");

        }
    }

    private void getPartNumber() {
        final ProgressDialog progressDialog = new ProgressDialog(FeedBackForm.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIService service= RetroClient.getApiService();
        Call<GetPartNumber> call=service.getpartnumber();
        call.enqueue(new Callback<GetPartNumber>() {
            @Override
            public void onResponse(Call<GetPartNumber> call, Response<GetPartNumber> response) {
                try{
                    progressDialog.dismiss();
                    if(response.body().getResult().equals("Success"))
                    {
                        partNo.setAdapter(new ArrayAdapter<String>(FeedBackForm.this,R.layout.support_simple_spinner_dropdown_item,response.body().getData()));
                        partNo.setThreshold(1);
                    }
                    else {
//                        alert.showAlertboxnegative("No parts found. Please try again later");
                        Toast.makeText(FeedBackForm.this, "No parts found. Please try again later", Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e)
                {
                    e.printStackTrace();
//                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                    Toast.makeText(FeedBackForm.this, "Something went wrong . Please try again later .", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<GetPartNumber> call, Throwable t) {
                progressDialog.dismiss();
//                alert.showAlertboxnegative("Something went wrong . Please try again later .");
            }
        });
    }
    private void sendFeedBack(String partNoString, String nameString, String emailString, String mobileString, String msgString, String contactTypeString, String cityString, String companyString)
    {
        final ProgressDialog progressDialog = new ProgressDialog(FeedBackForm.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIService service= RetroClient.getApiService();

        Call<FeedBackModel> call=service.sendFeedBack(partNoString,nameString,emailString,mobileString, mobileString,contactTypeString,cityString,companyString);
        try {
            call.enqueue(new Callback<FeedBackModel>() {
                @Override
                public void onResponse(Call<FeedBackModel> call, Response<FeedBackModel> response) {
                    progressDialog.dismiss();
                    try{

                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success")) {
                                    alert.showAlertboxnegative("Thank you for your Feedback. Your Feedback Submitted Successfully");
                            }
                            else
                            {
                                alert.showAlertbox("Invalid Response. Please try again Later");
                            }

                        }else {
                            alert.showAlertbox("Something went wrong . Please try again later .");
                        }

                    }catch (Exception e)
                    {
                        e.printStackTrace();
                        alert.showAlertbox("Bad Response. Please contact Admin");
                    }

                }

                @Override
                public void onFailure(Call<FeedBackModel> call, Throwable t) {
                    alert.showAlertbox("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
            alert.showAlertbox("Something went wrong . Please try again later .");

        }
    }


}
