package com.brainmagic.delphitvs;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.regionalmanager.RegionalMainmenu;
import com.brainmagic.delphitvs.salesactivities.SalesMainmenu;

import java.util.List;

import adapter.FiltersSegmentAdpater;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.filters.oemmodel.OEModelList;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FiltersAllmakeproducts extends AppCompatActivity {
    private LinearLayout layout_tractor,layout_passengervehicle,layout_commercialvehicle;
    private ImageView img_back,img_home;
    private Alert box;
    private ListView subCategoryList;
    private static final String TAG = "FiltersAllmakeproducts";
    private List<String> filters;
    private String ALLMAKEFILTERS="ALL MAKE FILTER";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filters_allmakeproducts);
        box=new Alert(this);
        img_back =(ImageView)findViewById(R.id.back);
//        img_home =(ImageView)findViewById(R.id.home);
        subCategoryList=findViewById(R.id.subcategory_segment_list);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


//        img_home.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(FiltersAllmakeproducts.this,MainActivity.class);
//                startActivity(i);
//            }
//        });
        layout_commercialvehicle = (LinearLayout)findViewById(R.id.layout_commercialvehicle);
        layout_passengervehicle = (LinearLayout)findViewById(R.id.layout_passengervehicle);
        layout_tractor = (LinearLayout)findViewById(R.id.layout_tractor);

        final ImageView img_menu = (ImageView)findViewById(R.id.menu);
        img_menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                img_menu.startAnimation(animation);
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(FiltersAllmakeproducts.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_home:
                                startActivity(new Intent(FiltersAllmakeproducts.this, MainActivity.class));
                                break;
                            case R.id.menu_aboutus:
                                startActivity(new Intent(FiltersAllmakeproducts.this, Aboutus.class));
                                break;
                            case R.id.menu_pricelist:
                                startActivity(new Intent(FiltersAllmakeproducts.this, PriceList.class));
                                break;
                            case R.id.menu_general:
                                startActivity(new Intent(FiltersAllmakeproducts.this, General.class));
                                break;
                            case R.id.menu_productcatalogue:
                                startActivity(new Intent(FiltersAllmakeproducts.this, ProductCatalogue.class));
                                break;

                            case R.id.menu_vehiclesegment:
                                startActivity(new Intent(FiltersAllmakeproducts.this, VehicleSegment.class));
                                break;

                            case R.id.menu_login:
                                SharedPreferences preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
                                if(preferences.getBoolean("salesLogin",false)) {
                                    if(preferences.getString("userType","").equals("SalesExecutive"))
                                    {
                                        Intent i = new Intent(FiltersAllmakeproducts.this, SalesMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                    else {
                                        Intent i = new Intent(FiltersAllmakeproducts.this, RegionalMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                }
                                else {
                                    Intent i = new Intent(FiltersAllmakeproducts.this, SalesLogin.class);
                                    startActivity(i);
                                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                }

                                break;

                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.aboutusmenu);
                pop.show();
            }
        });
        checkInternet();


        
        subCategoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                String item= (String) parent.getAdapter().getItem(position);
                if (filters!=null)
                {
                    String item=filters.get(position);
                    Log.d(TAG, "onItemClick: "+item);
                    Intent intent=new Intent(FiltersAllmakeproducts.this,SubCatagoryFilter.class);
                    intent.putExtra("Category","Sub");
                    intent.putExtra("Type","Filters");
                    if(item.equals("All"))
                    {
                        intent.putExtra("Segment","");
                        intent.putExtra("SubCategory","");
                    }
                    else if(item.equals(ALLMAKEFILTERS))
                    {
                        intent.putExtra("Segment","");
                        intent.putExtra("SubCategory","All Make Filter");
                    }
                    else {
                        intent.putExtra("Segment",item);
                        intent.putExtra("SubCategory","");
                    }
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.left_in,R.anim.left_out);
    }

    private void checkInternet(){
        NetworkConnection connection=new NetworkConnection(this);
        if(connection.CheckInternet())
        {
            callApi();
        }
        else {
            box.showAlertboxnegative("Please check your network connection and try again!");
        }
    }

    private void callApi()
    {
        final ProgressDialog progressDialog = new ProgressDialog(FiltersAllmakeproducts.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIService service=RetroClient.getApiService();
        Call<OEModelList> call=service.oeList();
        call.enqueue(new Callback<OEModelList>() {
            @Override
            public void onResponse(Call<OEModelList> call, Response<OEModelList> response) {
                try{
                progressDialog.dismiss();
                    if(response.body().getResult().equals("Success"))
                    {
                        filters=response.body().getData();
                        filters.add("All");
                        FiltersSegmentAdpater adpater=new FiltersSegmentAdpater(FiltersAllmakeproducts.this,filters);
                        subCategoryList.setAdapter(adpater);
                    }
                    else {
                        box.showAlertboxnegative("No data found. Please try again later");
                    }
                }catch (Exception e)
                {
                    e.printStackTrace();
                    box.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            }
            @Override
            public void onFailure(Call<OEModelList> call, Throwable t) {
                progressDialog.dismiss();
                box.showAlertboxnegative("Something went wrong . Please try again later .");
            }
        });
    }
}
