package com.brainmagic.delphitvs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.regionalmanager.RegionalMainmenu;
import com.brainmagic.delphitvs.salesactivities.SalesMainmenu;

import adapter.ProductDetailsAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.filters.details.FilterDetail;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FiltersOemBasedList extends AppCompatActivity {

    private ImageView img_back,img_home;
    private NetworkConnection networkConnection;
    private TextView homeTitle,productTitle,filterTitle,oemTitle,modelTitle;
    private Alert alert;
    private String oem,model;
    private ListView modelListView;
    private ProductDetailsAdapter filtersSegmentAdpater;
    private EditText partNoSearch;
    private Button goButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filters_oem_based_list);
        homeTitle=findViewById(R.id.home_title);
        img_back=findViewById(R.id.back);
        productTitle=findViewById(R.id.product_title);
        filterTitle=findViewById(R.id.filter_title);
        oemTitle=findViewById(R.id.oem_title);
        modelTitle=findViewById(R.id.model_title);
        modelTitle=findViewById(R.id.model_title);
        modelListView=findViewById(R.id.listsubcatagory);
        goButton=findViewById(R.id.go);
        partNoSearch=findViewById(R.id.search_partnofilter);

        alert=new Alert(this);
        oem=getIntent().getStringExtra("oem");
        model=getIntent().getStringExtra("model");

        oemTitle.setText(oem);
        modelTitle.setText(model);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final ImageView img_menu = (ImageView)findViewById(R.id.menu);
        img_menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                img_menu.startAnimation(animation);
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(FiltersOemBasedList.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_home:
                                startActivity(new Intent(FiltersOemBasedList.this, MainActivity.class));
                                break;
                            case R.id.menu_aboutus:
                                startActivity(new Intent(FiltersOemBasedList.this, Aboutus.class));
                                break;
                            case R.id.menu_pricelist:
                                startActivity(new Intent(FiltersOemBasedList.this, PriceList.class));
                                break;
                            case R.id.menu_general:
                                startActivity(new Intent(FiltersOemBasedList.this, General.class));
                                break;
                            case R.id.menu_productcatalogue:
                                startActivity(new Intent(FiltersOemBasedList.this, ProductCatalogue.class));
                                break;

                            case R.id.menu_vehiclesegment:
                                startActivity(new Intent(FiltersOemBasedList.this, VehicleSegment.class));
                                break;

                            case R.id.menu_login:
                                SharedPreferences preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
                                if(preferences.getBoolean("salesLogin",false)) {
                                    if(preferences.getString("userType","").equals("SalesExecutive"))
                                    {
                                        Intent i = new Intent(FiltersOemBasedList.this, SalesMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                    else {
                                        Intent i = new Intent(FiltersOemBasedList.this, RegionalMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                }
                                else {
                                    Intent i = new Intent(FiltersOemBasedList.this, SalesLogin.class);
                                    startActivity(i);
                                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                }


                                break;

                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.aboutusmenu);
                pop.show();
            }
        });


        NetworkConnection connection=new NetworkConnection(this);
        if(connection.CheckInternet())
        {
            getList();
        }
        else {
            alert.showAlertbox("Please check your network connection and try again!");
        }

        goButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String partNo=partNoSearch.getText().toString();
                filtersSegmentAdpater.filter(partNo);
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        });


    }

    private void getList()
    {
            final ProgressDialog progressDialog = new ProgressDialog(FiltersOemBasedList.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            APIService service = RetroClient.getApiService();
            Call<FilterDetail> call=service.filtereOeBasedList(oem,model);
            call.enqueue(new Callback<FilterDetail>() {
                @Override
                public void onResponse(Call<FilterDetail> call, Response<FilterDetail> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.body().getResult().equals("Success")){
                            filtersSegmentAdpater=new ProductDetailsAdapter(FiltersOemBasedList.this,response.body().getData());
                            modelListView.setAdapter(filtersSegmentAdpater);
                        }
                        else {
                            alert.showAlertboxnegative("No data found please try again later");
                        }

                    }catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        alert.showAlertboxnegative("Execption erroe please try again later");
                    }
                }

                @Override
                public void onFailure(Call<FilterDetail> call, Throwable t) {
                    progressDialog.dismiss();
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });
        }
}
