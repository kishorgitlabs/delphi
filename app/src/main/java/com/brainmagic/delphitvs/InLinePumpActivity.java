package com.brainmagic.delphitvs;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;

import android.os.Bundle;

import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.regionalmanager.RegionalMainmenu;
import com.brainmagic.delphitvs.salesactivities.SalesMainmenu;

import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.rotaryfiepdf.PDFDownload;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InLinePumpActivity extends AppCompatActivity {
    private ImageView img_back,img_menu;
    LinearLayout mAndMPump,mAndMGovernor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_line_pump);
        img_back =(ImageView)findViewById(R.id.back);
        img_menu =(ImageView)findViewById(R.id.menu);
        mAndMPump =findViewById(R.id.m_and_m_pump);
        mAndMGovernor =findViewById(R.id.m_and_m_governor);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });

        img_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                img_menu.startAnimation(animation);
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(InLinePumpActivity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_home:
                                startActivity(new Intent(InLinePumpActivity.this, MainActivity.class));
                                break;
                            case R.id.menu_aboutus:
                                startActivity(new Intent(InLinePumpActivity.this, Aboutus.class));
                                break;
                            case R.id.menu_pricelist:
                                startActivity(new Intent(InLinePumpActivity.this, PriceList.class));
                                break;
                            case R.id.menu_general:
                                startActivity(new Intent(InLinePumpActivity.this, General.class));
                                break;
                            case R.id.menu_productcatalogue:
                                startActivity(new Intent(InLinePumpActivity.this, ProductCatalogue.class));
                                break;

                            case R.id.menu_vehiclesegment:
                                startActivity(new Intent(InLinePumpActivity.this, VehicleSegment.class));
                                break;

                            case R.id.menu_login:
                                SharedPreferences preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
                                if(preferences.getBoolean("salesLogin",false)) {
                                    if(preferences.getString("userType","").equals("SalesExecutive"))
                                    {
                                        Intent i = new Intent(InLinePumpActivity.this, SalesMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                    else {
                                        Intent i = new Intent(InLinePumpActivity.this, RegionalMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                }
                                else {
                                    Intent i = new Intent(InLinePumpActivity.this, SalesLogin.class);
                                    startActivity(i);
                                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                }

                                break;

                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.aboutusmenu);
                pop.show();
            }
        });

        mAndMPump.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInternet("M&M Pump");
            }
        });
        mAndMGovernor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInternet("M&M Governor");
            }
        });
    }

    private void checkInternet(String pdfName)
    {
        NetworkConnection connection=new NetworkConnection(InLinePumpActivity.this);
        if(connection.CheckInternet())
        {
            getPdf(pdfName);
        }
        else {
            Alert alert=new Alert(InLinePumpActivity.this);
            alert.showAlertboxnegative("No Internet Connection. Please swtich On your Network Connection");
        }
    }

    public void getPdf(String pdfName)
    {
        final ProgressDialog progressDialog = new ProgressDialog(InLinePumpActivity.this);
        try{
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            APIService service = RetroClient.getApiService();
            Call<PDFDownload> call=service.getInlinePdf(pdfName);

            call.enqueue(new Callback<PDFDownload>() {
                @Override
                public void onResponse(Call<PDFDownload> call, Response<PDFDownload> response) {
                    progressDialog.dismiss();
                    try {
                        if(response.body().getResult().equals("Success")){

                            if(!response.body().getData().equals("Not Success")){
                                Intent searchAddress = new Intent(Intent.ACTION_VIEW, Uri.parse(response.body().getData()));
                                if(searchAddress!=null)
                                    startActivity(searchAddress);
                            }
                        }
                        else {
                            Alert alert=new Alert(InLinePumpActivity.this);
                            alert.showAlertboxnegative("File not found");
                        }
                    }catch (ActivityNotFoundException e)
                    {
                        Alert alert=new Alert(InLinePumpActivity.this);
                        alert.showAlertboxnegative("Something went wrong . Please try again later .");
                    }
                    catch (Exception e)
                    {
                        Alert alert=new Alert(InLinePumpActivity.this);
                        alert.showAlertboxnegative("Something went wrong . Please try again later .");
                    }
                }


                @Override
                public void onFailure(Call<PDFDownload> call, Throwable t) {
                    progressDialog.dismiss();
                    Alert alert=new Alert(InLinePumpActivity.this);
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            Alert alert=new Alert(InLinePumpActivity.this);
            alert.showAlertboxnegative("Something went wrong . Please try again later .");
        }
    }


}
