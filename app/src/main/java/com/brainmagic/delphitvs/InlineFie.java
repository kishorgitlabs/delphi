package com.brainmagic.delphitvs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;


import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.regionalmanager.RegionalMainmenu;
import com.brainmagic.delphitvs.salesactivities.SalesMainmenu;

import java.util.List;

import adapter.CommonRailProductsAdapter;
import model.api.APIService;
import model.api.RetroClient;
import model.filters.details.FilterDetailList;
import adapter.sales.CommonExpandableListViewInline;
import network.NetworkConnection;
import model.changepassword.CommonExpanDInlineFieSystem;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toaster.Toasts;

public class InlineFie extends AppCompatActivity {
    private ImageView img_home,img_back,img_menu;
    private TextView textView_one,textView_two;
    private String main_cat,sub_cat;
    private NetworkConnection networkConnection;
    private Toasts toasts;
    private ListView listView;
    private ExpandableListView listViews;
    private List<FilterDetailList> Data1;
    private CommonRailProductsAdapter productDetailsAdapter;
    private Animation animation;
    private AutoCompleteTextView searchedData;
    private CommonExpandableListViewInline expandableListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inline_category);

        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bounce);
        img_back = (ImageView)findViewById(R.id.back);
//        img_home = (ImageView)findViewById(R.id.home);
        img_menu = (ImageView)findViewById(R.id.menu);
        searchedData =findViewById(R.id.search_partno);
        textView_one = (TextView)findViewById(R.id.textView2);
//        textView_two = (TextView)findViewById(R.id.textView3);
        main_cat = getIntent().getStringExtra("inline_main");
        sub_cat = getIntent().getStringExtra("inline_sub");
        networkConnection = new NetworkConnection(this);
        toasts = new Toasts(this);
//        listView = (ListView) findViewById(R.id.pump_details_list);
        listViews = findViewById(R.id.expandablelistview);
//        textView_two.setText(sub_cat);
        textView_one.setText(main_cat);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });

//        img_home.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i = new Intent(CommonSubCategoryPump.this,MainActivity.class);
//                startActivity(i);
//                overridePendingTransition(R.anim.left_in,R.anim.left_out);
//            }
//        });
//
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
//            }
//            });
//
        img_menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                img_menu.startAnimation(animation);
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(InlineFie.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_home:
                                startActivity(new Intent(InlineFie.this, MainActivity.class));
                                break;
                            case R.id.menu_aboutus:
                                startActivity(new Intent(InlineFie.this, Aboutus.class));
                                break;
                            case R.id.menu_pricelist:
                                startActivity(new Intent(InlineFie.this, PriceList.class));
                                break;
                            case R.id.menu_general:
                                startActivity(new Intent(InlineFie.this, General.class));
                                break;
                            case R.id.menu_productcatalogue:
                                startActivity(new Intent(InlineFie.this, ProductCatalogue.class));
                                break;

                            case R.id.menu_vehiclesegment:
                                startActivity(new Intent(InlineFie.this, VehicleSegment.class));
                                break;

                            case R.id.menu_login:
                                SharedPreferences preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
                                if(preferences.getBoolean("salesLogin",false)) {
                                    if(preferences.getString("userType","").equals("SalesExecutive"))
                                    {
                                        Intent i = new Intent(InlineFie.this, SalesMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                    else {
                                        Intent i = new Intent(InlineFie.this, RegionalMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                }
                                else {
                                    Intent i = new Intent(InlineFie.this, SalesLogin.class);
                                    startActivity(i);
                                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                }


                                break;


                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.aboutusmenu);
                pop.show();
            }
        });

//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                startActivity(new Intent(CommonSubCategoryPump.this,CommonRailPumpMoreView.class)
//                        .putExtra("main_cat",main_cat)
//                        .putExtra("sub_cat",sub_cat)
//                        .putExtra("partNo", Data1.get(position).getPartNumber())
//                        .putExtra("app", Data1.get(position).getVehicleApplication())
//                );
//            }
//        });
        CheckInternet();
    }

    private void CheckInternet() {
        if(networkConnection.CheckInternet()){
            ValidatePart();
        }else {
            Toast.makeText(getApplicationContext(),"Please check your network connection and try again!",Toast.LENGTH_SHORT).show();
        }
    }

    private void ValidatePart() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(InlineFie.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            APIService service = RetroClient.getApiService();
            Call<CommonExpanDInlineFieSystem> call = service.commonExpandableLists(main_cat,sub_cat);
            call.enqueue(new Callback<CommonExpanDInlineFieSystem>() {
                @Override
                public void onResponse(Call<CommonExpanDInlineFieSystem> call, Response<CommonExpanDInlineFieSystem> response) {
                    if (response.body().getResult().equals("success")) {
                        progressDialog.dismiss();
//                        Data1=  response.body().getData();
//                        productDetailsAdapter=new ProductDetailsAdapter(CommonSubCategoryPump.this,Data1);
//                        productDetailsAdapter=new CommonRailProductsAdapter(CommonSubCategoryPump.this,Data1);
//                        listView.setAdapter(productDetailsAdapter);

                        expandableListView=new CommonExpandableListViewInline(InlineFie.this,response.body().getData(),main_cat,sub_cat);
                        listViews.setAdapter(expandableListView);

                    } else {
                        // alert.showAlertbox("No Rejecte-Rating is Found");
                        toasts.ShowErrorToast("No FeedBack is Found ");
                        // Toast.makeText(getApplicationContext(), "No Rejected-Rating is Found", Toast.LENGTH_LONG).show();
                        onBackPressed();
                        progressDialog.dismiss();
                    }
                }
                @Override
                public void onFailure(Call<CommonExpanDInlineFieSystem> call, Throwable t) {
                    toasts.ShowErrorToast("Something went wrong . Please try again later .");
                    // Toast.makeText(getApplicationContext(), "No RajectRating is Found", Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
            toasts.ShowErrorToast("Something went wrong . Please try again later .");
        }
    }

    public void checkKeyWords(View view)
    {
        String dataSearhed=searchedData.getText().toString();
        expandableListView.filter(dataSearhed);
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}
