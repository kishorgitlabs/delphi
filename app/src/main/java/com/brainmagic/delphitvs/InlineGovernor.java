package com.brainmagic.delphitvs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;


import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.regionalmanager.RegionalMainmenu;
import com.brainmagic.delphitvs.salesactivities.SalesMainmenu;

import java.util.ArrayList;

import adapter.Getter;
import adapter.InLineGovernorAdapter;
import adapter.InLineInjectorAdapter;
import alert.Alert;
import model.InlineFie.GovernorExplodedView;
import model.InlineFie.InjectorExplodedView;
import model.InlineFie.InjectorPojo;
import model.InlineFie.InlineGovernorPojo;
import model.api.APIService;
import model.api.RetroClient;
import model.rotaryfiepdf.PDFDownload;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toaster.Toasts;

public class InlineGovernor extends AppCompatActivity {

    private ImageView img_home,img_back,img_menu;
    private TextView textview1,textview2,textview3,textview4,pageTitle;
    private String main_cat,sub_cat,partNo;
    private NetworkConnection networkConnection;
    private Toasts toasts;
    private ListView listview;
    private Animation animation;
    private Alert showAlert;
    EditText searchedItems;
    private Button searchData;
//    private EditText search_partno;
    private Button search_rotary_service_items;
    private InLineGovernorAdapter adapter;
    ArrayList<Getter> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_rail_injector);

        searchedItems=(EditText)findViewById(R.id.search_edit_text);
        listview = (ListView) findViewById(R.id.listview);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bounce);
        search_rotary_service_items = (Button) findViewById(R.id.search_rotary_service_items);
        img_back = (ImageView) findViewById(R.id.back);
        img_menu = (ImageView) findViewById(R.id.menu);
        textview1 = (TextView) findViewById(R.id.textview1);
        textview2 = (TextView) findViewById(R.id.textview2);
        textview3 = (TextView) findViewById(R.id.textview3);
        textview4 = (TextView) findViewById(R.id.textview4);



        searchData = findViewById(R.id.search_rotary_service_items);
        main_cat = getIntent().getStringExtra("inline_governor_main");
        sub_cat = getIntent().getStringExtra("inline_governor_sub");
        partNo = getIntent().getStringExtra("governor_partNoo");

        networkConnection = new NetworkConnection(this);
        toasts = new Toasts(this);
        showAlert = new Alert(this);


        String main = getIntent().getStringExtra("inline_governor_main");
        String sub = getIntent().getStringExtra("inline_governor_sub");
        String service = getIntent().getStringExtra("governor_service");
        String parts = getIntent().getStringExtra("governor_partNoo");




        textview1.setText(main);
        textview2.setText(sub);
        textview3.setText(service);
        textview4.setText(parts);

        searchData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchedData=searchedItems.getText().toString();
                adapter.filter(searchedData);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
//                InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
//                inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

            }
        });


        search_rotary_service_items.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String searchedData=searchedItems.getText().toString();
                adapter.filter(searchedData);

                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

            }
        });


        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });



        img_menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                img_menu.startAnimation(animation);
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(InlineGovernor.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_home:
                                startActivity(new Intent(InlineGovernor.this, MainActivity.class));
                                break;
                            case R.id.menu_aboutus:
                                startActivity(new Intent(InlineGovernor.this, Aboutus.class));
                                break;
                            case R.id.menu_pricelist:
                                startActivity(new Intent(InlineGovernor.this, PriceList.class));
                                break;
                            case R.id.menu_general:
                                startActivity(new Intent(InlineGovernor.this, General.class));
                                break;
                            case R.id.menu_productcatalogue:
                                startActivity(new Intent(InlineGovernor.this, ProductCatalogue.class));
                                break;

                            case R.id.menu_vehiclesegment:
                                startActivity(new Intent(InlineGovernor.this, VehicleSegment.class));
                                break;

                            case R.id.menu_login:
                                SharedPreferences preferences = getSharedPreferences("Delphi", MODE_PRIVATE);
                                if (preferences.getBoolean("salesLogin", false)) {
                                    if (preferences.getString("userType", "").equals("SalesExecutive")) {
                                        Intent i = new Intent(InlineGovernor.this, SalesMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    } else {
                                        Intent i = new Intent(InlineGovernor.this, RegionalMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                } else {
                                    Intent i = new Intent(InlineGovernor.this, SalesLogin.class);
                                    startActivity(i);
                                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                }


                                break;


                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.aboutusmenu);
                pop.show();


            }
        });

        checkInternet();

    }

    private void checkInternet() {
        if(networkConnection.CheckInternet()){
            validatePart();
        }else {
            showAlert.showAlertboxnegative("Please check your network connection and try again!");
//            Toast.makeText(getApplicationContext(),"Please check your network connection and try again!",Toast.LENGTH_SHORT).show();
        }
    }

    private void validatePart() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(InlineGovernor.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            APIService service = RetroClient.getApiService();
            Call<InlineGovernorPojo> call = service.injectorservicelists(partNo);
            call.enqueue(new Callback<InlineGovernorPojo>() {
                @Override
                public void onResponse(Call<InlineGovernorPojo> call, Response<InlineGovernorPojo> response) {
                    if (response.body().getResult().equals("success")) {
                        progressDialog.dismiss();

                        adapter = new InLineGovernorAdapter(InlineGovernor.this, response.body().getData());
                        listview.setAdapter(adapter);

                    }
                    else {
                        // alert.showAlertbox("No Rejecte-Rating is Found");
                        showAlert.showAlertboxnegative("No data found please try again later");
//                        toasts.ShowErrorToast("No FeedBack is Found ");
                        // Toast.makeText(getApplicationContext(), "No Rejected-Rating is Found", Toast.LENGTH_LONG).show();
//                        onBackPressed();
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<InlineGovernorPojo> call, Throwable t) {
//                    toasts.ShowErrorToast("Something went wrong . Please try again later .");
                    showAlert.showAlertboxnegative("Something went wrong . Please try again later .");
                    // Toast.makeText(getApplicationContext(), "No RajectRating is Found", Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
//            toasts.ShowErrorToast("Something went wrong . Please try again later .");
            showAlert.showAlertboxnegative("Something went wrong . Please try again later .");
        }
    }

    public void explodedView(View view)
    {
        final ProgressDialog progressDialog = new ProgressDialog(InlineGovernor.this);
        try{
             String injector=getIntent().getStringExtra("governor_partNoo");

            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();


            APIService service=RetroClient.getApiService();
            Call<GovernorExplodedView> call=service.injectorexplodedViews(injector);
            call.enqueue(new Callback<GovernorExplodedView>() {
                @Override
                public void onResponse(Call<GovernorExplodedView> call, Response<GovernorExplodedView> response) {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            if(response.body().getResult().equals("success"))
                            {

                                Intent searchAddress = new Intent(Intent.ACTION_VIEW, Uri.parse(response.body().getData()));
                                progressDialog.dismiss();
                                if(searchAddress!=null) {
                                    startActivity(searchAddress);
                                }
                            }
                            else {
                                showAlert.showAlertboxnegative("No App is found to open this Link");
                            }
                        }catch (Exception e)
                        {
                            e.printStackTrace();
                            showAlert.showAlertboxnegative("Something went wrong . Please try again later .");
                        }
                    }
                    else {
                        showAlert.showAlertboxnegative("Failed to open the Link. Please try again later");

                    }
                }

                @Override
                public void onFailure(Call<GovernorExplodedView> call, Throwable t) {
                    showAlert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });



        }
        catch (Exception e)
        {
            progressDialog.dismiss();
            showAlert.showAlertboxnegative("Something went wrong . Please try again later .");
        }
    }

    public void downloadPDF(View view)
    {
        final ProgressDialog progressDialog = new ProgressDialog(InlineGovernor.this);
        try{
            String mainparts = getIntent().getStringExtra("governor_mainpartno");
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            APIService service = RetroClient.getApiService();
            Call<PDFDownload> call=service.pdfDownload(mainparts);

            call.enqueue(new Callback<PDFDownload>() {
                @Override
                public void onResponse(Call<PDFDownload> call, Response<PDFDownload> response) {
                    progressDialog.dismiss();
                    try {
                        if(response.body().getResult().equals("Success")){

                            if(!response.body().getData().equals("Not Success")){
                                Intent searchAddress = new Intent(Intent.ACTION_VIEW, Uri.parse(response.body().getData()));
                                if(searchAddress!=null)
                                    startActivity(searchAddress);
                            }
                        }
                        else {
                            showAlert.showAlertboxnegative("File not found");
                        }
                    }catch (ActivityNotFoundException e)
                    {
                        showAlert.showAlertboxnegative("Something went wrong . Please try again later .");
                    }
                    catch (Exception e)
                    {
                        showAlert.showAlertboxnegative("Something went wrong . Please try again later .");
                    }
                }


                @Override
                public void onFailure(Call<PDFDownload> call, Throwable t) {
                    progressDialog.dismiss();
                    showAlert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            showAlert.showAlertboxnegative("Something went wrong . Please try again later .");
        }
    }
}
