package com.brainmagic.delphitvs;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.fragment.NetworkBottomSheetNavigation;
import com.brainmagic.delphitvs.regionalmanager.RegionalMainmenu;
import com.brainmagic.delphitvs.salesactivities.SalesMainmenu;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.distributorcitylist.DistributorCityList;
import model.distributorlist.DistributorList;
import model.distributorregion.DistributorRegionList;
import model.network.dealer.dealerdetails.DealerList;
import model.network.dealer.dealerstatecity.DealerModel;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements NetworkBottomSheetNavigation.BottomSheetListener {
    private LinearLayout cardView_aboutus,cardView_productcatalogue,cardView_partnumbersearch,cardView_login,cardView_general,cardView_vehiclesegment;
    private   AutoCompleteTextView autoCompleteTextView;
    private ArrayList<String>partnumberlist;
    private ImageView bottomimg;
    private String  AccName;
    private ImageView img_Settings;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private TextView txt_cutomername;
    private Animation animation,animation1,animation2;
    private LinearLayout linearLayout_anim;
    private Alert alert;
    private NetworkBottomSheetNavigation bottomSheetNavigation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        preferences= getSharedPreferences("Delphi", MODE_PRIVATE);
        editor=preferences.edit();
        alert=new Alert(this);
        bottomimg = (ImageView)findViewById(R.id.stip);
        img_Settings = (ImageView)findViewById(R.id.settings);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bounce);
        animation1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.right_in);
        animation2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.left_in);
        cardView_aboutus = (LinearLayout) findViewById(R.id.cardview_aboutus);
        cardView_login = (LinearLayout) findViewById(R.id.cardview_login);

    //    txt_cutomername = (TextView)findViewById(R.id.customername);
    // txt_cutomername.setText("Welcome,"+AccName);
        cardView_partnumbersearch = (LinearLayout) findViewById(R.id.cardview_partnumbersearch);
        cardView_productcatalogue = (LinearLayout) findViewById(R.id.cardview_productcatalogue);
        linearLayout_anim = (LinearLayout)findViewById(R.id.carviewanim);
        //linearLayout_anim.startAnimation(animation);
        cardView_general = (LinearLayout) findViewById(R.id.cardview_general);
        cardView_vehiclesegment = (LinearLayout) findViewById(R.id.cardview_vehiclesegment);
        cardView_partnumbersearch.startAnimation(animation2);
        cardView_productcatalogue.startAnimation(animation1);
        cardView_aboutus.startAnimation(animation2);
        cardView_login.startAnimation(animation1);
        cardView_general.startAnimation(animation1);
        cardView_vehiclesegment.startAnimation(animation2);

        cardView_aboutus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,Aboutus.class);
                startActivity(i);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });

        cardView_productcatalogue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,ProductCatalogue.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }
        });

        cardView_partnumbersearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ShowAlertbox();
                startActivity(new Intent(MainActivity.this,PriceList.class));
            }
        });

        cardView_general.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,General.class);
                startActivity(i);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });

        cardView_vehiclesegment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                bottomSheetNavigation = new NetworkBottomSheetNavigation();
                bottomSheetNavigation.show(getSupportFragmentManager(), "exampleBottomSheet-");
                /*Intent i = new Intent(MainActivity.this,VehicleSegment.class);
                startActivity(i);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);*/
//                Toasts toasts = new Toasts(MainActivity.this);
//                toasts.ShowErrorToast("Under Production");

//                final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(
//                        MainActivity.this).create();
//                LayoutInflater inflater = (MainActivity.this).getLayoutInflater();
//                View dialog = inflater.inflate(R.layout.network_alert, null);
//                alertDialog.setView(dialog);
//                LinearLayout dealer = (LinearLayout) dialog.findViewById(R.id.servicedeler);
//                LinearLayout distibuter = (LinearLayout) dialog.findViewById(R.id.didtibuter);
//
//                dealer.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        try {
//                            Intent i = new Intent(MainActivity.this, Dealer.class);
//                            startActivity(i);
//                            alertDialog.dismiss();
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
//                            alertDialog.dismiss();
//                        }
//                    }
//                });
//                distibuter.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        try {
//                            Intent i = new Intent(MainActivity.this, Distributer.class);
//                            startActivity(i);
//                            alertDialog.dismiss();
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
//                            alertDialog.dismiss();
//
//                        }
//                    }
//                });
//                alertDialog.show();
            }
        });

        cardView_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(MainActivity.this, "This Page is Under Development", Toast.LENGTH_SHORT).show();
                if(preferences.getBoolean("salesLogin",false)) {
                    if(preferences.getString("userType","").equals("SalesExecutive"))
                    {
                        Intent i = new Intent(MainActivity.this, SalesMainmenu.class);
                        startActivity(i);
                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                    }
                    else {
                        Intent i = new Intent(MainActivity.this, RegionalMainmenu.class);
                        startActivity(i);
                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                    }
                }
                else {
                    Intent i = new Intent(MainActivity.this, SalesLogin.class);
                    startActivity(i);
                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                }
            }
        });


        final ImageView img_menu = (ImageView)findViewById(R.id.menu);
        img_menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                img_menu.startAnimation(animation);
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(MainActivity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_aboutus:
                                startActivity(new Intent(MainActivity.this, Aboutus.class));
                                break;
                            case R.id.menu_general:
                                startActivity(new Intent(MainActivity.this, General.class));
                                break;
                                case R.id.menu_productcatalogue:
                                startActivity(new Intent(MainActivity.this, ProductCatalogue.class));
                                break;

                                case R.id.menu_vehiclesegment:
                                startActivity(new Intent(MainActivity.this, VehicleSegment.class));
                                break;

                                case R.id.menu_login:
                                startActivity(new Intent(MainActivity.this, SalesLogin.class));
                                break;

                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.mainactivitymenu);
                pop.show();
            }
        });
    }

    private void ShowAlertbox() {
        final AlertDialog alertDialogbox = new AlertDialog.Builder(
                MainActivity.this).create();
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertpartnumberserach, null);
        alertDialogbox.getWindow().getAttributes().windowAnimations=R.style.DialogAnimations;
        alertDialogbox.setCanceledOnTouchOutside(true);
        alertDialogbox.setView(dialogView);
       // new getPartnumber().execute();
        Button search = (Button) dialogView.findViewById(R.id.search);
        autoCompleteTextView = (AutoCompleteTextView) dialogView.findViewById(R.id.autocompletetext);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =new Intent(MainActivity.this,ProductDetails.class).putExtra("partnumber",autoCompleteTextView.getText().toString());
                startActivity(i);
                alertDialogbox.dismiss();
                overridePendingTransition(R.anim.right_in, R.anim.right_out);
            }
        });


        alertDialogbox.show();

    }
/*
    private class getPartnumber extends AsyncTask<String,Void,String> {
        @Override
        protected void onPreExecute () {
            super.onPreExecute();
            partnumberlist = new ArrayList<>();
        }
        @SuppressLint("LongLogTag")
        @Override
        protected String doInBackground (String...strings){
            try {
                dbHelper = new DBHelper(HomePageActivity.this);
                db = dbHelper.readDataBase();

                String selectQuery = "select * From Partnumber_details";

                c = db.rawQuery(selectQuery, null);
                Log.e(TAG, "doInBackground: top");
                if (c.moveToFirst()) {
                    do {


                        partnumberlist.add(c.getString(c.getColumnIndex("part_number")));




                    }
                    while (c.moveToNext());
                }
                c.close();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "doInBackground: \"error\"");
                return "Error";
            }
            return "sucess";

        }
        @Override
        protected void onPostExecute (String s){
            super.onPostExecute(s);

            if (s.equals("sucess")) {

                autoCompleteTextView.setAdapter(new ArrayAdapter<String>(HomePageActivity.this, android.R.layout.simple_dropdown_item_1line, partnumberlist));
                autoCompleteTextView.setThreshold(1);

            }
        }
    }
*/

    @Override
    public void onBackPressed(){

        moveTaskToBack(true);
}

    @Override
    public void bottomSheetListener(String orderType) {
        bottomSheetNavigation.dismiss();
        if(orderType.equals("Dealer"))
        {
//            Intent i = new Intent(MainActivity.this, Dealer.class);
//                            startActivity(i);

//            getDealerDistributor(new ArrayList<String>());
            NetworkConnection connection=new NetworkConnection(MainActivity.this);
            if(connection.CheckInternet())
                getDealerStateList("Dealer");
            else
            {
                alert.showAlertbox("Please check your network connection and try again!");
            }

        }
        else if(orderType.equals("Distributor")){
//            Intent i = new Intent(MainActivity.this, Distributer.class);
//                            startActivity(i);
            NetworkConnection connection=new NetworkConnection(MainActivity.this);
            if(connection.CheckInternet())
                getDistributorRegion("Distributor");
            else
            {
                alert.showAlertbox("Please check your network connection and try again!");
            }
        }
        else {
            NetworkConnection connection=new NetworkConnection(MainActivity.this);
            if(connection.CheckInternet())
                getDealerStateList("Delphi Contacts");
            else
            {
                alert.showAlertbox("Please check your network connection and try again!");
            }
        }
    }

    private void getDistributorRegion(final String distributor) {
        final ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIService service= RetroClient.getApiService();
        Call<DistributorRegionList> call=service.getDistributorRegion();

        try {
            call.enqueue(new Callback<DistributorRegionList>() {
                @Override
                public void onResponse(Call<DistributorRegionList> call, Response<DistributorRegionList> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
                                getDealerDistributor(distributor,response.body().getData());
                            }
                            else {
                                alert.showAlertbox("No FeedBack Found. Please try again later");

                            }
                        }
                        else {
                            alert.showAlertbox("Bad Response. Please try again later");
                        }

                    }catch ( Exception e)
                    {
                        e.printStackTrace();
                        alert.showAlertbox("Something went wrong . Please try again later .");
                    }

                }

                @Override
                public void onFailure(Call<DistributorRegionList> call, Throwable t) {
                    progressDialog.dismiss();
                    alert.showAlertbox("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
            progressDialog.dismiss();
            alert.showAlertbox("Something went wrong . Please try again later .");
        }
    }

    private void getDealerStateList(final String networkType){
        final ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIService service= RetroClient.getApiService();
        Call<DealerModel> call=service.getDealerStateList();

        try {
            call.enqueue(new Callback<DealerModel>() {
                @Override
                public void onResponse(Call<DealerModel> call, Response<DealerModel> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {

                                getDealerDistributor(networkType,response.body().getData());

                            }
                            else {
                                alert.showAlertbox("No FeedBack Found. Please try again later");

                            }
                        }
                        else {
                            alert.showAlertbox("Bad Response. Please try again later");
                        }

                    }catch ( Exception e)
                    {
                        e.printStackTrace();
                        alert.showAlertbox("Something went wrong . Please try again later .");
                    }

                }

                @Override
                public void onFailure(Call<DealerModel> call, Throwable t) {
                    progressDialog.dismiss();
                    alert.showAlertbox("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
            progressDialog.dismiss();
            alert.showAlertbox("Something went wrong . Please try again later .");
        }
    }


    private void getDealerDistributor(final String networkType,List<String> statelist)
    {

        final AlertDialog alertDialog;
        alertDialog = new android.app.AlertDialog.Builder(
                MainActivity.this).create();
        LayoutInflater inflater = (MainActivity.this).getLayoutInflater();
        final View dialog = inflater.inflate(R.layout.dealer_alert, null);
        alertDialog.setView(dialog);
        alertDialog.setCancelable(false);


        final MaterialSpinner statespin = (MaterialSpinner) dialog.findViewById(R.id.statespin);
        final MaterialSpinner cityspin = (MaterialSpinner) dialog.findViewById(R.id.cityspin);
        statespin.setBackground(getResources().getDrawable(R.drawable.autotextback));
        cityspin.setBackground(getResources().getDrawable(R.drawable.autotextback));


//        statelist.clear();
        Set<String> hs = new HashSet<>();
        hs.addAll(statelist);
        Collections.sort(statelist);
        if (networkType.equals("Distributor")){
            statelist.add(0,"SELECT REGION");
        }else {
            statelist.add(0,"SELECT STATE");
        }

        statespin.setItems(statelist);
        statespin.setPadding(30, 0, 0, 0);

        statespin.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                String state = statespin.getText().toString().trim();
                if(!state.equals("SELECT REGION")||!state.equals("SELECT STATE"))
                {
//                    citylist.clear();
//                    citylist = db.distributorsDAO().getDealerCityList(State);
//                    Set<String> hsc = new HashSet<>();
//                    hsc.addAll(citylist);
                    NetworkConnection connection=new NetworkConnection(MainActivity.this);
                    if(connection.CheckInternet())
                        if(networkType.equals("Dealer"))
                            getDealerCityList(state,cityspin);
                        else if(networkType.equals("Distributor"))
                        {
                            getDistributorCity(networkType,state,cityspin);
                        }
                        else
                        {
                            getDelphiDistributorCityList(networkType,state,cityspin);
                        }
                    else
                        {
                            alert.showAlertbox("Please check your network connection and try again! ");
                        }
                }
                else
                {
//                    citylist.add(0,"SELECT CITY");
//                    citylist.clear();
//                    citylist.add(0,"SELECT CITY");
//                    cityspin.setItems(citylist);
                    cityspin.setPadding(30, 0, 0, 0);
                    Toast.makeText(getApplicationContext(), "Select any one", Toast.LENGTH_LONG).show();

                }
            }
        });


        Button search = (Button) dialog.findViewById(R.id.search);
        Button cancel = (Button) dialog.findViewById(R.id.cancel);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String state = statespin.getText().toString().trim();
                String city = cityspin.getText().toString().trim();
//                d_state.setText(State);
//                d_city.setText(City);

//                if(!state.equals("SELECT STATE")&& !city.equals("SELECT CITY") )
                if((!state.equals("SELECT STATE")&& !city.equals("SELECT CITY")) && (!state.equals("SELECT REGION")&& !TextUtils.isEmpty(city)))
                {
                    alertDialog.dismiss();
//                    getcityfilter();
                    if(networkType.equals("Dealer"))
                        getDealerDetails(networkType,state,city);
                    else if(networkType.equals("Distributor")) {
                        getDelphiDistributorDetails(networkType,state,city);
                    }
                    else {
                        getDelphiDistributorDetails(networkType,state,city);
                    }
                }
                else {
//                    Toast.makeText(getApplicationContext(), "Select state and city", Toast.LENGTH_LONG).show();
                    String[] stateVal = state.split(" ");
                    if(stateVal.length>=1 && (!state.equals("SELECT STATE") && !state.equals("SELECT REGION")))
                    {
                        Toast.makeText(getApplicationContext(), "SELECT CITY", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), state, Toast.LENGTH_LONG).show();
                    }


                }



            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onBackPressed();
                alertDialog.dismiss();

            }
        });
        alertDialog.show();
    }

    private void getDelphiDistributorDetails(final String networkType, final String state, final String city)
    {
        final ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        try{
            APIService service=RetroClient.getApiService();
            Call<DistributorList> call=service.getDistributorList(state,city);
            call.enqueue(new Callback<DistributorList>() {
                @Override
                public void onResponse(Call<DistributorList> call, Response<DistributorList> response) {
                    progressDialog.dismiss();
                    try {
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
                                startActivity(new Intent(MainActivity.this,ViewDistributerDetails.class)
                                        .putExtra("NetworkData", (Serializable) response.body().getData())
                                        .putExtra("region",state)
                                        .putExtra("city",city)
                                        .putExtra("networkType",networkType)
                                );
                            }else {
                             alert.showAlertbox("No Record Found. Please try again later");
                            }
                        }
                        else {
                            alert.showAlertbox("Bad Response. Please try again later");
                        }

                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                        alert.showAlertbox("Something went wrong . Please try again later .");
                    }
                }
                @Override
                public void onFailure(Call<DistributorList> call, Throwable t) {
                    progressDialog.dismiss();
                    alert.showAlertbox("Something went wrong . Please try again later .");
                }
            });
        }
        catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
            alert.showAlertbox("Exception error. Please try again later");
        }

    }

    private void getDealerDetails(final String networkType, final String state, final String city)
    {
        final ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        try{
            APIService service=RetroClient.getApiService();
            Call<DealerList> call=service.getDealerList(state,city);
            call.enqueue(new Callback<DealerList>() {
                @Override
                public void onResponse(Call<DealerList> call, Response<DealerList> response) {
                    progressDialog.dismiss();
                    try {
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
                                startActivity(new Intent(MainActivity.this,NetworkDetails.class)
                                        .putExtra("NetworkData", (Serializable) response.body().getData())
                                        .putExtra("state",state)
                                        .putExtra("city",city)
                                        .putExtra("networkType",networkType)
                                );

                            }else {
                                alert.showAlertbox("No Record Found. Please try again later");
                            }
                        }
                        else {
                            alert.showAlertbox("Bad Response. Please try again later");
                        }

                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                        alert.showAlertbox("Something went wrong . Please try again later .");
                    }
                }

                @Override
                public void onFailure(Call<DealerList> call, Throwable t) {
                    progressDialog.dismiss();
                    alert.showAlertbox("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
            alert.showAlertbox("Exception error. Please try again later");
        }

    }

    private void getDelphiDistributorCityList(String networkType,String state,final MaterialSpinner citySpin)
    {
        final ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        try{
            APIService service=RetroClient.getApiService();
            Call<DealerModel> call=service.getDelphiDistributorCityList(state, networkType);
            call.enqueue(new Callback<DealerModel>() {
                @Override
                public void onResponse(Call<DealerModel> call, Response<DealerModel> response) {
                    progressDialog.dismiss();
                    try {

                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
                                List<String >cityList= response.body().getData();
                                Collections.sort(cityList);
                                cityList.add(0,"SELECT CITY");
                                citySpin.setItems(cityList);
                                citySpin.setPadding(30, 0, 0, 0);

                            }else {
//                                alert.showAlertbox("No Record Found. Please try again later");
                                Toast.makeText(getApplicationContext(),"Select Region and State ",Toast.LENGTH_LONG).show();
                            }
                        }
                        else {
                            alert.showAlertbox("Please try again later");
                        }

                    }
                    catch (Exception e)
                    {

                        e.printStackTrace();
                        alert.showAlertbox("Exception error. Please try again later");
                    }
                }

                @Override
                public void onFailure(Call<DealerModel> call, Throwable t) {
                    progressDialog.dismiss();
                    alert.showAlertbox("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
            alert.showAlertbox("Exception error. Please try again later");
        }

    }

    private  void getDistributorCity(String networkType,String region,final MaterialSpinner citySpin)
    {
        final ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        try{
            APIService service=RetroClient.getApiService();
            Call<DistributorCityList> call=service.getDistributorList(region);
            call.enqueue(new Callback<DistributorCityList>() {
                @Override
                public void onResponse(Call<DistributorCityList> call, Response<DistributorCityList> response) {
                    progressDialog.dismiss();
                    try {

                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
                                List<String >cityList= response.body().getData();
                                Collections.sort(cityList);
                                cityList.add(0,"SELECT CITY");
                                citySpin.setItems(cityList);
                                citySpin.setPadding(30, 0, 0, 0);

                            }else {
                                alert.showAlertbox("No Record Found. Please try again later");
                            }
                        }
                        else {
                            alert.showAlertbox("Please try again later");
                        }

                    }
                    catch (Exception e)
                    {

                        e.printStackTrace();
                        alert.showAlertbox("Exception error. Please try again later");
                    }
                }

                @Override
                public void onFailure(Call<DistributorCityList> call, Throwable t) {
                    progressDialog.dismiss();
                    alert.showAlertbox("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
            alert.showAlertbox("Exception error. Please try again later");
        }

    }


    private void getDealerCityList(String state,final MaterialSpinner citySpin)
    {
        final ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();

        try{
            APIService service=RetroClient.getApiService();
            Call<DealerModel> call=service.getDealerCityList(state);
            call.enqueue(new Callback<DealerModel>() {
                @Override
                public void onResponse(Call<DealerModel> call, Response<DealerModel> response) {
                    progressDialog.dismiss();
                    try {

                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
                                List<String >cityList= response.body().getData();
                                Collections.sort(cityList);
                                cityList.add(0,"SELECT CITY");
                                citySpin.setItems(cityList);
                                citySpin.setPadding(30, 0, 0, 0);

                            }else {
                                alert.showAlertbox("No city found for selected state");
                            }
                        }
                        else {
                            alert.showAlertbox("Bad Response. Please try again later");
                        }

                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                        alert.showAlertbox("Something went wrong . Please try again later .");
                    }
                }

                @Override
                public void onFailure(Call<DealerModel> call, Throwable t) {
                    progressDialog.dismiss();
                    alert.showAlertbox("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
            alert.showAlertbox("Exception error. Please try again later");
        }

    }
}
