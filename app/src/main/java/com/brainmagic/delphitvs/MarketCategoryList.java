package com.brainmagic.delphitvs;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.regionalmanager.RegionalMainmenu;
import com.brainmagic.delphitvs.salesactivities.SalesMainmenu;

import java.util.List;

import adapter.CategoryAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.general.categorylist.CategoryData;
import model.general.categorylist.CategoryList;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MarketCategoryList extends AppCompatActivity {

    private TextView categoryHeader;
    private ImageView back;
    private Alert alert;
    private ListView categoryList;
    private List<CategoryData> categoryDataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_category_list);

        alert=new Alert(this);
        back=findViewById(R.id.back);

        String category=getIntent().getStringExtra("category");

        categoryHeader=findViewById(R.id.category_info_header);
        categoryList=findViewById(R.id.category_list);

        categoryHeader.setText(category);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        final ImageView img_menu = (ImageView)findViewById(R.id.menu);
        img_menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(MarketCategoryList.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_home:
                                startActivity(new Intent(MarketCategoryList.this, MainActivity.class));
                                break;
                            case R.id.menu_aboutus:
                                startActivity(new Intent(MarketCategoryList.this, Aboutus.class));
                                break;
                            case R.id.menu_pricelist:
                                startActivity(new Intent(MarketCategoryList.this, PriceList.class));
                                break;
                            case R.id.menu_general:
                                startActivity(new Intent(MarketCategoryList.this, General.class));
                                break;
                            case R.id.menu_productcatalogue:
                                startActivity(new Intent(MarketCategoryList.this, ProductCatalogue.class));
                                break;

                            case R.id.menu_vehiclesegment:
                                startActivity(new Intent(MarketCategoryList.this, VehicleSegment.class));
                                break;

                            case R.id.menu_login:
                                SharedPreferences preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
                                if(preferences.getBoolean("salesLogin",false)) {
                                    if(preferences.getString("userType","").equals("SalesExecutive"))
                                    {
                                        Intent i = new Intent(MarketCategoryList.this, SalesMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                    else {
                                        Intent i = new Intent(MarketCategoryList.this, RegionalMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                }
                                else {
                                    Intent i = new Intent(MarketCategoryList.this, SalesLogin.class);
                                    startActivity(i);
                                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                }

                                break;

                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.aboutusmenu);
                pop.show();
            }
        });

        categoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(getIntent().getStringExtra("category").equals("Brochure")) {
                    startActivity(new Intent(MarketCategoryList.this, CategoryDataView.class)
                            .putExtra("categoryType", getIntent().getStringExtra("category"))
                            .putExtra("categoryData", categoryDataList.get(position).getCategory())
                    );
                }else {
                    startActivity(new Intent(MarketCategoryList.this, VideoDataView.class)
                            .putExtra("categoryType", getIntent().getStringExtra("category"))
                            .putExtra("categoryData", categoryDataList.get(position).getCategory())
                    );
                }


            }
        });

        NetworkConnection connection=new NetworkConnection(this);
        if(connection.CheckInternet())
        {
            getCategoryList();
        }

        else {
            alert.showAlertboxnegative("No Internet Connection. Please try again Later");
        }
    }

    private void getCategoryList()
    {
        final ProgressDialog progressDialog = new ProgressDialog(MarketCategoryList.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIService service= RetroClient.getApiService();

        Call<CategoryList> call=service.getMarketCategory();
        try {
            call.enqueue(new Callback<CategoryList>() {
                @Override
                public void onResponse(Call<CategoryList> call, Response<CategoryList> response) {
                    progressDialog.dismiss();
                    try{

                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success")) {
                                categoryDataList=response.body().getData();
                                CategoryAdapter adapter = new CategoryAdapter(MarketCategoryList.this, response.body().getData());
                                categoryList.setAdapter(adapter);
                            }
                            else
                            {
                                alert.showAlertboxnegative("Invalid Response. Please try again Later");
                            }

                        }else {
                            alert.showAlertboxnegative("Something went wrong . Please try again later .");
                        }

                    }catch (Exception e)
                    {
                        e.printStackTrace();
                        alert.showAlertboxnegative("Bad Response. Please contact Admin");
                    }

                }

                @Override
                public void onFailure(Call<CategoryList> call, Throwable t) {
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
            alert.showAlertboxnegative("Something went wrong . Please try again later .");

        }
    }

}
