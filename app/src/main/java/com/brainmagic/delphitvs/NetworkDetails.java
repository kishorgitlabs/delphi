package com.brainmagic.delphitvs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.regionalmanager.RegionalMainmenu;
import com.brainmagic.delphitvs.salesactivities.SalesMainmenu;

import java.util.List;

import adapter.NetworkAdapter;
import model.network.dealer.dealerdetails.DealerDetails;

public class NetworkDetails extends AppCompatActivity {

    private ImageView img_back,img_home;
    private TextView root,sub;
    private Animation animation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_details);
        List<DealerDetails> networkData= (List<DealerDetails>) getIntent().getSerializableExtra("NetworkData");
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bounce);
        img_back =(ImageView)findViewById(R.id.back);
        TextView stateView =findViewById(R.id.state_network);
        TextView cityView =findViewById(R.id.city_network);
        TextView networkTypeView =findViewById(R.id.network_data);

        String state=getIntent().getStringExtra("state");
        String city=getIntent().getStringExtra("city");
        String networkType=getIntent().getStringExtra("networkType");
        root =findViewById(R.id.root);
        ListView networkList =findViewById(R.id.network_list);
        sub =findViewById(R.id.sub);

        stateView.setText(state);
        cityView.setText(city);
        networkTypeView.setText(networkType);


        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        final ImageView img_menu = (ImageView)findViewById(R.id.menu);
        img_menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                img_menu.startAnimation(animation);
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(NetworkDetails.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_home:
                                startActivity(new Intent(NetworkDetails.this, MainActivity.class));
                                break;
                            case R.id.menu_aboutus:
                                startActivity(new Intent(NetworkDetails.this, Aboutus.class));
                                break;
                            case R.id.menu_pricelist:
                                startActivity(new Intent(NetworkDetails.this, PriceList.class));
                                break;
                            case R.id.menu_general:
                                startActivity(new Intent(NetworkDetails.this, General.class));
                                break;
                            case R.id.menu_productcatalogue:
                                startActivity(new Intent(NetworkDetails.this, ProductCatalogue.class));
                                break;

                            case R.id.menu_vehiclesegment:
                                startActivity(new Intent(NetworkDetails.this, VehicleSegment.class));
                                break;

                            case R.id.menu_login:
                                SharedPreferences preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
                                if(preferences.getBoolean("salesLogin",false)) {
                                    if(preferences.getString("userType","").equals("SalesExecutive"))
                                    {
                                        Intent i = new Intent(NetworkDetails.this, SalesMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                    else {
                                        Intent i = new Intent(NetworkDetails.this, RegionalMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                }
                                else {
                                    Intent i = new Intent(NetworkDetails.this, SalesLogin.class);
                                    startActivity(i);
                                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                }

                                break;

                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.aboutusmenu);


                pop.show();
            }
        });

        NetworkAdapter adapter=new NetworkAdapter(this,networkData);
        networkList.setAdapter(adapter);
    }
}
