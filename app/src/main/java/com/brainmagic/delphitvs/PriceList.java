package com.brainmagic.delphitvs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.regionalmanager.RegionalMainmenu;
import com.brainmagic.delphitvs.salesactivities.SalesMainmenu;

import adapter.PriceListAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.pricelist.PriceDetails;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PriceList extends AppCompatActivity {

    private ListView priceList;
    private boolean isSearched = false;
    private PriceListAdapter adapter;
    private Alert alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price_list);
        ImageView menu = findViewById(R.id.menu);
        ImageView back = findViewById(R.id.back);
        priceList = findViewById(R.id.price_details_list);
        final EditText etSearch = findViewById(R.id.price_search_partno);
        final Button search = findViewById(R.id.price_search_button);

        alert = new Alert(this);
        NetworkConnection connection = new NetworkConnection(this);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String searchedItems = s.toString().trim();
                if (isSearched)
                    if (searchedItems.isEmpty()) {
                        Toast.makeText(PriceList.this, "Click on Search Button to Load Full List", Toast.LENGTH_SHORT).show();
                        isSearched = false;
                    }

            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String partNo = etSearch.getText().toString().trim();
                if (partNo.isEmpty())
                    isSearched = true;

                adapter.filter(partNo);
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                img_menu.startAnimation(animation);
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(PriceList.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_home:
                                startActivity(new Intent(PriceList.this, MainActivity.class));
                                break;
                            case R.id.menu_aboutus:
                                startActivity(new Intent(PriceList.this, Aboutus.class));
                                break;

                            case R.id.menu_general:
                                startActivity(new Intent(PriceList.this, General.class));
                                break;
                            case R.id.menu_productcatalogue:
                                startActivity(new Intent(PriceList.this, ProductCatalogue.class));
                                break;

                            case R.id.menu_vehiclesegment:
                                startActivity(new Intent(PriceList.this, VehicleSegment.class));
                                break;

                            case R.id.menu_login:
                                SharedPreferences preferences = getSharedPreferences("Delphi", MODE_PRIVATE);
                                if (preferences.getBoolean("salesLogin", false)) {
                                    if (preferences.getString("userType", "").equals("SalesExecutive")) {
                                        Intent i = new Intent(PriceList.this, SalesMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    } else {
                                        Intent i = new Intent(PriceList.this, RegionalMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                } else {
                                    Intent i = new Intent(PriceList.this, SalesLogin.class);
                                    startActivity(i);
                                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                }


                                break;

                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.aboutusmenu);
                pop.getMenu().findItem(R.id.menu_pricelist).setVisible(false);

                pop.show();
            }
        });
        if (connection.CheckInternet()) {
            getPriceList();
        } else {

            alert.showAlertboxnegative("No Internet Connection");
        }
    }

    private void getPriceList() {
        final ProgressDialog progressDialog = new ProgressDialog(PriceList.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIService service = RetroClient.getApiService();
        Call<PriceDetails> call = service.getPriceList();

        try {
            call.enqueue(new Callback<PriceDetails>() {
                @Override
                public void onResponse(Call<PriceDetails> call, Response<PriceDetails> response) {
                    progressDialog.dismiss();
                    try {
                        if (response.isSuccessful()) {
                            if (response.body().getResult().equals("Success")) {
                                adapter = new PriceListAdapter(PriceList.this, response.body().getData());
                                priceList.setAdapter(adapter);
                            } else {
                                alert.showAlertboxnegative("Invalid Response. Please try again Later");
                            }

                        } else {
                            alert.showAlertboxnegative("Something went wrong . Please try again later .");
                        }

                    } catch (Exception e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        alert.showAlertboxnegative("Bad Response. Please contact Admin");
                    }

                }

                @Override
                public void onFailure(Call<PriceDetails> call, Throwable t) {
                    progressDialog.dismiss();
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            progressDialog.dismiss();
            alert.showAlertboxnegative("Something went wrong . Please try again later .");
        }
    }
}
