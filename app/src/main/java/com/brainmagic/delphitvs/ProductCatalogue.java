package com.brainmagic.delphitvs;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.regionalmanager.RegionalMainmenu;
import com.brainmagic.delphitvs.salesactivities.SalesMainmenu;

import alert.Alert;


public class ProductCatalogue extends AppCompatActivity {
    private  ImageView img_back,img_home;
    private Animation animation;
    private Alert alert;
    private LinearLayout inlineFIE,inline_layout,allmakeFilter,allmakeSpares,rotary_layout,commonrail_layout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_catalogue);
        inlineFIE = (LinearLayout)findViewById(R.id.productlayout1);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bounce);

        alert=new Alert(this);

        inlineFIE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                alert.showAlertbox("This Page is Under Construction");
                Intent i = new Intent(ProductCatalogue.this,InlineFie.class);
                i.putExtra("inline_main","InLine FIE System");
                i.putExtra("inline_sub","M&M");
                startActivity(i);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });

        rotary_layout = (LinearLayout)findViewById(R.id.rotarylayout);
        rotary_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(ProductCatalogue.this, RotarySubCategoryPump.class);
                i.putExtra("MainCatagory","Rotary FIE System");
                i.putExtra("SubCatagory","Pump");
                startActivity(i);
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
//                Intent i = new Intent(ProductCatalogue.this,RotaryfieSystem.class);
//                startActivity(i);
//                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });

        commonrail_layout = (LinearLayout)findViewById(R.id.commonraillayout);
        commonrail_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(ProductCatalogue.this, CommonSubCategoryPump.class);
                i.putExtra("MainCatagory","Common Rail System");
                i.putExtra("SubCatagory","Pump");
                startActivity(i);
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
//                Intent i = new Intent(ProductCatalogue.this,CommonrailFiesystem.class);
//                startActivity(i);
//                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });

        allmakeFilter = (LinearLayout)findViewById(R.id.filter);
        allmakeFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ProductCatalogue.this,FiltersAllmakeproducts.class);
                startActivity(i);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });
        allmakeSpares = (LinearLayout)findViewById(R.id.spares);
        //AllmakeProducts is waste activity
        allmakeSpares.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ProductCatalogue.this,SparesAllmakeproducts.class);
                startActivity(i);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });

        img_back =(ImageView)findViewById(R.id.back);
//        img_home =(ImageView)findViewById(R.id.home);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


//        img_home.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(ProductCatalogue.this,MainActivity.class);
//                startActivity(i);
//            }
//        });

        final ImageView img_menu = (ImageView)findViewById(R.id.menu);
        img_menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                img_menu.startAnimation(animation);
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(ProductCatalogue.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_home:
                                startActivity(new Intent(ProductCatalogue.this, MainActivity.class));
                                break;
                            case R.id.menu_aboutus:
                                startActivity(new Intent(ProductCatalogue.this, Aboutus.class));
                                break;
                            case R.id.menu_pricelist:
                                startActivity(new Intent(ProductCatalogue.this, PriceList.class));
                                break;
                            case R.id.menu_general:
                                startActivity(new Intent(ProductCatalogue.this, General.class));
                                break;


                            case R.id.menu_vehiclesegment:
                                startActivity(new Intent(ProductCatalogue.this, VehicleSegment.class));
                                break;

                            case R.id.menu_login:
                                SharedPreferences preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
                                if(preferences.getBoolean("salesLogin",false)) {
                                    if(preferences.getString("userType","").equals("SalesExecutive"))
                                    {
                                        Intent i = new Intent(ProductCatalogue.this, SalesMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                    else {
                                        Intent i = new Intent(ProductCatalogue.this, RegionalMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                }
                                else {
                                    Intent i = new Intent(ProductCatalogue.this, SalesLogin.class);
                                    startActivity(i);
                                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                }


                                break;

                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.aboutusmenu);
                pop.getMenu().findItem(R.id.menu_productcatalogue).setVisible(false);

                pop.show();
            }
        });
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
