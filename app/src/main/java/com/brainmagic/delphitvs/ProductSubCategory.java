package com.brainmagic.delphitvs;

import android.content.Intent;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;


public class ProductSubCategory extends AppCompatActivity {
    private GridView gridView;
    private LinearLayout deliverylayout,elementlayout,nozzielayout;
    private ImageView img_back,img_home;
    private EditText edit_search;
    private String str_search;
    private ImageView search_img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_sub_category);
        edit_search = (EditText)findViewById(R.id.searchedit);
        search_img = (ImageView)findViewById(R.id.img_search);

        search_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_search = edit_search.getText().toString();
                Intent i = new Intent(ProductSubCategory.this, SparesProducts.class);
                    i.putExtra("searchid",str_search);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
        deliverylayout=(LinearLayout) findViewById(R.id.deliveryvalve_layout);
        elementlayout=(LinearLayout) findViewById(R.id.elementlayout);
        nozzielayout=(LinearLayout) findViewById(R.id.nozzlelayout);

        deliverylayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ProductSubCategory.this, SparesProducts.class);
                i.putExtra("searchname","DELIVERY VALVE");
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        elementlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ProductSubCategory.this, SparesProducts.class);
                i.putExtra("searchname","ELEMENT");
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        nozzielayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ProductSubCategory.this, SparesProducts.class);
                i.putExtra("searchname","NOZZLE");
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        img_back =(ImageView)findViewById(R.id.back);
        img_home =(ImageView)findViewById(R.id.home);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ProductSubCategory.this,MainActivity.class);
                startActivity(i);
            }
        });



    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
