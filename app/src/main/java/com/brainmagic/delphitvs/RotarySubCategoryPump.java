package com.brainmagic.delphitvs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.regionalmanager.RegionalMainmenu;
import com.brainmagic.delphitvs.salesactivities.SalesMainmenu;

import adapter.RotaryPumpAdapter;
import model.api.APIService;
import model.api.RetroClient;
import model.commonexpandablemodel.CommonRotaryExpandableList;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toaster.Toasts;

public class RotarySubCategoryPump extends AppCompatActivity {
    private ImageView img_home,img_back,img_menu;
    private TextView textView_one,textView_two;
    private String main_cat,sub_cat;
    private NetworkConnection networkConnection;
    private Toasts toasts;
    private ExpandableListView listView;
    private Animation animation;
    private RotaryPumpAdapter productDetailsAdapter;
//    private Button search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rotary_sub_category_pump);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bounce);
        img_back = (ImageView)findViewById(R.id.back);
//        img_home = (ImageView)findViewById(R.id.home);
        img_menu = (ImageView)findViewById(R.id.menu);
        textView_one = (TextView)findViewById(R.id.textView2);
//        textView_two = (TextView)findViewById(R.id.textView3);
        Button search = findViewById(R.id.search_rotary_items);
        final EditText searchedTxt = findViewById(R.id.search_partno);
        main_cat = getIntent().getStringExtra("MainCatagory");
        sub_cat = getIntent().getStringExtra("SubCatagory");
        networkConnection = new NetworkConnection(this);
        toasts = new Toasts(this);
        listView =findViewById(R.id.pump_details_list);
//        textView_two.setText(sub_cat);
        textView_one.setText(main_cat);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });

//        img_home.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i = new Intent(RotarySubCategoryPump.this,MainActivity.class);
//                startActivity(i);
//                overridePendingTransition(R.anim.left_in,R.anim.left_out);
//            }
//        });

        search.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                productDetailsAdapter.filter(searchedTxt.getText().toString());
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        });

        /*listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });*/

        img_menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                img_menu.startAnimation(animation);
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(RotarySubCategoryPump.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_home:
                                startActivity(new Intent(RotarySubCategoryPump.this, MainActivity.class));
                                break;
                            case R.id.menu_aboutus:
                                startActivity(new Intent(RotarySubCategoryPump.this, Aboutus.class));
                                break;
                            case R.id.menu_pricelist:
                                startActivity(new Intent(RotarySubCategoryPump.this, PriceList.class));
                                break;
                            case R.id.menu_general:
                                startActivity(new Intent(RotarySubCategoryPump.this, General.class));
                                break;
                            case R.id.menu_productcatalogue:
                                startActivity(new Intent(RotarySubCategoryPump.this, ProductCatalogue.class));
                                break;

                            case R.id.menu_vehiclesegment:
                                startActivity(new Intent(RotarySubCategoryPump.this, VehicleSegment.class));
                                break;

                            case R.id.menu_login:
                                SharedPreferences preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
                                if(preferences.getBoolean("salesLogin",false)) {
                                    if(preferences.getString("userType","").equals("SalesExecutive"))
                                    {
                                        Intent i = new Intent(RotarySubCategoryPump.this, SalesMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                    else {
                                        Intent i = new Intent(RotarySubCategoryPump.this, RegionalMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                }
                                else {
                                    Intent i = new Intent(RotarySubCategoryPump.this, SalesLogin.class);
                                    startActivity(i);
                                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                }


                                break;

                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.aboutusmenu);
                pop.show();
            }
        });

        checkInternet();
    }

    private void checkInternet() {
        if(networkConnection.CheckInternet()){
            validatePart();
        }else {
            Toast.makeText(getApplicationContext(),"Please check your network connection and try again!",Toast.LENGTH_SHORT).show();
        }
    }

    private void validatePart() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(RotarySubCategoryPump.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            APIService service = RetroClient.getApiService();
            Call<CommonRotaryExpandableList> call = service.rotaryOldNewServiceList(main_cat,sub_cat);
            call.enqueue(new Callback<CommonRotaryExpandableList>() {
                @Override
                public void onResponse(Call<CommonRotaryExpandableList> call, Response<CommonRotaryExpandableList> response) {
                    if (response.body().getResult().equals("success")) {
                        progressDialog.dismiss();
                        productDetailsAdapter=new RotaryPumpAdapter(RotarySubCategoryPump.this,response.body().getData(),main_cat,sub_cat);
                        listView.setAdapter(productDetailsAdapter);
                    } else {
                        toasts.ShowErrorToast("No FeedBack is Found ");
                        onBackPressed();
                        progressDialog.dismiss();
                    }
                }
                @Override
                public void onFailure(Call<CommonRotaryExpandableList> call, Throwable t) {
                    toasts.ShowErrorToast("Something went wrong . Please try again later .");
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            toasts.ShowErrorToast("Something went wrong . Please try again later .");
        }
    }
}
