package com.brainmagic.delphitvs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.regionalmanager.RegionalMainmenu;
import com.brainmagic.delphitvs.salesactivities.SalesMainmenu;

public class RotaryfieSystem extends AppCompatActivity {
    private ImageView img_back,img_home;
    private LinearLayout layout_filter,layout_pump;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rotaryfie_system);
        img_back =(ImageView)findViewById(R.id.back);
//        img_home =(ImageView)findViewById(R.id.home);
        layout_filter = (LinearLayout)findViewById(R.id.rotatryfie_filter);
        layout_pump = (LinearLayout)findViewById(R.id.rotatryfie_pump);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        layout_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(RotaryfieSystem.this,SubCatagoryFilter.class);
                i.putExtra("Category","Main");
                i.putExtra("MainCatagory","Rotary FIE System");
                i.putExtra("SubCatagory","Filter");
                startActivity(i);
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });

        layout_pump.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(RotaryfieSystem.this, RotarySubCategoryPump.class);
                i.putExtra("MainCatagory","Rotary FIE System");
                i.putExtra("SubCatagory","Pump");
                startActivity(i);
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });

//        img_home.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(RotaryfieSystem.this,MainActivity.class);
//                startActivity(i);
//                overridePendingTransition(R.anim.left_in,R.anim.left_out);
//            }
//        });

        final ImageView img_menu = (ImageView)findViewById(R.id.menu);
        img_menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                img_menu.startAnimation(animation);
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(RotaryfieSystem.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_home:
                                startActivity(new Intent(RotaryfieSystem.this, MainActivity.class));
                                break;
                            case R.id.menu_aboutus:
                                startActivity(new Intent(RotaryfieSystem.this, Aboutus.class));
                                break;
                            case R.id.menu_pricelist:
                                startActivity(new Intent(RotaryfieSystem.this, PriceList.class));
                                break;
                            case R.id.menu_general:
                                startActivity(new Intent(RotaryfieSystem.this, General.class));
                                break;
                            case R.id.menu_productcatalogue:
                                startActivity(new Intent(RotaryfieSystem.this, ProductCatalogue.class));
                                break;

                            case R.id.menu_vehiclesegment:
                                startActivity(new Intent(RotaryfieSystem.this, VehicleSegment.class));
                                break;

                            case R.id.menu_login:
                                SharedPreferences preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
                                if(preferences.getBoolean("salesLogin",false)) {
                                    if(preferences.getString("userType","").equals("SalesExecutive"))
                                    {
                                        Intent i = new Intent(RotaryfieSystem.this, SalesMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                    else {
                                        Intent i = new Intent(RotaryfieSystem.this, RegionalMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                }
                                else {
                                    Intent i = new Intent(RotaryfieSystem.this, SalesLogin.class);
                                    startActivity(i);
                                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                }


                                break;


                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.aboutusmenu);
                pop.show();
            }
        });

    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.left_in, R.anim.left_out);

    }
}
