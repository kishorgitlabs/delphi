package com.brainmagic.delphitvs;

import android.content.Intent;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class SalesExistingcustomer extends AppCompatActivity {
    private ImageView img_back,img_home;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_existingcustomer);
        img_back =(ImageView)findViewById(R.id.back);
        img_home =(ImageView)findViewById(R.id.home);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SalesExistingcustomer.this,MainActivity.class);
                startActivity(i);
            }
        });
    }
}
