package com.brainmagic.delphitvs;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;

import android.os.Bundle;

import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.regionalmanager.RegionalMainmenu;
import com.brainmagic.delphitvs.salesactivities.SalesMainmenu;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;

import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.forgotpassword.ForgotPasswordModel;
import model.sales.login.SalesLoginModel;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toaster.Toasts;

public class SalesLogin extends AppCompatActivity {
    private Animation animation;
    private Button btn_login;
    private TextView btn_forgot;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private Alert showAlert;
    private ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bounce);

        showAlert=new Alert(this);
        preferences= getSharedPreferences("Delphi", MODE_PRIVATE);
        editor=preferences.edit();
        final TextView uName=findViewById(R.id.sales_user_name);
        final TextView uPass=findViewById(R.id.sales_user_password);


        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        final ImageView img_menu = (ImageView)findViewById(R.id.menu);
        btn_login = (Button)findViewById(R.id.btn_login);
        btn_forgot = (TextView)findViewById(R.id.forgot_password);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name=uName.getText().toString();
                String pass=uPass.getText().toString();
                if(TextUtils.isEmpty(name))
                {
                    uName.setError("Enter Name");
                }
                else if (TextUtils.isEmpty(pass)){
                    uPass.setError("Enter Password");
                }
                else {
                    NetworkConnection connection=new NetworkConnection(SalesLogin.this);
                    if(connection.CheckInternet())
                     {
                        int sdkInt=Build.VERSION.SDK_INT;
                        String deviceName=getDeviceName();
                        String brand=capitalize(getDeviceName());

                        callApiForLogin(name,pass,brand,sdkInt);

                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                    }else {
                        showAlert.showAlertbox("No Internet Connection. Please try again Later");
                    }
                }
            }
        });

        btn_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlert();
            }
        });
        img_menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                img_menu.startAnimation(animation);
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(SalesLogin.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_home:
                                startActivity(new Intent(SalesLogin.this, MainActivity.class));
                                break;
                            case R.id.menu_general:
                                startActivity(new Intent(SalesLogin.this, General.class));
                                break;
                            case R.id.menu_aboutus:
                                startActivity(new Intent(SalesLogin.this, Aboutus.class));
                                break;

                            case R.id.menu_productcatalogue:
                                startActivity(new Intent(SalesLogin.this, ProductCatalogue.class));
                                break;

                            case R.id.menu_vehiclesegment:
                                startActivity(new Intent(SalesLogin.this, SalesLogin.class));
                                break;

                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.aboutusmenu);

                pop.show();
            }
        });



    }

    private void callApiForLogin(String name, String pass, String brand, int sdkInt)
    {
        final ProgressDialog progressDialog = new ProgressDialog(SalesLogin.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {
            APIService service = RetroClient.getApiService();
            Call<SalesLoginModel> call = service.setSalesLogin(name,pass,brand, String.valueOf(sdkInt));
            call.enqueue(new Callback<SalesLoginModel>() {
                @Override
                public void onResponse(Call<SalesLoginModel> call, Response<SalesLoginModel> response) {
                        progressDialog.dismiss();
                    try{
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("SESuccess"))
                            {
                                editor.putString("empId", String.valueOf(response.body().getData().getId()));
                                editor.putString("empName",response.body().getData().getUserName());
                                editor.putString("empEmailId",response.body().getData().getEmailId());
                                editor.putString("userType",response.body().getData().getUserType());
                                editor.putString("empCode",response.body().getData().getExecutiveCode());
                                editor.putString("empRegion",response.body().getData().getRegion());
                                editor.putString("empMobile",response.body().getData().getMobno());
                                editor.putString("regId",response.body().getData().getRegid());
                                editor.putBoolean("salesLogin", true);
//                                editor.putString("empCity",response.body().getData().getC());
//                                editor.putString("empState",response.body().getData().gets());
                                editor.commit();
                                Intent i = new Intent(SalesLogin.this, SalesMainmenu.class);
                                finish();
                                startActivity(i);
//                                if(response.body().getData().getUserType().equals("SalesExecutive"))
//                                {
//                                    Intent i = new Intent(SalesLogin.this, SalesMainmenu.class);
//                                    finish();
//                                    startActivity(i);
//                                }
//                                else
//                                    {
////                                    editor.putString("regId", String.valueOf(response.body().getData().getRegid()));
////                                    editor.commit();
//
//                                }
                            }

                        else   if(response.body().getResult().equals("RMSuccess"))
                            {
                                editor.putString("empId", String.valueOf(response.body().getData().getRegionid()));
                                editor.putString("empName",response.body().getData().getUserName());
                                editor.putString("empEmailId",response.body().getData().getEmailId());
                                editor.putString("userType","Regional Manager");
                                editor.putString("empCode",response.body().getData().getManagercode());
                                editor.putString("empRegion",response.body().getData().getRegion());
                                editor.putString("empMobile",response.body().getData().getMobileNo());
                             editor.putString("regId",response.body().getData().getRegionid());
                                editor.putBoolean("salesLogin", true);
//                                editor.putString("empCity",response.body().getData().getC());
//                                editor.putString("empState",response.body().getData().gets());
                                editor.commit();
                                editor.apply();
                                Intent i = new Intent(SalesLogin.this, RegionalMainmenu.class);
                                finish();
                                startActivity(i);
//                                if(response.body().getData().getUserType().equals("SalesExecutive"))
//                            {
//                                Intent i = new Intent(SalesLogin.this, SalesMainmenu.class);
//                                finish();
//                                startActivity(i);
//                            }


//                                    editor.putString("regId", String.valueOf(response.body().getData().getRegid()));
//                                    editor.commit();
                            }
                           else  {
                               Alert alert=new Alert(SalesLogin.this);
                               alert.showAlertbox("Invalid Login. Please check your User Name and Password");
                           }
                        }

                        else {
                            Alert alert=new Alert(SalesLogin.this);
                            alert.showAlertbox("Something went wrong . Please try again later .");
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();

                        Alert alert=new Alert(SalesLogin.this);
                        alert.showAlertbox(e.toString());
                    }
                }

                @Override
                public void onFailure(Call<SalesLoginModel> call, Throwable t) {
                    progressDialog.dismiss();
                    Alert alert=new Alert(SalesLogin.this);
                    alert.showAlertbox("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
            Alert alert=new Alert(SalesLogin.this);
            alert.showAlertbox("Local OrderResult is Corrupted. Please logout and login.");
        }


    }

    private void showAlert() {
        final AlertDialog alertDialogbox = new AlertDialog.Builder(
                SalesLogin.this).create();

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.forgotpassword, null);
        alertDialogbox.getWindow().getAttributes().windowAnimations=R.style.DialogAnimations;
        alertDialogbox.setCanceledOnTouchOutside(true);
        alertDialogbox.setView(dialogView);
        // new getPartnumber().execute();
        Button search = (Button) dialogView.findViewById(R.id.search);
        final EditText email = (EditText)dialogView.findViewById(R.id.email_forgot_password);
        final MaterialSpinner userTypeSpinner = dialogView.findViewById(R.id.user_type);
        List<String> userTypes=new ArrayList<>();
        userTypes.add("Select UserType");
        userTypes.add("Regional Manager");
        userTypes.add("Sales Executive");

        final Toasts toasts = new Toasts(this);
        userTypeSpinner.setItems(userTypes);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i =new Intent(SalesLogin.this,ProductDetails.class);
//                startActivity(i);
//                overridePendingTransition(R.anim.right_in, R.anim.right_out);

                if(TextUtils.isEmpty(email.getText().toString()))
                {
                    toasts.ShowErrorToast("Enter Email Id");
                }
                else if (userTypeSpinner.getText().toString().equals("Select UserType"))
                {
                    toasts.ShowErrorToast("Select User Type");
                }
                else {
                    String userType=userTypeSpinner.getText().toString();
                    if(userType.equals("Sales Executive"))
                    {
                        userType="SalesExecutive";
                        forgotPassword(alertDialogbox,email.getText().toString(),userType);

                    }
                    else {
                        toasts.ShowErrorToast("Regional Manager Flow is Under Development");
                    }
                }
                alertDialogbox.dismiss();
            }
        });
        alertDialogbox.show();

    }


    private void forgotPassword(final AlertDialog alertDialogbox, String emailId, String userType)
    {
        final ProgressDialog progressDialog = new ProgressDialog(SalesLogin.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        try{
            APIService service=RetroClient.getApiService();
            Call<ForgotPasswordModel> call=service.forgotPassword(emailId,userType);
            call.enqueue(new Callback<ForgotPasswordModel>() {
                @Override
                public void onResponse(Call<ForgotPasswordModel> call, Response<ForgotPasswordModel> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
                                alertDialogbox.dismiss();
                                Alert alert=new Alert(SalesLogin.this);
                                alert.showAlertbox("Password sent to your Mail Successfully");

                            }
                            else if(response.body().getResult().equals("NotSuccess")){
                                Alert alert=new Alert(SalesLogin.this);
                                alert.showAlertbox("Email id does not match for this UserType");

                            }
                            else {
                                Alert alert=new Alert(SalesLogin.this);
                                alert.showAlertbox("Error occurred. Please try again Later");
                            }
                        }else {
                            Alert alert=new Alert(SalesLogin.this);
                            alert.showAlertbox("Something went wrong . Please try again later .");
                        }
                    }
                    catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        Alert alert=new Alert(SalesLogin.this);
                        alert.showAlertbox("Email id is not valid");
                    }
                }

                @Override
                public void onFailure(Call<ForgotPasswordModel> call, Throwable t) {
                    Alert alert=new Alert(SalesLogin.this);
                    alert.showAlertbox("Something went wrong . Please try again later .");
                }
            });


        }catch (Exception e)
        {
            e.printStackTrace();
            Alert alert=new Alert(SalesLogin.this);
            alert.showAlertbox("CheckRegistrationResult is invalid. Please contact Admin");
        }

    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.left_in,R.anim.left_out);
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }

        return phrase.toString();
    }
}
