package com.brainmagic.delphitvs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;

import android.os.Bundle;

import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.regionalmanager.RegionalMainmenu;
import com.brainmagic.delphitvs.salesactivities.SalesMainmenu;

import adapter.RotaryServiceListAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.rotaryexplodview.RotaryExplodeView;
import model.rotaryfiepdf.PDFDownload;
import model.rotaryservicelist.RotaryServiceData;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toaster.Toasts;

public class ServiceListActivity extends AppCompatActivity {

    private ImageView img_home,img_back,img_menu;
    private TextView textView_one,textView_two,partNumber,pageTitle;
    private String main_cat,sub_cat,partNo;
    private NetworkConnection networkConnection;
    private Toasts toasts;
    private ListView listView;
    private Animation animation;
    private RotaryServiceListAdapter rotaryServiceList;
    private Alert showAlert;
    private Button searchData;
    private EditText searchedItems;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rotary_service_list);

        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bounce);
        img_back = (ImageView)findViewById(R.id.back);
        img_menu = (ImageView)findViewById(R.id.menu);
        textView_one = (TextView)findViewById(R.id.textView2);
        textView_two = (TextView)findViewById(R.id.textView3);
        partNumber = (TextView)findViewById(R.id.part_num_text);
        searchData = findViewById(R.id.search_rotary_service_items);
        searchedItems = findViewById(R.id.search_edit_text);
        main_cat = getIntent().getStringExtra("MainCatagory");
        sub_cat = getIntent().getStringExtra("SubCatagory");
        partNo = getIntent().getStringExtra("partNo");
        networkConnection = new NetworkConnection(this);
        toasts = new Toasts(this);
        showAlert = new Alert(this);
        listView = (ListView) findViewById(R.id.pump_details_list);
        textView_two.setText(sub_cat);
        String[] system=main_cat.split(" ");
        textView_one.setText(system[0]+" "+system[1]);

        partNumber.setText(partNo);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });

//        img_home.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i = new Intent(ServiceListActivity.this,MainActivity.class);
//                startActivity(i);
//                overridePendingTransition(R.anim.left_in,R.anim.left_out);
//            }
//        });

        searchData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchedData=searchedItems.getText().toString();
                rotaryServiceList.filter(searchedData);
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        });

        /*listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });*/

        img_menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                img_menu.startAnimation(animation);
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(ServiceListActivity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_home:
                                startActivity(new Intent(ServiceListActivity.this, MainActivity.class));
                                break;
                            case R.id.menu_aboutus:
                                startActivity(new Intent(ServiceListActivity.this, Aboutus.class));
                                break;
                            case R.id.menu_pricelist:
                                startActivity(new Intent(ServiceListActivity.this, PriceList.class));
                                break;
                            case R.id.menu_general:
                                startActivity(new Intent(ServiceListActivity.this, General.class));
                                break;
                            case R.id.menu_productcatalogue:
                                startActivity(new Intent(ServiceListActivity.this, ProductCatalogue.class));
                                break;

                            case R.id.menu_vehiclesegment:
                                startActivity(new Intent(ServiceListActivity.this, VehicleSegment.class));
                                break;

                            case R.id.menu_login:
                                SharedPreferences preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
                                if(preferences.getBoolean("salesLogin",false)) {
                                    if(preferences.getString("userType","").equals("SalesExecutive"))
                                    {
                                        Intent i = new Intent(ServiceListActivity.this, SalesMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                    else {
                                        Intent i = new Intent(ServiceListActivity.this, RegionalMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                }
                                else {
                                    Intent i = new Intent(ServiceListActivity.this, SalesLogin.class);
                                    startActivity(i);
                                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                }


                                break;


                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.aboutusmenu);
                pop.show();
            }
        });

        checkInternet();

    }

    private void checkInternet() {
        if(networkConnection.CheckInternet()){
            validatePart();
        }else {
            showAlert.showAlertboxnegative("Please check your network connection and try again!");
//            Toast.makeText(getApplicationContext(),"Please check your network connection and try again!",Toast.LENGTH_SHORT).show();
        }
    }

    private void validatePart() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(ServiceListActivity.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            APIService service = RetroClient.getApiService();
            Call<RotaryServiceData> call = service.rotaryServiceList(partNo);
            call.enqueue(new Callback<RotaryServiceData>() {
                @Override
                public void onResponse(Call<RotaryServiceData> call, Response<RotaryServiceData> response) {
                    if (response.body().getResult().equals("success")) {
                        progressDialog.dismiss();

                        rotaryServiceList = new RotaryServiceListAdapter(ServiceListActivity.this, response.body().getData());
                        listView.setAdapter(rotaryServiceList);
                    } else {
                        // alert.showAlertbox("No Rejecte-Rating is Found");
                        showAlert.showAlertboxnegative("No data found please try again later");
//                        toasts.ShowErrorToast("No FeedBack is Found ");
                        // Toast.makeText(getApplicationContext(), "No Rejected-Rating is Found", Toast.LENGTH_LONG).show();
//                        onBackPressed();
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<RotaryServiceData> call, Throwable t) {
//                    toasts.ShowErrorToast("Something went wrong . Please try again later .");
                    showAlert.showAlertboxnegative("Something went wrong . Please try again later .");
                    // Toast.makeText(getApplicationContext(), "No RajectRating is Found", Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
//            toasts.ShowErrorToast("Something went wrong . Please try again later .");
            showAlert.showAlertboxnegative("Something went wrong . Please try again later .");
        }
    }

    public void explodedView(View view)
    {
        final ProgressDialog progressDialog = new ProgressDialog(ServiceListActivity.this);
        try{
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            APIService service=RetroClient.getApiService();
            Call<RotaryExplodeView> call=service.explodedView(partNo);
            call.enqueue(new Callback<RotaryExplodeView>() {
                @Override
                public void onResponse(Call<RotaryExplodeView> call, Response<RotaryExplodeView> response) {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            if(response.body().getResult().equals("success"))
                            {

                                Intent searchAddress = new Intent(Intent.ACTION_VIEW, Uri.parse(response.body().getData()));
                                progressDialog.dismiss();
                                if(searchAddress!=null) {
                                    startActivity(searchAddress);
                                }
                            }
                            else {
                                showAlert.showAlertboxnegative("No App is found to open this Link");
                            }
                        }catch (Exception e)
                        {
                            e.printStackTrace();
                            showAlert.showAlertboxnegative("Something went wrong . Please try again later .");
                        }
                    }
                    else {
                        showAlert.showAlertboxnegative("Failed to open the Link. Please try again later");

                    }
                }

                @Override
                public void onFailure(Call<RotaryExplodeView> call, Throwable t) {
                    showAlert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });



        }catch (Exception e)
        {
            progressDialog.dismiss();
            showAlert.showAlertboxnegative("Something went wrong . Please try again later .");
        }
    }

    public void downloadPDF(View view)
    {
        final ProgressDialog progressDialog = new ProgressDialog(ServiceListActivity.this);
        try{
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            APIService service = RetroClient.getApiService();
            Call<PDFDownload> call=service.pdfDownload(partNo);

            call.enqueue(new Callback<PDFDownload>() {
                @Override
                public void onResponse(Call<PDFDownload> call, Response<PDFDownload> response) {
                    progressDialog.dismiss();
                    try {
                        if(response.body().getResult().equals("Success")){

                            if(!response.body().getData().equals("Not Success")){
                            Intent searchAddress = new Intent(Intent.ACTION_VIEW, Uri.parse(response.body().getData()));
                            if(searchAddress!=null)
                                startActivity(searchAddress);
                            }
                        }
                        else {
                            showAlert.showAlertboxnegative("File not found");
                        }
                    }
                    catch (ActivityNotFoundException e)
                    {
                        showAlert.showAlertboxnegative("Something went wrong . Please try again later .");
                    }
                    catch (Exception e)
                    {
                        showAlert.showAlertboxnegative("Something went wrong . Please try again later .");
                    }
                }


                @Override
                public void onFailure(Call<PDFDownload> call, Throwable t) {
                    progressDialog.dismiss();
                    showAlert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            showAlert.showAlertboxnegative("Something went wrong . Please try again later .");
        }
    }
}
