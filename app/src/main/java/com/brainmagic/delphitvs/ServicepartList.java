package com.brainmagic.delphitvs;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.regionalmanager.RegionalMainmenu;
import com.brainmagic.delphitvs.salesactivities.SalesMainmenu;

import java.util.List;

import adapter.ServicePartlistAdapter;
import model.api.APIService;
import model.api.RetroClient;
import model.servicepart.ServicePartlist;
import model.servicepart.ServicePartlistData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toaster.Toasts;

public class ServicepartList extends AppCompatActivity {
    private ImageView img_home,img_back;
    private TextView textView_maincatagory,textView_subcatagory,textView_servicepart;
    private String str_one,str_two,dtvsPartno;
    private List<ServicePartlist> Data1;
    private Toasts toasts;
    private ListView listView;
    private ServicePartlistAdapter ServicePartlistAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicepart_list);
        img_back = (ImageView)findViewById(R.id.back);
        img_home = (ImageView)findViewById(R.id.home);
        toasts = new Toasts(this);
        listView = (ListView)findViewById(R.id.listpend);
        dtvsPartno = getIntent().getStringExtra("DTVSPARTNO");
        textView_maincatagory = (TextView)findViewById(R.id.textView2);
        textView_subcatagory = (TextView)findViewById(R.id.textView3);
        textView_servicepart = (TextView)findViewById(R.id.textView4);
        str_one = getIntent().getStringExtra("MainCatagory");
        str_two = getIntent().getStringExtra("SubCatagory");
        textView_maincatagory.setText(str_one);
        textView_subcatagory.setText(str_two);
       textView_servicepart.setText("Service Part");
        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ServicepartList.this,MainActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });
        final ImageView img_menu = (ImageView)findViewById(R.id.menu);
        img_menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                img_menu.startAnimation(animation);
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(ServicepartList.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_home:
                                startActivity(new Intent(ServicepartList.this, MainActivity.class));
                                break;
                            case R.id.menu_aboutus:
                                startActivity(new Intent(ServicepartList.this, Aboutus.class));
                                break;
                            case R.id.menu_pricelist:
                                startActivity(new Intent(ServicepartList.this, PriceList.class));
                                break;
                            case R.id.menu_general:
                                startActivity(new Intent(ServicepartList.this, General.class));
                                break;
                            case R.id.menu_productcatalogue:
                                startActivity(new Intent(ServicepartList.this, ProductCatalogue.class));
                                break;

                            case R.id.menu_vehiclesegment:
                                startActivity(new Intent(ServicepartList.this, VehicleSegment.class));
                                break;

                            case R.id.menu_login:
                                SharedPreferences preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
                                if(preferences.getBoolean("salesLogin",false)) {
                                    if(preferences.getString("userType","").equals("SalesExecutive"))
                                    {
                                        Intent i = new Intent(ServicepartList.this, SalesMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                    else {
                                        Intent i = new Intent(ServicepartList.this, RegionalMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                }
                                else {
                                    Intent i = new Intent(ServicepartList.this, SalesLogin.class);
                                    startActivity(i);
                                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                }

                                break;


                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.aboutusmenu);
                pop.show();
            }
        });
        ValidateDtvs();
    }

    private void ValidateDtvs() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(ServicepartList.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            APIService service = RetroClient.getApiService();
            Call<ServicePartlistData> call = service.SERVICE_PARTLIST_DATA_CALL(dtvsPartno);
            call.enqueue(new Callback<ServicePartlistData>() {
                @Override
                public void onResponse(Call<ServicePartlistData> call, Response<ServicePartlistData> response) {
                    if (response.body().getResult().equals("success")) {
                        progressDialog.dismiss();
                        Data1=  response.body().getData();
                        ServicePartlistAdapter =new ServicePartlistAdapter(ServicepartList.this,Data1);
                        listView.setAdapter(ServicePartlistAdapter);
                    } else {
                        // alert.showAlertbox("No Rejecte-Rating is Found");
                        toasts.ShowErrorToast("No FeedBack is Found ");
                        // Toast.makeText(getApplicationContext(), "No Rejected-Rating is Found", Toast.LENGTH_LONG).show();
                        onBackPressed();
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ServicePartlistData> call, Throwable t) {
                    toasts.ShowErrorToast("Something went wrong . Please try again later .");
                    // Toast.makeText(getApplicationContext(), "No RajectRating is Found", Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            toasts.ShowErrorToast("Something went wrong . Please try again later .");
        }
    }


}
