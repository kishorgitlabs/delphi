package com.brainmagic.delphitvs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.regionalmanager.RegionalMainmenu;
import com.brainmagic.delphitvs.salesactivities.SalesMainmenu;

public class SparesAllmakeproducts extends AppCompatActivity {
    private GridView gridView;
    private LinearLayout deliverylayout,elementlayout,nozzielayout,kitlayout;
    private ImageView img_back,img_home,img_menu;
    private EditText edit_search;
    private String str_search;
    private ImageView search_img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spares_allmakeproducts);
        edit_search = (EditText)findViewById(R.id.searchedit);
        search_img = (ImageView)findViewById(R.id.img_search);

        search_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_search = edit_search.getText().toString();
                Intent i = new Intent(SparesAllmakeproducts.this, SparesProducts.class);
                i.putExtra("searchid",str_search);
                i.putExtra("searchname","");
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        deliverylayout=(LinearLayout) findViewById(R.id.deliveryvalve_layout);
        elementlayout=(LinearLayout) findViewById(R.id.elementlayout);
        nozzielayout=(LinearLayout) findViewById(R.id.nozzlelayout);
        kitlayout=(LinearLayout) findViewById(R.id.kitlayout);

        deliverylayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SparesAllmakeproducts.this, SparesProducts.class);
                i.putExtra("searchname","DELIVERY VALVE");
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        elementlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SparesAllmakeproducts.this, SparesProducts.class);
                i.putExtra("searchname","ELEMENT");
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        nozzielayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SparesAllmakeproducts.this, SparesProducts.class);
                i.putExtra("searchname","NOZZLE");
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        kitlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SparesAllmakeproducts.this, SparesProducts.class);
                i.putExtra("searchname","SERVICE KIT");
                startActivity(i);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
//                onBackPressed();
            }
        });


        img_back =(ImageView)findViewById(R.id.back);

        img_menu =(ImageView)findViewById(R.id.menu);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


//        img_home.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(SparesAllmakeproducts.this,MainActivity.class);
//                startActivity(i);
//            }
//
        img_menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(SparesAllmakeproducts.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_home:
                                startActivity(new Intent(SparesAllmakeproducts.this, MainActivity.class));
                                break;
                            case R.id.menu_aboutus:
                                startActivity(new Intent(SparesAllmakeproducts.this, Aboutus.class));
                                break;
                            case R.id.menu_pricelist:
                                startActivity(new Intent(SparesAllmakeproducts.this, PriceList.class));
                                break;
                            case R.id.menu_general:
                                startActivity(new Intent(SparesAllmakeproducts.this, General.class));
                                break;
                            case R.id.menu_productcatalogue:
                                startActivity(new Intent(SparesAllmakeproducts.this, ProductCatalogue.class));
                                break;

                            case R.id.menu_vehiclesegment:
                                startActivity(new Intent(SparesAllmakeproducts.this, VehicleSegment.class));
                                break;

                            case R.id.menu_login:
                                SharedPreferences preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
                                if(preferences.getBoolean("salesLogin",false)) {
                                    if(preferences.getString("userType","").equals("SalesExecutive"))
                                    {
                                        Intent i = new Intent(SparesAllmakeproducts.this, SalesMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                    else {
                                        Intent i = new Intent(SparesAllmakeproducts.this, RegionalMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                }
                                else {
                                    Intent i = new Intent(SparesAllmakeproducts.this, SalesLogin.class);
                                    startActivity(i);
                                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                }


                                break;

                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.aboutusmenu);
                pop.show();
            }
        });

    }


    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in,R.anim.left_out);
    }
}
