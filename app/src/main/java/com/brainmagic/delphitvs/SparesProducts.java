package com.brainmagic.delphitvs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.regionalmanager.RegionalMainmenu;
import com.brainmagic.delphitvs.salesactivities.SalesMainmenu;

import java.util.List;

import adapter.ProductCatalogueAdapter;
import alert.Alert;
import model.PartnumberSearch;
import model.PartnumberSearchData;
import model.allmakeproducts.AllMakeProductsSpares;
import model.allmakeproducts.AllMakeSparesData;
import model.api.APIService;
import model.api.RetroClient;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toaster.Toasts;

public class SparesProducts extends AppCompatActivity {
    private NetworkConnection networkConnection;
    private List<AllMakeSparesData> Data;
    private TextView navi_textview;
    private LinearLayout searchLayout;
    private ImageView img_back, img_home, img_menu;
    private List<PartnumberSearch> Data1;
    private String str_inlinevalue, str_partvalue;
    private ListView list_inline;
    private ProductCatalogueAdapter productCatalogueAdapter;
    private Button goButton;
    private EditText searchParts;
    private Toasts toasts;
    private Alert alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_inline_products);
        toasts = new Toasts(this);
        alert = new Alert(this);
        list_inline = (ListView) findViewById(R.id.listinline);
        searchParts = findViewById(R.id.search_partno);
        goButton = findViewById(R.id.go);
        str_inlinevalue = getIntent().getStringExtra("searchname");
        str_partvalue = getIntent().getStringExtra("searchid");
        searchLayout = (LinearLayout) findViewById(R.id.so2);
        networkConnection = new NetworkConnection(this);
        navi_textview = (TextView) findViewById(R.id.textView3);

        if (!str_inlinevalue.equals("")) {
            if (str_inlinevalue.equals("DELIVERY VALVE"))
                navi_textview.setText("Delivery Valve");
            else if (str_inlinevalue.equals("ELEMENT"))
                navi_textview.setText("Element");
            else if (str_inlinevalue.equals("NOZZLE"))
                navi_textview.setText("Nozzle");
            else
                navi_textview.setText("Kit");
        } else {
            navi_textview.setText("");
        }
        checkInternet();
        img_back = (ImageView) findViewById(R.id.back);
//         img_home =(ImageView)findViewById(R.id.home);
        img_menu = (ImageView) findViewById(R.id.menu);

        goButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                 finish();

                String partNo = searchParts.getText().toString();
//                 if(TextUtils.isEmpty(partNo))
//                 {
//                     alert.showAlertbox("Please Enter the Part No/Application");
//                 }
//                 else
                {
                    if (str_partvalue != null) {
                        productCatalogueAdapter.filterByPartValue(partNo);
                    } else if (str_inlinevalue != null) {
                        productCatalogueAdapter.filter(partNo);
                    } else {
                        alert.showAlertbox("Something went Wrong. Please try again");
                    }
                    InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        });

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


//         img_home.setOnClickListener(new View.OnClickListener() {
//             @Override
//             public void onClick(View v) {
//                 Intent i = new Intent(SparesProducts.this,MainActivity.class);
//                 startActivity(i);
//             }
//         });

        img_menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(
                        SparesProducts.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();

                    }
                });

                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_home:
                                startActivity(new Intent(SparesProducts.this, MainActivity.class));
                                break;
                            case R.id.menu_aboutus:
                                startActivity(new Intent(SparesProducts.this, Aboutus.class));
                                break;
                            case R.id.menu_pricelist:
                                startActivity(new Intent(SparesProducts.this, PriceList.class));
                                break;
                            case R.id.menu_general:
                                startActivity(new Intent(SparesProducts.this, General.class));
                                break;
                            case R.id.menu_productcatalogue:
                                startActivity(new Intent(SparesProducts.this, ProductCatalogue.class));
                                break;

                            case R.id.menu_vehiclesegment:
                                startActivity(new Intent(SparesProducts.this, VehicleSegment.class));
                                break;

                            case R.id.menu_login:
                                SharedPreferences preferences = getSharedPreferences("Delphi", MODE_PRIVATE);
                                if (preferences.getBoolean("salesLogin", false)) {
                                    if (preferences.getString("userType", "").equals("SalesExecutive")) {
                                        Intent i = new Intent(SparesProducts.this, SalesMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    } else {
                                        Intent i = new Intent(SparesProducts.this, RegionalMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                } else {
                                    Intent i = new Intent(SparesProducts.this, SalesLogin.class);
                                    startActivity(i);
                                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                }


                                break;
                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.aboutusmenu);
                pop.show();
            }
        });

    }

    private void checkInternet() {
        if (networkConnection.CheckInternet()) {
            validataValue();
        } else {
            Toast.makeText(getApplicationContext(), "Please check your network connection and try again!", Toast.LENGTH_SHORT).show();
        }
    }

    private void validataValue() {
        //if str_partvalue is null then spares items are searched
        if (str_partvalue != null) {
            ValidatePart();
//             searchLayout.setVisibility(View.GONE);
        } else if (str_inlinevalue != null) {
            validateFileds();
//             searchLayout.setVisibility(View.GONE);
        } else {
            toasts.ShowErrorToast("Something Error");
        }
    }

    private void ValidatePart() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(SparesProducts.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            APIService service = RetroClient.getApiService();

            Call<PartnumberSearchData> call = service.SearchPartnumber(str_partvalue);

            call.enqueue(new Callback<PartnumberSearchData>() {
                @Override
                public void onResponse(Call<PartnumberSearchData> call, Response<PartnumberSearchData> response) {
                    if (response.body().getResult().equals("success")) {
                        progressDialog.dismiss();
                        Data1 = response.body().getData();
                        productCatalogueAdapter = new ProductCatalogueAdapter(SparesProducts.this, Data1);
                        list_inline.setAdapter((ListAdapter) productCatalogueAdapter);
                    } else {
                        // alert.showAlertbox("No Rejecte-Rating is Found");
                        toasts.ShowErrorToast("No FeedBack is Found for that Part No");
                        // Toast.makeText(getApplicationContext(), "No Rejected-Rating is Found", Toast.LENGTH_LONG).show();
                        onBackPressed();
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<PartnumberSearchData> call, Throwable t) {
                    toasts.ShowErrorToast("Something went wrong . Please try again later .");
                    // Toast.makeText(getApplicationContext(), "No RajectRating is Found", Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            toasts.ShowErrorToast("Something went wrong . Please try again later .");
        }
    }

    private void validateFileds() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(SparesProducts.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);

            progressDialog.show();
            APIService service = RetroClient.getApiService();

            Call<AllMakeProductsSpares> call = service.INLINE_DATA_CALL(str_inlinevalue);
            call.enqueue(new Callback<AllMakeProductsSpares>() {
                @Override
                public void onResponse(Call<AllMakeProductsSpares> call, Response<AllMakeProductsSpares> response) {
                    if (response.body().getResult().equals("success")) {
                        progressDialog.dismiss();
                        Data = response.body().getData();
                        productCatalogueAdapter = new ProductCatalogueAdapter(SparesProducts.this, Data, true, str_inlinevalue);
                        list_inline.setAdapter((ListAdapter) productCatalogueAdapter);
                    } else {
                        toasts.ShowErrorToast("No FeedBack is Found");
                        onBackPressed();
                        progressDialog.dismiss();

                    }
                }

                @Override
                public void onFailure(Call<AllMakeProductsSpares> call, Throwable t) {
                    toasts.ShowErrorToast("Something went wrong . Please try again later .");
                    // Toast.makeText(getApplicationContext(), "No RajectRating is Found", Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            toasts.ShowErrorToast("Something went wrong . Please try again later .");

        }

    }


}
