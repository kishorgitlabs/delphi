package com.brainmagic.delphitvs;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;

import android.os.Bundle;
import android.text.TextUtils;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.service.RegistrationActivity;

import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.registeration.checkregistration.CheckRegistration;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreen extends AppCompatActivity {
    private static final long SPLASH_DISPLAY_LENGTH = 1500;
    Boolean isregister, islogin;
    String login_type;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    Boolean isLoggedin=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        myshare = getSharedPreferences("Dephi", MODE_PRIVATE);
        editor=myshare.edit();
        isregister = myshare.getBoolean("Register", false);
        isLoggedin = myshare.getBoolean("isLogin", false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /*  if (isregister) {*/

                if (isLoggedin) {
                    Intent mainIntent = new Intent(SplashScreen.this, MainActivity.class);
                    startActivity(mainIntent);
                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                    // overridePendingTransition(R.anim.rotate_animation,R.anim.rotate_animation);
                    finish();

                } else {
                    String mobileNo=myshare.getString("mobileNo","");
                    if(TextUtils.isEmpty(mobileNo))
                    {
                        Intent mainIntent = new Intent(SplashScreen.this, RegistrationActivity.class);
                        startActivity(mainIntent);
                        overridePendingTransition(R.anim.left_in, R.anim.left_out);
                        // overridePendingTransition(R.anim.rotate_animation,R.anim.rotate_animation);
                        finish();
                    }
                    else
                    {
                        NetworkConnection connection=new NetworkConnection(SplashScreen.this);
                        if(connection.CheckInternet())
                        {
                            checkRegistration(mobileNo);
                        }
                        else
                        {
                            Alert alert=new Alert(SplashScreen.this);
                            alert.showAlertboxnegative("Cannot connect to Internet. Please check your Internet Connection");
                        }
                    }


                }
                 /*else {
                    Intent mainIntent = new Intent(Splash_Activity.this, Registration_Activity.class);
                    //mainIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(mainIntent);
                    finish();
                }*/
            }
            /* Create an Intent that will start the Menu-Activity. */

        }, SPLASH_DISPLAY_LENGTH);
    }

//    @Override
//    public void onBackPressed() {
//        moveTaskToBack(true);
//    }


    private void checkRegistration(String mobileNo) {
        try {
            APIService service = RetroClient.getApiService();

            Call<CheckRegistration> call = service.checkRegistration(mobileNo);
            call.enqueue(new Callback<CheckRegistration>() {
                @Override
                public void onResponse(Call<CheckRegistration> call, Response<CheckRegistration> response) {
                    try{
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
                                editor.putBoolean("isLogin", true);
                                editor.putString("mobileNo","");
                                editor.commit();
                                Intent mainIntent = new Intent(SplashScreen.this, MainActivity.class);
                                startActivity(mainIntent);
                                overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                // overridePendingTransition(R.anim.rotate_animation,R.anim.rotate_animation);
                                finish();
                            }
                            else
                            {
                                Intent mainIntent = new Intent(SplashScreen.this, RegistrationActivity.class);
                                startActivity(mainIntent);
                                overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                // overridePendingTransition(R.anim.rotate_animation,R.anim.rotate_animation);
                                finish();
                            }
                        }


                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CheckRegistration> call, Throwable t) {

                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }


}
