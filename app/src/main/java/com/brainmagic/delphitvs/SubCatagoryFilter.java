package com.brainmagic.delphitvs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.regionalmanager.RegionalMainmenu;
import com.brainmagic.delphitvs.salesactivities.SalesMainmenu;

import java.util.List;

import adapter.FilterListAdapter;
import adapter.FiltersSegmentAdpater;
import adapter.ProductDetailsAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.filters.details.FilterDetail;
import model.filters.details.FilterDetailList;
import model.filters.oemmodel.OEModelList;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toaster.Toasts;

public class SubCatagoryFilter extends AppCompatActivity {
    private ImageView img_back,img_home;
    private TextView textView_one,subCat;
    private String mainCategory_type,subCategory,category,segment;
    private LinearLayout search_layout,layout_two,layout_three,allMakeLayout;
    private ListView listView,modelListView;
    private NetworkConnection networkConnection;
    private List<FilterDetailList> Data1;
    private AutoCompleteTextView search_partno;
    private Toasts toasts;
    private ProductDetailsAdapter productDetailsAdapter;
    private FilterListAdapter filterDetailsAdapter;
    private FiltersSegmentAdpater filtersSegmentAdpater;
//    private HorizontalScrollView oElist;
    private Alert showAlertbox;
    private static final String TAG = "SubCatagoryFilter";
    private List<String> modelList;
    private Button goButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_catagory_filter);
        textView_one = (TextView)findViewById(R.id.textView2);
        subCat = (TextView)findViewById(R.id.sub_cat);
        img_back = (ImageView)findViewById(R.id.back);
//        img_home = (ImageView)findViewById(R.id.home);
        showAlertbox=new Alert(this);
        toasts = new Toasts(this);
//        oElist =  findViewById(R.id.horilist);
        modelListView =  findViewById(R.id.model_list);
        allMakeLayout =  findViewById(R.id.all_list);
        search_layout =  findViewById(R.id.so2);
        search_partno = findViewById(R.id.search_partnofilter);
        goButton = findViewById(R.id.go);
        //layout_one = (LinearLayout)findViewById(R.id.cart_item_layout);
        listView = (ListView)findViewById(R.id.listsubcatagory);
        networkConnection = new NetworkConnection(this);

        category = getIntent().getStringExtra("Category");
        // Main commonrailfiesystem, inlineFieSystem, rotaryfiesystem,
        if(category.equals("Main"))
        {
            mainCategory_type = getIntent().getStringExtra("MainCatagory");
            subCategory = getIntent().getStringExtra("SubCatagory");
        }
        // FiltersAllmakeproducts
        else {
            mainCategory_type = getIntent().getStringExtra("Type");
            subCategory = getIntent().getStringExtra("SubCategory");
            segment = getIntent().getStringExtra("Segment");
        }

        if(TextUtils.isEmpty(subCategory) && TextUtils.isEmpty(segment))
        {
            subCat.setText("All");
        }
        else if(subCategory.equals("All Make Filter")){
            subCat.setText("ALL MAKE FILTER");
        }
        else {
            subCat.setText(segment);
            search_layout.setVisibility(View.GONE);
        }

        textView_one.setText(mainCategory_type);
//        img_home.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i = new Intent(SubCatagoryFilter.this,MainActivity.class);
//                startActivity(i);
//                overridePendingTransition(R.anim.left_in,R.anim.left_out);
//            }
//        });

       /* layout_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SubCatagoryFilter.this,ServicepartList.class);
                i.putExtra("MainCatagory",mainCategory_type);
                i.putExtra("SubCatagory",subCategory);
                startActivity(i);
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });

        layout_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SubCatagoryFilter.this,ServicepartList.class);
                i.putExtra("MainCatagory",mainCategory_type);
                i.putExtra("SubCatagory",subCategory);
                startActivity(i);
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });

        layout_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SubCatagoryFilter.this,ServicepartList.class);
                i.putExtra("MainCatagory",mainCategory_type);
                i.putExtra("SubCatagory",subCategory);
                startActivity(i);
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });*/

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });

        goButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newText=search_partno.getText().toString();
                if(category.equals("Main")){// This main category is for rotary and common rail system and this flow is removed

                    if(productDetailsAdapter!=null) {

                        productDetailsAdapter.filter(newText);

                        productDetailsAdapter.notifyDataSetChanged();

                    }
                }else {
                    if(filterDetailsAdapter!=null) {

                        filterDetailsAdapter.filter(newText);

                        filterDetailsAdapter.notifyDataSetChanged();

                    }
                }

                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        });

//        search_partno.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                if(category.equals("Main")){
//                    if(productDetailsAdapter!=null) {
//
//                        productDetailsAdapter.filter(newText);
//
//                        productDetailsAdapter.notifyDataSetChanged();
//
//                    }
//                }else {
//                    if(filterDetailsAdapter!=null) {
//
//                        filterDetailsAdapter.filter(newText);
//
//                        filterDetailsAdapter.notifyDataSetChanged();
//
//                    }
//                }
//
//                return false;
//            }
//        });

        modelListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

//                Log.d(TAG, "onItemClick: "+parent.getI);
                startActivity(new Intent(SubCatagoryFilter.this,FiltersOemBasedList.class)
                .putExtra("oem",segment)
                .putExtra("model",modelList.get(position)));
            }
        });

        ImageView img_menu = (ImageView)findViewById(R.id.menu);
        img_menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                img_menu.startAnimation(animation);
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(SubCatagoryFilter.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_home:
                                startActivity(new Intent(SubCatagoryFilter.this, MainActivity.class));
                                break;
                            case R.id.menu_aboutus:
                                startActivity(new Intent(SubCatagoryFilter.this, Aboutus.class));
                                break;
                            case R.id.menu_pricelist:
                                startActivity(new Intent(SubCatagoryFilter.this, PriceList.class));
                                break;
                            case R.id.menu_general:
                                startActivity(new Intent(SubCatagoryFilter.this, General.class));
                                break;
                            case R.id.menu_productcatalogue:
                                startActivity(new Intent(SubCatagoryFilter.this, ProductCatalogue.class));
                                break;

                            case R.id.menu_vehiclesegment:
                                startActivity(new Intent(SubCatagoryFilter.this, VehicleSegment.class));
                                break;

                            case R.id.menu_login:
                                SharedPreferences preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
                                if(preferences.getBoolean("salesLogin",false)) {
                                    if(preferences.getString("userType","").equals("SalesExecutive"))
                                    {
                                        Intent i = new Intent(SubCatagoryFilter.this, SalesMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                    else {
                                        Intent i = new Intent(SubCatagoryFilter.this, RegionalMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                }
                                else {
                                    Intent i = new Intent(SubCatagoryFilter.this, SalesLogin.class);
                                    startActivity(i);
                                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                }


                                break;

                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.aboutusmenu);
                pop.show();
            }
        });

        checkInternet();


    }

    private void checkInternet() {
        if(networkConnection.CheckInternet()){
            if(category.equals("Main"))
                ValidatePart();
            else {
                if(TextUtils.isEmpty(subCategory) && TextUtils.isEmpty(segment))
                {
//                    subCat.setText("All");
                    getFilterDetails();
                }
                else if(subCategory.equals("All Make Filter")){
//                    subCat.setText("ALL MAKE FILTER");
                    getFilterDetails();
                }
                else {
//                    subCat.setText(segment);

                    getOEList();
                }
//                getFilterDetails();
            }
        }else {
            Toast.makeText(getApplicationContext(),"Please check your network connection and try again!",Toast.LENGTH_SHORT).show();
        }
    }

    private void getOEList()
    {
        final ProgressDialog progressDialog = new ProgressDialog(SubCatagoryFilter.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIService service = RetroClient.getApiService();
        Call<OEModelList> call=service.oEBasedModel(segment);
        call.enqueue(new Callback<OEModelList>() {
            @Override
            public void onResponse(Call<OEModelList> call, Response<OEModelList> response) {
                progressDialog.dismiss();
                try{
                    if(response.body().getResult().equals("Success")){
                        allMakeLayout.setVisibility(View.GONE);
//                        oElist.setVisibility(View.GONE);
                        modelListView.setVisibility(View.VISIBLE);
                        modelList=response.body().getData();
                        filtersSegmentAdpater=new FiltersSegmentAdpater(SubCatagoryFilter.this,response.body().getData());
                        modelListView.setAdapter(filtersSegmentAdpater);
                    }
                    else {
                        showAlertbox.showAlertboxnegative("No data found please try again later");
                    }

                }catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                    showAlertbox.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            }

            @Override
            public void onFailure(Call<OEModelList> call, Throwable t) {
                progressDialog.dismiss();
                showAlertbox.showAlertboxnegative("Something went wrong . Please try again later .");
            }
        });
    }

    private void getFilterDetails()
    {
        final ProgressDialog progressDialog = new ProgressDialog(SubCatagoryFilter.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIService service = RetroClient.getApiService();
        Call<FilterDetail> call=service.filterList(subCategory,segment,mainCategory_type);
        call.enqueue(new Callback<FilterDetail>() {
            @Override
            public void onResponse(Call<FilterDetail> call, Response<FilterDetail> response) {
                progressDialog.dismiss();
                try{
                    if(response.body().getResult().equals("Success")){
                        filterDetailsAdapter=new FilterListAdapter(SubCatagoryFilter.this,response.body().getData());
                        listView.setAdapter(filterDetailsAdapter);
                    }
                    else {
                        showAlertbox.showAlertboxnegative("No record found please try again later");
                    }

                }catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                    showAlertbox.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            }

            @Override
            public void onFailure(Call<FilterDetail> call, Throwable t) {
                progressDialog.dismiss();
                showAlertbox.showAlertboxnegative("Something went wrong . Please try again later .");
            }
        });
    }

    private void ValidatePart() {
        final ProgressDialog progressDialog = new ProgressDialog(SubCatagoryFilter.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {

            APIService service = RetroClient.getApiService();
            Call<FilterDetail> call = service.PRODUCT_DETAILS_DATA_CALL(mainCategory_type,subCategory);
            call.enqueue(new Callback<FilterDetail>() {
                @Override
                public void onResponse(Call<FilterDetail> call, Response<FilterDetail> response) {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("success")) {
                        Data1=response.body().getData();
                        productDetailsAdapter=new ProductDetailsAdapter(SubCatagoryFilter.this,Data1);

                        listView.setAdapter(productDetailsAdapter);
                    } else {
//                        showAlertbox.showAlertboxnegative("No Rejecte-Rating is Found");
                        showAlertbox.showAlertboxnegative("No data found please try again later");
//                        toasts.ShowErrorToast("No FeedBack is Found ");
                        // Toast.makeText(getApplicationContext(), "No Rejected-Rating is Found", Toast.LENGTH_LONG).show();
//                        onBackPressed();

                    }
                }

                @Override
                public void onFailure(Call<FilterDetail> call, Throwable t) {
                    toasts.ShowErrorToast("Something went wrong . Please try again later .");
                    // Toast.makeText(getApplicationContext(), "No RajectRating is Found", Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            progressDialog.dismiss();
            e.printStackTrace();
            toasts.ShowErrorToast("Something went wrong . Please try again later .");
        }
    }

    private void searchText(String newText)
    {

    }
}
