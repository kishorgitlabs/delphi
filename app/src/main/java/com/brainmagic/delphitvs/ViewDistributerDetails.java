package com.brainmagic.delphitvs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.regionalmanager.RegionalMainmenu;
import com.brainmagic.delphitvs.salesactivities.SalesMainmenu;

import java.util.List;

import adapter.DistributorAdapter;
import model.distributorlist.DistributorData;

public class ViewDistributerDetails extends AppCompatActivity {

    private ImageView back,home,menu;
    private Animation animation;
    private ListView distributorlist;
    private DistributorAdapter distributorAdapter;
    private List<DistributorData> distributorData;
    private TextView region_dis,city_dis;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_distributer_details);
        distributorData=(List<DistributorData>) getIntent().getSerializableExtra("NetworkData");
        menu = (ImageView)findViewById(R.id.menu);
        back = (ImageView)findViewById(R.id.back);
        distributorlist=findViewById(R.id.distributorlist);
        region_dis=findViewById(R.id.region_dis);
        city_dis=findViewById(R.id.city_network);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bounce);

        region_dis.setText(getIntent().getStringExtra("region"));
        city_dis.setText(getIntent().getStringExtra("city"));

        distributorAdapter=new DistributorAdapter(ViewDistributerDetails.this,distributorData);
        distributorlist.setAdapter(distributorAdapter);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu.startAnimation(animation);
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(ViewDistributerDetails.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_home:
                                startActivity(new Intent(ViewDistributerDetails.this, MainActivity.class));
                                break;
                            case R.id.menu_aboutus:
                                startActivity(new Intent(ViewDistributerDetails.this, Aboutus.class));
                                break;
                            case R.id.menu_pricelist:
                                startActivity(new Intent(ViewDistributerDetails.this, PriceList.class));
                                break;
                            case R.id.menu_general:
                                startActivity(new Intent(ViewDistributerDetails.this, General.class));
                                break;
                            case R.id.menu_productcatalogue:
                                startActivity(new Intent(ViewDistributerDetails.this, ProductCatalogue.class));
                                break;

                            case R.id.menu_vehiclesegment:
                                startActivity(new Intent(ViewDistributerDetails.this, VehicleSegment.class));
                                break;

                            case R.id.menu_login:
                                SharedPreferences preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
                                if(preferences.getBoolean("salesLogin",false)) {
                                    if(preferences.getString("userType","").equals("SalesExecutive"))
                                    {
                                        Intent i = new Intent(ViewDistributerDetails.this, SalesMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                    else {
                                        Intent i = new Intent(ViewDistributerDetails.this, RegionalMainmenu.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    }
                                }
                                else {
                                    Intent i = new Intent(ViewDistributerDetails.this, SalesLogin.class);
                                    startActivity(i);
                                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                }


                                break;
                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.aboutusmenu);
                pop.show();
            }
        });

    }
}
