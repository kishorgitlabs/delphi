package com.brainmagic.delphitvs.fragment;

import android.content.Context;
import android.os.Bundle;



import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.brainmagic.delphitvs.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import io.reactivex.annotations.NonNull;


public class NetworkBottomSheetNavigation extends BottomSheetDialogFragment {

    private BottomSheetListener bottomSheetListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.network_alert, container,false);
        LinearLayout dealer = (LinearLayout) view.findViewById(R.id.servicedeler);
        LinearLayout distibuter = (LinearLayout) view.findViewById(R.id.didtibuter);
        LinearLayout delphiContacts = (LinearLayout) view.findViewById(R.id.delphi_contacts);

                dealer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                           bottomSheetListener.bottomSheetListener("Dealer");

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    }
                });
                distibuter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            bottomSheetListener.bottomSheetListener("Distributor");

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();


                        }
                    }
                });

        delphiContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    bottomSheetListener.bottomSheetListener("Delphi Contacts");

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();


                }
            }
        });


        return view;
    }

    public interface BottomSheetListener
    {
         void bottomSheetListener(String orderType);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            bottomSheetListener= (BottomSheetListener) context;
        }catch (Exception e)
        {

            e.printStackTrace();
        }
    }
}
