package com.brainmagic.delphitvs.regionalmanager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;





import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.service.BGServicenormalRegional;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.net.ssl.HttpsURLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import alert.Alert;
import io.reactivex.annotations.NonNull;
import model.api.APIService;
import model.api.RetroClient;
import model.sales.travelhistory.travelwithhistoryresult.UploadCoordinatesRegional;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import room.AppDatabase;

import static model.api.RetroClient.APIKEY;
public class AttendanceActiveRegional extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        android.location.LocationListener {
    private static final String TAG = "AttendanceActivitySales";
    public final String ACTION_START_FOREGROUND_SERVICE = "ACTION_START_FOREGROUND_SERVICE";
    public final String ACTION_STOP_FOREGROUND_SERVICE = "ACTION_STOP_FOREGROUND_SERVICE";
    private boolean permissionGranted = true;
    private LocationRequest mLocationRequest;
    private Alert box;
    private Location lastLocation;
    private String[] locationAccess = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.INTERNET};
    private GoogleApiClient googleApiClient;
    private WindowManager manager;
    private LocationManager locationManager;
    private SharedPreferences.Editor editor;
    private SharedPreferences preferences;
    private String manufacturer;
    private int IGNORE_BATTERY_OPTIMIZATION_REQUEST = 1003;
    private int REQUEST_CHECK_SETTINGS=100;
    protected final int REQUEST_LOCATION_SETTINGS = 300;
    protected final int TWO_MINUTES = 2* 60 * 1000;
    protected int MIN_DISTANCE_CHANGE_FOR_UPDATES=500;
    protected static final int RESULT_LOCATION_CODE_CANCELLED = 0;
    private String address ,attendanceDate="",currentDate;
    private Button attendancebtn;
    private boolean isCheckedIn = false;
    private TextView addressTextView;
    private ProgressDialog progress;
    private Location myLocation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);

        box = new Alert(this);
        manufacturer = Build.MANUFACTURER;
        preferences = getSharedPreferences("Delphi", MODE_PRIVATE);
        editor = preferences.edit();
        myLocation=new Location(LocationManager.GPS_PROVIDER);
        locationManager=(LocationManager)this.getSystemService(LOCATION_SERVICE);
        TextView sName=findViewById(R.id.s_name);
        TextView designation = (TextView) findViewById(R.id.designation);
        addressTextView = (TextView) findViewById(R.id.address);
        attendancebtn = (Button) findViewById(R.id.attendancebtn);
        ImageView menu =(ImageView)findViewById(R.id.menu);
        ImageView img_back =(ImageView)findViewById(R.id.back);

        isCheckedIn = preferences.getBoolean("isCheckedIn", isCheckedIn);
        progress = new ProgressDialog(this);
        progress.setMessage("Loading...");
        progress.setTitle("Retrieving your Location Details");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        sName.setText(preferences.getString("empName", ""));
        designation.setText(preferences.getString("userType", ""));

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        currentDate=df.format(c.getTime());

        attendanceDate=preferences.getString("attendanceDate","");

        if (attendanceDate.equals(currentDate)) {
            //isCheckedIn is to check whether the user clicked 'In time' or 'Out Time'
            if (!isCheckedIn) {
                attendancebtn.setText("In Time");
            }
            else {
                attendancebtn.setText("Out Time");
            }
        }
        else {
            attendancebtn.setText("In Time");
            isCheckedIn = false;
            editor.putBoolean("isCheckedIn", isCheckedIn);
            editor.commit();
        }
        attendancebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(addressTextView.getText())) {
                    Alert alert=new Alert(AttendanceActiveRegional.this);
                    alert.showAlertbox("No Address Found");
                } else {
                    try {
                        checkinternet();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        NetworkConnection connection = new NetworkConnection(this);
        if (connection.CheckInternet()){
            init();
        }
        else {
            box.showAlertboxnegative("No Internet Connection");
        }
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(AttendanceActiveRegional.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.regional_home:
                                startActivity(new Intent(AttendanceActiveRegional.this, MainActivity.class));
                                break;

                            case R.id.regional_mark_attendance:
//                                startActivity(new Intent(AttendanceHistoryRegional.this, AttendanceActivityRegional.class));
                                break;

                            case R.id.regional_view_attendance:
                                startActivity(new Intent(AttendanceActiveRegional.this, AttendanceViewActivityRegional.class));
                                break;
                            case R.id.regional_view_visitreport:
                                startActivity(new Intent(AttendanceActiveRegional.this, ViewVisitReportRegional.class));
                                break;

                            case R.id.regional_logout:
                                editor.putBoolean("salesLogin", false);
                                editor.commit();
                                startActivity(new Intent(AttendanceActiveRegional.this, MainActivity.class));
                                break;

                            case R.id.regional_change_password:
                                String phone = preferences.getString("empMobile", "");
                                final Alert alert = new Alert(AttendanceActiveRegional.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;

                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.regional_manager_menu);
                pop.show();
            }
        });    }

    private void getAutoStartPermission(final Intent intent){
        Alert alert=new Alert(AttendanceActiveRegional.this);
        alert.showAlertBoxWithListener("Please enable autostart option to use this Application efficiently",View.GONE);
        alert.setOnPositiveClickListener(new Alert.onPositiveClickListener() {
            @Override
            public void onPositiveClick() {
                editor.putBoolean("autoStart",false);
                editor.commit();
                startActivity(intent);
            }
        });
    }
    private void checkinternet() throws ParseException {
        NetworkConnection networkConnection = new NetworkConnection(AttendanceActiveRegional.this);
        if (networkConnection.CheckInternet()) {
            markAttendance();
        } else {
            Alert alert=new Alert(AttendanceActiveRegional.this);
            alert.showAlertbox("Please Check your Internet Connection !!");
        }
    }
    private void markAttendance() throws ParseException {
        attendancebtn.setVisibility(View.GONE);
        if (isCheckedIn) {
//            new startAttendance().execute("stop", "");
            attendance("stop");
        } else {
//            new startAttendance().execute("start", "");
            attendance("start");
        }
        attendancebtn.setVisibility(View.VISIBLE);
    }
    private void attendance(final String attendance) throws ParseException {
        final ProgressDialog progressDialog = new ProgressDialog(AttendanceActiveRegional.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String date = df.format(c.getTime());

        Calendar cal = Calendar.getInstance();
        Date timeFormat = cal.getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("kk:mm:ss");
        final String attendanceTime =simpleDateFormat.format(timeFormat);//24 hours format

        SimpleDateFormat simpleDateFormats = new SimpleDateFormat("HH:mm a");
        final String currentTime =simpleDateFormats.format(timeFormat);//12 hours format

        SimpleDateFormat sdf_ = new SimpleDateFormat("EEEE");
        Date dateFormat = new Date();
        String day = sdf_.format(dateFormat);

        final String empCode=preferences.getString("empCode","");
        String empId=preferences.getString("empId","");
        String empName=preferences.getString("empName","");
        String userType=preferences.getString("userType","");

        try {
            APIService service = RetroClient.getApiService();
            Call<RegionalManagerPojo> call = null;
            AttendanceInOutRegional inOut=new AttendanceInOutRegional();
            inOut.setEmpId(empId);
            inOut.setEmpCode(empCode);
            inOut.setName(empName);
            inOut.setDate(date);
            inOut.setAttendDay(day);
            inOut.setDesignation(userType);
            final AppDatabase appDatabase=AppDatabase.getAppDatabase(AttendanceActiveRegional.this);
            if(attendance.equals("start"))
            {
                List<UploadCoordinatesRegional> coordinate=new ArrayList<>();
                inOut.setAttendanceTime(attendanceTime);
                inOut.setInLatitude(String.valueOf(myLocation.getLatitude()));
                inOut.setInLongitude(String.valueOf(myLocation.getLongitude()));
                inOut.setAddress(address);
                inOut.setSetCoordinateList(coordinate);
                call=service.regionalattendance(inOut);
            }
            else {
                List<UploadCoordinatesRegional> coordinates=appDatabase.ProductsDAO().getCoordinate();
                inOut.setAttendanceTime(attendanceTime);
                inOut.setOutLatitude(String.valueOf(myLocation.getLatitude()));
                inOut.setOutLongitude(String.valueOf(myLocation.getLongitude()));
                inOut.setOutAddress(address);
                inOut.setSetCoordinateList(coordinates);
                call=service.regionalattendance(inOut);
            }
            call.enqueue(new Callback<RegionalManagerPojo>() {
                @Override
                public void onResponse(Call<RegionalManagerPojo> call, Response<RegionalManagerPojo> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.body().getResult().equals("Success"))
                        {
                            if(attendance.equals("start"))
                            {
                                inflateAlertBox(currentTime,"InTime");
                                isCheckedIn=true;
                                attendancebtn.setText("Out Time");
                                editor.putString("attendanceDate",currentDate);
                                editor.putLong("trackId",response.body().getData().getId());
                                editor.putString("latitude", String.valueOf(myLocation.getLatitude()));
                                editor.putString("longitude", String.valueOf(myLocation.getLongitude()));
                                editor.putBoolean("isCheckedIn",isCheckedIn);
                                editor.commit();
                                ContextCompat.startForegroundService(AttendanceActiveRegional.this,new Intent(AttendanceActiveRegional.this, BGServicenormalRegional.class).setAction(ACTION_START_FOREGROUND_SERVICE));
                            }
                            else {
                                inflateAlertBox(currentTime,"OutTime");
                                stopService(new Intent(AttendanceActiveRegional.this, BGServicenormalRegional.class));
                                isCheckedIn=false;
                                editor.putString("attendanceDate","");
                                attendancebtn.setText("In Time");
                                editor.putBoolean("isCheckedIn",isCheckedIn);
                                editor.commit();
                                appDatabase.ProductsDAO().deleteAttendances();
                            }
                        }
                        else {
                            Alert alert=new Alert(AttendanceActiveRegional.this);
                            alert.showAlertbox("Something went wrong . Please try again later .");
                        }
                    }catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        Alert alert=new Alert(AttendanceActiveRegional.this);
                        alert.showAlertbox("Something went wrong . Please try again later .");
                    }
                }
                @Override
                public void onFailure(Call< RegionalManagerPojo > call, Throwable t) {
                    progressDialog.dismiss();
                    Alert alert=new Alert(AttendanceActiveRegional.this);
                    alert.showAlertbox("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
            Alert alert=new Alert(AttendanceActiveRegional.this);
            alert.showAlertbox("Local MarkAttendanceResult is Corrupted. Please logout and login.");
        }
    }
    private void inflateAlertBox(String formattedtime,String attendanceInfo) {
        try {
            Alert alert=new Alert(AttendanceActiveRegional.this);
            alert.showAlertBoxWithListener("Your Current Time " + formattedtime + " is marked as your "+ attendanceInfo +" Attendance!",View.GONE);
            alert.setOnPositiveClickListener(new Alert.onPositiveClickListener() {
                @Override
                public void onPositiveClick() {
                    Intent i = new Intent(AttendanceActiveRegional.this, MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    private class GeocodeAsyncTask extends AsyncTask<Double, Void, Address> {
        String errorMessage = "";
        @SuppressLint("LongLogTag")
        @Override
        protected Address doInBackground(Double... latlang) {
            Geocoder geocoder = new Geocoder(AttendanceActiveRegional.this, Locale.getDefault());
            List<Address> addresses = null;
            if (geocoder.isPresent()) {
                try {
                    addresses = geocoder.getFromLocation(latlang[0], latlang[1], 1);
                    Log.d(TAG, "doInBackground: ************");
                } catch (IOException ioException) {
                    errorMessage = "Service Not Available";
                    Log.e(TAG, errorMessage, ioException);
                } catch (IllegalArgumentException illegalArgumentException) {
                    errorMessage = "Invalid Latitude or Longitude Used";
                    Log.e(TAG, errorMessage + ". " +
                            "Latitude = " + latlang[0] + ", Longitude = " +
                            latlang[1], illegalArgumentException);
                }
                if (addresses != null && addresses.size() > 0)
                    return addresses.get(0);
            } else {
                new GetGeoCodeAPIAsynchTask().execute(latlang[0], latlang[1]);
            }
            return null;
        }
        @SuppressLint("LongLogTag")
        protected void onPostExecute(Address addresss) {
            if (addresss == null) {
                new GetGeoCodeAPIAsynchTask().execute(myLocation.getLatitude(), myLocation.getLongitude());
                Log.d(TAG, "onPostExecute: *****");
            } else {
                address = addresss.getAddressLine(0);
                editor.putString("ToAddress", address);
                editor.commit();
                editor.apply();
                String City = addresss.getLocality();
                Log.d(TAG, "onPostExecute: **************************" + City);
                String city = addresss.getLocality();
                String state = addresss.getAdminArea();
                //create your custom title
                String title = city + "-" + state;
                addressTextView.setText(address +
                        "\n"
                        + title);
                editor.putString("FromAddress", address);
                Geocoder geocoder = new Geocoder(AttendanceActiveRegional.this);
                try {
                    ArrayList<Address> addresses = (ArrayList<Address>) geocoder.getFromLocationName("karur", 50);
                    for (Address address3 : addresses) {
                        double lat = address3.getLatitude();
                        double lon = address3.getLongitude();
                    }
                } catch (IOException e) {
                    e.printStackTrace();

                }
            }
        }
    }
    private class GetGeoCodeAPIAsynchTask extends AsyncTask<Double, Void, String[]> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected String[] doInBackground(Double... latlang) {
            String response;
            try {
                String URL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latlang[0] + "," + latlang[1] + "&key=" + APIKEY;
                Log.v("URL", URL);
                response = getLatLongByURL(URL);
                return new String[]{response};
            } catch (Exception e) {
                return new String[]{"error"};
            }
        }
        @SuppressLint("LongLogTag")
        @Override
        protected void onPostExecute(String... result) {
            try {
                JSONObject jsonObject = new JSONObject(result[0]);

                address = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(0).getString("long_name");

                editor.putString("FromAddress", address);
                editor.putString("ToAddress", address);
                editor.commit();
                editor.apply();
                String City = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(2).getString("long_name");

                String state = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(4).getString("long_name");

                Log.d(TAG, "onPostExecute: " + City);

                Log.d("Address", "" + address);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    public String getLatLongByURL(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public float getDistance(double lat1, double lon1, double lat2, double lon2) {
        float distance = 0f;
        if (Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        System.out.println(lat1 + " " + lon1 + " " + lat2 + " " + lon2);
        String url = "https://maps.googleapis.com/maps/api/directions/xml?origin=" + lat1 + "," + lon1 + "&destination=" + lat2 + "," + lon2 + "&key=" + APIKEY + "";

        String tag[] = {"value"};  //will give distance as string e.g 1.2 km
        // or tag[] = {"value"} if you want to get distance in metre e.g. 1234
        Log.i("URL display == ", url);
        String address[] = {"end_address"};
        HttpResponse response = null;
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();
            HttpPost httpPost = new HttpPost(url);
            response = httpClient.execute(httpPost, localContext);
            InputStream is = response.getEntity().getContent();
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(is);

            if (doc != null) {
                NodeList nl, n2;
                ArrayList args = new ArrayList();
                ArrayList args1 = new ArrayList();
                for (String s : tag) {
                    nl = doc.getElementsByTagName(s);
                    if (nl.getLength() > 0) {
                        Node node = nl.item(nl.getLength() - 1);
                        args.add(node.getTextContent());
                    } else {
                        args.add("0");
                    }
                    for (String s1 : address) {
                        n2 = doc.getElementsByTagName(s1);
                        if (n2.getLength() > 0) {
                            Node node = n2.item(n2.getLength() - 1);
                            args1.add(node.getTextContent());
                        } else {
                            args1.add("no address");
                        }
                    }
                }
                String dis = String.format("%s", args.get(0));
                distance = Float.parseFloat(dis) / 1000;
                Log.d(TAG, "getDistance: Distance"+distance);
//                    end_address = args1.get(0).toString().replace("'", "");
            } else {
                System.out.print("Doc is null");
                Log.d(TAG, "getDistance: Doc is null ");

                if (lat1 != 0.0) {
                    Location locationA = new Location("point A");
                    locationA.setLatitude(lat1);
                    locationA.setLongitude(lon1);
                    Location locationB = new Location("point B");
                    locationB.setLatitude(lat2);
                    locationB.setLongitude(lon2);
                    distance = locationA.distanceTo(locationB) / 1000;
                    Log.d(TAG, "getDistance: small distance "+distance);
                }
            }
            if (distance == 0) {
                Log.d(TAG, "getDistance: inside distance is zero");
                if (lat1 != 0.0) {
                    Log.d(TAG, "getDistance: inside lat not one");
                    Location locationA = new Location("point A");
                    locationA.setLatitude(lat1);
                    locationA.setLongitude(lon1);
                    Location locationB = new Location("point B");
                    locationB.setLatitude(lat2);
                    locationB.setLongitude(lon2);
                    distance = locationA.distanceTo(locationB) / 1000;
                }
            }
        } catch (Exception e) {
            Log.d(TAG, "getDistance: inside exception in getDistance");
            e.printStackTrace();
        }
        try {
            DecimalFormat df = new DecimalFormat();
            df.setMaximumFractionDigits(3);
            Log.d("distance from google **", Float.toString(distance));
            distance = Float.parseFloat(df.format(distance));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return distance;
    }
    private void getAutoStartForApp() {
        try {
            Intent intent = new Intent();
            if ("xiaomi".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));

            } else if ("oppo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity"));

            } else if ("vivo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"));

            } else if ("Letv".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity"));

            } else if ("Honor".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity"));

            }  else if ("realme".equalsIgnoreCase(manufacturer)) {

                intent.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity"));

            } else if ("asus".equalsIgnoreCase(manufacturer)) {

                intent.setComponent(new ComponentName("com.asus.mobilemanager", "com.asus.mobilemanager.powersaver.PowerSaverSettings"));

            }else if ("nokia".equalsIgnoreCase(manufacturer)) {

                intent.setComponent(new ComponentName("com.evenwell.powersaving.g3", "com.evenwell.powersaving.g3.exception.PowerSaverExceptionActivity"));
            }
            else {
            }
            List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            if (list.size() > 0) {
                getAutoStartPermission(intent);
            }

        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    private void init() {
        if (getServicesAvailable()) {
            if (checkPermission()) {
//                if(googleApiClient==null)
                createGoogleApi();
//                if(mLocationRequest==null)
                createLocationRequest();
                if (lastLocation == null)
                    lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                enableGPS();
            } else {
                initPermission();
            }
//                getLastKnownLocation();
        }
    }
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        progress.show();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
        }
        if (lastLocation == null)
            lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
//        createLocationRequest();
        if (isGPSEnabled()) {
            if (lastLocation != null) {
                Log.i(TAG, "LasKnown location. " +
                        "Long: " + lastLocation.getLongitude() +
                        " | Lat: " + lastLocation.getLatitude());
//                writeLastLocation();
//                createLocationRequest();//no need location gps
                setFusedLocationUpdate();
//                        animateCameraInMap(new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude()));
            }
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (googleApiClient != null) {
//            if(!googleApiClient.isConnected())
            googleApiClient.connect();
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        if (googleApiClient != null)
            if (googleApiClient.isConnected())
                googleApiClient.disconnect();
    }

    public boolean getServicesAvailable() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (api.isUserResolvableError(isAvailable)) {
            Dialog dialog = api.getErrorDialog(this, isAvailable, 0);
            dialog.show();
            return false;
        } else {
            Toast.makeText(this, "Cannot Connect To Play Services", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private void initPermission() {
        if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(AttendanceActiveRegional.this, locationAccess[0]) ||
                PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(AttendanceActiveRegional.this, locationAccess[1]) ||
                PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(AttendanceActiveRegional.this, locationAccess[2]) ||
                PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(AttendanceActiveRegional.this, locationAccess[3])
        ) {
            ActivityCompat.requestPermissions(AttendanceActiveRegional.this, locationAccess, REQUEST_CHECK_SETTINGS);
        } else {
            permissionGranted = true;
            init();
//            setMDM();
//            startLocationUpdate();
        }
    }
    private boolean isGPSEnabled() {
//        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        return gps_enabled;
    }

    private void enableGPS() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
//                        setMDM();
                        setFusedLocationUpdate();
                        if(preferences.getBoolean("autoStart",true))
                        {
                            getAutoStartForApp();
                        }
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(
                                    AttendanceActiveRegional.this,
                                    REQUEST_LOCATION_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            Log.e(TAG, "Exception : " + e.getMessage());
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.e(TAG, "Location settings are not satisfied.");
                        break;
                }
            }
        });
    }
    private synchronized void createGoogleApi() {
        Log.d(TAG, "createGoogleApi()");
        {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(AttendanceActiveRegional.this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(100);
        mLocationRequest.setFastestInterval(100);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(10);
    }
    @Override
    public void onRequestPermissionsResult(final int requestCode, final String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            int len = permissions.length;
            int permissionIndex=-1;
            for (int i = 0; i < len; i++) {
                if (ActivityCompat.checkSelfPermission(AttendanceActiveRegional.this, permissions[i]) != PackageManager.PERMISSION_GRANTED) {
                    permissionGranted = false;
                    permissionIndex=i;
                }
            }
            if (permissionGranted) {
                permissionGranted = true;
//                setMDM();
                init();
            } else {
                if(permissionIndex==0 || permissionIndex==1)
                    invokeAlert("You did not give permission to access your Location. Do want to exit", permissions, requestCode);
                else invokeAlert("You did not give permission to access your Storage. Do want to exit", permissions, requestCode);
            }
        }
    }
    private void requestManualPermission(String[] permissions, int requestCode) {
        ActivityCompat.requestPermissions(this, permissions, requestCode);
    }
    // Check for permission to access Location
    private boolean checkPermission() {
        Log.d(TAG, "checkPermission()");
        // Ask for permission if it wasn't granted yet
        return (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED);
    }
    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged: ");

        if(progress!=null)
            if(progress.isShowing())
                progress.dismiss();
        // check_button.setVisibility(View.VISIBLE);
        if (location != null) {
            myLocation.setLatitude(location.getLatitude());
            myLocation.setLongitude(location.getLongitude());
            editor.putString("latitude", myLocation.getLatitude()+"");
            editor.putString("longitude", myLocation.getLongitude()+"");
            Log.d(TAG, "onLocationChanged: " + myLocation.getLatitude() + " long " + myLocation.getLongitude());
            editor.commit();
            new GeocodeAsyncTask().execute(myLocation.getLatitude(), myLocation.getLongitude());
            //Or Do whatever you want with your location
        } else if (myLocation != null) {
            editor.putString("latitude", myLocation.getLatitude()+"");
            editor.putString("longitude", myLocation.getLongitude()+"");
            Log.d(TAG, "onLocationChanged: " + myLocation.getLatitude() + " long " + myLocation.getLongitude());
            editor.commit();
            new GeocodeAsyncTask().execute(myLocation.getLatitude(), myLocation.getLongitude());
        }
    }
    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) {
    }

    @Override
    public void onProviderDisabled(String s) {
    }
    private void setFusedLocationUpdate() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
        }
        locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER,
                TWO_MINUTES,
                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
        if (googleApiClient.isConnected())
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, mLocationRequest, this);
    }
    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }
    private void invokeAlert(String msg, final String[] permissions, final int requestCode) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(AttendanceActiveRegional.this);
        dialog.setTitle("Location");
        dialog.setMessage(msg);
        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                requestManualPermission(permissions, requestCode);
                permissionGranted = true;
            }
        });
        dialog.show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: ");
        if (googleApiClient != null)
            if (googleApiClient.isConnected()) {
                googleApiClient.disconnect();
            }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_LOCATION_SETTINGS) {
            if (resultCode == RESULT_LOCATION_CODE_CANCELLED) {

                Alert alert=new Alert(AttendanceActiveRegional.this);
                alert.showAlertBoxWithListener("You cannot use this Application without turning ON your GPS",View.VISIBLE);
                alert.setOnPositiveClickListener(new Alert.onPositiveClickListener() {
                    @Override
                    public void onPositiveClick() {
                        enableGPS();
                    }
                });
                alert.setOnNegativeClickListener(new Alert.onNegativeClickListener() {
                    @Override
                    public void onPositiveClick() {
                        finish();
                    }
                });
            } else {
//                setMDM();
                setFusedLocationUpdate();
                getAutoStartForApp();
            }
        }
    }
}
