package com.brainmagic.delphitvs.regionalmanager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;

import java.util.List;

import adapter.sales.AttendanceHistoryRegionalAdapter;
import adapter.sales.AttendanceViewRegionalAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.regional.salesnamelist.DataRegionalInOutTime;
import model.regional.salesnamelist.RegionalAttendanceView;
import model.regional.salesnamelist.RegionalInOutTime;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AttendanceHistoryRegional extends AppCompatActivity {
private ListView regional_attendance_history_list;
private AttendanceHistoryRegionalAdapter adapter;
private ImageView regiona_back,regional_menu;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private String regional_from_date,regiona_from_day;
    private TextView attendance_history_date,attendance_history_day;
    private List<DataRegionalInOutTime> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_attendancd_history);


        preferences = getSharedPreferences("Delphi", MODE_PRIVATE);
        editor = preferences.edit();
        regiona_back=findViewById(R.id.regional_back);
        regional_menu=findViewById(R.id.regional_menu);
        regional_attendance_history_list= (ListView)findViewById(R.id.regional_attendance_history_list);
        regional_from_date=getIntent().getStringExtra("regional_date");
        regiona_from_day=getIntent().getStringExtra("regional_day");
        attendance_history_date=findViewById(R.id.attendance_history_date);
        attendance_history_day=findViewById(R.id.attendance_history_day);

        attendance_history_date.setText(regional_from_date);
        attendance_history_day.setText(regiona_from_day);

        NetworkConnection con=new NetworkConnection(AttendanceHistoryRegional.this);
        if(con.CheckInternet()){
            viewregionalattendance();
        }
        else{
            Alert alert=new Alert(AttendanceHistoryRegional.this);
            alert.showAlertboxnegative("Please check your network connection and try again!");

        }
        regiona_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });

        regional_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final PopupMenu pop = new PopupMenu(AttendanceHistoryRegional.this, view);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu popupMenu) {
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {

                            case R.id.regional_home:
                                startActivity(new Intent(AttendanceHistoryRegional.this, MainActivity.class));
                                break;

                            case R.id.regional_mark_attendance:
                                startActivity(new Intent(AttendanceHistoryRegional.this, AttendanceActiveRegional.class));
                                break;

                            case R.id.regional_view_attendance:
                                startActivity(new Intent(AttendanceHistoryRegional.this, AttendanceViewActivityRegional.class));
                                break;
                            case R.id.regional_view_visitreport:
                                startActivity(new Intent(AttendanceHistoryRegional.this, ViewVisitReportRegional.class));
                                break;

                            case R.id.regional_logout:
                                editor.putBoolean("salesLogin", false);
                                editor.commit();
                                startActivity(new Intent(AttendanceHistoryRegional.this, MainActivity.class));
                                break;

                            case R.id.regional_change_password:
                                String phone = preferences.getString("empMobile", "");
                                final Alert alert = new Alert(AttendanceHistoryRegional.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;

                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.regional_manager_menu);
                pop.show();
            }
        });

    }

    private void viewregionalattendance(){


        final ProgressDialog progressDialog = new ProgressDialog(AttendanceHistoryRegional.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();


        try {

            APIService service = RetroClient.getApiService();
            Call<RegionalInOutTime> call = service.getinandouttime(preferences.getString("empId",""),regional_from_date);


            call.enqueue(new Callback<RegionalInOutTime>() {
                @Override
                public void onResponse(Call<RegionalInOutTime> call, Response<RegionalInOutTime> response) {
                    progressDialog.dismiss();

                    try {

                        if(response.isSuccessful()){
                            if(response.body().getResult().equals("Success")){

                             data=response.body().getdata();
                             adapter=new AttendanceHistoryRegionalAdapter(AttendanceHistoryRegional.this,data);
                                regional_attendance_history_list.setAdapter(adapter);
                            }
                            else {

                                Alert alert = new Alert(AttendanceHistoryRegional.this);
                                alert.showAlertboxnegative("No Record Found");
                            }
                        }

                    }
                    catch (Exception e){
                        e.printStackTrace();
                        Alert alert=new Alert(AttendanceHistoryRegional.this);
                        alert.showAlertboxnegative("Something went wrong . Please try again later .");
                    }
                }

                @Override
                public void onFailure(Call<RegionalInOutTime> call, Throwable t) {

                    progressDialog.dismiss();
                    Alert alert=new Alert(AttendanceHistoryRegional.this);
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });

        }
        catch (Exception e){

            e.printStackTrace();
            Alert alert=new Alert(AttendanceHistoryRegional.this);
            alert.showAlertboxnegative("Something went wrong . Please try again later .");

        }

regional_attendance_history_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        String[] dates=data.get(i).getDate().split("T");
        Intent intent=new Intent(AttendanceHistoryRegional.this,AttendanceTravelHistoryRegional.class);
        intent.putExtra("regional_name",data.get(i).getRegName());
        intent.putExtra("regional_id",data.get(i).getRegid());
        intent.putExtra("regional_trackid",data.get(i).getId());
        intent.putExtra("regional_date",dates[0]);
        intent.putExtra("regional_inaddress",data.get(i).getAddress());
        intent.putExtra("regional_distance",data.get(i).getDistance());
        intent.putExtra("regional_intime",data.get(i).getInTime());
        intent.putExtra("regional_outtime",data.get(i).getOutTime());
        intent.putExtra("regional_outaddres",data.get(i).getOutAddress());
        startActivity(intent);



    }
});
    }

}
