package com.brainmagic.delphitvs.regionalmanager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.salesactivities.ViewAttendanceCoordinateActivitySales;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import adapter.sales.AttendanceTravelHistoryRegionalAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.regional.salesnamelist.DataRegionalTimeHistoryPojo;
import model.regional.salesnamelist.ResponseRegionalTimeHistoryPojo;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AttendanceTravelHistoryRegional extends AppCompatActivity {


    private ListView regional_travel_history;
    private ImageView regional_back,regional_menu;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private TextView sales_in_time,sales_out_time;
    private String region_intime,region_outtime,region_trackid,region_empid,region_date,region_in_address,region_out_address,trackid;
    private List<DataRegionalTimeHistoryPojo> data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_attendance_coordinate);


        region_intime=getIntent().getStringExtra("regional_intime");
        region_outtime=getIntent().getStringExtra("regional_outtime");
        region_trackid=getIntent().getStringExtra("regional_trackid");
        region_empid=getIntent().getStringExtra("regional_id");
        region_date=getIntent().getStringExtra("regional_date");
        region_in_address=getIntent().getStringExtra("regional_inaddress");
        region_out_address=getIntent().getStringExtra("regional_outaddres");

        preferences = getSharedPreferences("Delphi", MODE_PRIVATE);
        editor = preferences.edit();
        regional_menu=findViewById(R.id.regional_menu);
       regional_back=findViewById(R.id.regional_back);
       regional_travel_history= (ListView)findViewById(R.id.regional_travel_history);
        sales_in_time=findViewById(R.id.sales_in_time);
        sales_out_time=findViewById(R.id.sales_out_time);

sales_in_time.setText(region_intime);
sales_out_time.setText(region_outtime);


        regional_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });

        regional_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final PopupMenu pop = new PopupMenu(AttendanceTravelHistoryRegional.this, view);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu popupMenu) {
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {

                            case R.id.regional_home:
                                startActivity(new Intent(AttendanceTravelHistoryRegional.this, MainActivity.class));
                                break;

                            case R.id.regional_mark_attendance:
                                startActivity(new Intent(AttendanceTravelHistoryRegional.this, AttendanceActiveRegional.class));
                                break;

                            case R.id.regional_view_attendance:
                                startActivity(new Intent(AttendanceTravelHistoryRegional.this, AttendanceViewActivityRegional.class));
                                break;
                            case R.id.regional_view_visitreport:
                                startActivity(new Intent(AttendanceTravelHistoryRegional.this, ViewVisitReportRegional.class));
                                break;

                            case R.id.regional_logout:
                                editor.putBoolean("salesLogin", false);
                                editor.commit();
                                startActivity(new Intent(AttendanceTravelHistoryRegional.this, MainActivity.class));
                                break;

                            case R.id.regional_change_password:
                                String phone = preferences.getString("empMobile", "");
                                final Alert alert = new Alert(AttendanceTravelHistoryRegional.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;

                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.regional_manager_menu);
                pop.show();
            }
        });

        NetworkConnection con=new NetworkConnection(AttendanceTravelHistoryRegional.this);
        if(con.CheckInternet()){
            getcoordinateslist();
        }
        else{
            Alert alert=new Alert(AttendanceTravelHistoryRegional.this);
            alert.showAlertboxnegative("Please check your network connection and try again!");

        }

    }

    private void getcoordinateslist()
    {
        final ProgressDialog progressDialog = new ProgressDialog(AttendanceTravelHistoryRegional.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {
            APIService service = RetroClient.getApiService();

            Call<ResponseRegionalTimeHistoryPojo> call = service.viewregionmap(region_empid,region_date,region_trackid);
            call.enqueue(new Callback<ResponseRegionalTimeHistoryPojo>() {
                @Override
                public void onResponse(Call<ResponseRegionalTimeHistoryPojo> call, Response<ResponseRegionalTimeHistoryPojo> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {

                                List<DataRegionalTimeHistoryPojo>  coordinates=response.body().getData();
                                DataRegionalTimeHistoryPojo viewSalesCoordinateResultIn=new DataRegionalTimeHistoryPojo();
                                try {
                                    final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                                    final Date dateObj = sdf.parse(region_intime);
                                    Log.d(ViewAttendanceCoordinateActivitySales.class.getName(), "onResponse: ");
                                    viewSalesCoordinateResultIn.setAddress(region_in_address);
                                    viewSalesCoordinateResultIn.setTimedate(new SimpleDateFormat("hh:mm aa").format(dateObj));
                                }catch (Exception e)
                                {
                                    e.printStackTrace();
                                }
                                coordinates.add(0,viewSalesCoordinateResultIn);
                                DataRegionalTimeHistoryPojo viewSalesCoordinateResultOut=new DataRegionalTimeHistoryPojo();
                                try{
                                    final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                                    final Date dateObj = sdf.parse(region_outtime);
                                    viewSalesCoordinateResultOut.setAddress(region_out_address);
                                    viewSalesCoordinateResultOut.setTimedate(new SimpleDateFormat("hh:mm a").format(dateObj));

                                }catch (Exception e)
                                {
                                    e.printStackTrace();
                                }
                               coordinates.add(coordinates.size(),viewSalesCoordinateResultOut);

                                AttendanceTravelHistoryRegionalAdapter viewAttendanceAdapterSales=new AttendanceTravelHistoryRegionalAdapter(AttendanceTravelHistoryRegional.this,response.body().getData());
                               regional_travel_history .setAdapter(viewAttendanceAdapterSales);
                            }
                            else if(response.body().getResult().equals("NotSuccess"))
                            {
                                Alert alert=new Alert(AttendanceTravelHistoryRegional.this);
                                alert.showAlertboxnegative("No Attendance Coordinate records Found");
                            }
                            else {
                                Alert alert=new Alert(AttendanceTravelHistoryRegional.this);
                                alert.showAlertboxnegative("Invalid Login. Please check your User Name and Password");
                            }
                        }
                        else {
                            Alert alert=new Alert(AttendanceTravelHistoryRegional.this);
                            alert.showAlertboxnegative("Something went wrong . Please try again later .");
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                        Alert alert=new Alert(AttendanceTravelHistoryRegional.this);
                        alert.showAlertboxnegative("Something went wrong . Please try again later .");
                    }

                }

                @Override
                public void onFailure(Call<ResponseRegionalTimeHistoryPojo> call, Throwable t) {
                    progressDialog.dismiss();
                    Alert alert=new Alert(AttendanceTravelHistoryRegional.this);
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
            progressDialog.dismiss();
            Alert alert=new Alert(AttendanceTravelHistoryRegional.this);
            alert.showAlertboxnegative("Local Data is Corrupted. Please logout and login.");
        }
    }
}
