package com.brainmagic.delphitvs.regionalmanager;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.salesactivities.ViewVisitReport;

import java.util.Calendar;
import java.util.List;

import adapter.regional.DateAttendanceViewRegionalAdapter;
import adapter.sales.AttendanceViewRegionalAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.regional.salesnamelist.DataRegionalAttendanceView;
import model.regional.salesnamelist.DataRegionalDateWiseView;
import model.regional.salesnamelist.RegionalAttendanceView;
import model.regional.salesnamelist.RegionalDateWiseView;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AttendanceViewActivityRegional extends AppCompatActivity {

private ImageView regional_back,regional_menu;
private TextView regional_from_date,regional_to_date;
private Button regional_search_date_wise;
private ListView regional_attendance_list_view;
private AttendanceViewRegionalAdapter adapter;
private SharedPreferences preferences;
private SharedPreferences.Editor editor;
private List<DataRegionalAttendanceView> data;
private String regional_from_dates,regional_to_dates;

    @Override
    protected void onCreate(Bundle savedInstanceState)   {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_attendance);

        preferences = getSharedPreferences("Delphi", MODE_PRIVATE);
        editor = preferences.edit();
        regional_back = findViewById(R.id.back);
        regional_menu = findViewById(R.id.menu);
        regional_from_date = findViewById(R.id.from_date);
        regional_to_date = findViewById(R.id.to_date);
        regional_search_date_wise = findViewById(R.id.search_date_wise);
        regional_attendance_list_view = findViewById(R.id.sales_attendance_view);




        NetworkConnection network=new NetworkConnection(AttendanceViewActivityRegional.this);
        if(network.CheckInternet()){

            checkattendance();
        }
        else {
            Alert alert=new Alert(AttendanceViewActivityRegional.this);
            alert.showAlertboxnegative("No Internet Connection. Please try again Later");
        }


        regional_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);

            }
        });

        regional_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final PopupMenu pop = new PopupMenu(AttendanceViewActivityRegional.this, view);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu popupMenu) {
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {

                            case R.id.regional_home:
                                startActivity(new Intent(AttendanceViewActivityRegional.this, MainActivity.class));
                                break;

                            case R.id.regional_mark_attendance:
                                startActivity(new Intent(AttendanceViewActivityRegional.this, AttendanceActiveRegional.class));
                                break;

                            case R.id.regional_view_attendance:

                                break;
                            case R.id.regional_view_visitreport:
                                startActivity(new Intent(AttendanceViewActivityRegional.this, ViewVisitReportRegional.class));
                                break;

                            case R.id.regional_logout:
                                editor.putBoolean("salesLogin", false);
                                editor.commit();
                                startActivity(new Intent(AttendanceViewActivityRegional.this, MainActivity.class));
                                break;

                            case R.id.regional_change_password:
                                String phone = preferences.getString("empMobile", "");
                                final Alert alert = new Alert(AttendanceViewActivityRegional.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;

                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.regional_manager_menu);
                pop.show();
            }
        });

        regional_from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(AttendanceViewActivityRegional.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                regional_from_date.setText(year + "-" +(monthOfYear + 1) + "-" +dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

            }
        });


        regional_to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(AttendanceViewActivityRegional.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                regional_to_date.setText(year + "-" +(monthOfYear + 1) + "-" +dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

            }
        });
        regional_search_date_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                regional_from_dates=regional_from_date.getText().toString();
                regional_to_dates=regional_to_date.getText().toString();

                if(regional_from_dates.equals("")){
                    Toast.makeText(getApplicationContext(),"Please Select From Date",Toast.LENGTH_LONG).show();
                }
                else if(regional_to_dates.equals("")){
                    Toast.makeText(getApplicationContext(),"Please Select TO Date",Toast.LENGTH_LONG).show();
                }
                else{
                    NetworkConnection connection=new NetworkConnection(AttendanceViewActivityRegional.this);
                    if(connection.CheckInternet()){
                        salesexecutiveattendance();
                    }
                    else{
                        Alert alert=new Alert(AttendanceViewActivityRegional.this);
                        alert.showAlertboxnegative("Please check your network connection and try again!");
                    }
                }
            }
        });


    }
    private void checkattendance(){


        final ProgressDialog progressDialog = new ProgressDialog(AttendanceViewActivityRegional.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();


        try {

            APIService service = RetroClient.getApiService();
            Call<RegionalAttendanceView> call = service.getattendancelist(preferences.getString("empId",""));

            call.enqueue(new Callback<RegionalAttendanceView>() {
                @Override
                public void onResponse(Call<RegionalAttendanceView> call, Response<RegionalAttendanceView> response) {
                    progressDialog.dismiss();

                    try {

                        if(response.isSuccessful()){
                            if(response.body().getResult().equals("Success")){

                                adapter=new AttendanceViewRegionalAdapter(AttendanceViewActivityRegional.this,response.body().getData());
                                regional_attendance_list_view.setAdapter(adapter);
                            }
                            else {
                                Alert alert = new Alert(AttendanceViewActivityRegional.this);
                                alert.showAlertboxnegative("No Record Found");
                            }
                        }

                    }
                    catch (Exception e){
                        e.printStackTrace();
                        Alert alert=new Alert(AttendanceViewActivityRegional.this);
                        alert.showAlertboxnegative("Something went wrong . Please try again later .");
                    }
                }

                @Override
                public void onFailure(Call<RegionalAttendanceView> call, Throwable t) {

                    progressDialog.dismiss();
                    Alert alert=new Alert(AttendanceViewActivityRegional.this);
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });

        }
        catch (Exception e){

            e.printStackTrace();
            Alert alert=new Alert(AttendanceViewActivityRegional.this);
            alert.showAlertboxnegative("Something went wrong . Please try again later .");

        }


    }

    private void salesexecutiveattendance(){


        final ProgressDialog progressDialog = new ProgressDialog(AttendanceViewActivityRegional.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();


        try {

            APIService service = RetroClient.getApiService();
            Call<RegionalAttendanceView> call = service.getdatewiselist(preferences.getString("empId",""),(preferences.getString("empCode","")),regional_from_dates,regional_to_dates);

            call.enqueue(new Callback<RegionalAttendanceView>() {
                @Override
                public void onResponse(Call<RegionalAttendanceView> call, Response<RegionalAttendanceView> response) {
                    progressDialog.dismiss();

                    try {

                        if(response.isSuccessful()){
                            if(response.body().getResult().equals("Success")){

                                adapter=new AttendanceViewRegionalAdapter(AttendanceViewActivityRegional.this,response.body().getData());
                                regional_attendance_list_view.setAdapter(adapter);
                            }
                            else {
                                Alert alert = new Alert(AttendanceViewActivityRegional.this);
                                alert.showAlertboxnegative("No Record Found");
                            }
                        }

                    }
                    catch (Exception e){
                        e.printStackTrace();
                        Alert alert=new Alert(AttendanceViewActivityRegional.this);
                        alert.showAlertboxnegative("Something went wrong . Please try again later .");
                    }
                }

                @Override
                public void onFailure(Call<RegionalAttendanceView> call, Throwable t) {

                    progressDialog.dismiss();
                    Alert alert=new Alert(AttendanceViewActivityRegional.this);
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });

        }
        catch (Exception e){

            e.printStackTrace();
            Alert alert=new Alert(AttendanceViewActivityRegional.this);
            alert.showAlertboxnegative("Something went wrong . Please try again later .");

        }


    }


}