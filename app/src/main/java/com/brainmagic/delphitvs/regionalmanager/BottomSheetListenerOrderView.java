package com.brainmagic.delphitvs.regionalmanager;

import android.content.Context;
import android.os.Bundle;



import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.fragment.RegionalManagerBottomsheet;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import io.reactivex.annotations.NonNull;

public class BottomSheetListenerOrderView extends BottomSheetDialogFragment {

    private Bottomsheets listeners;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


            View view = inflater.inflate(R.layout.rmvieworderdetailsfragment,container,false);

            LinearLayout currentmonth =(LinearLayout)view.findViewById(R.id.currentmonth);
            LinearLayout datewise =(LinearLayout)view.findViewById(R.id.datewise);

            currentmonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    listeners.bottomsheetlisteners("currentmonth");
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        datewise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    listeners.bottomsheetlisteners("datewiseselect");
                } catch (Exception e) {
                    e.printStackTrace();

                }

            }
        });


return view;
    }

    public interface Bottomsheets{

        void bottomsheetlisteners(String orderselect);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listeners = (Bottomsheets) context;
        } catch (Exception e) {

            e.printStackTrace();
        }
    }
}
