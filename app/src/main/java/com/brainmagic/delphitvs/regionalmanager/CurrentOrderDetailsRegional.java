package com.brainmagic.delphitvs.regionalmanager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;

import java.util.List;

import adapter.regional.CurrentOrderDetailsRegionlaAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.regional.salesnamelist.CurrentOrderDetailsRegionalPojo;
import model.regional.salesnamelist.OrderList;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrentOrderDetailsRegional extends AppCompatActivity {

    private ImageView current_month_menu,current_month_back;
    private ListView current_month_lists;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
     private CurrentOrderDetailsRegionlaAdapter adapter;
     private List<OrderList>data;
     private String name,id;
     private TextView currnt_month_amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_r_m_view_order_details);

        preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
        editor=preferences.edit();
        current_month_menu=findViewById(R.id.current_month_menu);
        current_month_back=findViewById(R.id.current_month_back);
        current_month_lists=findViewById(R.id.current_month_list2);
        current_month_lists.setAdapter(adapter);

        name=getIntent().getStringExtra("salesdate");
        id=getIntent().getStringExtra("salesids");

        currnt_month_amount=findViewById(R.id.currnt_month_amount);

        current_month_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final PopupMenu pop =new PopupMenu(CurrentOrderDetailsRegional.this,view);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu popupMenu) {
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()){

                            case R.id.regional_home:
                                startActivity(new Intent(CurrentOrderDetailsRegional.this, MainActivity.class));
                                break;
                            case R.id.regional_mark_attendance:
                                startActivity(new Intent(CurrentOrderDetailsRegional.this, AttendanceActiveRegional.class));
                                break;
                            case R.id.regional_view_attendance:
                                startActivity(new Intent(CurrentOrderDetailsRegional.this, AttendanceViewActivityRegional.class));
                                break;
                            case R.id.regional_view_visitreport:
                                startActivity(new Intent(CurrentOrderDetailsRegional.this, ViewVisitReportRegional.class));
                                break;
                            case R.id.regional_change_password:
                                String phone=preferences.getString("empMobile","");
                                final Alert alert=new Alert(CurrentOrderDetailsRegional.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;

                            case R.id.regional_logout:
                                editor.putBoolean("salesLogin",false);
                                editor.commit();
                                startActivity(new Intent(CurrentOrderDetailsRegional.this,MainActivity.class).
                                        addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                        }


                        return false;
                    }

                });
                pop.inflate(R.menu.regional_manager_menu);
                pop.show();
            }
        });
        current_month_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
                overridePendingTransition(R.anim.left_in,R.anim.left_out);

            }
        });

        NetworkConnection connection =new NetworkConnection(CurrentOrderDetailsRegional.this);
        if(connection.CheckInternet()){
            getsalesdetails();
        }
        else{

            Alert alert=new Alert(CurrentOrderDetailsRegional.this);
            alert.showAlertboxnegative("Please check your network connection and try again!");
        }
    }

    private void getsalesdetails() {
        final ProgressDialog dialog =new ProgressDialog(CurrentOrderDetailsRegional.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.setIndeterminate(true);
        dialog.show();


        APIService service = RetroClient.getApiService();
        Call<CurrentOrderDetailsRegionalPojo>call=service.salesdetails(id,name);

        try {
            call.enqueue(new Callback<CurrentOrderDetailsRegionalPojo>() {
                @Override
                public void onResponse(Call<CurrentOrderDetailsRegionalPojo> call, Response<CurrentOrderDetailsRegionalPojo> response) {

                    try {

                        if(response.isSuccessful()){

                            if(response.body().getResult().equals("Success")){
                                dialog.dismiss();
                                data=response.body().getData().getOrderList();
                                String amount=response.body().getData().getTot();
                               currnt_month_amount.setText("Rs."+String.valueOf(amount));
                                adapter=new CurrentOrderDetailsRegionlaAdapter(CurrentOrderDetailsRegional.this,data);
                                current_month_lists.setAdapter(adapter);

                            }
                            else{
                                dialog.dismiss();
                                Alert alert=new Alert(CurrentOrderDetailsRegional.this);
                                alert.showAlertboxnegative("NO Record Found");
                            }
                        }else{
                            dialog.dismiss();
                            Alert alert=new Alert(CurrentOrderDetailsRegional.this);
                            alert.showAlertboxnegative("Something went wrong . Please try again later .");
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                        Alert alert=new Alert(CurrentOrderDetailsRegional.this);
                        alert.showAlertboxnegative("Something went wrong . Please try again later .");
                    }
                }

                @Override
                public void onFailure(Call<CurrentOrderDetailsRegionalPojo> call, Throwable t) {
                    dialog.dismiss();
                    Alert alert=new Alert(CurrentOrderDetailsRegional.this);
                    alert.showAlertbox("Something went wrong . Please try again later .");

                }
            });
        }catch (Exception e){
            e.printStackTrace();
            Alert alert=new Alert(CurrentOrderDetailsRegional.this);
            alert.showAlertbox("Something went wrong . Please try again later .");
        }

        current_month_lists.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent intent=new Intent(CurrentOrderDetailsRegional.this,CurrentOrderHistoryRegional.class);
               intent.putExtra("orderid",data.get(i).getOrderid());
                startActivity(intent);

            }
        });
    }
}
