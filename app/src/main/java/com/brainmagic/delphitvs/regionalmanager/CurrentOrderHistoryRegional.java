package com.brainmagic.delphitvs.regionalmanager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;

import java.util.List;

import adapter.regional.CurrentOrderHistoryRegionalAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.regional.salesnamelist.CurrentOrderHistoryPojo;
import model.regional.salesnamelist.Datum;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrentOrderHistoryRegional extends AppCompatActivity {

    private ImageView current_month_menu,current_month_back;
    private ListView current_month_lists;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private CurrentOrderHistoryRegionalAdapter adapter;
    private List<Datum>data;
    private String orderids;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_month_order_details);

        NetworkConnection connection=new NetworkConnection(CurrentOrderHistoryRegional.this);
        if(connection.CheckInternet())
        {

            getorderdetails();
        }
        else{
            Alert alert=new Alert(CurrentOrderHistoryRegional.this);
            alert.showAlertboxnegative("Please check your network connection and try again!");
        }

        preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
        editor=preferences.edit();
        current_month_menu=findViewById(R.id.current_month_menu);
        current_month_back=findViewById(R.id.current_month_back);
        current_month_lists=findViewById(R.id.current_month_lists3);


        current_month_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final PopupMenu pop =new PopupMenu(CurrentOrderHistoryRegional.this,view);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu popupMenu) {
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()){

                            case R.id.regional_home:
                                startActivity(new Intent(CurrentOrderHistoryRegional.this, MainActivity.class));
                                break;
                            case R.id.regional_mark_attendance:
                                startActivity(new Intent(CurrentOrderHistoryRegional.this, AttendanceActiveRegional.class));
                                break;
                            case R.id.regional_view_attendance:
                                startActivity(new Intent(CurrentOrderHistoryRegional.this, AttendanceViewActivityRegional.class));
                                break;
                            case R.id.regional_view_visitreport:
                                startActivity(new Intent(CurrentOrderHistoryRegional.this, ViewVisitReportRegional.class));
                                break;
                            case R.id.regional_change_password:
                                String phone=preferences.getString("empMobile","");
                                final Alert alert=new Alert(CurrentOrderHistoryRegional.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;

                            case R.id.regional_logout:
                                editor.putBoolean("salesLogin",false);
                                editor.commit();
                                startActivity(new Intent(CurrentOrderHistoryRegional.this,MainActivity.class).
                                        addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                        }


                        return false;
                    }

                });
                pop.inflate(R.menu.regional_manager_menu);
                pop.show();
            }
        });
        current_month_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
                overridePendingTransition(R.anim.left_in,R.anim.left_out);

            }
        });

    }

    private void getorderdetails() {
        orderids=getIntent().getStringExtra("orderid");
        final ProgressDialog dialog =new ProgressDialog(CurrentOrderHistoryRegional.this);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setMessage("Loading...");
        dialog.show();

        APIService service = RetroClient.getApiService();
        Call<CurrentOrderHistoryPojo> call=service.getorderdetils(orderids);


        try {
            call.enqueue(new Callback<CurrentOrderHistoryPojo>() {
                @Override
                public void onResponse(Call<CurrentOrderHistoryPojo> call, Response<CurrentOrderHistoryPojo> response) {

                    try {
                        if(response.isSuccessful()){
                            if(response.body().getResult().equals("Success")){
                                dialog.dismiss();
                                data=response.body().getData();
                                adapter=new CurrentOrderHistoryRegionalAdapter(CurrentOrderHistoryRegional.this,data);
                                current_month_lists.setAdapter(adapter);
                            }
                            else{
                                dialog.dismiss();
                                Alert alert=new Alert(CurrentOrderHistoryRegional.this);
                                alert.showAlertboxnegative("No Record Found");
                            }
                        }
                        else{
                            dialog.dismiss();
                            Alert alert=new Alert(CurrentOrderHistoryRegional.this);
                            alert.showAlertboxnegative("Something went wrong . Please try again later .");
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        Alert alert=new Alert(CurrentOrderHistoryRegional.this);
                        alert.showAlertbox("Something went wrong . Please try again later .");
                    }


                }

                @Override
                public void onFailure(Call<CurrentOrderHistoryPojo> call, Throwable t) {
                dialog.dismiss();
                Alert alert=new Alert(CurrentOrderHistoryRegional.this);
                alert.showAlertbox("Something went wrong . Please try again later .");

                }
            });
        }catch (Exception e){
            e.printStackTrace();
            Alert alert=new Alert(CurrentOrderHistoryRegional.this);
            alert.showAlertbox("Something went wrong . Please try again later .");

        }
    }
}
