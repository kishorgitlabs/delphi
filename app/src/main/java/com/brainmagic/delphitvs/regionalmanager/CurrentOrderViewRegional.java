package com.brainmagic.delphitvs.regionalmanager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;

import java.util.List;

import adapter.regional.CurrentOrderViewRegionalAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.regional.OrderDetail;
import model.regional.CurrentOrderViewRegionalPojo;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrentOrderViewRegional extends AppCompatActivity {

    private ImageView current_month_menu, current_month_back;
    private ListView current_month_lists;
    private TextView current_month_amont;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private CurrentOrderViewRegionalAdapter adapter;
    private List<OrderDetail> data;
    private String data1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_r_m_view_sales_executive_month_order);

        preferences = getSharedPreferences("Delphi", MODE_PRIVATE);
        editor = preferences.edit();
        current_month_menu = findViewById(R.id.current_month_menu);
        current_month_back = findViewById(R.id.current_month_back);
        current_month_lists = findViewById(R.id.current_month_lists1);
        current_month_amont = findViewById(R.id.currnt_month_amount);

        current_month_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final PopupMenu pop = new PopupMenu(CurrentOrderViewRegional.this, view);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu popupMenu) {
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.regional_home:
                                startActivity(new Intent(CurrentOrderViewRegional.this, MainActivity.class));
                                break;

                            case R.id.regional_mark_attendance:
                                startActivity(new Intent(CurrentOrderViewRegional.this, AttendanceActiveRegional.class));
                                break;
                                case R.id.regional_view_attendance:
                                startActivity(new Intent(CurrentOrderViewRegional.this, AttendanceViewActivityRegional.class));
                                break;
                            case R.id.regional_view_visitreport:
                                startActivity(new Intent(CurrentOrderViewRegional.this, ViewVisitReportRegional.class));
                                break;

                            case R.id.regional_logout:
                                editor.putBoolean("salesLogin", false);
                                editor.commit();
                                startActivity(new Intent(CurrentOrderViewRegional.this, MainActivity.class));
                                break;

                            case R.id.regional_change_password:
                                String phone = preferences.getString("empMobile", "");
                                final Alert alert = new Alert(CurrentOrderViewRegional.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;

                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.regional_manager_menu);
                pop.show();
            }
        });
        current_month_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);

            }
        });
        NetworkConnection connetion = new NetworkConnection(this);
        if (connetion.CheckInternet()) {
            showsalesname();
        } else {
            Alert alert = new Alert(this);
            alert.showAlertboxnegative("Please check your network connection and try again!");
        }
    }

    private void showsalesname() {

        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        APIService service = RetroClient.getApiService();
        Call<CurrentOrderViewRegionalPojo> call = service.getsalesname(preferences.getString("regId", ""));

        try {
            call.enqueue(new Callback<CurrentOrderViewRegionalPojo>() {
                @Override
                public void onResponse(Call<CurrentOrderViewRegionalPojo> call, Response<CurrentOrderViewRegionalPojo> response) {

                    try {
                        if (response.isSuccessful()) {

                            if (response.body().getResult().equals("Success")) {
                                  dialog.dismiss();

                                    data = response.body().getData().getOrderDetail();
                                   data1 = response.body().getData().getGrandTotal();
                                   adapter = new CurrentOrderViewRegionalAdapter(CurrentOrderViewRegional.this, response.body().getData().getOrderDetail());
                                  current_month_lists.setAdapter(adapter);
                                   current_month_amont.setText( "RS." + String.valueOf(data1));

                            } else {
                                dialog.dismiss();
                                Alert alert = new Alert(CurrentOrderViewRegional.this);
                                alert.showAlertboxnegative("No Record Found");
                            }
                        } else {
                            dialog.dismiss();
                            Alert alert = new Alert(CurrentOrderViewRegional.this);
                            alert.showAlertboxnegative("Something went wrong . Please try again later .");
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                        Alert alert = new Alert(CurrentOrderViewRegional.this);
                        alert.showAlertboxnegative("No Record Found");

                    }
                }

                @Override
                public void onFailure(Call<CurrentOrderViewRegionalPojo> call, Throwable t) {
                        dialog.dismiss();
                    Alert alert = new Alert(CurrentOrderViewRegional.this);
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Alert alert = new Alert(CurrentOrderViewRegional.this);
            alert.showAlertbox("No Record Found");


        }

        current_month_lists.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String[] date=data.get(i).getDate().split("T");

                Intent intent=new Intent(CurrentOrderViewRegional.this,CurrentOrderDetailsRegional.class);
                intent.putExtra("salesdate",date[0]);
                intent.putExtra("salesids",data.get(i).getExeid());
                startActivity(intent);
            }
        });





    }
}
