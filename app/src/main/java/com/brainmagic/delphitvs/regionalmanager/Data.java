package com.brainmagic.delphitvs.regionalmanager;

public class Data{
	private String outLongitude;
	private String designation;
	private Object flag;
	private String address;
	private String outTime;
	private Object disid;
	private String empCode;
	private String inLongitude;
	private String date;
	private String inLatitude;
	private String name;
	private Object totalDuration;
	private String outAddress;
	private String attendDay;
	private String inTime;
	private String outLatitude;
	private Object regName;
	private String createdDate;
	private int id;
	private int empId;
	private Object regid;
	private String distance;

	public void setOutLongitude(String outLongitude){
		this.outLongitude = outLongitude;
	}

	public String getOutLongitude(){
		return outLongitude;
	}

	public void setDesignation(String designation){
		this.designation = designation;
	}

	public String getDesignation(){
		return designation;
	}

	public void setFlag(Object flag){
		this.flag = flag;
	}

	public Object getFlag(){
		return flag;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setOutTime(String outTime){
		this.outTime = outTime;
	}

	public String getOutTime(){
		return outTime;
	}

	public void setDisid(Object disid){
		this.disid = disid;
	}

	public Object getDisid(){
		return disid;
	}

	public void setEmpCode(String empCode){
		this.empCode = empCode;
	}

	public String getEmpCode(){
		return empCode;
	}

	public void setInLongitude(String inLongitude){
		this.inLongitude = inLongitude;
	}

	public String getInLongitude(){
		return inLongitude;
	}

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setInLatitude(String inLatitude){
		this.inLatitude = inLatitude;
	}

	public String getInLatitude(){
		return inLatitude;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setTotalDuration(Object totalDuration){
		this.totalDuration = totalDuration;
	}

	public Object getTotalDuration(){
		return totalDuration;
	}

	public void setOutAddress(String outAddress){
		this.outAddress = outAddress;
	}

	public String getOutAddress(){
		return outAddress;
	}

	public void setAttendDay(String attendDay){
		this.attendDay = attendDay;
	}

	public String getAttendDay(){
		return attendDay;
	}

	public void setInTime(String inTime){
		this.inTime = inTime;
	}

	public String getInTime(){
		return inTime;
	}

	public void setOutLatitude(String outLatitude){
		this.outLatitude = outLatitude;
	}

	public String getOutLatitude(){
		return outLatitude;
	}

	public void setRegName(Object regName){
		this.regName = regName;
	}

	public Object getRegName(){
		return regName;
	}

	public void setCreatedDate(String createdDate){
		this.createdDate = createdDate;
	}

	public String getCreatedDate(){
		return createdDate;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setEmpId(int empId){
		this.empId = empId;
	}

	public int getEmpId(){
		return empId;
	}

	public void setRegid(Object regid){
		this.regid = regid;
	}

	public Object getRegid(){
		return regid;
	}

	public void setDistance(String distance){
		this.distance = distance;
	}

	public String getDistance(){
		return distance;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"outLongitude = '" + outLongitude + '\'' + 
			",designation = '" + designation + '\'' + 
			",flag = '" + flag + '\'' + 
			",address = '" + address + '\'' + 
			",outTime = '" + outTime + '\'' + 
			",disid = '" + disid + '\'' + 
			",empCode = '" + empCode + '\'' + 
			",inLongitude = '" + inLongitude + '\'' + 
			",date = '" + date + '\'' + 
			",inLatitude = '" + inLatitude + '\'' + 
			",name = '" + name + '\'' + 
			",totalDuration = '" + totalDuration + '\'' + 
			",outAddress = '" + outAddress + '\'' + 
			",attendDay = '" + attendDay + '\'' + 
			",inTime = '" + inTime + '\'' + 
			",outLatitude = '" + outLatitude + '\'' + 
			",regName = '" + regName + '\'' + 
			",createdDate = '" + createdDate + '\'' + 
			",id = '" + id + '\'' + 
			",empId = '" + empId + '\'' + 
			",regid = '" + regid + '\'' + 
			",distance = '" + distance + '\'' + 
			"}";
		}
}
