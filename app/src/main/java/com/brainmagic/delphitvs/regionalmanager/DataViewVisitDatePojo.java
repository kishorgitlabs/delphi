package com.brainmagic.delphitvs.regionalmanager;

import java.io.Serializable;

public class DataViewVisitDatePojo implements Serializable {
	private int id;
	private String salesPersonName;
	private String placeOfVisit;
	private String customerName;
	private String custMobileNo;
	private String customerType;
	private String visitedDate;
	private Object remark;
	private Object time;
	private Object purposeOfVisit;
	private Object nextVisit;
	private int exeId;

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setSalesPersonName(String salesPersonName){
		this.salesPersonName = salesPersonName;
	}

	public String getSalesPersonName(){
		return salesPersonName;
	}

	public void setPlaceOfVisit(String placeOfVisit){
		this.placeOfVisit = placeOfVisit;
	}

	public String getPlaceOfVisit(){
		return placeOfVisit;
	}

	public void setCustomerName(String customerName){
		this.customerName = customerName;
	}

	public String getCustomerName(){
		return customerName;
	}

	public void setCustMobileNo(String custMobileNo){
		this.custMobileNo = custMobileNo;
	}

	public String getCustMobileNo(){
		return custMobileNo;
	}

	public void setCustomerType(String customerType){
		this.customerType = customerType;
	}

	public String getCustomerType(){
		return customerType;
	}

	public void setVisitedDate(String visitedDate){
		this.visitedDate = visitedDate;
	}

	public String getVisitedDate(){
		return visitedDate;
	}

	public void setRemark(Object remark){
		this.remark = remark;
	}

	public Object getRemark(){
		return remark;
	}

	public void setTime(Object time){
		this.time = time;
	}

	public Object getTime(){
		return time;
	}

	public void setPurposeOfVisit(Object purposeOfVisit){
		this.purposeOfVisit = purposeOfVisit;
	}

	public Object getPurposeOfVisit(){
		return purposeOfVisit;
	}

	public void setNextVisit(Object nextVisit){
		this.nextVisit = nextVisit;
	}

	public Object getNextVisit(){
		return nextVisit;
	}

	public void setExeId(int exeId){
		this.exeId = exeId;
	}

	public int getExeId(){
		return exeId;
	}

	@Override
 	public String toString(){
		return 
			"DataViewVisitDatePojo{" + 
			"id = '" + id + '\'' + 
			",salesPersonName = '" + salesPersonName + '\'' + 
			",placeOfVisit = '" + placeOfVisit + '\'' + 
			",customerName = '" + customerName + '\'' + 
			",custMobileNo = '" + custMobileNo + '\'' + 
			",customerType = '" + customerType + '\'' + 
			",visitedDate = '" + visitedDate + '\'' + 
			",remark = '" + remark + '\'' + 
			",time = '" + time + '\'' + 
			",purposeOfVisit = '" + purposeOfVisit + '\'' + 
			",nextVisit = '" + nextVisit + '\'' + 
			",exeId = '" + exeId + '\'' + 
			"}";
		}
}