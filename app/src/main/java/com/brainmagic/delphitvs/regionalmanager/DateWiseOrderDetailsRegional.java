package com.brainmagic.delphitvs.regionalmanager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;

import java.util.List;

import adapter.regional.DateWiseOrderDetailsRegionalAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.regional.salesnamelist.CurrentOrderDetailsRegionalPojo;
import model.regional.salesnamelist.OrderList;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DateWiseOrderDetailsRegional extends AppCompatActivity {
    private ImageView date_wise_menu,getDate_wise_menu_wise_back;
    private ListView date_wise_list;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private TextView currnt_month_amount;
    private String salesid,salesdate,sales_amount;
    private List<OrderList>data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_wise_order_details);

        preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
        editor=preferences.edit();
        date_wise_menu=findViewById(R.id.date_wise_menu);
        getDate_wise_menu_wise_back=findViewById(R.id.date_wise_back);
        date_wise_list=findViewById(R.id.datewise_list);
        currnt_month_amount=findViewById(R.id.currnt_month_amount);

        salesid=getIntent().getStringExtra("sales_order_id");
        salesdate=getIntent().getStringExtra("sales_order_date");




        date_wise_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final PopupMenu pop =new PopupMenu(DateWiseOrderDetailsRegional.this,view);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu popupMenu) {
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()){

                            case R.id.regional_home:
                                startActivity(new Intent(DateWiseOrderDetailsRegional.this, MainActivity.class));
                                break;
                            case R.id.regional_mark_attendance:
                                startActivity(new Intent(DateWiseOrderDetailsRegional.this, AttendanceActiveRegional.class));
                                break;
                            case R.id.regional_view_attendance:
                                startActivity(new Intent(DateWiseOrderDetailsRegional.this, AttendanceViewActivityRegional.class));
                                break;
                            case R.id.regional_view_visitreport:
                                startActivity(new Intent(DateWiseOrderDetailsRegional.this, ViewVisitReportRegional.class));
                                break;
                            case R.id.regional_change_password:
                                String phone=preferences.getString("empMobile","");
                                final Alert alert=new Alert(DateWiseOrderDetailsRegional.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;

                            case R.id.regional_logout:
                                editor.putBoolean("salesLogin",false);
                                editor.commit();
                                startActivity(new Intent(DateWiseOrderDetailsRegional.this,MainActivity.class).
                                        addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                        }


                        return false;
                    }

                });
                pop.inflate(R.menu.regional_manager_menu);
                pop.show();
            }
        });
        getDate_wise_menu_wise_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
                overridePendingTransition(R.anim.left_in,R.anim.left_out);

            }
        });

        NetworkConnection connection=new NetworkConnection(this);
        if(connection.CheckInternet())
        {
            getMoreDetails();
        }
        else {
            Alert alert=new Alert(this);
            alert.showAlertboxnegative("No Internet Connection. Please try again Later");
        }
    }



    private void getMoreDetails(){

        final ProgressDialog progressDialog =new ProgressDialog(DateWiseOrderDetailsRegional.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();


        APIService service= RetroClient.getApiService();
        Call<CurrentOrderDetailsRegionalPojo> call=service.salesdetails(salesid,salesdate);

        try {

            call.enqueue(new Callback<CurrentOrderDetailsRegionalPojo>() {
                @Override
                public void onResponse(Call<CurrentOrderDetailsRegionalPojo> call, final Response<CurrentOrderDetailsRegionalPojo> response) {

                    if(response.isSuccessful()){
                        if(response.body().getResult().equals("Success")){
                            progressDialog.dismiss();

                            currnt_month_amount.setVisibility(View.VISIBLE);
                            sales_amount=response.body().getData().getTot();
                            currnt_month_amount.setText("Rs."+String.valueOf(sales_amount));

                    data=response.body().getData().getOrderList();
                    DateWiseOrderDetailsRegionalAdapter adapter=new DateWiseOrderDetailsRegionalAdapter(DateWiseOrderDetailsRegional.this,data);
                        date_wise_list.setAdapter(adapter);

                        date_wise_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                Intent intent = new Intent(DateWiseOrderDetailsRegional.this,DateWiseOrderHistoryRegional.class);
                                intent.putExtra("order_id",data.get(i).getOrderid());
                                startActivity(intent);


                            }
                        });

                        }
                        else {
                            Alert alert = new Alert(DateWiseOrderDetailsRegional.this);
                            alert.showAlertboxnegative("No Record Found");
                        }
                    }
                }

                @Override
                public void onFailure(Call<CurrentOrderDetailsRegionalPojo> call, Throwable t) {
                    progressDialog.dismiss();

                    Alert alert=new Alert(DateWiseOrderDetailsRegional.this);
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });

        }catch (Exception e){
            e.printStackTrace();
            Alert alert=new Alert(DateWiseOrderDetailsRegional.this);
            alert.showAlertboxnegative("Something went wrong . Please try again later .");
        }

    }

}
