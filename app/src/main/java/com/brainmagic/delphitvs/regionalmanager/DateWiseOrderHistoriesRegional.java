package com.brainmagic.delphitvs.regionalmanager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;

import java.util.List;

import adapter.regional.DateWiseOrderHistoryRegionalAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.regional.salesnamelist.CurrentOrderHistoryPojo;
import model.regional.salesnamelist.Datum;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DateWiseOrderHistoriesRegional extends AppCompatActivity {

    private ImageView date_wise_menu,getDate_wise_menu_wise_back;
    private ListView date_wise_list;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private DateWiseOrderHistoryRegionalAdapter adapter;
    private String sales_order_id;
    private List<Datum> datas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_wise_view_orders);

        preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
        editor=preferences.edit();
        date_wise_menu=findViewById(R.id.date_wise_menu);
        getDate_wise_menu_wise_back=findViewById(R.id.date_wise_back);
        date_wise_list=findViewById(R.id.date_wise_list);
        sales_order_id=getIntent().getStringExtra("sales_orders_ids");


        date_wise_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final PopupMenu pop =new PopupMenu(DateWiseOrderHistoriesRegional.this,view);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu popupMenu) {
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()){

                            case R.id.regional_home:
                                startActivity(new Intent(DateWiseOrderHistoriesRegional.this, MainActivity.class));
                                break;
                            case R.id.regional_mark_attendance:
                                startActivity(new Intent(DateWiseOrderHistoriesRegional.this, AttendanceActiveRegional.class));
                                break;
                            case R.id.regional_view_attendance:
                                startActivity(new Intent(DateWiseOrderHistoriesRegional.this, AttendanceViewActivityRegional.class));
                                break;
                            case R.id.regional_view_visitreport:
                                startActivity(new Intent(DateWiseOrderHistoriesRegional.this, ViewVisitReportRegional.class));
                                break;
                            case R.id.regional_change_password:
                                String phone=preferences.getString("empMobile","");
                                final Alert alert=new Alert(DateWiseOrderHistoriesRegional.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;

                            case R.id.regional_logout:
                                editor.putBoolean("salesLogin",false);
                                editor.commit();
                                startActivity(new Intent(DateWiseOrderHistoriesRegional.this,MainActivity.class).
                                        addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                        }


                        return false;
                    }

                });
                pop.inflate(R.menu.regional_manager_menu);
                pop.show();
            }
        });
        getDate_wise_menu_wise_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
                overridePendingTransition(R.anim.left_in,R.anim.left_out);

            }
        });

        NetworkConnection connection=new NetworkConnection(DateWiseOrderHistoriesRegional.this);
        if(connection.CheckInternet()){

            getmoredetails();
        }
        else{
            Alert alert=new Alert(this);
            alert.showAlertboxnegative("No Internet Connection. Please try again Later");
        }

    }

    private void getmoredetails(){

        final ProgressDialog progressDialog =new ProgressDialog(DateWiseOrderHistoriesRegional.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        APIService service = RetroClient.getApiService();
        Call<CurrentOrderHistoryPojo> call=service.getorderdetils(sales_order_id);


        try {
            call.enqueue(new Callback<CurrentOrderHistoryPojo>() {
                @Override
                public void onResponse(Call<CurrentOrderHistoryPojo> call, Response<CurrentOrderHistoryPojo> response) {

                    try {
                        if(response.isSuccessful()){
                            if(response.body().getResult().equals("Success")){
                                progressDialog.dismiss();
                                datas=response.body().getData();
                                DateWiseOrderHistoryRegionalAdapter adapters=new DateWiseOrderHistoryRegionalAdapter(DateWiseOrderHistoriesRegional.this,datas);
                                date_wise_list.setAdapter(adapters);
                            }
                            else{
                                progressDialog.dismiss();
                                Alert alert=new Alert(DateWiseOrderHistoriesRegional.this);
                                alert.showAlertboxnegative("No Record Found");
                            }
                        }
                        else{
                            progressDialog.dismiss();
                            Alert alert=new Alert(DateWiseOrderHistoriesRegional.this);
                            alert.showAlertboxnegative("Something went wrong . Please try again later .");
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        Alert alert=new Alert(DateWiseOrderHistoriesRegional.this);
                        alert.showAlertbox("Something went wrong . Please try again later .");
                    }

                }

                @Override
                public void onFailure(Call<CurrentOrderHistoryPojo> call, Throwable t) {
                    progressDialog.dismiss();
                    Alert alert=new Alert(DateWiseOrderHistoriesRegional.this);
                    alert.showAlertbox("Something went wrong . Please try again later .");

                }
            });
        }catch (Exception e){
            e.printStackTrace();
            Alert alert=new Alert(DateWiseOrderHistoriesRegional.this);
            alert.showAlertbox("Something went wrong . Please try again later .");

        }



    }




}

