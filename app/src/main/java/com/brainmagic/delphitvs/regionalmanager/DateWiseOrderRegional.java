package com.brainmagic.delphitvs.regionalmanager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;

import java.util.List;

import adapter.regional.DateWiseOrderRegionalAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.partdetails.SalesOrderDatePojo;
import model.sales.order.orderhistory.OrderList;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DateWiseOrderRegional extends AppCompatActivity {

    private ImageView date_wise_menu,getDate_wise_menu_wise_back;
    private ListView date_wise_list;
    private TextView date_wise_amount;
    private DateWiseOrderRegionalAdapter adapter;
    private TextView sales_from_date,sales_to_date,currnt_month_amount;
    private String sales_order_from_date,sales_order_to_date,sales_order_id,amount;
 private SharedPreferences preferences;
 private SharedPreferences.Editor editor;
 private List<OrderList>datas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_r_m_date_wise_details);

        sales_from_date=findViewById(R.id.sales_from_date);
        sales_to_date=findViewById(R.id.sales_to_date);
        preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
        editor=preferences.edit();
        date_wise_menu=findViewById(R.id.date_wise_menu);
        getDate_wise_menu_wise_back=findViewById(R.id.date_wise_back);
        date_wise_list=findViewById(R.id.datewise_list);
        date_wise_amount=findViewById(R.id.date_wise_amount);

sales_order_id=getIntent().getStringExtra("sales_order_id");
sales_order_from_date=getIntent().getStringExtra("sales_from_date");
sales_order_to_date=getIntent().getStringExtra("sales_to_date");

sales_from_date.setText(sales_order_from_date);
sales_to_date.setText(sales_order_to_date);


        date_wise_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final PopupMenu pop =new PopupMenu(DateWiseOrderRegional.this,view);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu popupMenu) {
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()){

                            case R.id.regional_home:
                                startActivity(new Intent(DateWiseOrderRegional.this, MainActivity.class));
                                break;
                            case R.id.regional_mark_attendance:
                                startActivity(new Intent(DateWiseOrderRegional.this, AttendanceActiveRegional.class));
                                break;
                            case R.id.regional_view_attendance:
                                startActivity(new Intent(DateWiseOrderRegional.this, AttendanceViewActivityRegional.class));
                                break;
                            case R.id.regional_view_visitreport:
                                startActivity(new Intent(DateWiseOrderRegional.this, ViewVisitReportRegional.class));
                                break;
                            case R.id.regional_change_password:
                                String phone=preferences.getString("empMobile","");
                                final Alert alert=new Alert(DateWiseOrderRegional.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;

                            case R.id.regional_logout:
                                editor.putBoolean("salesLogin",false);
                                editor.commit();
                                startActivity(new Intent(DateWiseOrderRegional.this,MainActivity.class).
                                        addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                        }


                        return false;
                    }

                });
                pop.inflate(R.menu.regional_manager_menu);
                pop.show();
            }
        });
        getDate_wise_menu_wise_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
                overridePendingTransition(R.anim.left_in,R.anim.left_out);

            }
        });


        NetworkConnection connection=new NetworkConnection(DateWiseOrderRegional.this);
        if(connection.CheckInternet()){

            getsalesorders();
        }
else{
    Alert alert=new Alert(DateWiseOrderRegional.this);
    alert.showAlertboxnegative("Please check your network connection and try again!");
        }

    }
    private void getsalesorders(){

        final ProgressDialog progressDialog=new ProgressDialog(DateWiseOrderRegional.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading...");
        progressDialog.setIndeterminate(true);
        progressDialog.show();


        APIService service=RetroClient.getApiService();
        Call<SalesOrderDatePojo>call=service.getSalesOrderHistoryDateWise(sales_order_id,sales_order_from_date,sales_order_to_date);

        try {

            call.enqueue(new Callback<SalesOrderDatePojo>() {
                @Override
                public void onResponse(Call<SalesOrderDatePojo> call, final Response<SalesOrderDatePojo> response) {

                    if(response.isSuccessful()){

                        if(response.body().getResult().equals("Success")){

                            progressDialog.dismiss();
                            date_wise_amount.setVisibility(View.VISIBLE);

                            DateWiseOrderRegionalAdapter adapter   =new DateWiseOrderRegionalAdapter(DateWiseOrderRegional.this,response.body().getData().getOrderList());
                            date_wise_list.setAdapter(adapter);
                            amount=response.body().getData().getTot();
                            date_wise_amount.setText("Rs."+String.valueOf(amount));

                            date_wise_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                    String[] dates=response.body().getData().getOrderList().get(i).getInsertDate().split("T");
                                    Intent intent=new Intent(DateWiseOrderRegional.this,DateWiseOrderDetailsRegional.class);
                                    intent.putExtra("sales_order_id",response.body().getData().getOrderList().get(i).getExeId());
                                    intent.putExtra("sales_order_date",dates[0]);
                                    startActivity(intent);

                                }
                            });

                        }
                        else {
                            Alert alert = new Alert(DateWiseOrderRegional.this);
                            alert.showAlertboxnegative("No Record Found");
                        }
                    }
                }

                @Override
                public void onFailure(Call<SalesOrderDatePojo> call, Throwable t) {
                    progressDialog.dismiss();
                    Alert alert=new Alert(DateWiseOrderRegional.this);
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });

        }catch (Exception e){
            e.printStackTrace();
            Alert alert=new Alert(DateWiseOrderRegional.this);
            alert.showAlertboxnegative("Something went wrong . Please try again later .");
        }


    }


    }

