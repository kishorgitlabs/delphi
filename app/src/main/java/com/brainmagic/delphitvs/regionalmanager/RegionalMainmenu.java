package com.brainmagic.delphitvs.regionalmanager;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;




import android.view.LayoutInflater;
import android.view.View;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;

import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.fragment.RegionalManagerBottomsheet;
import com.brainmagic.delphitvs.salesactivities.ViewAttendanceActivitySales;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.Calendar;
import java.util.List;

import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.regional.ViewVisitReportPojo;
import model.regional.ViewVisitResult;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegionalMainmenu extends AppCompatActivity implements BottomSheetListenerOrderView.Bottomsheets,RegionalManagerBottomsheet.Bottomsheet {
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
     private LinearLayout regional_attendance,regional_sales_attendance,regional_order_history,regional_view_visit_report;
     private AlertDialog dialog;
     private BottomSheetDialogFragment bottomnavigation;
     private SearchableSpinner orderspinner,attendancespinner;
     private String sales_order_id,sales_from_date,sales_to_date,attendance_sales_id,attendance_execode,searchspinners;
     private String salesfromdate,salestodate;
     private List<ViewVisitResult>data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regional_mainmenu);

        preferences = getSharedPreferences("Delphi", MODE_PRIVATE);
        editor = preferences.edit();
        preferences.getString("userType", "");
        regional_attendance=findViewById(R.id.regional_attendance);
        regional_sales_attendance=findViewById(R.id.regional_sales_attendance);
        regional_order_history=findViewById(R.id.regional_order_history);
        regional_view_visit_report=findViewById(R.id.regional_view_visit_report);

regional_attendance.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        bottomnavigation=new RegionalManagerBottomsheet();
        bottomnavigation.show(getSupportFragmentManager(),"exampleBottomSheet-");
    }
});


        regional_sales_attendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final AlertDialog.Builder alert=new AlertDialog.Builder(RegionalMainmenu.this);
                LayoutInflater inflater=getLayoutInflater();
                View views=inflater.inflate(R.layout.view_sales_executive_list,null);
                attendancespinner=(SearchableSpinner)views.findViewById(R.id.searchspinners);
                final TextView fromdate=(TextView)views.findViewById(R.id.sales_from_date);
                final TextView todate=(TextView)views.findViewById(R.id.sales_to_date);
                Button salesnamesearch=(Button)views.findViewById(R.id.sales_view_search);
                alert.setView(views);
                dialog=alert.create();
                dialog.show();

                attendancespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        attendance_sales_id=data.get(i).getId();
                        attendance_execode=data.get(i).getExecutiveCode();

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                salesnamesearch.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        sales_from_date=fromdate.getText().toString();
                         sales_to_date=todate.getText().toString();

                        if(sales_from_date.equals("")){
                            Toast.makeText(getApplicationContext(),"Please Choose From Date",Toast.LENGTH_LONG).show();
                        }
                        else if(sales_to_date.equals("")){
                            Toast.makeText(getApplicationContext(),"Please Choose To Date",Toast.LENGTH_LONG).show();
                        }
                        else{

                            NetworkConnection connection=new NetworkConnection(RegionalMainmenu.this);
                            if(connection.CheckInternet()){
                                salesexecutiveattendance();
                            }
                            else{
                                Alert alert=new Alert(RegionalMainmenu.this);
                                alert.showAlertboxnegative("Please check your network connection and try again!");
                            }

                        }
                    }
                });

                fromdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final Calendar c = Calendar.getInstance();
                        int mYear = c.get(Calendar.YEAR);
                        int mMonth = c.get(Calendar.MONTH);
                        int mDay = c.get(Calendar.DAY_OF_MONTH);

                        DatePickerDialog datePickerDialog = new DatePickerDialog(RegionalMainmenu.this,
                                new DatePickerDialog.OnDateSetListener() {

                                    @Override
                                    public void onDateSet(DatePicker view, int year,
                                                          int monthOfYear, int dayOfMonth) {

                                        fromdate.setText(year + "-" +(monthOfYear + 1) + "-" +dayOfMonth);

                                    }
                                }, mYear, mMonth, mDay);
                        datePickerDialog.show();

                    }
                });
                todate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final Calendar c = Calendar.getInstance();
                        int mYear = c.get(Calendar.YEAR);
                        int mMonth = c.get(Calendar.MONTH);
                        int mDay = c.get(Calendar.DAY_OF_MONTH);

                        DatePickerDialog datePickerDialog = new DatePickerDialog(RegionalMainmenu.this,
                                new DatePickerDialog.OnDateSetListener() {

                                    @Override
                                    public void onDateSet(DatePicker view, int year,
                                                          int monthOfYear, int dayOfMonth) {

                                        todate.setText(year + "-" +(monthOfYear + 1) + "-" +dayOfMonth);

                                    }
                                }, mYear, mMonth, mDay);
                        datePickerDialog.show();

                    }
                });

                try {

                    final ProgressDialog progressDialog = new ProgressDialog(RegionalMainmenu.this);
                    progressDialog.setIndeterminate(true);
                    progressDialog.setMessage("Loading...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    APIService service =RetroClient.getApiService();
                    Call<ViewVisitReportPojo>call=service.getsaleslists(preferences.getString("empId",""));
                    call.enqueue(new Callback<ViewVisitReportPojo>() {
                        @Override
                        public void onResponse(Call<ViewVisitReportPojo> call, Response<ViewVisitReportPojo> response) {

                            if(response.isSuccessful()){
                                if(response.body().getResult().equals("Success")){

                                    progressDialog.dismiss();
                                    data=response.body().getData();
//                                    ViewVisitResult visitreport=new ViewVisitResult();
////                                    visitreport.setName("All");
////                                    data.add(0,visitreport);
                                    ArrayAdapter adapter=new ArrayAdapter(RegionalMainmenu.this,R.layout.simple_spinner_item,data);
                                    attendancespinner.setAdapter(adapter);
                                }
                                else{
                                    progressDialog.dismiss();
                                    Alert alert=new Alert(RegionalMainmenu.this);
                                    alert.showAlertboxnegative("No Record Found");
                                }
                            }
                        }
                        @Override
                        public void onFailure(Call<ViewVisitReportPojo> call, Throwable t) {
                            progressDialog.dismiss();
                            Alert alert=new Alert(RegionalMainmenu.this);
                            alert.showAlertboxnegative("Something went wrong . Please try again later ..");
                        }
                    });
                }
                catch (Exception e){

                    e.printStackTrace();
                    Alert alerts=new Alert(RegionalMainmenu.this);
                    alerts.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            }
        });

        regional_view_visit_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent =new Intent(RegionalMainmenu.this,ViewVisitReportRegional.class);
                startActivity(intent);
            }
        });


        regional_order_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomnavigation=new BottomSheetListenerOrderView();
                bottomnavigation.show(getSupportFragmentManager(),"exampleBottomSheet-");
            }
        });
    }

    @Override
    public void bottomsheetlisteners(String orderselect) {

        if(orderselect.equals("currentmonth")){
            bottomnavigation.dismiss();
            Intent intet=new Intent(RegionalMainmenu.this,CurrentOrderViewRegional.class);
            startActivity(intet);
        }
        else{
            bottomnavigation.dismiss();
            final AlertDialog.Builder builder =new AlertDialog.Builder(RegionalMainmenu.this);
            final LayoutInflater inflate=getLayoutInflater();
            View views =inflate.inflate(R.layout.datewisenamelist,null);
            orderspinner=(SearchableSpinner)views.findViewById(R.id.searchspinner);
            final TextView fromdate=(TextView)views.findViewById(R.id.sales_from_date);
            final TextView todate=(TextView)views.findViewById(R.id.sales_to_date);
            Button salesnamesearch=(Button)views.findViewById(R.id.sales_view_search);

            try {
                final ProgressDialog progressDialog = new ProgressDialog(RegionalMainmenu.this);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Loading...");
                progressDialog.setCancelable(false);
                progressDialog.show();
                APIService service =RetroClient.getApiService();
                Call<ViewVisitReportPojo>call=service.getsaleslists(preferences.getString("empId",""));

                try {
                    call.enqueue(new Callback<ViewVisitReportPojo>() {
                        @Override
                        public void onResponse(Call<ViewVisitReportPojo> call, Response<ViewVisitReportPojo> response) {

                            if(response.isSuccessful()){
                                if(response.body().getResult().equals("Success")){
                                    progressDialog.dismiss();
                                    data=response.body().getData();
                                    ViewVisitResult visitreport=new ViewVisitResult();
//                                    visitreport.setName("All");
//                                    data.add(0,visitreport);
                                    ArrayAdapter adapter=new ArrayAdapter(RegionalMainmenu.this,R.layout.simple_spinner_item,data);
                                    orderspinner.setAdapter(adapter);
                                }
                                else{
                                    progressDialog.dismiss();
                                    Alert alert=new Alert(RegionalMainmenu.this);
                                    alert.showAlertboxnegative("No Record Found");
                                }
                             }
                        }

                        @Override
                        public void onFailure(Call<ViewVisitReportPojo> call, Throwable t) {
                            progressDialog.dismiss();
                            Alert alert=new Alert(RegionalMainmenu.this);
                            alert.showAlertboxnegative("Something went wrong . Please try again later ..");
                        }
                    });

                }catch (Exception e){
                    Alert alert=new Alert(RegionalMainmenu.this);
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            }
            catch (Exception e){
                Alert alert=new Alert(RegionalMainmenu.this);
                alert.showAlertboxnegative("Something went wrong . Please try again later .");
            }
orderspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        sales_order_id=data.get(i).getId();

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
});


            salesnamesearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    bottomnavigation.dismiss();
                     salesfromdate=fromdate.getText().toString();
                     salestodate=todate.getText().toString();
                      searchspinners=orderspinner.getSelectedItem().toString();
                    if(salesfromdate.equals("")){
                        Toast.makeText(getApplicationContext(),"Please Choose From Date",Toast.LENGTH_LONG).show();
                    }
                    else if(salestodate.equals("")){
                        Toast.makeText(getApplicationContext(),"Please Choose To Date",Toast.LENGTH_LONG).show();
                     }
                    else{
                        NetworkConnection connection=new NetworkConnection(RegionalMainmenu.this);
                        if(connection.CheckInternet()){
                           getsalesnames();
                        }
                        else{
                            Alert alert=new Alert(RegionalMainmenu.this);
                            alert.showAlertboxnegative("Please check your network connection and try again!");
                        }
                    }
                }
            });
            fromdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR);
                    int mMonth = c.get(Calendar.MONTH);
                    int mDay = c.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePickerDialog = new DatePickerDialog(RegionalMainmenu.this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {

                                    fromdate.setText(year + "-" +(monthOfYear + 1) + "-" +dayOfMonth);

                                }
                            }, mYear, mMonth, mDay);
                    datePickerDialog.show();

                }
            });


            todate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR);
                    int mMonth = c.get(Calendar.MONTH);
                    int mDay = c.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePickerDialog = new DatePickerDialog(RegionalMainmenu.this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {

                                    todate.setText(year + "-" +(monthOfYear + 1) + "-" +dayOfMonth);

                                }
                            }, mYear, mMonth, mDay);
                    datePickerDialog.show();

                }
            });
            builder.setView(views);
            dialog=builder.create();
            dialog.show();
        }
    }
    @Override
    public void onBackPressed() {
        Intent inten=new Intent(RegionalMainmenu.this, MainActivity.class);
        startActivity(inten);
    }
    public void salesexecutiveattendance(){

//        if(attendancespinner.getSelectedItem().toString().equals("All")){
//
//        }
//        else{

            Intent intents=new Intent(RegionalMainmenu.this,SalesAttendanceViewRegional.class);
            intents.putExtra("salesexeid",attendance_sales_id);
            intents.putExtra("sales_from_date",sales_from_date);
            intents.putExtra("sales_to_date",sales_to_date);
            intents.putExtra("salesexecode",attendance_execode);
            startActivity(intents);
            dialog.dismiss();
//        }

    }

    @Override
    public void bottomsheetlistener(String order) {
        if(order.equals("mark")){
            bottomnavigation.dismiss();
            Intent intent=new Intent(RegionalMainmenu.this, AttendanceActiveRegional.class);
            startActivity(intent);
        }
        else {
            bottomnavigation.dismiss();
            Intent intent = new Intent(RegionalMainmenu.this, AttendanceViewActivityRegional.class);
            startActivity(intent);
        }
    }
    private void getsalesnames(){
//
////        if(searchspinners.equals("All")){
////            dialog.dismiss();
////            Intent intent=new Intent(RegionalMainmenu.this,DateWiseOrdersRegional.class);
////
////            intent.putExtra("sales_from_date",salesfromdate);
////            intent.putExtra("sales_to_date",salestodate);
////            startActivity(intent);
//
//
//        }
//        else{
            dialog.dismiss();
            Intent intent=new Intent(RegionalMainmenu.this,DateWiseOrderRegional.class);
            intent.putExtra("sales_order_id",sales_order_id);
            intent.putExtra("sales_from_date",salesfromdate);
            intent.putExtra("sales_to_date",salestodate);
            startActivity(intent);

//        }
    }
}

