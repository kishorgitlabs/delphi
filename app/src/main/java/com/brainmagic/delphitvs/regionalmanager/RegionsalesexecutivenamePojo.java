package com.brainmagic.delphitvs.regionalmanager;

import java.util.List;

public class RegionsalesexecutivenamePojo{
	private String result;
	private List<String> data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(List<String> data){
		this.data = data;
	}

	public List<String> getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"RegionsalesexecutivenamePojo{" + 
			"result = '" + result + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}