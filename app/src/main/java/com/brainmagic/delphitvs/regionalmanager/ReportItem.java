package com.brainmagic.delphitvs.regionalmanager;

public class ReportItem{
	private String custMobileNo;
	private String visitedDate;
	private Object nextVisit;
	private String salesPersonName;
	private String placeOfVisit;
	private Object purposeOfVisit;
	private int exeId;
	private Object time;
	private int id;
	private String customerName;
	private String customerType;
	private String remark;

	public void setCustMobileNo(String custMobileNo){
		this.custMobileNo = custMobileNo;
	}

	public String getCustMobileNo(){
		return custMobileNo;
	}

	public void setVisitedDate(String visitedDate){
		this.visitedDate = visitedDate;
	}

	public String getVisitedDate(){
		return visitedDate;
	}

	public void setNextVisit(Object nextVisit){
		this.nextVisit = nextVisit;
	}

	public Object getNextVisit(){
		return nextVisit;
	}

	public void setSalesPersonName(String salesPersonName){
		this.salesPersonName = salesPersonName;
	}

	public String getSalesPersonName(){
		return salesPersonName;
	}

	public void setPlaceOfVisit(String placeOfVisit){
		this.placeOfVisit = placeOfVisit;
	}

	public String getPlaceOfVisit(){
		return placeOfVisit;
	}

	public void setPurposeOfVisit(Object purposeOfVisit){
		this.purposeOfVisit = purposeOfVisit;
	}

	public Object getPurposeOfVisit(){
		return purposeOfVisit;
	}

	public void setExeId(int exeId){
		this.exeId = exeId;
	}

	public int getExeId(){
		return exeId;
	}

	public void setTime(Object time){
		this.time = time;
	}

	public Object getTime(){
		return time;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setCustomerName(String customerName){
		this.customerName = customerName;
	}

	public String getCustomerName(){
		return customerName;
	}

	public void setCustomerType(String customerType){
		this.customerType = customerType;
	}

	public String getCustomerType(){
		return customerType;
	}

	public void setRemark(String remark){
		this.remark = remark;
	}

	public String getRemark(){
		return remark;
	}

	@Override
 	public String toString(){
		return 
			"ReportItem{" + 
			"custMobileNo = '" + custMobileNo + '\'' + 
			",visitedDate = '" + visitedDate + '\'' + 
			",nextVisit = '" + nextVisit + '\'' + 
			",salesPersonName = '" + salesPersonName + '\'' + 
			",placeOfVisit = '" + placeOfVisit + '\'' + 
			",purposeOfVisit = '" + purposeOfVisit + '\'' + 
			",exeId = '" + exeId + '\'' + 
			",time = '" + time + '\'' + 
			",id = '" + id + '\'' + 
			",customerName = '" + customerName + '\'' + 
			",customerType = '" + customerType + '\'' + 
			",remark = '" + remark + '\'' + 
			"}";
		}
}
