package com.brainmagic.delphitvs.regionalmanager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;

import java.util.List;

import adapter.regional.SalesAttendanceTimeRegionalAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.sales.viewattendance.attendancehistory.SalesAttendanceHistory;
import model.sales.viewattendance.attendancehistory.SalesAttendanceHistoryResult;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SalesAttendanceTimeRegional extends AppCompatActivity {

private ImageView menu,back;
private ListView regional_attendance_history_list;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private TextView attendance_history_date,attendance_history_day;
    private String sales_date,sales_day,sales_id;
    private List<SalesAttendanceHistoryResult>data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_attendancd_history);

        preferences = getSharedPreferences("Delphi", MODE_PRIVATE);
        editor = preferences.edit();
        menu=findViewById(R.id.regional_menu);
        back=findViewById(R.id.regional_back);
        regional_attendance_history_list=findViewById(R.id.regional_attendance_history_list);
        attendance_history_date=findViewById(R.id.attendance_history_date);
        attendance_history_day=findViewById(R.id.attendance_history_day);

    sales_date=getIntent().getStringExtra("regional_sales_date");
    sales_day=getIntent().getStringExtra("regional_sales_day");
sales_id=getIntent().getStringExtra("regional_sales_id");


    attendance_history_date.setText(sales_date);
    attendance_history_day.setText(sales_day);


        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final PopupMenu pop = new PopupMenu(SalesAttendanceTimeRegional.this, view);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu popupMenu) {
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.regional_home:
                                startActivity(new Intent(SalesAttendanceTimeRegional.this, MainActivity.class));
                                break;

                            case R.id.regional_mark_attendance:
                                startActivity(new Intent(SalesAttendanceTimeRegional.this, AttendanceActiveRegional.class));
                                break;
                            case R.id.regional_view_attendance:
                                startActivity(new Intent(SalesAttendanceTimeRegional.this, AttendanceViewActivityRegional.class));
                                break;
                            case R.id.regional_view_visitreport:
                                startActivity(new Intent(SalesAttendanceTimeRegional.this, ViewVisitReportRegional.class));
                                break;

                            case R.id.regional_logout:
                                editor.putBoolean("salesLogin", false);
                                editor.commit();
                                startActivity(new Intent(SalesAttendanceTimeRegional.this, MainActivity.class));
                                break;

                            case R.id.regional_change_password:
                                String phone = preferences.getString("empMobile", "");
                                final Alert alert = new Alert(SalesAttendanceTimeRegional.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;

                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.regional_manager_menu);
                pop.show();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);

            }
        });

        NetworkConnection connection=new NetworkConnection(SalesAttendanceTimeRegional.this);
        if(connection.CheckInternet()){

            getsalesattendacelist();
        }
        else{
            Alert alert=new Alert(SalesAttendanceTimeRegional.this);
            alert.showAlertboxnegative("Please check your network connection and try again!");
        }
    }

    private void getsalesattendacelist(){


        final ProgressDialog dialog=new ProgressDialog(SalesAttendanceTimeRegional.this);
        dialog.setIndeterminate(true);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        APIService service= RetroClient.getApiService();
        Call<SalesAttendanceHistory>call=service.salesAttendanceHistoryView(sales_id,sales_date);

        try {

            call.enqueue(new Callback<SalesAttendanceHistory>() {
                @Override
                public void onResponse(Call<SalesAttendanceHistory> call, Response<SalesAttendanceHistory> response) {
                    if(response.isSuccessful()){
                        if(response.body().getResult().equals("Success")){

                            dialog.dismiss();
                            data=response.body().getData();
                            SalesAttendanceTimeRegionalAdapter adapter=new SalesAttendanceTimeRegionalAdapter(SalesAttendanceTimeRegional.this,data);
                            regional_attendance_history_list.setAdapter(adapter);
                        }
                        else{
                            Alert alert=new Alert(SalesAttendanceTimeRegional.this);
                            alert.showAlertboxnegative("No Record Found");
                        }
                    }
                }

                @Override
                public void onFailure(Call<SalesAttendanceHistory> call, Throwable t) {

                    dialog.dismiss();
                    Alert alert=new Alert(SalesAttendanceTimeRegional.this);
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });

        }catch (Exception e){
            e.printStackTrace();
            Alert alert=new Alert(SalesAttendanceTimeRegional.this);
            alert.showAlertboxnegative("Something went wrong . Please try again later .");

        }
        regional_attendance_history_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

//                String[]datess=data.get(i).getDate().split("T");
//
//                String sales_datess=datess[0];

                Intent intent=new Intent(SalesAttendanceTimeRegional.this,SalesTravelHistoryRegional.class);

                intent.putExtra("sales_hitory_empid",data.get(i).getEmpId());
                intent.putExtra("sales_history_dates",sales_date);
               intent.putExtra("sales_trackids",data.get(i).getId());

               intent.putExtra("sales_in_time",data.get(i).getInTime());
               intent.putExtra("sales_out_time",data.get(i).getOutTime());
               intent.putExtra("sales_in_address",data.get(i).getAddress());
               intent.putExtra("sales_out_address",data.get(i).getOutAddress());
                startActivity(intent);

            }
        });
    }

}
