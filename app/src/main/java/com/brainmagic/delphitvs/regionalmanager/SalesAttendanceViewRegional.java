package com.brainmagic.delphitvs.regionalmanager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;

import java.util.List;

import adapter.regional.SalesAttendanceViewRegionalAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.sales.viewattendance.attendance.SalesAttendance;
import model.sales.viewattendance.attendance.SalesAttendanceResult;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SalesAttendanceViewRegional extends AppCompatActivity {

    private ImageView menu,back;
    private TextView sales_from_date,sales_to_date;
    private ListView sales_attendance_list_view;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private String fromdate,todate,salesid,salesexecode;
    private List<SalesAttendanceResult>data;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_r_m_view_sales_executive_attendance);

        preferences = getSharedPreferences("Delphi", MODE_PRIVATE);
        editor = preferences.edit();
menu=findViewById(R.id.menu);
back=findViewById(R.id.back);
sales_from_date=findViewById(R.id.sales_from_date);
sales_to_date=findViewById(R.id.sales_to_date);
sales_attendance_list_view=findViewById(R.id.sales_attendance_list_view);

        fromdate=getIntent().getStringExtra("sales_from_date");
        todate=getIntent().getStringExtra("sales_to_date");
        salesid=getIntent().getStringExtra("salesexeid");
        salesexecode=getIntent().getStringExtra("salesexecode");


sales_from_date.setText(fromdate);
sales_to_date.setText(todate);

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final PopupMenu pop = new PopupMenu(SalesAttendanceViewRegional.this, view);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu popupMenu) {
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.regional_home:
                                startActivity(new Intent(SalesAttendanceViewRegional.this, MainActivity.class));
                                break;

                            case R.id.regional_mark_attendance:
                                startActivity(new Intent(SalesAttendanceViewRegional.this, AttendanceActiveRegional.class));
                                break;
                            case R.id.regional_view_attendance:
//                                startActivity(new Intent(SalesAttendanceViewRegional.this, AttendanceViewActivityRegional.class));
                                break;
                            case R.id.regional_view_visitreport:
                                startActivity(new Intent(SalesAttendanceViewRegional.this, ViewVisitReportRegional.class));
                                break;

                            case R.id.regional_logout:
                                editor.putBoolean("salesLogin", false);
                                editor.commit();
                                startActivity(new Intent(SalesAttendanceViewRegional.this, MainActivity.class));
                                break;

                            case R.id.regional_change_password:
                                String phone = preferences.getString("empMobile", "");
                                final Alert alert = new Alert(SalesAttendanceViewRegional.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;

                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.regional_manager_menu);
                pop.show();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);

            }
        });
        NetworkConnection connection=new NetworkConnection(SalesAttendanceViewRegional.this);
        if(connection.CheckInternet()){
            getsaleslist();
        }
        else{

            Alert alert=new Alert(SalesAttendanceViewRegional.this);
            alert.showAlertboxnegative("Please check your network connection and try again!");
        }

    }

    private void getsaleslist() {
        final ProgressDialog progressDialog = new ProgressDialog(SalesAttendanceViewRegional.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();


        try {
            APIService service=RetroClient.getApiService();
            Call<SalesAttendance>call=service.viewsalesbyregional(salesid,salesexecode,fromdate,todate);

            call.enqueue(new Callback<SalesAttendance>() {
                @Override
                 public void onResponse(Call<SalesAttendance> call, Response<SalesAttendance> response) {
                    if(response.isSuccessful()){
                        if(response.body().getResult().equals("Success")){
                            progressDialog.dismiss();
                            data=response.body().getData();
                            SalesAttendanceViewRegionalAdapter adapter=new SalesAttendanceViewRegionalAdapter(SalesAttendanceViewRegional.this,data);
                            sales_attendance_list_view.setAdapter(adapter);


                        }
                        else{
                            progressDialog.dismiss();
                            Alert alert=new Alert(SalesAttendanceViewRegional.this);
                            alert.showAlertboxnegative("No Record Found.");
                        }


                    }
                }

                @Override
                public void onFailure(Call<SalesAttendance> call, Throwable t) {
                    progressDialog.dismiss();
                    Alert alert=new Alert(SalesAttendanceViewRegional.this);
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");

                }
            });

        }catch (Exception e){
                e.printStackTrace();
            Alert alert=new Alert(SalesAttendanceViewRegional.this);
            alert.showAlertboxnegative("Something went wrong . Please try again later ..");


        }

    sales_attendance_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            String[] salesdates=data.get(i).getDate().split("T");

            Intent intent=new Intent(SalesAttendanceViewRegional.this,SalesAttendanceTimeRegional.class);
            intent.putExtra("regional_sales_date",salesdates[0]);
            intent.putExtra("regional_sales_day",data.get(i).getAttendDay());
            intent.putExtra("regional_sales_id",salesid);
            startActivity(intent);


        }
    });

    }

}
