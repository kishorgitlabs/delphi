package com.brainmagic.delphitvs.regionalmanager;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import android.view.View;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;

import com.brainmagic.delphitvs.Constants;
import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.map.animatepolyline.MapAnimatorHistory;
import com.brainmagic.delphitvs.map.getPolyline;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import alert.Alert;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import model.api.APIService;
import model.api.RetroClient;
import model.sales.viewattendance.salescoordinate.SalesCoordinates;
import model.sales.viewattendance.salescoordinate.SalesDataList;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SalesMapActive extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private List<LatLng> listLatLng = new ArrayList<>();
    private List<LatLng> listLatLngRoutes = new ArrayList<>();
//    private List<Codination> codinations;
    private String fromAddress="",toAddress="";
    private Alert alert;
    private String Rempid;
    private LatLng source,destination;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        alert=new Alert(SalesMapActive.this);
        sharedPreferences = this.getSharedPreferences(Constants.Config, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        fromAddress=getIntent().getStringExtra("RfromAddress");
        toAddress=getIntent().getStringExtra("RtoAddress");
        Rempid=getIntent().getStringExtra("Rempid");
        source=new LatLng(Double.valueOf(getIntent().getStringExtra("RinLat")),Double.valueOf(getIntent().getStringExtra("RinLon")));
        destination=new LatLng(Double.valueOf(getIntent().getStringExtra("RoutLat")),Double.valueOf(getIntent().getStringExtra("RoutLon")));

//        source = new LatLng(Double.valueOf(getIntent().getStringExtra("fromlatitude")), Double.valueOf(getIntent().getStringExtra("fromlongitude")));

        checkInternet();

    }

    private void checkInternet() {
        NetworkConnection net = new NetworkConnection(SalesMapActive.this);
        if (net.CheckInternet()) {
            getcoordinates();
//            new GetTravelHistory().execute("select distinct * from coordinate where datetime ='" + Date + "' and S_id ='"+empid+"'");
        } else {
            alert.showAlertboxnegative("Please check your network connection and try again!");
        }
    }

    private void getcoordinates() {
        try {
            final ArrayList<LatLng> points = new ArrayList<LatLng>();
            final ProgressDialog progressDialog = new ProgressDialog(SalesMapActive.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            APIService service = RetroClient.getApiService();
            // Calling JSON
            Call<SalesCoordinates> call = service.viewSalesCoordinate(Rempid, String.valueOf(getIntent().getStringExtra("RtrackId")),getIntent().getStringExtra("Rdate"));
            call.enqueue(new Callback<SalesCoordinates>() {
                @Override
                public void onResponse(Call<SalesCoordinates> call, Response<SalesCoordinates> response) {
                    progressDialog.dismiss();

                    if(response.isSuccessful()) {
                        if (response.body().getResult().equals("Success")) {
//                        source=new LatLng(response.body().getData().getSource().getInLatitude(),response.body().getData().getSource().getInLongitude());
//                        destination=new LatLng(response.body().getData().getDesignation().getOutLatitude(),response.body().getData().getDesignation().getOutLongitude());
                            List<SalesDataList> codinations = response.body().getData();

                            for (SalesDataList codination : codinations) {
                                double lat = (codination.getLatitude());
                                double lng = (codination.getLangtitude());
                                LatLng position = new LatLng(lat, lng);
                                points.add(position);
                            }
//                        animatePolyLine(points, source, destination);
                            setUpPolyLine();
                        }
                    }else  if (response.body().getResult().equals("NotSuccess")){
                        getFailureAlert("Trip coordination wrong");
                    }
                }
                @Override
                public void onFailure(Call<SalesCoordinates> call, Throwable t) {
                    progressDialog.dismiss();
                    getFailureAlert(t.getMessage());
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
            getFailureAlert(ex.getMessage());
        }
    }

//    private void animatePolyLine(ArrayList<LatLng> points, LatLng source, LatLng destination) {
//        if (mMap != null && listLatLng != null && destination != null) {
////            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
//            {
//                MapAnimatorRealTravelRoute.getInstance().animateRoute(MapsActivity.this, mMap, points, source, destination,fromaddress,toaddress);
//            }
//        } else {
//            Toast.makeText(getApplicationContext(), "Map not ready", Toast.LENGTH_LONG).show();
//        }
//    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
    }

    protected void setUpPolyLine() {
        if (source != null && destination != null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://maps.googleapis.com/maps/api/directions/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            getPolyline polyline = retrofit.create(getPolyline.class);

//            polyline.getPolylineData(source.latitude + "," + source.longitude, destination.latitude + "," + destination.longitude,getResources().getString(R.string.google_maps_keycera))

            polyline.getPolylineData(source.latitude + "," + source.longitude,
//                    destination.latitude + "," + destination.longitude,getResources().getString(R.string.google_maps_key))
                    destination.latitude + "," + destination.longitude,Constants.APIKEY)
                    .enqueue(new Callback<JsonObject>() {
                        @RequiresApi(api = Build.VERSION_CODES.M)
                        @Override
                        public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                            JsonObject gson = new JsonParser().parse(response.body().toString()).getAsJsonObject();
                            try {
                                Single.just(parse(new JSONObject(gson.toString())))
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new Consumer<List<List<HashMap<String, String>>>>() {
                                            @Override
                                            public void accept(List<List<HashMap<String, String>>> lists) throws Exception {
                                                drawPolyline(lists, source, destination);
                                            }
                                        });


                            }
                            catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<JsonObject> call, Throwable t) {
                            Toast.makeText(SalesMapActive.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();

    }
    public List<List<HashMap<String, String>>> parse(JSONObject jObject) {

        List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String, String>>>();
        JSONArray jRoutes = null;
        JSONArray jLegs = null;
        JSONArray jSteps = null;

        try {

            jRoutes = jObject.getJSONArray("routes");

            /** Traversing all routes */
            for (int i = 0; i < jRoutes.length(); i++) {
                jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
                List path = new ArrayList<HashMap<String, String>>();

                /** Traversing all legs */
                for (int j = 0; j < jLegs.length(); j++) {
                    jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");

                    /** Traversing all steps */
                    for (int k = 0; k < jSteps.length(); k++) {
                        String polyline = "";
                        polyline = (String) ((JSONObject) ((JSONObject) jSteps.get(k)).get("polyline")).get("points");
                        List<LatLng> list = decodePoly(polyline);

                        /** Traversing all points */
                        for (int l = 0; l < list.size(); l++) {
                            HashMap<String, String> hm = new HashMap<String, String>();
                            hm.put("lat", Double.toString(((LatLng) list.get(l)).latitude));
                            hm.put("lng", Double.toString(((LatLng) list.get(l)).longitude));
                            path.add(hm);
                        }
                    }
                    routes.add(path);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception ignored) {
        }
        return routes;
    }
    private void getFailureAlert(String msg) {
        alert.showAlertBoxWithListener(msg,View.GONE);
        alert.setOnPositiveClickListener(new Alert.onPositiveClickListener() {
            @Override
            public void onPositiveClick() {

                finish();
            }
        });
//        alerterror=new android.support.v7.app.AlertDialog.Builder(MapsActivity.this).create();
//        View orderplacedshow= LayoutInflater.from(MapsActivity.this).inflate(R.layout.orderplaced,null);
//        TextView ordernumber=orderplacedshow.findViewById(R.id.ordernumber);
//        TextView okcart=orderplacedshow.findViewById(R.id.okcart);
//        ordernumber.setText(alert);
//
//        okcart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent home=new Intent(getApplicationContext(), NewHomeLTVS.class);
//                home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(home);
//            }
//        });
//        alerterror.setView(orderplacedshow);
//        alerterror.setCanceledOnTouchOutside(false);
//        alerterror.show();
    }

    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void drawPolyline(List<List<HashMap<String, String>>> result, LatLng source, LatLng destination) {

        ArrayList<LatLng> points = null;
        PolylineOptions lineOptions = null;
        listLatLngRoutes.clear();
        // Traversing through all the routes
        if (result.size() != 0) {
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));

                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                this.listLatLngRoutes.addAll(points);
            }
            animatePolyLine(listLatLng, source, destination,listLatLngRoutes);


        } else {
            setUpPolyLine();
        }

    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void animatePolyLine(List<LatLng> listLatLng, LatLng source, LatLng destination, List<LatLng> listLatLngRoutes) {
        if (mMap != null && listLatLng != null && destination != null) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            {
                MapAnimatorHistory.getInstance().animateRoute(SalesMapActive.this, mMap, listLatLng, source, destination,fromAddress,toAddress,listLatLngRoutes);
            }
        } else {
            Toast.makeText(getApplicationContext(), "Map not ready", Toast.LENGTH_LONG).show();
        }
    }

}
