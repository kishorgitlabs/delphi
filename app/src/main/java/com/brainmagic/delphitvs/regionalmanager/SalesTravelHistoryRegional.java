package com.brainmagic.delphitvs.regionalmanager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.salesactivities.ViewAttendanceCoordinateActivitySales;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import adapter.regional.SalesTravelHistoryRegionalAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.sales.viewattendance.salescoordinate.SalesCoordinates;
import model.sales.viewattendance.salescoordinate.SalesDataList;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SalesTravelHistoryRegional extends AppCompatActivity {

    private ImageView menu,back;
    private ListView regional_travel_history;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private TextView sales_in_time,sales_out_time;
    private String sales_history_date,sales_hitory_empid,sales_trackid,sales_in_times,sales_out_times,sales_in_address,sales_out_address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_attendance_coordinate);

        preferences = getSharedPreferences("Delphi", MODE_PRIVATE);
        editor = preferences.edit();
        menu=findViewById(R.id.regional_menu);
        back=findViewById(R.id.regional_back);
        regional_travel_history=findViewById(R.id.regional_travel_history);
        sales_in_time=findViewById(R.id.sales_in_time);
        sales_out_time=findViewById(R.id.sales_out_time);

        sales_history_date=getIntent().getStringExtra("sales_history_dates");
        sales_hitory_empid=getIntent().getStringExtra("sales_hitory_empid");
        sales_trackid=getIntent().getStringExtra("sales_trackids");
        sales_in_times=getIntent().getStringExtra("sales_in_time");
        sales_out_times=getIntent().getStringExtra("sales_out_time");
        sales_in_address=getIntent().getStringExtra("sales_in_address");
        sales_out_address=getIntent().getStringExtra("sales_out_address");

        sales_in_time.setText(sales_in_times);
        sales_out_time.setText(sales_out_times);

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final PopupMenu pop = new PopupMenu(SalesTravelHistoryRegional.this, view);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu popupMenu) {
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.regional_home:
                                startActivity(new Intent(SalesTravelHistoryRegional.this, MainActivity.class));
                                break;

                            case R.id.regional_mark_attendance:
                                startActivity(new Intent(SalesTravelHistoryRegional.this, AttendanceActiveRegional.class));
                                break;
                            case R.id.regional_view_attendance:
                                startActivity(new Intent(SalesTravelHistoryRegional.this, AttendanceViewActivityRegional.class));
                                break;
                            case R.id.regional_view_visitreport:
                                startActivity(new Intent(SalesTravelHistoryRegional.this, ViewVisitReportRegional.class));
                                break;

                            case R.id.regional_logout:
                                editor.putBoolean("salesLogin", false);
                                editor.commit();
                                startActivity(new Intent(SalesTravelHistoryRegional.this, MainActivity.class));
                                break;

                            case R.id.regional_change_password:
                                String phone = preferences.getString("empMobile", "");
                                final Alert alert = new Alert(SalesTravelHistoryRegional.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;

                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.regional_manager_menu);
                pop.show();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);

            }
        });


        NetworkConnection connection=new NetworkConnection(SalesTravelHistoryRegional.this);
        if(connection.CheckInternet()){
            getcoordinateslist();
        }
        else{

            Alert alert=new Alert(SalesTravelHistoryRegional.this);
            alert.showAlertboxnegative("Please check your network connection and try again!");
        }

    }

    private void getcoordinateslist()
    {
        final ProgressDialog progressDialog = new ProgressDialog(SalesTravelHistoryRegional.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {
            APIService service = RetroClient.getApiService();

            Call<SalesCoordinates> call = service.viewSalesCoordinate(sales_hitory_empid,sales_trackid,sales_history_date);
            call.enqueue(new Callback<SalesCoordinates>() {
                @Override
                public void onResponse(Call<SalesCoordinates> call, Response<SalesCoordinates> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
                                List<SalesDataList>  coordinates=response.body().getData();
                                SalesDataList viewSalesCoordinateResultIn=new SalesDataList();
                                try {
                                    final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                                    final Date dateObj = sdf.parse(sales_in_times);
                                    Log.d(ViewAttendanceCoordinateActivitySales.class.getName(), "onResponse: ");
                                    viewSalesCoordinateResultIn.setAddress(sales_in_address);
                                    viewSalesCoordinateResultIn.setTimedate(new SimpleDateFormat("hh:mm aa").format(dateObj));
                                }catch (Exception e)
                                {
                                    e.printStackTrace();
                                }
                                coordinates.add(0,viewSalesCoordinateResultIn);
                                SalesDataList viewSalesCoordinateResultOut=new SalesDataList();
                                try{
                                    final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                                    final Date dateObj = sdf.parse(sales_out_times);
                                    viewSalesCoordinateResultOut.setAddress(sales_out_address);
                                    viewSalesCoordinateResultOut.setTimedate(new SimpleDateFormat("hh:mm a").format(dateObj));

                                }catch (Exception e)
                                {
                                    e.printStackTrace();
                                }
                                coordinates.add(coordinates.size(),viewSalesCoordinateResultOut);
                                SalesTravelHistoryRegionalAdapter viewAttendanceAdapterSales=new SalesTravelHistoryRegionalAdapter(SalesTravelHistoryRegional.this,coordinates);
                                regional_travel_history .setAdapter(viewAttendanceAdapterSales);
                            }
                            else if(response.body().getResult().equals("NotSuccess"))
                            {
                                Alert alert=new Alert(SalesTravelHistoryRegional.this);
                                alert.showAlertboxnegative("No Attendance Coordinate records Found");
                            }
                            else {
                                Alert alert=new Alert(SalesTravelHistoryRegional.this);
                                alert.showAlertboxnegative("Invalid Login. Please check your User Name and Password");
                            }
                        }
                        else {
                            Alert alert=new Alert(SalesTravelHistoryRegional.this);
                            alert.showAlertboxnegative("Something went wrong . Please try again later .");
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                        Alert alert=new Alert(SalesTravelHistoryRegional.this);
                        alert.showAlertboxnegative("Something went wrong . Please try again later .");
                    }

                }

                @Override
                public void onFailure(Call<SalesCoordinates> call, Throwable t) {
                    progressDialog.dismiss();
                    Alert alert=new Alert(SalesTravelHistoryRegional.this);
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
            progressDialog.dismiss();
            Alert alert=new Alert(SalesTravelHistoryRegional.this);
            alert.showAlertboxnegative("Local Data is Corrupted. Please logout and login.");
        }
    }



}
