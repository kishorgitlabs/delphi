package com.brainmagic.delphitvs.regionalmanager;




import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "regionalattendance")

public class UpLoadCoordinateRegional {


    @PrimaryKey(autoGenerate = true)
    private int pId;
    @ColumnInfo(name="Address")
    @SerializedName("Address")
    private String Address;
    @ColumnInfo(name="Datetime")
    @SerializedName("Date")
    private String Datetime;
    @ColumnInfo(name="Distance")
    @SerializedName("distance")
    private Double Distance;
    @ColumnInfo(name="EmpId")
    @SerializedName("empId")
    private Long EmpId;
    @ColumnInfo(name="EmpName")
    @SerializedName("empName")
    private String EmpName;
    @ColumnInfo(name="langtitude")
    @SerializedName("longitude")
    private Double Langtitude;
    @ColumnInfo(name="latitude")
    @SerializedName("latitude")
    private Double Latitude;
    @ColumnInfo(name="timedate")
    @SerializedName("time")
    private String Timedate;
    @ColumnInfo(name="TrackId")
    @SerializedName("trackId")
    private Long TrackId;
    @ColumnInfo(name="updateddate")
    @SerializedName("updateddate")
    private String Updateddate;


    public int getpId() {
        return pId;
    }

    public void setpId(int pId) {
        this.pId = pId;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getDatetime() {
        return Datetime;
    }

    public void setDatetime(String datetime) {
        Datetime = datetime;
    }

    public Double getDistance() {
        return Distance;
    }

    public void setDistance(Double distance) {
        Distance = distance;
    }

    public Long getEmpId() {
        return EmpId;
    }

    public void setEmpId(Long empId) {
        EmpId = empId;
    }

    public String getEmpName() {
        return EmpName;
    }

    public void setEmpName(String empName) {
        EmpName = empName;
    }

    public Double getLangtitude() {
        return Langtitude;
    }

    public void setLangtitude(Double langtitude) {
        Langtitude = langtitude;
    }

    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        Latitude = latitude;
    }

    public String getTimedate() {
        return Timedate;
    }

    public void setTimedate(String timedate) {
        Timedate = timedate;
    }

    public Long getTrackId() {
        return TrackId;
    }

    public void setTrackId(Long trackId) {
        TrackId = trackId;
    }

    public String getUpdateddate() {
        return Updateddate;
    }

    public void setUpdateddate(String updateddate) {
        Updateddate = updateddate;
    }
}
