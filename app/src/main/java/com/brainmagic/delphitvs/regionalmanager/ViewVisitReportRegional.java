package com.brainmagic.delphitvs.regionalmanager;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;


import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.salesactivities.ViewAttendanceActivitySales;
import com.brainmagic.delphitvs.salesactivities.ViewVisitReport;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import adapter.regional.ViewSalesNameAdapter;
import adapter.regional.ViewVisitRegionaSearchlAdapter;
import adapter.regional.ViewVisitReportRegionalAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.regional.DataItem;
import model.regional.ViewVisitReportPojo;
import model.sales.visitreport.Datum;
import model.sales.visitreport.ViewVisitReportModel;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class  ViewVisitReportRegional extends AppCompatActivity {

    private ImageView view_visit_rm_menu, view_visit_rm_back;
    private TextView view_visit_rm_fromdate, view_visit_rm_todate;
    private MaterialSpinner view_visit_rm_customertype;
    private Button view_visit_rm_search;
    private ListView view_visit_report_rm_list;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private String salesname, salesid;
    private AlertDialog dialog;
    private List<Datum> visitreport;
    private ViewSalesNameAdapter adapters;
    private String sales_from_dates, sales_to_dates, sales_customertypes;
    List<DataItem> datas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_r_m_view_visit_report);


        view_visit_rm_menu = findViewById(R.id.view_visit_rm_menu);
        view_visit_rm_back = findViewById(R.id.view_visit_rm_back);
        view_visit_rm_fromdate = findViewById(R.id.view_visit_report_rm_fromdate);
        view_visit_rm_todate = findViewById(R.id.view_visit_report_rm_todate);
        view_visit_rm_customertype = findViewById(R.id.view_visit_report_rm_customertype);
        view_visit_rm_search = findViewById(R.id.view_visit_report_rm_searchs);
        view_visit_report_rm_list = findViewById(R.id.view_visit_report_rm_list);
        preferences = getSharedPreferences("Delphi", MODE_PRIVATE);
        editor = preferences.edit();
        salesname = preferences.getString("empName", "");


        view_visit_rm_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sales_from_dates = view_visit_rm_fromdate.getText().toString();
                sales_to_dates = view_visit_rm_todate.getText().toString();
                sales_customertypes = view_visit_rm_customertype.getText().toString();


                if (sales_from_dates.equals("")) {
                    Toast.makeText(getApplicationContext(), "Select From Date", Toast.LENGTH_LONG).show();

                } else if (sales_to_dates.equals("")) {
                    Toast.makeText(getApplicationContext(), "Select To Date", Toast.LENGTH_LONG).show();
                } else if (sales_customertypes.equals("Select Customer Type")) {
                    Toast.makeText(getApplicationContext(), "Select Customer Type", Toast.LENGTH_LONG).show();
                } else {
                    NetworkConnection connection = new NetworkConnection(ViewVisitReportRegional.this);

                    if (connection.CheckInternet()) {
                        viewVisitReportBySearch();
                    } else {
                        Alert alert = new Alert(ViewVisitReportRegional.this);
                        alert.showAlertboxnegative("Please Switch On Your Internet Connection");
                    }


                }

            }
        });
        view_visit_rm_fromdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(ViewVisitReportRegional.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                view_visit_rm_fromdate.setText(year + "-" +(monthOfYear + 1) + "-" +dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

            }
        });
        view_visit_rm_todate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(ViewVisitReportRegional.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                view_visit_rm_todate.setText(year + "-" +(monthOfYear + 1) + "-" +dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                    datePickerDialog.show();

            }
        });


        List<String> customertype = new ArrayList<>();
        customertype.add("Select Customer Type");
        customertype.add("Electrician");
        customertype.add("Retailer");
        customertype.add("Mechanic");
        customertype.add("Others");

        view_visit_rm_customertype.setItems(customertype);

        view_visit_rm_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });


        view_visit_rm_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final PopupMenu pop = new PopupMenu(ViewVisitReportRegional.this, view);
                pop.inflate(R.menu.regional_manager_menu);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu popupMenu) {
                        pop.dismiss();
                    }
                });

                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.regional_home:
                                startActivity(new Intent(ViewVisitReportRegional.this, MainActivity.class));
                                break;

                            case R.id.regional_mark_attendance:
                                startActivity(new Intent(ViewVisitReportRegional.this, AttendanceActiveRegional.class));
                                break;

                            case R.id.regional_view_attendance:

                                startActivity(new Intent(ViewVisitReportRegional.this, AttendanceViewActivityRegional.class));
                                break;
                            case R.id.regional_view_visitreport:
                                break;
                            case R.id.regional_logout:
                                editor.putBoolean("salesLogin", false);
                                editor.commit();
                                startActivity(new Intent(ViewVisitReportRegional.this, MainActivity.class).
                                        addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;

                            case R.id.regional_change_password:
                                String phone = preferences.getString("empMobile", "");
                                final Alert alert = new Alert(ViewVisitReportRegional.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;
                        }
                        return false;
                    }

                });
                pop.show();

            }
        });

        NetworkConnection connection = new NetworkConnection(ViewVisitReportRegional.this);
        if (connection.CheckInternet()) {

            getsaleslist();
        } else {
            Alert alert = new Alert(ViewVisitReportRegional.this);
            alert.showAlertboxnegative("Please check your network connection and try again!");
        }

    }


    private void getsaleslist() {

        AlertDialog.Builder builder = new AlertDialog.Builder(ViewVisitReportRegional.this);
        LayoutInflater inflate = getLayoutInflater();
        View views = inflate.inflate(R.layout.dialog_layout_regional, null);
        builder.setView(views);
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();

        final EditText regional_search_box = (EditText) views.findViewById(R.id.regional_search_box);
        final ListView regional_search_list = (ListView) views.findViewById(R.id.regional_search_list);
        TextView regional_search_close = (TextView) views.findViewById(R.id.regional_search_close);

        regional_search_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ViewVisitReportRegional.this, RegionalMainmenu.class);
                startActivity(intent);

            }
        });

        regional_search_box.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                String text = regional_search_box.getText().toString().toLowerCase(Locale.getDefault());
                adapters.filter(text);
            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });

        try {

            APIService service = RetroClient.getApiService();
            retrofit2.Call<ViewVisitReportPojo> call = service.getsaleslist(preferences.getString("empId", ""));

            call.enqueue(new Callback<ViewVisitReportPojo>() {
                @Override
                public void onResponse(retrofit2.Call<ViewVisitReportPojo> call, final Response<ViewVisitReportPojo> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getResult().equals("Success")) {


                            adapters = new ViewSalesNameAdapter(ViewVisitReportRegional.this, response.body().getData());
                            regional_search_list.setAdapter(adapters);

                            regional_search_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    dialog.dismiss();
                                    salesid = response.body().getData().get(i).getId();

                                    NetworkConnection connection = new NetworkConnection(ViewVisitReportRegional.this);
                                    if (connection.CheckInternet()) {
                                        getcustomerlist();
                                    } else {
                                        Alert alert = new Alert(ViewVisitReportRegional.this);
                                        alert.showAlertboxnegative("Please check your network connection and try again!");
                                    }

                                }
                            });

                        } else {
                            Alert alert = new Alert(ViewVisitReportRegional.this);
                            alert.showAlertboxnegative("No Record Found");
                        }
                    }
                }

                @Override
                public void onFailure(retrofit2.Call<ViewVisitReportPojo> call, Throwable t) {

                    Alert alert = new Alert(ViewVisitReportRegional.this);
                    alert.showAlertboxnegative("Something went wrong . Please try again later ..");
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
            Alert alerts = new Alert(ViewVisitReportRegional.this);
            alerts.showAlertboxnegative("Something went wrong . Please try again later .");
        }


    }


    private void getcustomerlist() {

        final ProgressDialog progressDialog = new ProgressDialog(ViewVisitReportRegional.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading...");
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        APIService api = RetroClient.getApiService();
        retrofit2.Call<ViewVisitReportModel> call = api.viewVisitReport(salesid);

        try {
            call.enqueue(new Callback<ViewVisitReportModel>() {
                @Override
                public void onResponse(retrofit2.Call<ViewVisitReportModel> call, Response<ViewVisitReportModel> response) {
                    try {
                        progressDialog.dismiss();
                        if (response.isSuccessful()) {
                            if (response.body().getResult().equals("Success")) {
                                visitreport = response.body().getData();
                                ViewVisitReportRegionalAdapter adapter = new ViewVisitReportRegionalAdapter(ViewVisitReportRegional.this, visitreport);
                                view_visit_report_rm_list.setAdapter(adapter);
                            } else if (response.body().getResult().equals("NotSuccess")) {
                                Alert alertbox = new Alert(ViewVisitReportRegional.this);
                                alertbox.showAlertboxnegative("No Record Found");
                            } else {
                                Alert alertbox = new Alert(ViewVisitReportRegional.this);
                                alertbox.showAlertboxnegative("Something went wrong . Please try again later .");
                            }
                        }
                    } catch (Exception e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        Alert alertbox = new Alert(ViewVisitReportRegional.this);
                        alertbox.showAlertboxnegative("Something went wrong . Please try again later .");
                    }
                }

                @Override
                public void onFailure(retrofit2.Call<ViewVisitReportModel> call, Throwable t) {
                    progressDialog.dismiss();
                    Alert alertbox = new Alert(ViewVisitReportRegional.this);
                    alertbox.showAlertboxnegative("Something went wrong . Please try again later .");

                }

            });
        } catch (Exception e) {
            progressDialog.dismiss();
            e.printStackTrace();
            Alert alertbox = new Alert(ViewVisitReportRegional.this);
            alertbox.showAlertboxnegative("Something went wrong . Please try again later .");
        }

    }


    private void viewVisitReportBySearch() {

        final ProgressDialog dialog = new ProgressDialog(ViewVisitReportRegional.this);
        dialog.setIndeterminate(true);
        dialog.setMessage("Loading");
        dialog.setCancelable(true);
        dialog.show();

        APIService api=RetroClient.getApiService();


        Call<ViewVisitReportModel> call=api.viewVisitReportDateWise(sales_from_dates,sales_to_dates,salesid,sales_customertypes);

        try {
            call.enqueue(new Callback<ViewVisitReportModel>() {
                @Override
                public void onResponse(Call<ViewVisitReportModel> call, Response<ViewVisitReportModel> response) {
                    try{
                        dialog.dismiss();
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
                                ViewVisitRegionaSearchlAdapter adapter=new ViewVisitRegionaSearchlAdapter(ViewVisitReportRegional.this,response.body().getData());
                                view_visit_report_rm_list.setAdapter(adapter);
                            }
                            else if(response.body().getResult().equals("NotSuccess"))
                            {
                                Alert alertbox=new Alert(ViewVisitReportRegional.this);
                                alertbox.showAlertbox("No Record Found");
                            }
                            else {
                                Alert alertbox=new Alert(ViewVisitReportRegional.this);
                                alertbox.showAlertbox("Something went wrong . Please try again later .");
                            }
                        }
                    }catch (Exception e){
                        dialog.dismiss();
                        e.printStackTrace();
                        Alert alertbox=new Alert(ViewVisitReportRegional.this);
                        alertbox.showAlertbox("Something went wrong . Please try again later .");
                    }
                }

                @Override
                public void onFailure(Call<ViewVisitReportModel> call, Throwable t) {
                    dialog.dismiss();
                    Alert alertbox=new Alert(ViewVisitReportRegional.this);
                    alertbox.showAlertbox("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            dialog.dismiss();
            e.printStackTrace();
            Alert alertbox=new Alert(ViewVisitReportRegional.this);
            alertbox.showAlertbox("Something went wrong . Please try again later .");
        }
    }
}


