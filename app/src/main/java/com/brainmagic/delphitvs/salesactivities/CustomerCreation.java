package com.brainmagic.delphitvs.salesactivities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.OpenableColumns;


import android.os.Bundle;

import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.General;
import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.ProductCatalogue;
import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.SalesLogin;
import com.brainmagic.delphitvs.VehicleSegment;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.myhexaville.smartimagepicker.ImagePicker;
import com.myhexaville.smartimagepicker.OnImagePickedListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.sales.createcustomer.CreateCustomerModel;
import model.sales.createcustomer.imageupload.ImageUpload;
import model.sales.customerdetails.search.CustomerNameList;
import network.NetworkConnection;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerCreation extends AppCompatActivity {

    private ImageView img_back,menu;
    private Alert alert;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private MaterialSpinner customerTypes,state;
    private EditText shopName,city, contactPerson, mobileNo, emailId, addressOne, addresstwo, pinCode, gstNo;
    private Button imageUpload;
    private Uri i;
    private ImagePicker imagePicker;
    private File mImageFile;
    private String mImageName = "";
    ImageView  selectimage;
    String imagename="";
    private LinearLayout imageLayout;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_creation);

        preferences= getSharedPreferences("Delphi", MODE_PRIVATE);
        alert=new Alert(this);
        editor=preferences.edit();
        progressDialog = new ProgressDialog(CustomerCreation.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);

        img_back =(ImageView)findViewById(R.id.back);
        customerTypes=findViewById(R.id.customer_type);
        final LinearLayout shopLayout=findViewById(R.id.shop_layout);
        shopName=findViewById(R.id.shop_name_edit_customer);
        contactPerson=findViewById(R.id.contact_person_edit_customer);
        emailId=findViewById(R.id.email_id_edit_customer);
        addressOne=findViewById(R.id.customer_address_one);
        addresstwo=findViewById(R.id.customer_address_two);
        pinCode=findViewById(R.id.customer_pincode_edit_customer);
        gstNo=findViewById(R.id.gst_no_edit_customer);
        state=findViewById(R.id.customer_state);
        city=findViewById(R.id.customer_city);
        mobileNo=findViewById(R.id.mobile_no_edit_customer);
        imageUpload=findViewById(R.id.image_upload);
        selectimage=findViewById(R.id.uploaded_image);
        imageLayout=findViewById(R.id.image_layout);

        List<String> userTypes=new ArrayList<>();
        userTypes.add("Select Customer");
        userTypes.add("Mechanic");
        userTypes.add("Retailer");
        userTypes.add("Electrician");
        userTypes.add("Others");

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        menu=findViewById(R.id.menu);

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(CustomerCreation.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.sales_home:
                                startActivity(new Intent(CustomerCreation.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.dashboard:
                                startActivity(new Intent(CustomerCreation.this, SalesMainmenu.class));
                                break;


                            case R.id.view_customer:
                                startActivity(new Intent(CustomerCreation.this, ViewCustomerActivity.class));
                                break;

                            case R.id.visit_report:
                                startActivity(new Intent(CustomerCreation.this, VisitReport.class));
                                break;
                            case R.id.view_visit_report:
                                startActivity(new Intent(CustomerCreation.this, ViewVisitReport.class));
                                break;
//                            case R.id.view_cart:
//                                startActivity(new Intent(CustomerCreation.this, SelectPartDetails.class));
//                                break;
                            case R.id.sales_logout:
                                editor.putBoolean("salesLogin",false);
                                editor.commit();
                                startActivity(new Intent(CustomerCreation.this, MainActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.sales_change_password:
                                String phone=preferences.getString("empMobile","");
                                final Alert alert=new Alert(CustomerCreation.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;
                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.sales_menu);
                pop.getMenu().findItem(R.id.create_customer).setVisible(false);

                pop.show();
            }
        });

//        ArrayAdapter arrayAdapter=new ArrayAdapter(this,android.R.layout.simple_spinner_item,userTypes);
//
//        customerTypes.setAdapter(arrayAdapter);

        customerTypes.setItems(userTypes);

        imageUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imagePicker=new ImagePicker(CustomerCreation.this, null, new OnImagePickedListener() {
                    @Override
                    public void onImagePicked(Uri imageUri) {
                        imageLayout.setVisibility(View.VISIBLE);
                        selectimage.setImageURI(imageUri);
                        mImageName = getFileName(imageUri);
                        mImageFile = getFileFromImage();
                        i=imageUri;
                    }
                });
                imagePicker.choosePicture(true);
            }
        });

        customerTypes.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                String customerType=(String) item.toString();
//                view.getText().toString();
                if(customerType.equals("Others"))
                {
                    shopLayout.setVisibility(View.GONE);
                }
                else {
                    shopLayout.setVisibility(View.VISIBLE);
                }
            }
        });
        final NetworkConnection connection=new NetworkConnection(this);
        state.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                String selectedState=(String) item.toString();
                if(connection.CheckInternet())
                {
                    if(selectedState.equals("Select State"))
                    {
                        try {
                            List<String> list=new ArrayList<>();
                            list.add("Select City");
//                            city.setItems(list);
                        }catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                    else {
                        setCity(selectedState);
                    }

                }
                else {
                    Alert alert=new Alert(CustomerCreation.this);
                    alert.showAlertboxnegative("Please connect to Internet. Please try again later");
                }
            }
        });


        if(connection.CheckInternet())
        {
            setState();
        }
        else {
            Alert alert=new Alert(CustomerCreation.this);
            alert.showAlertboxnegative("Please connect to Internet. Please try again later");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            imagePicker.handleActivityResult(resultCode, requestCode, data);

        }
    }

    @SuppressLint("Range")
    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    private File getFileFromImage() {
        try {
            BitmapDrawable drawable = (BitmapDrawable) selectimage.getDrawable();
            Bitmap bitmap = drawable.getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            File directory = new File(getFilesDir(), "profile");
            if (!directory.exists())
                directory.mkdirs();
            File myappFile = new File(directory
                    + File.separator + mImageName);
            FileOutputStream fos = new FileOutputStream(myappFile);
            fos.write(byteArray);
//                        mImageName = File_URL + myappFile.getName();
            return myappFile;
        } catch (Exception e) {
            e.printStackTrace();
            return new File("");
        }
    }


    public void submit(View view)
    {
//        alert.showAlertboxnegative("Customer Added Successfully");
        String customer=customerTypes.getText().toString();
        String shopNameString=shopName.getText().toString();
        String contactPersonString=contactPerson.getText().toString();
        String mobileNoString=mobileNo.getText().toString();
        String emailIdString=emailId.getText().toString();
        String stateString=state.getText().toString();
        String cityString=city.getText().toString();
        String addressOneString=addressOne.getText().toString();
//        String addressTwoString=customerTypes.getText().toString();
        String pinCodeString=pinCode.getText().toString();
        String gstString=gstNo.getText().toString();

        NetworkConnection connection=new NetworkConnection(CustomerCreation.this);
        if(connection.CheckInternet()) {

            if (customer.equals("Select Customer")) {
                customerTypes.setError("Select Customer");
                Toast.makeText(this, "Select Customer", Toast.LENGTH_SHORT).show();
            } else if (!customer.equals("Others")) {
                if (TextUtils.isEmpty(shopNameString)) {
                    shopName.setError("Enter Shop Name");
                    Toast.makeText(this, "Enter Shop Name", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(contactPersonString)) {
                    contactPerson.setError("Enter Contact Person");
                    Toast.makeText(this, "Enter Contact Person", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(mobileNoString)) {
                    mobileNo.setError("Enter Mobile Number");
                    Toast.makeText(this, "Enter Mobile Number", Toast.LENGTH_SHORT).show();
                }
//            else if(TextUtils.isEmpty(emailIdString))
//            {
//                emailId.setError("Enter email Id");
//                Toast.makeText(this, "Enter email Id", Toast.LENGTH_SHORT).show();
//            }
                else if (stateString.equals("Select State")) {
                    state.setError("Select State");
                    Toast.makeText(this, "Select State", Toast.LENGTH_SHORT).show();
                } else if (cityString.equals("Select City")) {
                    city.setError("Enter City name");
                    Toast.makeText(this, "Select City", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(addressOneString)) {
                    addressOne.setError("Enter Address");
                    Toast.makeText(this, "Enter Address", Toast.LENGTH_SHORT).show();
                } else {
                    if(mImageFile!=null)
                        uploadimage(mImageFile, contactPersonString, shopNameString, customer, mobileNoString, emailIdString, stateString, cityString, addressOneString, gstString, pinCodeString);
                    else
                        createCustomer(contactPersonString,shopNameString,customer,mobileNoString,emailIdString,stateString,cityString,addressOneString,gstString,pinCodeString);
                }
            } else if (TextUtils.isEmpty(contactPersonString)) {
                contactPerson.setError("Enter Contact Person");
                Toast.makeText(this, "Enter Contact Person", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(mobileNoString)) {
                mobileNo.setError("Enter Mobile Number");
                Toast.makeText(this, "Enter Mobile Number", Toast.LENGTH_SHORT).show();
            }
//        else if(TextUtils.isEmpty(emailIdString))
//        {
//            emailId.setError("Enter email Id");
//            Toast.makeText(this, "Enter email Id", Toast.LENGTH_SHORT).show();
//        }
            else if (TextUtils.isEmpty(stateString)) {
                state.setError("Select state");
                Toast.makeText(this, "Select state", Toast.LENGTH_SHORT).show();
            } else if (cityString.equals("Select City")) {
                city.setError("Enter  city name");
                Toast.makeText(this, "Select city", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(addressOneString)) {
                addressOne.setError("Enter address");
                Toast.makeText(this, "Enter address", Toast.LENGTH_SHORT).show();
            } else {
                if(mImageFile!=null)
                    uploadimage(mImageFile, contactPersonString, shopNameString, customer, mobileNoString, emailIdString, stateString, cityString, addressOneString, gstString, pinCodeString);
                else
                    createCustomer(contactPersonString,shopNameString,customer,mobileNoString,emailIdString,stateString,cityString,addressOneString,gstString,pinCodeString);
            }
        }else {
            Alert alert=new Alert(CustomerCreation.this);
            alert.showAlertboxnegative("No Internet Connection. Please try again");
        }
    }

    private void uploadimage(File selectimage, final String contactPersonString, final String shopNameString, final String customer, final String mobileNoString, final String emailIdString, final String stateString, final String cityString, final String addressOneString, final String gstString, final String pinCodeString) {

        progressDialog.show();
        try {

            APIService service = RetroClient.getApiService();
            String type="image/png";
            imagename=selectimage.getName();
            RequestBody requestBody = RequestBody.create(MediaType.parse(type), selectimage);
            MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", selectimage.getName(), requestBody);
            final Call<ImageUpload> request = service.imageUpload(filePart);
            request.enqueue(new Callback<ImageUpload>() {
                @Override
                public void onResponse(Call<ImageUpload> call, Response<ImageUpload> response) {
                    if ("Success.".equals(response.body().getMessage())) {
                        createCustomer(contactPersonString,shopNameString,customer,mobileNoString,emailIdString,stateString,cityString,addressOneString,gstString,pinCodeString);
                    }
                    else {
                        progressDialog.dismiss();
                        Alert alert=new Alert(CustomerCreation.this);
                        alert.showAlertboxnegative("Cannot upload this Image. Please try with other image.");
                    }
                }
                @Override
                public void onFailure(Call<ImageUpload> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.v("Upload Exception", t.getMessage());
                    t.printStackTrace();
                    Alert alert=new Alert(CustomerCreation.this);
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }

            });
        }
        catch (Exception ex) {
            progressDialog.dismiss();
            Log.v("Exception", ex.getMessage());
            Alert alert=new Alert(CustomerCreation.this);
            alert.showAlertboxnegative("Local data is corrupted. Please logout and login");

        }

    }

    private void createCustomer(String contactPersonString, String shopNameString, String customer, String mobileNoString, String emailIdString, String stateString, String cityString, String addressOneString, String gstString, String pinCodeString)
    {
        try {
            APIService service = RetroClient.getApiService();
            Call<CreateCustomerModel> call = service.createCustomer(preferences.getString("empId",""),preferences.getString("empRegion",""),
                    preferences.getString("regId",""),customer,
                    contactPersonString,mobileNoString,shopNameString,shopNameString/*dealer name is shop name*/,emailIdString,stateString,cityString,
                    addressOneString,pinCodeString,gstString,mImageName);

            call.enqueue(new Callback<CreateCustomerModel>() {
                @Override
                public void onResponse(Call<CreateCustomerModel> call, Response<CreateCustomerModel> response) {
                    progressDialog.dismiss();
                    try
                    {
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
                                Alert alert=new Alert(CustomerCreation.this);
                                alert.showAlertboxnegative("Customer Created Successfully");
                            }
                            else {
                                Alert alert=new Alert(CustomerCreation.this);
                                alert.showAlertboxnegative("Cannot load State List");
                            }
                        }
                        else
                        {
                            Alert alert=new Alert(CustomerCreation.this);
                            alert.showAlertboxnegative("Something went wrong . Please try again later .");

                        }
                    }
                    catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        Alert alert=new Alert(CustomerCreation.this);
                        alert.showAlertboxnegative("Something went wrong . Please try again later .");
                    }
                }

                @Override
                public void onFailure(Call<CreateCustomerModel> call, Throwable t) {
                    progressDialog.dismiss();
                    Alert alert=new Alert(CustomerCreation.this);
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
            Alert alert=new Alert(CustomerCreation.this);
            alert.showAlertboxnegative("Local ChangePasswordResult is Corrupted. Please Logout and Login");
        }
    }

    private void setState()
    {
        final ProgressDialog progressDialog = new ProgressDialog(CustomerCreation.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {
            APIService service = RetroClient.getApiService();
            Call<CustomerNameList> call = service.getStateList(preferences.getString("empRegion",""));

            call.enqueue(new Callback<CustomerNameList>() {
                @Override
                public void onResponse(Call<CustomerNameList> call, Response<CustomerNameList> response) {
                    progressDialog.dismiss();
                    try
                    {
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
                                List<String> stateList=response.body().getData();
                                stateList.add(0,"Select State");
                                state.setItems(stateList);
                            }
                            else {
                                Alert alert=new Alert(CustomerCreation.this);
                                alert.showAlertboxnegative("Cannot load State List");
                            }
                        }
                        else
                        {
                            Alert alert=new Alert(CustomerCreation.this);
                            alert.showAlertboxnegative("Something went wrong . Please try again later .");

                        }
                    }
                    catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        Alert alert=new Alert(CustomerCreation.this);
                        alert.showAlertboxnegative("Something went wrong . Please try again later .");
                    }
                }

                @Override
                public void onFailure(Call<CustomerNameList> call, Throwable t) {
                    progressDialog.dismiss();
                    Alert alert=new Alert(CustomerCreation.this);
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
            progressDialog.dismiss();
            Alert alert=new Alert(CustomerCreation.this);
            alert.showAlertboxnegative("Local ChangePasswordResult is Corrupted. Please Logout and Login");
        }
    }

    private void setCity(String selectedState)
    {
        final ProgressDialog progressDialog = new ProgressDialog(CustomerCreation.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {
            APIService service = RetroClient.getApiService();
            Call<CustomerNameList> call = service.getCityList(selectedState);

            call.enqueue(new Callback<CustomerNameList>() {
                @Override
                public void onResponse(Call<CustomerNameList> call, Response<CustomerNameList> response) {
                    progressDialog.dismiss();
                    try
                    {
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
                                List<String> cityList=response.body().getData();
                                cityList.add(0,"Select City");
//                                city.setItems(cityList);
                            }
                            else {
                                Alert alert=new Alert(CustomerCreation.this);
                                alert.showAlertbox("Cannot load City List for selected State");
                            }
                        }
                        else
                        {
                            Alert alert=new Alert(CustomerCreation.this);
                            alert.showAlertbox("Something went wrong . Please try again later .");

                        }
                    }
                    catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        Alert alert=new Alert(CustomerCreation.this);
                        alert.showAlertboxnegative("Something went wrong . Please try again later .");
                    }
                }

                @Override
                public void onFailure(Call<CustomerNameList> call, Throwable t) {
                    progressDialog.dismiss();
                    Alert alert=new Alert(CustomerCreation.this);
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
            Alert alert=new Alert(CustomerCreation.this);
            alert.showAlertboxnegative("Local ChangePasswordResult is Corrupted. Please Logout and Login");
        }
    }

}
