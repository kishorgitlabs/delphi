package com.brainmagic.delphitvs.salesactivities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;

import java.util.List;

import adapter.regional.SalesFullOrderDetilsAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.regional.salesnamelist.CurrentOrderHistoryPojo;
import model.regional.salesnamelist.Datum;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SalesFullOrdersDetails extends AppCompatActivity {

    private ImageView current_month_menu,current_month_back;
    private ListView current_month_lists;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private String sales_order_id,sales_orders_ids;
    private List<Datum> datas;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_full_order_details);


        preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
        editor=preferences.edit();
        current_month_menu=findViewById(R.id.current_month_menu);
        current_month_back=findViewById(R.id.current_month_back);
        current_month_lists=findViewById(R.id.current_month_lists3);
        sales_order_id=getIntent().getStringExtra("sales_order_id");
        sales_orders_ids=getIntent().getStringExtra("sales_orders_ids");

        current_month_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(SalesFullOrdersDetails.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.sales_home:
                                startActivity(new Intent(SalesFullOrdersDetails.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.dashboard:
                                startActivity(new Intent(SalesFullOrdersDetails.this, SalesMainmenu.class));
                                break;
                            case R.id.create_customer:
                                startActivity(new Intent(SalesFullOrdersDetails.this, CustomerCreation.class));
                                break;

                            case R.id.view_customer:
                                startActivity(new Intent(SalesFullOrdersDetails.this, ViewCustomerActivity.class));
                                break;

                            case R.id.visit_report:
                                startActivity(new Intent(SalesFullOrdersDetails.this, VisitReport.class));
                                break;
                            case R.id.view_visit_report:
                                startActivity(new Intent(SalesFullOrdersDetails.this, ViewVisitReport.class));
                                break;
//                            case R.id.view_cart:
//                                startActivity(new Intent(SalesFullOrdersDetails.this, SelectPartDetails.class));
//                                break;
                            case R.id.sales_logout:
                                editor.putBoolean("salesLogin",false);
                                editor.commit();
                                startActivity(new Intent(SalesFullOrdersDetails.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.sales_change_password:
                                String phone=preferences.getString("empMobile","");
                                final Alert alert=new Alert(SalesFullOrdersDetails.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;
                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.sales_menu);
                pop.show();
            }
        });
        current_month_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
                overridePendingTransition(R.anim.left_in,R.anim.left_out);

            }
        });
        NetworkConnection connection=new NetworkConnection(this);
        if(connection.CheckInternet())
        {
            getMoreDetails();

        }
        else {
            Alert alert=new Alert(this);
            alert.showAlertboxnegative("No Internet Connection. Please try again Later");
        }

    }

private void getMoreDetails(){


    final ProgressDialog dialog =new ProgressDialog(SalesFullOrdersDetails.this);
    dialog.setIndeterminate(true);
    dialog.setCancelable(false);
    dialog.setMessage("Loading...");
    dialog.show();

    APIService service = RetroClient.getApiService();
    Call<CurrentOrderHistoryPojo> call=service.getorderdetils(sales_orders_ids);


    try {
        call.enqueue(new Callback<CurrentOrderHistoryPojo>() {
            @Override
            public void onResponse(Call<CurrentOrderHistoryPojo> call, Response<CurrentOrderHistoryPojo> response) {

                try {
                    if(response.isSuccessful()){
                        if(response.body().getResult().equals("Success")){
                            dialog.dismiss();
                            datas=response.body().getData();
                            SalesFullOrderDetilsAdapter adapters=new SalesFullOrderDetilsAdapter(SalesFullOrdersDetails.this,datas);
                            current_month_lists.setAdapter(adapters);
                        }
                        else{
                            dialog.dismiss();
                            Alert alert=new Alert(SalesFullOrdersDetails.this);
                            alert.showAlertboxnegative("No Record Found");
                        }
                    }
                    else{
                        dialog.dismiss();
                        Alert alert=new Alert(SalesFullOrdersDetails.this);
                        alert.showAlertboxnegative("Something went wrong . Please try again later .");
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    Alert alert=new Alert(SalesFullOrdersDetails.this);
                    alert.showAlertbox("Something went wrong . Please try again later .");
                }

            }

            @Override
            public void onFailure(Call<CurrentOrderHistoryPojo> call, Throwable t) {
                dialog.dismiss();
                Alert alert=new Alert(SalesFullOrdersDetails.this);
                alert.showAlertbox("Something went wrong . Please try again later .");

            }
        });
    }catch (Exception e){
        e.printStackTrace();
        Alert alert=new Alert(SalesFullOrdersDetails.this);
        alert.showAlertbox("Something went wrong . Please try again later .");

    }


}
}

