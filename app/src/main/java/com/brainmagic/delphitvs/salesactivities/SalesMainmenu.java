package com.brainmagic.delphitvs.salesactivities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.provider.Settings;


import android.os.Bundle;

import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.General;
import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.ProductCatalogue;
import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.SalesLogin;
import com.brainmagic.delphitvs.VehicleSegment;
import com.brainmagic.delphitvs.fragment.AttendanceBottomSheetNavigation;
import com.brainmagic.delphitvs.fragment.SalesBottomSheetNavigation;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import alert.Alert;

public class SalesMainmenu extends AppCompatActivity implements AttendanceBottomSheetNavigation.BottomSheetListener, SalesBottomSheetNavigation.SalesBottomSheetListener {
    private LinearLayout layout_sales;
    private ImageView img_back,img_home,menu;
    private Alert alert;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private BottomSheetDialogFragment bottomSheetNavigation;

    private void askBatteryOptimization() {
        String packageName = getPackageName();
        PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (pm.isIgnoringBatteryOptimizations(packageName)) {
//                intent.setAction(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS);
//                intent.setData(Uri.parse("package:" + getPackageName()));
            } else {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setData(Uri.parse("package:" + packageName));
                startActivity(intent);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_mainmenu);
        img_back =(ImageView)findViewById(R.id.back);

        preferences= getSharedPreferences("Delphi", MODE_PRIVATE);
        editor=preferences.edit();

        alert=new Alert(this);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        menu=findViewById(R.id.menu);

        layout_sales = (LinearLayout)findViewById(R.id.layout_sales);

        layout_sales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                alert.showAlertbox("This page is under Construction");
//                Intent i = new Intent(SalesMainmenu.this, SalesActivity.class);
//                startActivity(i);
//                overridePendingTransition(R.anim.left_in,R.anim.left_out);
                bottomSheetNavigation=new SalesBottomSheetNavigation();
                bottomSheetNavigation.show(getSupportFragmentManager(),"exampleBottomSheet-");
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(SalesMainmenu.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.sales_logout:
                                editor.putBoolean("salesLogin",false);
                                editor.commit();
                                startActivity(new Intent(SalesMainmenu.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                break;
                            case R.id.menu_general:
                                startActivity(new Intent(SalesMainmenu.this, General.class));
                                break;
                            case R.id.menu_productcatalogue:
                                startActivity(new Intent(SalesMainmenu.this, ProductCatalogue.class));
                                break;
                            case R.id.menu_vehiclesegment:
                                startActivity(new Intent(SalesMainmenu.this, VehicleSegment.class));
                                break;

                            case R.id.menu_login:
                                startActivity(new Intent(SalesMainmenu.this, SalesLogin.class));
                                break;
                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.sales_menu);
                pop.show();
            }
        });
        askBatteryOptimization();
    }

    public void attendance(View view)
    {
        bottomSheetNavigation=new AttendanceBottomSheetNavigation();
        bottomSheetNavigation.show(getSupportFragmentManager(),"exampleBottomSheet-");
    }

    public void createCustomer(View view)
    {
        startActivity(new Intent(SalesMainmenu.this, CustomerCreation.class));
    }

    public void viewCustomer(View view)
    {
//        alert.showAlertbox("This page is under Construction");
        startActivity(new Intent(SalesMainmenu.this, ViewCustomerActivity.class));
    }
    public void visitReport(View view)
    {
        startActivity(new Intent(SalesMainmenu.this, VisitReport.class));
    }

    public void viewVisitReport(View view)
    {
//        alert.showAlertbox("This page is under Construction");
        startActivity(new Intent(SalesMainmenu.this, ViewVisitReport.class));
    }



    @Override
    public void bottomSheetListener(String attendanceType) {
        bottomSheetNavigation.dismiss();
        if(attendanceType.equals("Mark"))
        {
            startActivity(new Intent(SalesMainmenu.this, AttendanceActivitySales.class));
        }
        else {
            startActivity(new Intent(SalesMainmenu.this, ViewAttendanceActivitySales.class));
        }
    }

    @Override
    public void salesBottomSheetListener(String salesType) {
        bottomSheetNavigation.dismiss();
        if(salesType.equals("Sales"))
        {
            startActivity(new Intent(SalesMainmenu.this, SelectCustomer.class));
        }
        else {
            startActivity(new Intent(SalesMainmenu.this, SalesOrderHistory.class));
        }
    }
}
