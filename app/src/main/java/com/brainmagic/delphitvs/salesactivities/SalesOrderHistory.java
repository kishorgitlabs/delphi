package com.brainmagic.delphitvs.salesactivities;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;

import java.util.Calendar;

import adapter.sales.SalesOrderDateAdapter;
import adapter.sales.SalesOrderDateWiseAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.orderdetails.SalesOrdersPojo;
import model.partdetails.SalesOrderDatePojo;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SalesOrderHistory extends AppCompatActivity {

    private ListView dataList;
    private String fromDateString, toDateString,totalvalue,amount,date;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private TextView grandTotalValue;
    private SalesOrderDateAdapter adapters;
    private EditText from_date,to_date;
    private Button search_date_wise;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_order);

        preferences = getSharedPreferences("Delphi", MODE_PRIVATE);
        editor = preferences.edit();
        ImageView img_back = (ImageView) findViewById(R.id.back);
        ImageView menu = (ImageView) findViewById(R.id.menu);
        from_date = findViewById(R.id.from_date);
        to_date = findViewById(R.id.to_date);
        dataList = findViewById(R.id.data_list);
        grandTotalValue = findViewById(R.id.grand_total_value);
        search_date_wise = findViewById(R.id.search_date_wise);



        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });


        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(SalesOrderHistory.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.sales_home:
                                startActivity(new Intent(SalesOrderHistory.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.dashboard:
                                startActivity(new Intent(SalesOrderHistory.this, SalesMainmenu.class));
                                break;
                            case R.id.create_customer:
                                startActivity(new Intent(SalesOrderHistory.this, CustomerCreation.class));
                                break;

                            case R.id.view_customer:
                                startActivity(new Intent(SalesOrderHistory.this, ViewCustomerActivity.class));
                                break;

                            case R.id.visit_report:
                                startActivity(new Intent(SalesOrderHistory.this, VisitReport.class));
                                break;
                            case R.id.view_visit_report:
                                startActivity(new Intent(SalesOrderHistory.this, ViewVisitReport.class));
                                break;
//                            case R.id.view_cart:
//                                startActivity(new Intent(SalesOrderHistory.this, SelectPartDetails.class));
//                                break;
                            case R.id.sales_logout:
                                editor.putBoolean("salesLogin", false);
                                editor.commit();
                                startActivity(new Intent(SalesOrderHistory.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.sales_change_password:
                                String phone = preferences.getString("empMobile", "");
                                final Alert alert = new Alert(SalesOrderHistory.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;
                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.sales_menu);
                pop.show();
            }
        });

        from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(SalesOrderHistory.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                from_date.setText(year + "-" +(monthOfYear + 1) + "-" +dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

            }
        });

        to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(SalesOrderHistory.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                to_date.setText(year + "-" +(monthOfYear + 1) + "-" +dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

            }
        });

        NetworkConnection connection=new NetworkConnection(SalesOrderHistory.this);
        if(connection.CheckInternet()){
            getsalesorderlist();
        }
        else{
            Alert alert=new Alert(SalesOrderHistory.this);
            alert.showAlertboxnegative("No Internet Connection. Please switch On your Internet Connection");
        }
    }
    private void getsalesorderlist(){

        final ProgressDialog progressDialog=new ProgressDialog(SalesOrderHistory.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading...");
        progressDialog.setIndeterminate(true);
        progressDialog.show();


        APIService service=RetroClient.getApiService();
        Call<SalesOrdersPojo>call=service.getOrderHistory(preferences.getString("empId",""));

        try {

            call.enqueue(new Callback<SalesOrdersPojo>() {
                @Override
                public void onResponse(Call<SalesOrdersPojo> call, final Response<SalesOrdersPojo> response) {

                    if(response.isSuccessful()) {

                        if (response.body().getResult().equals("Success")) {

                             progressDialog.dismiss();
                             grandTotalValue.setVisibility(View.VISIBLE);
                             totalvalue=response.body().getData().getGrandTotal();
                             grandTotalValue.setText("Rs."+String.valueOf(totalvalue));

                             SalesOrderDateAdapter adapters=new SalesOrderDateAdapter(SalesOrderHistory.this,response.body().getData().getOrderDetail());
                             dataList.setAdapter(adapters);

                             dataList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                 @Override
                                 public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                     Intent intent=new Intent(SalesOrderHistory.this,ViewFullSalesOrderHistory.class);
                                     intent.putExtra("sales_order_date",response.body().getData().getOrderDetail().get(i).getDate());
                                     startActivity(intent);

                                 }
                             });

                        } else {
                            Alert alert = new Alert(SalesOrderHistory.this);
                            alert.showAlertboxnegative("No Record Found");
                        }
                    }

                }
                @Override
                public void onFailure(Call<SalesOrdersPojo> call, Throwable t) {
                    progressDialog.dismiss();
                    Alert alert=new Alert(SalesOrderHistory.this);
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });


        }catch (Exception e){

            e.printStackTrace();
            Alert alert=new Alert(SalesOrderHistory.this);
            alert.showAlertboxnegative("Something went wrong . Please try again later .");

        }

        search_date_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                grandTotalValue.setVisibility(View.INVISIBLE);
                fromDateString=from_date.getText().toString();
                toDateString=to_date.getText().toString();

                if(fromDateString.equals("")){
                    Toast.makeText(getApplicationContext(),"Select From Date", Toast.LENGTH_LONG).show();
                }
                else if(toDateString.equals("")){
                    Toast.makeText(getApplicationContext(),"Select To Date",Toast.LENGTH_LONG).show();
                }
                 else{
                    NetworkConnection connection=new NetworkConnection(SalesOrderHistory.this);
                    if(connection.CheckInternet()){
                        getsalesorderdatewise();
                    }
                    else{
                        Alert alert=new Alert(SalesOrderHistory.this);
                        alert.showAlertboxnegative("No Internet Connection. Please switch On your Internet Connection");
                    }
                }
            }
        });
    }


    private void getsalesorderdatewise(){

        final ProgressDialog progressDialog=new ProgressDialog(SalesOrderHistory.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading...");
        progressDialog.setIndeterminate(true);
        progressDialog.show();


        APIService service=RetroClient.getApiService();
        Call<SalesOrderDatePojo>call=service.getSalesOrderHistoryDateWise(preferences.getString("empId",""),fromDateString,toDateString);

        try {

            call.enqueue(new Callback<SalesOrderDatePojo>() {
                @Override
                public void onResponse(Call<SalesOrderDatePojo> call, final Response<SalesOrderDatePojo> response) {

                    if(response.isSuccessful()){

                        if(response.body().getResult().equals("Success")){

                            progressDialog.dismiss();
                            grandTotalValue.setVisibility(View.VISIBLE);

                            SalesOrderDateWiseAdapter adapter   =new SalesOrderDateWiseAdapter(SalesOrderHistory.this,response.body().getData().getOrderList());
                            dataList.setAdapter(adapter);

                            amount=response.body().getData().getTot();
                            grandTotalValue.setText("Rs."+String.valueOf(amount));



                            dataList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                    dataList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                                             String[] dates=response.body().getData().getOrderList().get(i).getInsertDate().split("T");
                                             date=dates[0];

                                            Intent intent=new Intent(SalesOrderHistory.this,SalesOrderDetails.class);
                                            intent.putExtra("sales_date_wise",date);
                                            startActivity(intent);
                                        }
                                    });

                                }
                            });

                        }
                        else {
                            Alert alert = new Alert(SalesOrderHistory.this);
                            alert.showAlertboxnegative("No Record Found");
                        }
                    }
                }

                @Override
                public void onFailure(Call<SalesOrderDatePojo> call, Throwable t) {
                    progressDialog.dismiss();
                    Alert alert=new Alert(SalesOrderHistory.this);
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });

        }catch (Exception e){
            e.printStackTrace();
            Alert alert=new Alert(SalesOrderHistory.this);
            alert.showAlertboxnegative("Something went wrong . Please try again later .");
        }

    }



}
