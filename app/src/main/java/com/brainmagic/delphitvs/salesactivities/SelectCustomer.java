package com.brainmagic.delphitvs.salesactivities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;

import android.os.Bundle;

import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.Aboutus;
import com.brainmagic.delphitvs.General;
import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.ProductCatalogue;
import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.SalesLogin;
import com.brainmagic.delphitvs.VehicleSegment;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import java.util.List;

import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.customerdetailslist.CustomerDetails;
import model.customerlist.CustomerList;
import model.distributorregion.DistributorRegionList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import room.AppDatabase;

public class SelectCustomer extends AppCompatActivity {

    private AutoCompleteTextView city,customerlist;
    private TextView addresstext,mobiletext,emailidtext,gsttext;
    private Button proceed;
    private String selectedusertype,contactpersonname,selectedmobile,selectedcusid,selectedcity,selectedcustomer;
    private Alert alert;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private List<String> customelist;
    private ArrayAdapter customerlistadapter;
    private AppDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_customer);
        db=AppDatabase.getAppDatabase(this);
        alert=new Alert(this);
        ImageView img_back =(ImageView)findViewById(R.id.back);
        ImageView menu =(ImageView)findViewById(R.id.menu);
        addresstext=findViewById(R.id.addresstext);
        mobiletext=findViewById(R.id.mobiletext);
        emailidtext=findViewById(R.id.emailidtext);
        gsttext=findViewById(R.id.gsttext);
        proceed=findViewById(R.id.proceed);
        customerlist=findViewById(R.id.customerlist);
        preferences= getSharedPreferences("Delphi", MODE_PRIVATE);
        editor=preferences.edit();




        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(SelectCustomer.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.sales_home:
                                startActivity(new Intent(SelectCustomer.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.dashboard:
                                startActivity(new Intent(SelectCustomer.this, SalesMainmenu.class));
                                break;
                            case R.id.create_customer:
                                startActivity(new Intent(SelectCustomer.this, CustomerCreation.class));
                                break;

                            case R.id.view_customer:
                                startActivity(new Intent(SelectCustomer.this, ViewCustomerActivity.class));
                                break;

                            case R.id.visit_report:
                                startActivity(new Intent(SelectCustomer.this, VisitReport.class));
                                break;
                            case R.id.view_visit_report:
                                startActivity(new Intent(SelectCustomer.this, ViewVisitReport.class));
                                break;
//                            case R.id.view_cart:
//                                startActivity(new Intent(SelectCustomer.this, SelectPartDetails.class));
//                                break;
                            case R.id.sales_logout:
                                editor.putBoolean("salesLogin",false);
                                editor.commit();
                                startActivity(new Intent(SelectCustomer.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.sales_change_password:
                                String phone=preferences.getString("empMobile","");
                                final Alert alert=new Alert(SelectCustomer.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;
                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.sales_menu);
                pop.show();
            }
        });

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               if (selectedcustomer==(null)||TextUtils.isEmpty(selectedcustomer)){
                    StyleableToast st = new StyleableToast(SelectCustomer.this,
                            "Please select company name", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }
                else if (selectedmobile.equals(db.ProductsDAO().getCustomerMobileNo())||db.ProductsDAO().getCartDao().size()==0){
                    Intent customerdetails=new Intent(getApplicationContext(),SelectPartDetails.class);
                    customerdetails.putExtra("selectedmobile",selectedmobile);
                    customerdetails.putExtra("selectedcusid","selectedcusid");
                    customerdetails.putExtra("contactpersonname",contactpersonname);
                    customerdetails.putExtra("fromSelectCustomerActivity","selectCustomer");
                    customerdetails.putExtra("selectedusertype",selectedusertype);
                    startActivity(customerdetails);
                    customerlist.setText("");
//                    city.setText("");
                    emailidtext.setText("");
                    addresstext.setText("");
                    gsttext.setText("");
                    selectedusertype="";
                    mobiletext.setText("");
                    selectedcustomer="";

                }
                else {
//                   Intent selectparts=new Intent(getApplicationContext(),SelectPartDetails.class);
//                   selectparts.putExtra("selectedusertype",selectedusertype);
//                   selectparts.putExtra("contactpersonname",contactpersonname);
//                   selectparts.putExtra("selectedcusid",selectedcusid);
//                   startActivity(selectparts);
                    StyleableToast st = new StyleableToast(SelectCustomer.this,
                            "You Have Already Added Parts For Another User Please Place Order Or Delete", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }

            }
        });

        customerlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup(customelist);
            }
        });

        getCustomerList();
    }

    private void getCustomerList() {
        final ProgressDialog progressDialog = new ProgressDialog(SelectCustomer.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIService service= RetroClient.getApiService();
        Call<CustomerList> call=service.getCustomerList(preferences.getString("empId",""));
        try {
            call.enqueue(new Callback<CustomerList>() {
                @Override
                public void onResponse(Call<CustomerList> call, Response<CustomerList> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
                                customelist=response.body().getData();
                                customelist.remove(null);

                            }
                            else {
                                alert.showAlertboxnegative("No Customer List Found. Please Create a Customer in Create Customer");
                            }
                        }
                        else {
                            alert.showAlertboxnegative("Bad Response. Please try again later");
                        }

                    }catch ( Exception e)
                    {
                        e.printStackTrace();
                        alert.showAlertboxnegative("Something went wrong . Please try again later .");
                    }
                }

                @Override
                public void onFailure(Call<CustomerList> call, Throwable t) {
                    progressDialog.dismiss();
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
            progressDialog.dismiss();
            alert.showAlertbox("Something went wrong . Please try again later .");
        }

    }

    private void showPopup(List<String> customernamemobile) {
        try {
            final android.app.AlertDialog alertDialog;
            alertDialog = new android.app.AlertDialog.Builder(this).create();
            LayoutInflater inflater = ((Activity) this).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.popupcityadapter, null);

            alertDialog.setView(dialogView);
            final ListView list = (ListView) dialogView.findViewById(R.id.listdetails);
            final  TextView close=dialogView.findViewById(R.id.close);
            final  TextView spinerTitle=dialogView.findViewById(R.id.spinerTitle);
            spinerTitle.setText("Select Customer");
            customerlistadapter =new ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,customernamemobile);
            list.setAdapter(customerlistadapter);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    selectedcity = list.getItemAtPosition(i).toString();
                    customerlist.setText(selectedcity);
                    alertDialog.dismiss();
                    if (selectedcity==(null)||selectedcity.equals("")){

                    }else {
                        String namesplit[] =selectedcity.split("-");
                        selectedcustomer=namesplit[0].trim();
                        selectedmobile=namesplit[1].trim();
                        mobiletext.setText(selectedmobile);
                        getCustomerDetails();
                    }
                }
            });
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void getCustomerDetails() {
        final ProgressDialog progressDialog = new ProgressDialog(SelectCustomer.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIService service= RetroClient.getApiService();
        Call<CustomerDetails> call=service.getCustomerDetail(selectedmobile);
        try {
            call.enqueue(new Callback<CustomerDetails>() {
                @Override
                public void onResponse(Call<CustomerDetails> call, Response<CustomerDetails> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
                                addresstext.setText(response.body().getData().getDealerAddress());
                                emailidtext.setText(response.body().getData().getDealerEmail());
                                gsttext.setText(response.body().getData().getGST());
                                selectedusertype=response.body().getData().getUserType();
                                selectedcusid=response.body().getData().getId();
                                contactpersonname=response.body().getData().getContactPersonname();
                            }
                            else {
                                alert.showAlertbox("Customer Details Not Found. Please try again later");
                            }
                        }
                        else {
                            alert.showAlertbox("Bad Response. Please try again later");
                        }
                    }catch ( Exception e)
                    {
                        e.printStackTrace();
                        alert.showAlertbox("Something went wrong . Please try again later .");
                    }
                }
                @Override
                public void onFailure(Call<CustomerDetails> call, Throwable t) {
                    progressDialog.dismiss();
                    alert.showAlertbox("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
            progressDialog.dismiss();
            alert.showAlertbox("Something went wrong . Please try again later .");
        }

    }
}
