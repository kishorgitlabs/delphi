package com.brainmagic.delphitvs.salesactivities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.Aboutus;
import com.brainmagic.delphitvs.General;
import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.PriceList;
import com.brainmagic.delphitvs.ProductCatalogue;
import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.SalesLogin;
import com.brainmagic.delphitvs.VehicleSegment;
import com.brainmagic.delphitvs.ViewDistributerDetails;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import adapter.sales.SelectDistributerAdapter;
import alert.Alert;
import model.CartDAO;
import model.api.APIService;
import model.api.RetroClient;
import model.distributorcitylist.DistributorCityList;
import model.distributorlist.DistributorData;
import model.distributorlist.DistributorList;
import model.distributorregion.DistributorRegionList;
import model.orderdetails.Order;
import model.orderdetails.OrderDetails;
import model.orderdetails.OrderPart;
import model.placeorder.PlaceOrder;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import room.AppDatabase;

public class SelectDistributorForOrder extends AppCompatActivity implements SelectDistributerAdapter.Disdetails {


    private Alert alert;
    private SelectDistributerAdapter selectDistributerAdapter;
    private ListView distributerlist;
    private List<DistributorData> distributorData;
    private String disid="",discode="",disaddress="",orderdate;
    private AppDatabase appDatabase;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private OrderDetails orderDetails;
    private Button placeorder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_distributor_for_order);

        preferences= getSharedPreferences("Delphi", MODE_PRIVATE);
        editor=preferences.edit();

        alert=new Alert(this);
        appDatabase=AppDatabase.getAppDatabase(SelectDistributorForOrder.this);
        List<CartDAO> cartDAOS=appDatabase.ProductsDAO().getCartDao();
        placeorder=findViewById(R.id.placeorder);
        orderDetails=new OrderDetails();
        List<OrderPart> details=new ArrayList<>();
        int overalltotal=0;
        for (CartDAO cartItem:cartDAOS) {
            OrderPart orderPart=new OrderPart();
            orderPart.setPartNo(cartItem.getPartnumber());
            orderPart.setQuantity(cartItem.getQty());
            orderPart.setAmountPerUnit(cartItem.getMrp());
            orderPart.setGrandTotal(cartItem.getTotalamount());
            orderPart.setProductName(cartItem.getDescription());
            orderPart.setExeId(preferences.getString("empId",""));
            details.add(orderPart);
            String totalamt=cartItem.getTotalamount();
            int tt=Integer.parseInt(totalamt);
            overalltotal+=tt;
        }
        //date
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        orderdate = df.format(c.getTime());
        orderDetails.setOrderParts(details);


        final Order order=new Order();
        order.setTotal(String.valueOf(overalltotal));
        order.setExeId(preferences.getString("empId",""));
        order.setRegId(preferences.getString("regId",""));
        order.setCusId(preferences.getString("selectedcusid",""));
        order.setCustomerType(getIntent().getStringExtra("selectedusertype"));
        order.setContactPerson(getIntent().getStringExtra("contactpersonname"));
        order.setOrderDate(orderdate);


        ImageView img_back =(ImageView)findViewById(R.id.back);
        ImageView menu =(ImageView)findViewById(R.id.menu);
        distributerlist=findViewById(R.id.distributerlist);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });


        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(SelectDistributorForOrder.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_home:
                                startActivity(new Intent(SelectDistributorForOrder.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.menu_aboutus:
                                startActivity(new Intent(SelectDistributorForOrder.this, Aboutus.class));
                                break;
                            case R.id.menu_productcatalogue:
                                startActivity(new Intent(SelectDistributorForOrder.this, ProductCatalogue.class));
                                break;

                            case R.id.menu_general:
                                startActivity(new Intent(SelectDistributorForOrder.this, General.class));
                                break;

                            case R.id.menu_pricelist:
                                startActivity(new Intent(SelectDistributorForOrder.this, PriceList.class));
                                break;
                            case R.id.menu_login:
                                startActivity(new Intent(SelectDistributorForOrder.this, SalesLogin.class));
                                break;
//                            case R.id.view_cart:
//                                startActivity(new Intent(SelectDistributorForOrder.this, SelectPartDetails.class));
//                                break;
//                            case R.id.sales_logout:
//                                editor.putBoolean("salesLogin",false);
//                                editor.commit();
//                                startActivity(new Intent(SelectDistributorForOrder.this, MainActivity.class)
//                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
//                                break;
//                            case R.id.sales_change_password:
//                                String phone=preferences.getString("empMobile","");
//                                final Alert alert=new Alert(SelectDistributorForOrder.this);
//                                alert.changePasswordAlert(phone);
//                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
//                                    @Override
//                                    public void onMessage(String msg) {
//                                        alert.showAlertbox(msg);
//                                    }
//                                });

//                                break;
                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.aboutusmenu);
                pop.show();
            }
        });

        placeorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (disid.equals("")){
                    Toast.makeText(SelectDistributorForOrder.this,"Please Select Distributor",Toast.LENGTH_SHORT).show();
                }else{
                    order.setDisId(disid);
                    orderDetails.setOrder(order);
                    placeorder();
                }

            }
        });
        getDistributorRegion("Distributor");

    }


    private void getDistributorRegion(final String distributor) {
        final ProgressDialog progressDialog = new ProgressDialog(SelectDistributorForOrder.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIService service= RetroClient.getApiService();
        Call<DistributorRegionList> call=service.getDistributorRegion();
        try {
            call.enqueue(new Callback<DistributorRegionList>() {
                @Override
                public void onResponse(Call<DistributorRegionList> call, Response<DistributorRegionList> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {

                                getDealerDistributor(distributor,response.body().getData());

                            }
                            else {
                                alert.showAlertboxnegative("No Distributor List Found. Please try again later");

                            }
                        }
                        else {
                            alert.showAlertboxnegative("Bad Response. Please try again later");
                        }

                    }catch ( Exception e)
                    {
                        e.printStackTrace();
                        alert.showAlertboxnegative("Something went wrong . Please try again later .");
                    }

                }

                @Override
                public void onFailure(Call<DistributorRegionList> call, Throwable t) {
                    progressDialog.dismiss();
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
            progressDialog.dismiss();
            alert.showAlertbox("Something went wrong . Please try again later .");
        }
    }

    private void getDealerDistributor(final String
                                              networkType,List<String> statelist)
    {

        final AlertDialog alertDialog;
        alertDialog = new AlertDialog.Builder(
                SelectDistributorForOrder.this).create();
        LayoutInflater inflater = (SelectDistributorForOrder.this).getLayoutInflater();
        final View dialog = inflater.inflate(R.layout.dealer_alert, null);
        alertDialog.setView(dialog);
        alertDialog.setCancelable(false);


        final MaterialSpinner statespin = (MaterialSpinner) dialog.findViewById(R.id.statespin);
        final MaterialSpinner cityspin = (MaterialSpinner) dialog.findViewById(R.id.cityspin);
        statespin.setBackground(getResources().getDrawable(R.drawable.autotextback));
        cityspin.setBackground(getResources().getDrawable(R.drawable.autotextback));


//        statelist.clear();
        Set<String> hs = new HashSet<>();
        hs.addAll(statelist);
        Collections.sort(statelist);
        if (networkType.equals("Distributor")){
            statelist.add(0,"SELECT REGION");
        }else {
            statelist.add(0,"SELECT STATE");
        }

        statespin.setItems(statelist);
        statespin.setPadding(30, 0, 0, 0);

        statespin.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                String state = statespin.getText().toString().trim();
                if(!state.equals("SELECT REGION")||!state.equals("SELECT STATE"))
                {
                    NetworkConnection connection=new NetworkConnection(SelectDistributorForOrder.this);
                    if(connection.CheckInternet())

                        getDistributorCity(networkType,state,cityspin);
                    else
                    {
                        alert.showAlertbox("Please check your network connection and try again! ");
                    }
                }
                else
                {
                    cityspin.setPadding(30, 0, 0, 0);
                    Toast.makeText(getApplicationContext(), "Select any one", Toast.LENGTH_LONG).show();

                }
            }
        });





        Button search = (Button) dialog.findViewById(R.id.search);
        Button cancel = (Button) dialog.findViewById(R.id.cancel);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String state = statespin.getText().toString().trim();
                String city = cityspin.getText().toString().trim();
//                d_state.setText(State);
//                d_city.setText(City);

                if(!state.equals("SELECT STATE")&& !city.equals("SELECT CITY"))
                {
                    alertDialog.dismiss();
                    getDelphiDistributorDetails(networkType,state,city);
                }
                else {
                    Toast.makeText(getApplicationContext(), "Select state and city", Toast.LENGTH_LONG).show();
                }



            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onBackPressed();
                alertDialog.dismiss();
                finish();

            }
        });
        alertDialog.show();
    }

    private  void getDistributorCity(String networkType,String region,final MaterialSpinner citySpin)
    {
        final ProgressDialog progressDialog = new ProgressDialog(SelectDistributorForOrder.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        try{
            APIService service=RetroClient.getApiService();
            Call<DistributorCityList> call=service.getDistributorList(region);
            call.enqueue(new Callback<DistributorCityList>() {
                @Override
                public void onResponse(Call<DistributorCityList> call, Response<DistributorCityList> response) {
                    progressDialog.dismiss();
                    try {

                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
                                List<String >cityList= response.body().getData();
                                Collections.sort(cityList);
                                cityList.add(0,"SELECT CITY");
                                citySpin.setItems(cityList);
                                citySpin.setPadding(30, 0, 0, 0);

                            }else {
                                Toast.makeText(getApplicationContext(), "Select Region and city", Toast.LENGTH_LONG).show();
//                                alert.showAlertbox("No Record Found. Please try again later");
                            }
                        }
                        else {
                            alert.showAlertbox("Please try again later");
                        }

                    }
                    catch (Exception e)
                    {

                        e.printStackTrace();
                        alert.showAlertbox("Exception error. Please try again later");
                    }
                }

                @Override
                public void onFailure(Call<DistributorCityList> call, Throwable t) {
                    progressDialog.dismiss();
                    alert.showAlertbox("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
            alert.showAlertbox("Exception error. Please try again later");
        }

    }

    private void getDelphiDistributorDetails(final String networkType, final String state, final String city)
    {
        final ProgressDialog progressDialog = new ProgressDialog(SelectDistributorForOrder.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        try{
            APIService service=RetroClient.getApiService();
            Call<DistributorList> call=service.getDistributorList(state,city);
            call.enqueue(new Callback<DistributorList>() {
                @Override
                public void onResponse(Call<DistributorList> call, Response<DistributorList> response) {
                    progressDialog.dismiss();
                    try {
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
                                distributorData=response.body().getData();
                                selectDistributerAdapter=new SelectDistributerAdapter(SelectDistributorForOrder.this,distributorData);
                                distributerlist.setAdapter(selectDistributerAdapter);
                            }else {
                                alert.showAlertbox("No Record Found. Please try again later");
                            }
                        }
                        else {
                            alert.showAlertbox("Bad Response. Please try again later");
                        }

                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                        alert.showAlertbox("Something went wrong . Please try again later .");
                    }
                }
                @Override
                public void onFailure(Call<DistributorList> call, Throwable t) {
                    progressDialog.dismiss();
                    alert.showAlertbox("Something went wrong . Please try again later .");
                }
            });
        }
        catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
            alert.showAlertbox("Exception error. Please try again later");
        }

    }

    private void placeorder() {
        final ProgressDialog progressDialog = new ProgressDialog(SelectDistributorForOrder.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIService service= RetroClient.getApiService();
        Call<PlaceOrder> call=service.placeOrder(orderDetails);
        try {
            call.enqueue(new Callback<PlaceOrder>() {
                @Override
                public void onResponse(Call<PlaceOrder> call, Response<PlaceOrder> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
                                alert.showAlertBoxWithListener("Your Order has been placed successfully, Order reference number is"+" "+response.body().getData(),View.GONE);
                                alert.setOnPositiveClickListener(new Alert.onPositiveClickListener() {
                                    @Override
                                    public void onPositiveClick() {
                                        Intent sales=new Intent(getApplicationContext(),SalesMainmenu.class);
                                        sales.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(sales);
                                    }
                                });
                                appDatabase.ProductsDAO().deleteccart();
                            }
                            else {
                                alert.showAlertbox("Cannot Place the Order. Please try again later");

                            }
                        }
                        else {
                            alert.showAlertbox("Bad Response. Please try again later");
                        }

                    }catch ( Exception e)
                    {
                        e.printStackTrace();
                        alert.showAlertbox("Something went wrong . Please try again later .");
                    }

                }

                @Override
                public void onFailure(Call<PlaceOrder> call, Throwable t) {
                    progressDialog.dismiss();
                    alert.showAlertbox("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
            progressDialog.dismiss();
            alert.showAlertbox("Something went wrong . Please try again later .");
        }
    }


    @Override
    public void senddistributerid(String distriid) {
        disid=distriid;

    }

    @Override
    public void senddistributername(String discodes) {
        discode=discodes;
    }

    @Override
    public void senddistributeraddress(String distriaddress) {
        disaddress=distriaddress;
    }
}
