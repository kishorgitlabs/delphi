package com.brainmagic.delphitvs.salesactivities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;


import android.os.Bundle;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.brainmagic.delphitvs.FiltersAllmakeproducts;
import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import java.util.List;

import adapter.FiltersSegmentAdpater;
import adapter.sales.ViewCartAdapter;
import alert.Alert;
import model.CartDAO;
import model.api.APIService;
import model.api.RetroClient;
import model.filters.oemmodel.OEModelList;
import model.getpartnumber.GetPartNumber;
import model.partdetails.PartDetails;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import room.AppDatabase;
import toaster.Toasts;

public class SelectPartDetails extends AppCompatActivity implements ViewCartAdapter.Recretatesipelistview {

    public SwipeMenuListView parts;
    private ViewCartAdapter orderPartsAdapter;
    private AutoCompleteTextView partnumber;
    private Alert box;
    private List<String> partnumberlist;
    private String selectedpartnumber,selectedmrpprice,totalvalues="",quantity,partdescription,selectedusertype,contactpersonname,selectedcusid;
    private EditText partdesc,qty;
    private int totqty,totvalues;
    private Button addtocart,selectdistributer;
    private AppDatabase appDatabase;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_part_details);

        box=new Alert(this);
        appDatabase=AppDatabase.getAppDatabase(SelectPartDetails.this);
        preferences=getSharedPreferences("Delphi",MODE_PRIVATE);
        editor=preferences.edit();
        parts = findViewById(R.id.parts);

        ImageView img_back =(ImageView)findViewById(R.id.back);
        ImageView menu =(ImageView)findViewById(R.id.menu);
        selectdistributer = findViewById(R.id.selectdistributer);
        partnumber = findViewById(R.id.partnumber);
        partdesc = findViewById(R.id.partdesc);
        qty = findViewById(R.id.qty);
        addtocart = findViewById(R.id.addtocart);
        selectedusertype=getIntent().getStringExtra("selectedusertype");
        contactpersonname=getIntent().getStringExtra("contactpersonname");
        selectedcusid=getIntent().getStringExtra("selectedcusid");

        final List<CartDAO> cartDAOS=appDatabase.ProductsDAO().getCartDao();

        if (cartDAOS.size()!=0){
            orderPartsAdapter = new ViewCartAdapter(SelectPartDetails.this,cartDAOS);
            parts.setAdapter(orderPartsAdapter);
            if(!"selectCustomer".equals(getIntent().getStringExtra("fromSelectCustomerActivity"))) {
                LinearLayout addToCartlayout = findViewById(R.id.add_to_cart_layout);
                addToCartlayout.setVisibility(View.GONE);
            }
        }
        else {
            if(!"selectCustomer".equals(getIntent().getStringExtra("fromSelectCustomerActivity")))
            {
                finish();
                Toasts toasts=new Toasts(this);
                toasts.ShowErrorToast("No Cart Items Found");
            }
        }
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(SelectPartDetails.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.sales_home:
                                startActivity(new Intent(SelectPartDetails.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.dashboard:
                                startActivity(new Intent(SelectPartDetails.this, SalesMainmenu.class));
                                break;
                            case R.id.create_customer:
                                startActivity(new Intent(SelectPartDetails.this, CustomerCreation.class));
                                break;

                            case R.id.view_customer:
                                startActivity(new Intent(SelectPartDetails.this, ViewCustomerActivity.class));
                                break;

                            case R.id.visit_report:
                                startActivity(new Intent(SelectPartDetails.this, VisitReport.class));
                                break;
                            case R.id.view_visit_report:
                                startActivity(new Intent(SelectPartDetails.this, ViewVisitReport.class));
                                break;
//                            case R.id.view_cart:
////                                startActivity(new Intent(SelectPartDetails.this, SelectPartDetails.class));
//                                break;
                            case R.id.sales_logout:
                                editor.putBoolean("salesLogin",false);
                                editor.commit();
                                startActivity(new Intent(SelectPartDetails.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.sales_change_password:
                                String phone=preferences.getString("empMobile","");
                                final Alert alert=new Alert(SelectPartDetails.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;
                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.sales_menu);
                pop.show();
            }
        });

        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item width
                deleteItem.setWidth(70);
                // set a icon
                deleteItem.setIcon(R.drawable.delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        parts.setMenuCreator(creator);

        parts.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                CartDAO item=cartDAOS.get(position);
                switch (index) {
                    case 0:
                        String partnuembrtodelete=item.getPartnumber();
                        showDeleteAlertBox(partnuembrtodelete);
                        break;
                }
                return false;
            }
        });

        selectdistributer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cartDAOS.size()!=0){
                    Intent selectdistributor=new Intent(getApplicationContext(),SelectDistributorForOrder.class);
                    selectdistributor.putExtra("selectedusertype",selectedusertype);
                    selectdistributor.putExtra("contactpersonname",contactpersonname);
                    selectdistributor.putExtra("selectedcusid",selectedcusid);
                    startActivity(selectdistributor);
                }else {
                    Toast.makeText(SelectPartDetails.this,"No items in cart",Toast.LENGTH_SHORT).show();
                }

            }
        });

        qty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length()>0){
                    try {
                        quantity=editable.toString();
                        totqty=Integer.parseInt(quantity);
                        totvalues= Integer.parseInt(selectedmrpprice);
                        totalvalues= String.valueOf((totqty *totvalues));
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

            }
        });

        partnumber.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
             selectedpartnumber=partnumber.getAdapter().getItem(i).toString();
                getpartdetails();
            }
        });

        addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                partdescription=partdesc.getText().toString();
                if (TextUtils.isEmpty(selectedpartnumber)){
                    Toast.makeText(SelectPartDetails.this,"Enter Partnumber",Toast.LENGTH_SHORT).show();

                } else {
                    List<String> partsincart=appDatabase.ProductsDAO().getpartnumber();
                    if (partsincart.contains(selectedpartnumber)){
                        appDatabase.ProductsDAO().updatecart(totalvalues,quantity,selectedpartnumber);
                        partnumber.setText("");
                        partdesc.setText("");
                        qty.setText("");
                        recreate();
                    }else {
                        CartDAO cartDAO=new CartDAO();
                        cartDAO.setCustomermobilenumber(getIntent().getStringExtra("selectedmobile"));
                        cartDAO.setPartnumber(selectedpartnumber);
                        cartDAO.setMrp(selectedmrpprice);
                        cartDAO.setTotalamount(totalvalues);
                        cartDAO.setQty(quantity);
                        cartDAO.setDescription(partdescription);
                        appDatabase.ProductsDAO().insertCartdao(cartDAO);
                        partnumber.setText("");
                        partdesc.setText("");
                        qty.setText("");
                        recreate();
                    }

                }
            }
        });
        getPartNumber();
    }

    private void getpartdetails() {
        final ProgressDialog progressDialog = new ProgressDialog(SelectPartDetails.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIService service= RetroClient.getApiService();
        Call<PartDetails> call=service.getpartdetails(selectedpartnumber);
        call.enqueue(new Callback<PartDetails>() {
            @Override
            public void onResponse(Call<PartDetails> call, Response<PartDetails> response) {
                try{
                    progressDialog.dismiss();
                    if(response.body().getResult().equals("success"))
                    {
                        partdesc.setText(response.body().getData().get(0).getDescription());
                        selectedmrpprice=response.body().getData().get(0).getMRP();
                        if (selectedmrpprice.contains(".")){
//                            String spiltmrp[] = selectedmrpprice.split("\\.");
                            String mrp =selectedmrpprice.split("\\.")[0];
                            selectedmrpprice=mrp;

                        }
                        qty.setText("1");
                    }
                    else {
                        box.showAlertboxnegative("Please try again later");
                    }
                }catch (Exception e)
                {
                    e.printStackTrace();
                    box.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            }
            @Override
            public void onFailure(Call<PartDetails> call, Throwable t) {
                progressDialog.dismiss();
                box.showAlertboxnegative("Something went wrong . Please try again later .");
            }
        });
    }

    private void getPartNumber() {
        final ProgressDialog progressDialog = new ProgressDialog(SelectPartDetails.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIService service= RetroClient.getApiService();
        Call<GetPartNumber> call=service.getpartnumber();
        call.enqueue(new Callback<GetPartNumber>() {
            @Override
            public void onResponse(Call<GetPartNumber> call, Response<GetPartNumber> response) {
                try{
                    progressDialog.dismiss();
                    if(response.body().getResult().equals("Success"))
                    {
                        partnumberlist=response.body().getData();
                        partnumber.setAdapter(new ArrayAdapter<String>(SelectPartDetails.this,R.layout.support_simple_spinner_dropdown_item,partnumberlist));
                        partnumber.setThreshold(1);
                    }
                    else {
                        box.showAlertboxnegative("No parts found. Please try again later");
                    }
                }catch (Exception e)
                {
                    e.printStackTrace();
                    box.showAlertbox("Something went wrong . Please try again later .");
                }
            }
            @Override
            public void onFailure(Call<GetPartNumber> call, Throwable t) {
                progressDialog.dismiss();
                box.showAlertboxnegative("Something went wrong . Please try again later .");
            }
        });
    }

    private void showDeleteAlertBox(final String deletepartnumber)
    {
        final AlertDialog alertDialog=new AlertDialog.Builder(SelectPartDetails.this).create();
        View deleteAlert= LayoutInflater.from(SelectPartDetails.this).inflate(R.layout.cart_delete_alert,null);
        TextView yes=deleteAlert.findViewById(R.id.delete_yes);
        TextView no=deleteAlert.findViewById(R.id.delete_no);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                appDatabase.ProductsDAO().deletepartnumber(deletepartnumber);
                orderPartsAdapter.notifyDataSetChanged();
                recreate();
                alertDialog.dismiss();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        alertDialog.setView(deleteAlert);
        alertDialog.show();
    }

    @Override
    public void recreted() {
        recreate();
    }
}
