package com.brainmagic.delphitvs.salesactivities;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;

import java.util.Calendar;
import java.util.List;

import adapter.sales.SalesAttendanceViewAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.sales.viewattendance.attendance.SalesAttendance;
import model.sales.viewattendance.attendance.SalesAttendanceResult;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewAttendanceActivitySales extends AppCompatActivity {

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private ListView attendanceList;
    private static TextView fromDateText,toDateText;
    private static String fromDateString ,toDateString ;
    private Button searchDateWise;
    private SalesAttendanceViewAdapter viewAttendanceAdapterSales;
private List<SalesAttendanceResult>datas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_attendance);

        preferences= getSharedPreferences("Delphi", MODE_PRIVATE);
        editor=preferences.edit();

        fromDateText=findViewById(R.id.from_date);
        toDateText=findViewById(R.id.to_date);
        searchDateWise=findViewById(R.id.search_date_wise);
        ImageView img_back =(ImageView)findViewById(R.id.back);
        ImageView menu =(ImageView)findViewById(R.id.menu);
        attendanceList=findViewById(R.id.sales_attendance_view);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(ViewAttendanceActivitySales.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.sales_home:
                                startActivity(new Intent(ViewAttendanceActivitySales.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.dashboard:
                                startActivity(new Intent(ViewAttendanceActivitySales.this, SalesMainmenu.class));
                                break;
                            case R.id.create_customer:
                                startActivity(new Intent(ViewAttendanceActivitySales.this, CustomerCreation.class));
                                break;

                            case R.id.view_customer:
                                startActivity(new Intent(ViewAttendanceActivitySales.this, ViewCustomerActivity.class));
                                break;

                            case R.id.visit_report:
                                startActivity(new Intent(ViewAttendanceActivitySales.this, VisitReport.class));
                                break;
                            case R.id.view_visit_report:
                                startActivity(new Intent(ViewAttendanceActivitySales.this, ViewVisitReport.class));
                                break;
//                            case R.id.view_cart:
//                                startActivity(new Intent(ViewAttendanceActivitySales.this, SelectPartDetails.class));
//                                break;
                            case R.id.sales_logout:
                                editor.putBoolean("salesLogin",false);
                                editor.commit();
                                startActivity(new Intent(ViewAttendanceActivitySales.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.sales_change_password:
                                String phone=preferences.getString("empMobile","");
                                final Alert alert=new Alert(ViewAttendanceActivitySales.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;
                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.sales_menu);
                pop.show();
            }
        });


        fromDateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(ViewAttendanceActivitySales.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                fromDateText.setText(year + "-" +(monthOfYear + 1) + "-" +dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

            }
        });
        toDateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(ViewAttendanceActivitySales.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                toDateText.setText(year + "-" +(monthOfYear + 1) + "-" +dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

            }
        });

        searchDateWise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fromDateString=fromDateText.getText().toString();
                toDateString=toDateText.getText().toString();

                if (fromDateString.equals("")){

                    Toast.makeText(getApplicationContext(),"Select From Date",Toast.LENGTH_LONG).show();

                }
                else if (toDateString.equals("")){
                    Toast.makeText(getApplicationContext(),"Select To Date",Toast.LENGTH_LONG).show();


                }else {
                    NetworkConnection connection= new NetworkConnection(ViewAttendanceActivitySales.this);
                    if(connection.CheckInternet())
                    {
                        dateWiseAttendance();
                    }
                    else {
                        Alert alert=new Alert(ViewAttendanceActivitySales.this);
                        alert.showAlertboxnegative("No Internet Connection. Please try again Later");
                    }
                }
            }
        });

        checkInternetConnection();

    }

    private void checkInternetConnection()
    {
        NetworkConnection connection= new NetworkConnection(ViewAttendanceActivitySales.this);
        if(connection.CheckInternet())
        {
            viewAttendance();
        }
        else {
            Alert alert=new Alert(this);
            alert.showAlertboxnegative("No Internet Connection. Please try again Later");
        }
    }

    private void viewAttendance()
    {
        final ProgressDialog progressDialog = new ProgressDialog(ViewAttendanceActivitySales.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {
            APIService service = RetroClient.getApiService();

            Call<SalesAttendance> call = service.salesAttendanceView(preferences.getString("empId",""));

            call.enqueue(new Callback<SalesAttendance>() {
                @Override
                public void onResponse(Call<SalesAttendance> call, Response<SalesAttendance> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {

                                datas=response.body().getData();
                                viewAttendanceAdapterSales=new SalesAttendanceViewAdapter(ViewAttendanceActivitySales.this,datas);
                                attendanceList.setAdapter(viewAttendanceAdapterSales);
                            }
                            else if(response.body().getResult().toLowerCase().equals("notsuccess")) {
                                Alert alert=new Alert(ViewAttendanceActivitySales.this);
                                alert.showAlertboxnegative("No Attendance Record Found");
                            }
                            else {
                                Alert alert=new Alert(ViewAttendanceActivitySales.this);
                                alert.showAlertboxnegative("Invalid Login. Please check your User Name and Password");
                            }
                        }
                        else {
                            Alert alert=new Alert(ViewAttendanceActivitySales.this);
                            alert.showAlertboxnegative("Something went wrong . Please try again later .");
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                        Alert alert=new Alert(ViewAttendanceActivitySales.this);
                        alert.showAlertboxnegative("Something went wrong . Please try again later .");
                    }

                }

                @Override
                public void onFailure(Call<SalesAttendance> call, Throwable t) {
                    progressDialog.dismiss();
                    Alert alert=new Alert(ViewAttendanceActivitySales.this);
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
            progressDialog.dismiss();
            Alert alert=new Alert(ViewAttendanceActivitySales.this);
            alert.showAlertboxnegative("Local OrderResult is Corrupted. Please logout and login.");
        }
    }

    private void dateWiseAttendance()
    {
        final ProgressDialog progressDialog = new ProgressDialog(ViewAttendanceActivitySales.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {
            APIService service = RetroClient.getApiService();

            Call<SalesAttendance> call = service.viewSalesByDate(preferences.getString("empId",""),preferences.getString("empCode",""),fromDateString,toDateString);

            call.enqueue(new Callback<SalesAttendance>() {
                @Override
                public void onResponse(Call<SalesAttendance> call, Response<SalesAttendance> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {

                                datas=response.body().getData();
                                viewAttendanceAdapterSales=new SalesAttendanceViewAdapter(ViewAttendanceActivitySales.this,datas);
                                attendanceList.setAdapter(viewAttendanceAdapterSales);
                            }
                            else if(response.body().getResult().toLowerCase().equals("notsuccess")) {
                                Alert alert=new Alert(ViewAttendanceActivitySales.this);
                                alert.showAlertbox("No Attendance Record Found");
                            }
                            else {
                                Alert alert=new Alert(ViewAttendanceActivitySales.this);
                                alert.showAlertboxnegative("Invalid Login. Please check your User Name and Password");
                            }
                        }
                        else {
                            Alert alert=new Alert(ViewAttendanceActivitySales.this);
                            alert.showAlertboxnegative("Something went wrong . Please try again later .");
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                        Alert alert=new Alert(ViewAttendanceActivitySales.this);
                        alert.showAlertboxnegative("Something went wrong . Please try again later .");
                    }

                }

                @Override
                public void onFailure(Call<SalesAttendance> call, Throwable t) {
                    progressDialog.dismiss();
                    Alert alert=new Alert(ViewAttendanceActivitySales.this);
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
            progressDialog.dismiss();
            Alert alert=new Alert(ViewAttendanceActivitySales.this);
            alert.showAlertboxnegative("Local OrderResult is Corrupted. Please logout and login.");
        }
    }


}
