package com.brainmagic.delphitvs.salesactivities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import adapter.sales.ViewAttendanceCoordinateAdapterSales;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.sales.viewattendance.salescoordinate.SalesCoordinates;
import model.sales.viewattendance.salescoordinate.SalesDataList;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewAttendanceCoordinateActivitySales extends AppCompatActivity {

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private ListView attendanceCoordinateList;
    private TextView sales_coordinate_date,sales_coordinate_day;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_attendance_coordinate);

        preferences= getSharedPreferences("Delphi", MODE_PRIVATE);
        editor=preferences.edit();
        attendanceCoordinateList=findViewById(R.id.regional_travel_history);

        sales_coordinate_date    =findViewById(R.id.sales_in_time);
        sales_coordinate_day    =findViewById(R.id.sales_out_time);
        ImageView img_back =(ImageView)findViewById(R.id.regional_back);
        ImageView menu =(ImageView)findViewById(R.id.regional_menu);

        sales_coordinate_date.setText(getIntent().getStringExtra("inTime"));
        sales_coordinate_day.setText(getIntent().getStringExtra("outTime"));

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(ViewAttendanceCoordinateActivitySales.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.sales_home:
                                startActivity(new Intent(ViewAttendanceCoordinateActivitySales.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.dashboard:
                                startActivity(new Intent(ViewAttendanceCoordinateActivitySales.this, SalesMainmenu.class));
                                break;
                            case R.id.create_customer:
                                startActivity(new Intent(ViewAttendanceCoordinateActivitySales.this, CustomerCreation.class));
                                break;

                            case R.id.view_customer:
                                startActivity(new Intent(ViewAttendanceCoordinateActivitySales.this, ViewCustomerActivity.class));
                                break;

                            case R.id.visit_report:
                                startActivity(new Intent(ViewAttendanceCoordinateActivitySales.this, VisitReport.class));
                                break;
                            case R.id.view_visit_report:
                                startActivity(new Intent(ViewAttendanceCoordinateActivitySales.this, ViewVisitReport.class));
                                break;
//                            case R.id.view_cart:
//                                startActivity(new Intent(ViewAttendanceCoordinateActivitySales.this, SelectPartDetails.class));
//                                break;
                            case R.id.sales_logout:
                                editor.putBoolean("salesLogin",false);
                                editor.commit();
                                startActivity(new Intent(ViewAttendanceCoordinateActivitySales.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.sales_change_password:
                                String phone=preferences.getString("empMobile","");
                                final Alert alert=new Alert(ViewAttendanceCoordinateActivitySales.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;
                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.sales_menu);
                pop.show();
            }
        });

        checkInternetConnection();
    }

    private void checkInternetConnection()
    {
        NetworkConnection connection= new NetworkConnection(ViewAttendanceCoordinateActivitySales.this);
        if(connection.CheckInternet())
        {
            viewAttendance();
        }
        else {
            Alert alert=new Alert(this);
            alert.showAlertboxnegative("No Internet Connection. Please try again Later");
        }
    }

    private void viewAttendance()
    {
        final ProgressDialog progressDialog = new ProgressDialog(ViewAttendanceCoordinateActivitySales.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {
            APIService service = RetroClient.getApiService();

            Call<SalesCoordinates> call = service.viewSalesCoordinate(preferences.getString("empId",""), (getIntent().getStringExtra("trackId")),getIntent().getStringExtra("date"));

            call.enqueue(new Callback<SalesCoordinates>() {
                @Override
                public void onResponse(Call<SalesCoordinates> call, Response<SalesCoordinates> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {

                                List<SalesDataList>  coordinates=response.body().getData();

                                SalesDataList viewSalesCoordinateResultIn=new SalesDataList();
                                try {
                                    final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                                    final Date dateObj = sdf.parse(getIntent().getStringExtra("inTime"));
                                    Log.d(ViewAttendanceCoordinateActivitySales.class.getName(), "onResponse: ");
                                    viewSalesCoordinateResultIn.setAddress(getIntent().getStringExtra("inAddress"));
                                    viewSalesCoordinateResultIn.setTimedate(new SimpleDateFormat("hh:mm aa").format(dateObj));
                                }catch (Exception e)
                                {
                                    e.printStackTrace();
                                }
                                coordinates.add(0,viewSalesCoordinateResultIn);
                                SalesDataList viewSalesCoordinateResultOut=new SalesDataList();
                                try{
                                    final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                                    final Date dateObj = sdf.parse(getIntent().getStringExtra("outTime"));
                                    viewSalesCoordinateResultOut.setAddress(getIntent().getStringExtra("outAddress"));
                                    viewSalesCoordinateResultOut.setTimedate(new SimpleDateFormat("hh:mm a").format(dateObj));

                                }catch (Exception e)
                                {
                                    e.printStackTrace();
                                }
                                coordinates.add(coordinates.size(),viewSalesCoordinateResultOut);
                                ViewAttendanceCoordinateAdapterSales viewAttendanceAdapterSales=new ViewAttendanceCoordinateAdapterSales(ViewAttendanceCoordinateActivitySales.this,coordinates);
                                attendanceCoordinateList.setAdapter(viewAttendanceAdapterSales);
                            }
                            else if(response.body().getResult().equals("NotSuccess"))
                            {
                                Alert alert=new Alert(ViewAttendanceCoordinateActivitySales.this);
                                alert.showAlertboxnegative("No Attendance Coordinate records Found");
                            }
                            else {
                                Alert alert=new Alert(ViewAttendanceCoordinateActivitySales.this);
                                alert.showAlertboxnegative("Invalid Login. Please check your User Name and Password");
                            }
                        }
                        else {
                            Alert alert=new Alert(ViewAttendanceCoordinateActivitySales.this);
                            alert.showAlertboxnegative("Something went wrong . Please try again later .");
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                        Alert alert=new Alert(ViewAttendanceCoordinateActivitySales.this);
                        alert.showAlertboxnegative("Something went wrong . Please try again later .");
                    }

                }

                @Override
                public void onFailure(Call<SalesCoordinates> call, Throwable t) {
                    progressDialog.dismiss();
                    Alert alert=new Alert(ViewAttendanceCoordinateActivitySales.this);
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
            progressDialog.dismiss();
            Alert alert=new Alert(ViewAttendanceCoordinateActivitySales.this);
            alert.showAlertboxnegative("Local Data is Corrupted. Please logout and login.");
        }
    }
}
