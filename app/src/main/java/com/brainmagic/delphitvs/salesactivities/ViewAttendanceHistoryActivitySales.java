package com.brainmagic.delphitvs.salesactivities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;

import adapter.sales.ViewAttendanceHistoryAdapterSales;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.sales.viewattendance.attendancehistory.SalesAttendanceHistory;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewAttendanceHistoryActivitySales extends AppCompatActivity {

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private ListView regional_attendance_history_list;
    private ImageView regional_menu,regional_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_attendancd_history);

        preferences= getSharedPreferences("Delphi", MODE_PRIVATE);
        editor=preferences.edit();
        ImageView img_back =(ImageView)findViewById(R.id.back);
        regional_attendance_history_list=findViewById(R.id.regional_attendance_history_list);
        regional_menu=findViewById(R.id.regional_menu);
        regional_back=findViewById(R.id.regional_back);
        TextView date =findViewById(R.id.attendance_history_date);
        TextView day =findViewById(R.id.attendance_history_day);

        date.setText(getIntent().getStringExtra("date"));
        day.setText(getIntent().getStringExtra("day"));

        regional_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });

        regional_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(ViewAttendanceHistoryActivitySales.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.sales_home:
                                startActivity(new Intent(ViewAttendanceHistoryActivitySales.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.dashboard:
                                startActivity(new Intent(ViewAttendanceHistoryActivitySales.this, SalesMainmenu.class));
                                break;
                            case R.id.create_customer:
                                startActivity(new Intent(ViewAttendanceHistoryActivitySales.this, CustomerCreation.class));
                                break;

                            case R.id.view_customer:
                                startActivity(new Intent(ViewAttendanceHistoryActivitySales.this, ViewCustomerActivity.class));
                                break;

                            case R.id.visit_report:
                                startActivity(new Intent(ViewAttendanceHistoryActivitySales.this, VisitReport.class));
                                break;
                            case R.id.view_visit_report:
                                startActivity(new Intent(ViewAttendanceHistoryActivitySales.this, ViewVisitReport.class));
                                break;
//                            case R.id.view_cart:
//                                startActivity(new Intent(ViewAttendanceHistoryActivitySales.this, SelectPartDetails.class));
//                                break;
                            case R.id.sales_logout:
                                editor.putBoolean("salesLogin",false);
                                editor.commit();
                                startActivity(new Intent(ViewAttendanceHistoryActivitySales.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.sales_change_password:
                                String phone=preferences.getString("empMobile","");
                                final Alert alert=new Alert(ViewAttendanceHistoryActivitySales.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;
                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.sales_menu);
                pop.show();
            }
        });

        checkInternetConnection();
    }

    private void checkInternetConnection()
    {
        NetworkConnection connection= new NetworkConnection(ViewAttendanceHistoryActivitySales.this);
        if(connection.CheckInternet())
        {
            viewAttendance();
        }
        else {
            Alert alert=new Alert(ViewAttendanceHistoryActivitySales.this);
            alert.showAlertboxnegative("No Internet Connection. Please try again Later");
        }
    }

    private void viewAttendance() {
        final ProgressDialog progressDialog = new ProgressDialog(ViewAttendanceHistoryActivitySales.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {
            APIService service = RetroClient.getApiService();

            Call<SalesAttendanceHistory> call = service.salesAttendanceHistoryView(preferences.getString("empId", ""),getIntent().getStringExtra("date"));

            call.enqueue(new Callback<SalesAttendanceHistory>() {
                @Override
                public void onResponse(Call<SalesAttendanceHistory> call, Response<SalesAttendanceHistory> response) {
                    progressDialog.dismiss();
                    try {
                        if (response.isSuccessful()) {
                            if (response.body().getResult().equals("Success")) {
                                ViewAttendanceHistoryAdapterSales viewAttendanceHistoryAdapterSales=new ViewAttendanceHistoryAdapterSales(ViewAttendanceHistoryActivitySales.this,
                                        response.body().getData(),getIntent().getStringExtra("date"),getIntent().getStringExtra("day"));
                                regional_attendance_history_list.setAdapter(viewAttendanceHistoryAdapterSales);
                            }

                            else if(response.body().getResult().toLowerCase().equals("notsuccess")) {
                                Alert alert=new Alert(ViewAttendanceHistoryActivitySales.this);
                                alert.showAlertboxnegative("No Attendance History Found");
                            }else {
                                Alert alert = new Alert(ViewAttendanceHistoryActivitySales.this);
                                alert.showAlertboxnegative("Invalid Login. Please check your User Name and Password");
                            }
                        } else {
                            Alert alert = new Alert(ViewAttendanceHistoryActivitySales.this);
                            alert.showAlertboxnegative("Something went wrong . Please try again later .");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Alert alert = new Alert(ViewAttendanceHistoryActivitySales.this);
                        alert.showAlertboxnegative("Something went wrong . Please try again later .");
                    }

                }

                @Override
                public void onFailure(Call<SalesAttendanceHistory> call, Throwable t) {
                    progressDialog.dismiss();
                    Alert alert = new Alert(ViewAttendanceHistoryActivitySales.this);
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            progressDialog.dismiss();
            Alert alert = new Alert(ViewAttendanceHistoryActivitySales.this);
            alert.showAlertboxnegative("Local OrderResult is Corrupted. Please logout and login.");
        }
    }
}
