package com.brainmagic.delphitvs.salesactivities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;

import java.util.List;

import adapter.sales.ViewCustomerAdapter;
import alert.Alert;
import alert.OnSpinerItemClick;
import alert.SpinnerDialog;
import model.api.APIService;
import model.api.RetroClient;
import model.sales.customerdetails.GetCustomerDetails;
import model.sales.customerdetails.search.CustomerNameList;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewCustomerActivity extends AppCompatActivity {

    private Alert box;
    private SharedPreferences.Editor editor;
    private SharedPreferences preferences;
    private ListView customerList;
    private TextView customerName;
    private Button search;
    private SpinnerDialog spinnerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_customer);

        box=new Alert(this);
        preferences = getSharedPreferences("Delphi", MODE_PRIVATE);
        editor = preferences.edit();

        ImageView img_back =(ImageView)findViewById(R.id.back);
        ImageView menu =(ImageView)findViewById(R.id.menu);
        customerList =findViewById(R.id.customers_listview);
        customerName =findViewById(R.id.customer_name_search);
        search =findViewById(R.id.search_by_customer_button);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(ViewCustomerActivity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.sales_home:
                                startActivity(new Intent(ViewCustomerActivity.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.dashboard:
                                startActivity(new Intent(ViewCustomerActivity.this, SalesMainmenu.class));
                                break;
                            case R.id.create_customer:
                                startActivity(new Intent(ViewCustomerActivity.this, CustomerCreation.class));
                                break;



                            case R.id.visit_report:
                                startActivity(new Intent(ViewCustomerActivity.this, VisitReport.class));
                                break;
                            case R.id.view_visit_report:
                                startActivity(new Intent(ViewCustomerActivity.this, ViewVisitReport.class));
                                break;
//                            case R.id.view_cart:
//                                startActivity(new Intent(ViewCustomerActivity.this, SelectPartDetails.class));
//                                break;
                            case R.id.sales_logout:
                                editor.putBoolean("salesLogin",false);
                                editor.commit();
                                startActivity(new Intent(ViewCustomerActivity.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.sales_change_password:
                                String phone=preferences.getString("empMobile","");
                                final Alert alert=new Alert(ViewCustomerActivity.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;
                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.sales_menu);
                pop.getMenu().findItem(R.id.view_customer).setVisible(false);

                pop.show();
            }
        });
        spinnerDialog=new SpinnerDialog(ViewCustomerActivity.this,"Search Customer");
        customerName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerDialog.showSpinerDialog();
            }
        });

        spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String var1, int var2) {
                customerName.setText(var1);
            }
        });


        NetworkConnection connection=new NetworkConnection(this);
        if(connection.CheckInternet())
        {
            getCustomerNameList();
            getCustomerDetails();
        }
        else {
            box.showAlertboxnegative("No Internet Connection. Please try again Later");
        }

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!customerName.getText().toString().equals("All"))
                {
                    getNotifyCustomerDetails(customerName.getText().toString());
                }
                else {
                    getCustomerDetails();
                }
            }
        });

    }

    private void getCustomerNameList()
    {
        final ProgressDialog loading = ProgressDialog.show(this, getResources().getString(R.string.app_name), "Loading...", false, false);
        try {
            APIService service = RetroClient.getApiService();
            Call<CustomerNameList> call = service.getCustomerName(preferences.getString("empId", ""));

            call.enqueue(new Callback<CustomerNameList>() {
                @Override
                public void onResponse(Call<CustomerNameList> call, Response<CustomerNameList> response) {
                    loading.dismiss();
                    try {
                        if (response.isSuccessful()) {
                            if (response.body().getResult().equals("Success")) {
                                List<String> customerNameList=response.body().getData();
                                customerNameList.add(0,"All");
                                spinnerDialog.setItems(customerNameList);
                            }
                            else {
                                box.showAlertboxnegative("No Record found");
                            }
                        } else {

                            box.showAlertboxnegative("Something went wrong . Please try again later .");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        box.showAlertboxnegative("Something went wrong. Please Logout and Login");
                    }
                }

                @Override
                public void onFailure(Call<CustomerNameList> call, Throwable t) {
                    loading.dismiss();
                    box.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            e.printStackTrace();
            box.showAlertboxnegative("OrderResult not Found. Please try again Later");
        }
    }

    private void getNotifyCustomerDetails(String customerName) {
        final ProgressDialog loading = ProgressDialog.show(this, getResources().getString(R.string.app_name), "Loading...", false, false);
        try {
            APIService service = RetroClient.getApiService();
            Call<GetCustomerDetails> call = service.viewCustomerList(preferences.getString("empId", ""),customerName);

            call.enqueue(new Callback<GetCustomerDetails>() {
                @Override
                public void onResponse(Call<GetCustomerDetails> call, Response<GetCustomerDetails> response) {
                    loading.dismiss();
                    try {
                        if (response.isSuccessful()) {
                            if (response.body().getResult().equals("Success")) {
                                ViewCustomerAdapter viewCustomerAdapter=new ViewCustomerAdapter(ViewCustomerActivity.this,response.body().getData());
                                customerList.setAdapter(viewCustomerAdapter);
                            }
                            else {
                                box.showAlertboxnegative("No Record found");
                            }
                        } else {

                            box.showAlertboxnegative("Something went wrong . Please try again later .");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        box.showAlertboxnegative("Something went wrong. Please Logout and Login");
                    }
                }

                @Override
                public void onFailure(Call<GetCustomerDetails> call, Throwable t) {
                    loading.dismiss();
                    box.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            e.printStackTrace();
            box.showAlertboxnegative("OrderResult not Found. Please try again Later");
        }

    }

    private void getCustomerDetails() {
        final ProgressDialog loading = ProgressDialog.show(this, getResources().getString(R.string.app_name), "Loading...", false, false);
        try {
            APIService service = RetroClient.getApiService();
            Call<GetCustomerDetails> call = service.getCustomerDetails(preferences.getString("empId", ""));

            call.enqueue(new Callback<GetCustomerDetails>() {
                @Override
                public void onResponse(Call<GetCustomerDetails> call, Response<GetCustomerDetails> response) {
                    loading.dismiss();
                    try {
                        if (response.isSuccessful()) {
                            if (response.body().getResult().equals("Success")) {
                                ViewCustomerAdapter viewCustomerAdapter=new ViewCustomerAdapter(ViewCustomerActivity.this,response.body().getData());
                                customerList.setAdapter(viewCustomerAdapter);
                            }
                            else {
                                box.showAlertboxnegative("No Record found");
                            }
                        } else {

                            box.showAlertboxnegative("Something went wrong . Please try again later .");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        box.showAlertboxnegative("Something went wrong. Please Logout and Login");
                    }
                }

                @Override
                public void onFailure(Call<GetCustomerDetails> call, Throwable t) {
                    loading.dismiss();
                    box.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            e.printStackTrace();
            box.showAlertboxnegative("OrderResult not Found. Please try again Later");
        }

    }
}
