package com.brainmagic.delphitvs.salesactivities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;

import adapter.sales.ViewSalesOrderFullHistoryAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.regional.salesnamelist.CurrentOrderDetailsRegionalPojo;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewFullSalesOrderHistory extends AppCompatActivity {

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private ListView dataList;
    private ImageView menu,back;
    private String sales_order_date,sales_amount;
    private ViewSalesOrderFullHistoryAdapter adapter;
    private TextView currnt_month_amount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_full_sales_order_history);


        preferences= getSharedPreferences("Delphi", MODE_PRIVATE);
        editor=preferences.edit();

        menu   =(ImageView)findViewById(R.id.menu);
        back =(ImageView)findViewById(R.id.back);
        dataList =findViewById(R.id.data_list);
        currnt_month_amount=findViewById(R.id.currnt_month_amount);

        sales_order_date=getIntent().getStringExtra("sales_order_date");

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(ViewFullSalesOrderHistory.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.sales_home:
                                startActivity(new Intent(ViewFullSalesOrderHistory.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.dashboard:
                                startActivity(new Intent(ViewFullSalesOrderHistory.this, SalesMainmenu.class));
                                break;
                            case R.id.create_customer:
                                startActivity(new Intent(ViewFullSalesOrderHistory.this, CustomerCreation.class));
                                break;

                            case R.id.view_customer:
                                startActivity(new Intent(ViewFullSalesOrderHistory.this, ViewCustomerActivity.class));
                                break;

                            case R.id.visit_report:
                                startActivity(new Intent(ViewFullSalesOrderHistory.this, VisitReport.class));
                                break;
                            case R.id.view_visit_report:
                                startActivity(new Intent(ViewFullSalesOrderHistory.this, ViewVisitReport.class));
                                break;
//                            case R.id.view_cart:
//                                startActivity(new Intent(ViewFullSalesOrderHistory.this, SelectPartDetails.class));
//                                break;
                            case R.id.sales_logout:
                                editor.putBoolean("salesLogin",false);
                                editor.commit();
                                startActivity(new Intent(ViewFullSalesOrderHistory.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.sales_change_password:
                                String phone=preferences.getString("empMobile","");
                                final Alert alert=new Alert(ViewFullSalesOrderHistory.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;
                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.sales_menu);
                pop.show();
            }
        });

        NetworkConnection connection=new NetworkConnection(this);
        if(connection.CheckInternet())
        {
            getMoreDetails();
        }
        else {
            Alert alert=new Alert(this);
            alert.showAlertboxnegative("No Internet Connection. Please try again Later");
        }
    }

    private void getMoreDetails(){

        final ProgressDialog progressDialog =new ProgressDialog(ViewFullSalesOrderHistory.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();


        APIService service=RetroClient.getApiService();
        Call<CurrentOrderDetailsRegionalPojo>call=service.salesdetails(preferences.getString("empId",""),sales_order_date);

        try {

            call.enqueue(new Callback<CurrentOrderDetailsRegionalPojo>() {
                @Override
                public void onResponse(Call<CurrentOrderDetailsRegionalPojo> call, final Response<CurrentOrderDetailsRegionalPojo> response) {

                    if(response.isSuccessful()){
                        if(response.body().getResult().equals("Success")){
                            progressDialog.dismiss();
                           currnt_month_amount.setVisibility(View.VISIBLE);
                            sales_amount=response.body().getData().getTot();
                            currnt_month_amount.setText("Rs."+String.valueOf(sales_amount));

                            adapter=new ViewSalesOrderFullHistoryAdapter(ViewFullSalesOrderHistory.this,response.body().getData().getOrderList());
                            dataList.setAdapter(adapter);

                            dataList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                    Intent intent=new Intent(ViewFullSalesOrderHistory.this,SalesFullOrderDetails.class);
                                    intent.putExtra("sales_order_id",response.body().getData().getOrderList().get(i).getOrderid());
                                    startActivity(intent);

                                }
                            });

                        }
                        else {
                            Alert alert = new Alert(ViewFullSalesOrderHistory.this);
                            alert.showAlertboxnegative("No Record Found");
                        }
                    }
                }

                @Override
                public void onFailure(Call<CurrentOrderDetailsRegionalPojo> call, Throwable t) {
                    progressDialog.dismiss();
                    Alert alert=new Alert(ViewFullSalesOrderHistory.this);
                    alert.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });

        }catch (Exception e){
            e.printStackTrace();
            Alert alert=new Alert(ViewFullSalesOrderHistory.this);
            alert.showAlertboxnegative("Something went wrong . Please try again later .");
        }



    }
}
