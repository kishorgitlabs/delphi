package com.brainmagic.delphitvs.salesactivities;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;


import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import adapter.sales.ViewVisitReportAdapter;
import alert.Alert;
import model.api.APIService;
import model.api.RetroClient;
import model.sales.visitreport.ViewVisitReportModel;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewVisitReport extends AppCompatActivity {

    private ImageView back,menu;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private TextView fromDate, toDate;
    private MaterialSpinner customerTypeSpinner;
    private ListView viewVisitReport;
    String executiveName="";
    private ProgressDialog progressDialog;
    private Button view_visit_report_rm_searchs;
    private String sales_from_date,sales_to_date,sales_customer_type;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_visit_report);

        myshare = getSharedPreferences("Delphi", MODE_PRIVATE);
        editor = myshare.edit();
        back=(ImageView) findViewById(R.id.back);
        menu=(ImageView) findViewById(R.id.menu);
        fromDate=findViewById(R.id.from_date);
        toDate=findViewById(R.id.to_date);
        customerTypeSpinner=findViewById(R.id.customer_type);
        viewVisitReport=findViewById(R.id.view_visit_report_list);
        view_visit_report_rm_searchs=findViewById(R.id.visit_report);
//        appname.setText(salesname);


        final List<String> customerTypes=new ArrayList<>();
        customerTypes.add("Select Customer Type");
        customerTypes.add("Electrician");
        customerTypes.add("Retailer");
        customerTypes.add("Mechanic");
        customerTypes.add("Others");

        customerTypeSpinner.setItems(customerTypes);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//               onBackPressed();
                finish();
            }
        });

        view_visit_report_rm_searchs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                 sales_from_date=fromDate.getText().toString();
                 sales_to_date=toDate.getText().toString();
                 sales_customer_type=customerTypeSpinner.getText().toString();


                if(sales_from_date.equals("")){

                    Toast.makeText(getApplicationContext(),"Select From Date",Toast.LENGTH_LONG).show();
                }
                else if(sales_to_date.equals("")){
                    Toast.makeText(getApplicationContext(),"Select To Date",Toast.LENGTH_LONG).show();
                }
                else if(sales_customer_type.equals("Select Customer Type")){
                    Toast.makeText(getApplicationContext(),"Select Customer Type",Toast.LENGTH_LONG).show();
                }
                else{

                    NetworkConnection connection=new NetworkConnection(ViewVisitReport.this);

                    if(connection.CheckInternet()){
                        viewVisitReportBySearch();
                    }
                    else{
                        Alert alert=new Alert(ViewVisitReport.this);
                        alert.showAlertboxnegative("Please Switch On Your Internet Connection");
                    }
                }


            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(ViewVisitReport.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.sales_home:
                                startActivity(new Intent(ViewVisitReport.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.dashboard:
                                startActivity(new Intent(ViewVisitReport.this, SalesMainmenu.class));
                                break;
                            case R.id.create_customer:
                                startActivity(new Intent(ViewVisitReport.this, CustomerCreation.class));
                                break;

                            case R.id.view_customer:
                                startActivity(new Intent(ViewVisitReport.this, ViewCustomerActivity.class));
                                break;

                            case R.id.visit_report:
                                startActivity(new Intent(ViewVisitReport.this, VisitReport.class));
                                break;

//                            case R.id.view_cart:
//                                startActivity(new Intent(ViewVisitReport.this, SelectPartDetails.class));
//                                break;
                            case R.id.sales_logout:
                                editor.putBoolean("salesLogin",false);
                                editor.commit();
                                startActivity(new Intent(ViewVisitReport.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.sales_change_password:
                                String phone=myshare.getString("empMobile","");
                                final Alert alert=new Alert(ViewVisitReport.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;
                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.sales_menu);
                pop.getMenu().findItem(R.id.view_visit_report).setVisible(false);

                pop.show();
            }
        });

        fromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(ViewVisitReport.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                fromDate.setText(year + "-" +(monthOfYear + 1) + "-" +dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

            }
        });

        toDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(ViewVisitReport.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                toDate.setText(year + "-" +(monthOfYear + 1) + "-" +dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

            }
        });
        NetworkConnection network = new NetworkConnection(ViewVisitReport.this);
        if(network.CheckInternet()) {
//            if (myshare.getString("userType", "").equals("Sales")) {
//                String userName=myshare.getString("username","");
                viewVisitReportDetails();
//            } else {
//                getSalesManagerReportList();
//            }
        }
        else {
            Alert alertbox=new Alert(this);
            alertbox.showAlertboxnegative("No Internet Connection. Please switch On your Internet Connection");
        }
    }

//    private void spinnerAdapter(List<String> customerTypeList){
////        ArrayAdapter arrayAdapter=new ArrayAdapter(ViewVisitReportModel.this,android.R.layout.simple_spinner_dropdown_item,customerTypeList);
////        customerTypes.setAdapter(arrayAdapter);
//        customerTypes.setItems(customerTypeList);
//    }



    private void viewVisitReportDetails()
    {
        final ProgressDialog loading = ProgressDialog.show(this, getResources().getString(R.string.app_name), "Loading...", false, false);
        APIService api= RetroClient.getApiService();


        Call<ViewVisitReportModel> call=api.viewVisitReport(myshare.getString("empId",""));

        try {
            call.enqueue(new Callback<ViewVisitReportModel>() {
                @Override
                public void onResponse(Call<ViewVisitReportModel> call, Response<ViewVisitReportModel> response) {
                    try{
                        loading.dismiss();
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
                                ViewVisitReportAdapter adapter=new ViewVisitReportAdapter(ViewVisitReport.this,response.body().getData());
                                viewVisitReport.setAdapter(adapter);
                            }
                            else if(response.body().getResult().equals("NotSuccess"))
                            {
                                Alert alertbox=new Alert(ViewVisitReport.this);
                                alertbox.showAlertboxnegative("No Record Found");
                            }
                            else {
                                Alert alertbox=new Alert(ViewVisitReport.this);
                                alertbox.showAlertboxnegative("Something went wrong . Please try again later .");
                            }
                        }
                    }catch (Exception e){
                        loading.dismiss();
                        e.printStackTrace();
                        Alert alertbox=new Alert(ViewVisitReport.this);
                        alertbox.showAlertboxnegative("Something went wrong . Please try again later .");
                    }
                }

                @Override
                public void onFailure(Call<ViewVisitReportModel> call, Throwable t) {
                    loading.dismiss();
                    Alert alertbox=new Alert(ViewVisitReport.this);
                    alertbox.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            loading.dismiss();
            e.printStackTrace();
            Alert alertbox=new Alert(ViewVisitReport.this);
            alertbox.showAlertboxnegative("Something went wrong . Please try again later .");
        }
    }




    private void viewVisitReportBySearch()
    {
        final ProgressDialog loading = ProgressDialog.show(this, getResources().getString(R.string.app_name), "Loading...", false, false);
        APIService api=RetroClient.getApiService();


        Call<ViewVisitReportModel> call=api.viewVisitReportDateWise(sales_from_date,sales_to_date,myshare.getString("empId",""),sales_customer_type);

        try {
            call.enqueue(new Callback<ViewVisitReportModel>() {
                @Override
                public void onResponse(Call<ViewVisitReportModel> call, Response<ViewVisitReportModel> response) {
                    try{
                        loading.dismiss();
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
                                ViewVisitReportAdapter adapter=new ViewVisitReportAdapter(ViewVisitReport.this,response.body().getData());
                                viewVisitReport.setAdapter(adapter);
                            }
                            else if(response.body().getResult().equals("NotSuccess"))
                            {
                                Alert alertbox=new Alert(ViewVisitReport.this);
                                alertbox.showAlertbox("No Record Found");
                            }
                            else {
                                Alert alertbox=new Alert(ViewVisitReport.this);
                                alertbox.showAlertbox("Something went wrong . Please try again later .");
                            }
                        }
                    }catch (Exception e){
                        loading.dismiss();
                        e.printStackTrace();
                        Alert alertbox=new Alert(ViewVisitReport.this);
                        alertbox.showAlertbox("Something went wrong . Please try again later .");
                    }
                }

                @Override
                public void onFailure(Call<ViewVisitReportModel> call, Throwable t) {
                    loading.dismiss();
                    Alert alertbox=new Alert(ViewVisitReport.this);
                    alertbox.showAlertbox("Something went wrong . Please try again later .");
                }
            });
        }catch (Exception e)
        {
            loading.dismiss();
            e.printStackTrace();
            Alert alertbox=new Alert(ViewVisitReport.this);
            alertbox.showAlertbox("Something went wrong . Please try again later .");
        }
    }


    private void getSalesManagerReportList()// Regional Manager Flow
    {
//        final SpinnerDialog dialog=new SpinnerDialog(ViewVisitReportModel.this,"Select Sales Executives");
//        final ProgressDialog loading = ProgressDialog.show(this, getResources().getString(R.string.app_name), "Loading...", false, false);
//        APIService api=RetroClient.getApiService();

//        int regid=myshare.getInt("idlistreg",0);
//        Call<ViewVisitReportRM> call=api.viewVisitReportRM(regid);
//
//        try {
//            call.enqueue(new Callback<ViewVisitReportRM>() {
//                @Override
//                public void onResponse(Call<ViewVisitReportRM> call, Response<ViewVisitReportRM> response) {
//                    try{
//                        loading.dismiss();
//                        if(response.isSuccessful())
//                        {
//                            if(response.body().getResult().equals("Success"))
//                            {
//                                dialog.setItems(response.body().getData());
//                                dialog.showSpinerDialog();
//                            }
//                            else {
//                                Alert alertbox=new Alert(ViewVisitReportModel.this);
//                                alertbox.showAlertboxnegative("Record sent is Invalid. Please Login again to view this OrderResult");
//                            }
//                        }
//                    }catch (Exception e){
//                        loading.dismiss();
//                        e.printStackTrace();
//                        Alert alertbox=new Alert(ViewVisitReportModel.this);
//                        alertbox.showAlertboxnegative("Something went wrong . Please try again later .");
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<ViewVisitReportRM> call, Throwable t) {
//                    loading.dismiss();
//                    Alert alertbox=new Alert(ViewVisitReportModel.this);
//                    alertbox.showAlertboxnegative("Something went wrong . Please try again later .");
//                }
//            });
//        }catch (Exception e)
//        {
//            loading.dismiss();
//            e.printStackTrace();
//            Alert alertbox=new Alert(ViewVisitReportModel.this);
//            alertbox.showAlertboxnegative("Something went wrong . Please try again later .");
//        }

//        dialog.bindOnSpinerListener(new OnSpinerItemClick(){
//            @Override
//            public void onClick(String userName, int var2) {
//                executiveName=userName;
//                viewVisitReportDetails(userName);
//            }
//        });
    }
}
