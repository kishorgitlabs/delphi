package com.brainmagic.delphitvs.salesactivities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

import alert.Alert;
import io.reactivex.annotations.NonNull;
import model.api.APIService;
import model.api.RetroClient;
import model.sales.customerdetails.GetCustomerDetails;
import model.sales.customerdetails.GetCustomerDetailsResult;
import model.sales.visitreport.VisitReportModel;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static model.api.RetroClient.APIKEY;


public class VisitReport extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        android.location.LocationListener {
    private static final String TAG = "AttendanceActivitySales";
    public final String ACTION_START_FOREGROUND_SERVICE = "ACTION_START_FOREGROUND_SERVICE";
    private boolean permissionGranted = true;
    private LocationRequest mLocationRequest;
    private Alert box;
    private Location lastLocation;
    private String[] locationAccess = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.INTERNET};
    private GoogleApiClient googleApiClient;
    private WindowManager manager;
    private LocationManager locationManager;
    private SharedPreferences.Editor editor;
    private SharedPreferences preferences;
    private String manufacturer;
    private int IGNORE_BATTERY_OPTIMIZATION_REQUEST = 1003;
    private int REQUEST_CHECK_SETTINGS=100;
    protected final int REQUEST_LOCATION_SETTINGS = 300;
    protected final int TWO_MINUTES = 2* 60 * 1000;
    protected int MIN_DISTANCE_CHANGE_FOR_UPDATES=500;
    protected static final int RESULT_LOCATION_CODE_CANCELLED = 0;
    private String address;
    private ProgressDialog progress;
    private Location myLocation;
    private EditText customerNameText,remark;
    private AutoCompleteTextView mobileNo;
    private TextView placeVisitedAddress,visitDate;
    private MaterialSpinner customerTypes;
    private Button submit;
    private List<GetCustomerDetailsResult> getCustomerDetailsResults;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit_report);
        box = new Alert(this);

        preferences = getSharedPreferences("Delphi", MODE_PRIVATE);
        editor = preferences.edit();
        locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        myLocation=new Location(LocationManager.GPS_PROVIDER);

        ImageView menu =(ImageView)findViewById(R.id.menu);
        ImageView img_back =(ImageView)findViewById(R.id.back);
        submit =findViewById(R.id.add_visit_report);


        progress = new ProgressDialog(this);
        progress.setMessage("Loading...");
        progress.setTitle("Retrieving your Location Details");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);



        customerNameText=findViewById(R.id.customer_name);
        visitDate=findViewById(R.id.visit_date);
        remark=findViewById(R.id.activity);
        placeVisitedAddress=findViewById(R.id.place_visit);
        mobileNo=findViewById(R.id.mobile_no);
        customerTypes=findViewById(R.id.customer_type_spinner);

        Calendar c = Calendar.getInstance();
        c.getTime();
        SimpleDateFormat df1 = new SimpleDateFormat("dd/MM/yyyy");
        String currentDate = df1.format(c.getTime());

        visitDate.setText(currentDate);

        List<String> userTypes=new ArrayList<>();
        userTypes.add("Select Customer");
        userTypes.add("Mechanic");
        userTypes.add("Retailer");
        userTypes.add("Electrician");
        userTypes.add("Others");

//        ArrayAdapter arrayAdapter=new ArrayAdapter(this,android.R.layout.simple_spinner_item,userTypes);
//
//        customerTypes.setAdapter(arrayAdapter);
        customerTypes.setItems(userTypes);



        final NetworkConnection connection = new NetworkConnection(this);
        if (connection.CheckInternet()){
            getCustomerDetails();
            init();
        }
        else {
            box.showAlertboxnegative("No Internet Connection");
        }


        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in,R.anim.left_out);
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(VisitReport.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.sales_home:
                                startActivity(new Intent(VisitReport.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.dashboard:
                                startActivity(new Intent(VisitReport.this, SalesMainmenu.class));
                                break;
                            case R.id.create_customer:
                                startActivity(new Intent(VisitReport.this, CustomerCreation.class));
                                break;

                            case R.id.view_customer:
                                startActivity(new Intent(VisitReport.this, ViewCustomerActivity.class));
                                break;


                            case R.id.view_visit_report:
                                startActivity(new Intent(VisitReport.this, ViewVisitReport.class));
                                break;
//                            case R.id.view_cart:
//                                startActivity(new Intent(VisitReport.this, SelectPartDetails.class));
//                                break;
                            case R.id.sales_logout:
                                editor.putBoolean("salesLogin",false);
                                editor.commit();
                                startActivity(new Intent(VisitReport.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.sales_change_password:
                                String phone=preferences.getString("empMobile","");
                                final Alert alert=new Alert(VisitReport.this);
                                alert.changePasswordAlert(phone);
                                alert.setOnButtonClickListener(new Alert.onButtonClickListener() {
                                    @Override
                                    public void onMessage(String msg) {
                                        alert.showAlertbox(msg);
                                    }
                                });

                                break;
                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.sales_menu);
                pop.getMenu().findItem(R.id.visit_report).setVisible(false);

                pop.show();
            }
        });

        mobileNo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String customerName=getCustomerDetailsResults.get(position).getContactPersonname();
                customerNameText.setText(customerName);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String customerType=customerTypes.getText().toString();
                String customerMobileNo=mobileNo.getText().toString();
                String customerName=customerNameText.getText().toString();
                String address=placeVisitedAddress.getText().toString();
                String date=visitDate.getText().toString();
                String remarks=remark.getText().toString();

                if(customerType.equals("Select Customer"))
                {
                    customerTypes.setError("Select Customer");
                    Toast.makeText(VisitReport.this, "Select Customer", Toast.LENGTH_SHORT).show();
                }
                else if(customerMobileNo.length()<10)
                {
                    mobileNo.setError("Enter Customer Number");
                    Toast.makeText(VisitReport.this, "Enter Customer Number", Toast.LENGTH_SHORT).show();
                }
                else if(TextUtils.isEmpty(customerName))
                {
//                    customerTypes.setError("Customer ");
                    Toast.makeText(VisitReport.this, "Enter Customer Name", Toast.LENGTH_SHORT).show();
                }
                else if(TextUtils.isEmpty(address))
                {
                    Toast.makeText(VisitReport.this, "No Address Found. Please make sure that gps is enabled", Toast.LENGTH_SHORT).show();
                }
                else {
//                    NetworkConnection connection1=new NetworkConnection(VisitReport.this);
                    if(connection.CheckInternet())
                        addVisitReport(customerType,customerMobileNo,customerName,address,date,remarks);
                    else
                    {
                        box.showAlertboxnegative("No Internet Connection");
                    }
                }

            }
        });
    }

    private void getCustomerDetails()
    {
        try {
            APIService service = RetroClient.getApiService();
            Call<GetCustomerDetails> call=service.getCustomerDetails(preferences.getString("empId",""));

            call.enqueue(new Callback<GetCustomerDetails>() {
                @Override
                public void onResponse(Call<GetCustomerDetails> call, Response<GetCustomerDetails> response) {
                    try{
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
                                getCustomerDetailsResults=response.body().getData();
                                getMobileNumber(getCustomerDetailsResults);
                            }
//                            else {
//
//                                box.showAlertboxnegative("No Mobile number found");
//                            }
                        }
                        else {

                            box.showAlertboxnegative("Something went wrong . Please try again later .");
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                        box.showAlertboxnegative("No Mobile number found");
                    }
                }

                @Override
                public void onFailure(Call<GetCustomerDetails> call, Throwable t) {
                    box.showAlertboxnegative("Something went wrong . Please try again later .");
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
            box.showAlertboxnegative("OrderResult not Found. Please try again Later");
        }
    }

    private void addVisitReport(String customerType, String customerMobileNo, String customerName, String address, String date, String remarks)
    {
        try {
            APIService service = RetroClient.getApiService();
            Call<VisitReportModel> call=service.addVisitReport(preferences.getString("empName",""),preferences.getString("empId",""),customerMobileNo,
                    address,date,customerName,customerType,remarks);

            call.enqueue(new Callback<VisitReportModel>() {
                @Override
                public void onResponse(Call<VisitReportModel> call, Response<VisitReportModel> response) {
                    try{
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
                                box.showAlertboxnegative("Report Added Successfully. Thank you");
                            }
                            else {

                                box.showAlertbox("Cannot Add this Report Successfully. Please try again Later");
                            }
                        }
                        else {

                            box.showAlertbox("Something went wrong . Please try again later .");
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                        box.showAlertbox("Cannot Add Visit Report. Please try again Later");
                    }
                }

                @Override
                public void onFailure(Call<VisitReportModel> call, Throwable t) {
                    box.showAlertbox("Something went wrong . Please try again later .");
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
            box.showAlertbox("OrderResult not Found. Please try again Later");
        }
    }



    private void getMobileNumber(List<GetCustomerDetailsResult> strings) {
        mobileNo.setAdapter(new ArrayAdapter<GetCustomerDetailsResult>(VisitReport.this, android.R.layout.simple_dropdown_item_1line,strings));
        mobileNo.setThreshold(1);
    }

    private class GeocodeAsyncTask extends AsyncTask<Double, Void, Address> {

        String errorMessage = "";
        @SuppressLint("LongLogTag")
        @Override
        protected Address doInBackground(Double... latlang) {
            Geocoder geocoder = new Geocoder(VisitReport.this, Locale.getDefault());
            List<Address> addresses = null;
            if (geocoder.isPresent()) {
                try {
                    addresses = geocoder.getFromLocation(latlang[0], latlang[1], 1);
                    Log.d(TAG, "doInBackground: ************");
                } catch (IOException ioException) {
                    errorMessage = "Service Not Available";
                    Log.e(TAG, errorMessage, ioException);
                } catch (IllegalArgumentException illegalArgumentException) {
                    errorMessage = "Invalid Latitude or Longitude Used";
                    Log.e(TAG, errorMessage + ". " +
                            "Latitude = " + latlang[0] + ", Longitude = " +
                            latlang[1], illegalArgumentException);
                }

                if (addresses != null && addresses.size() > 0)
                    return addresses.get(0);
            } else {
                new GetGeoCodeAPIAsynchTask().execute(latlang[0], latlang[1]);
            }

            return null;
        }

        @SuppressLint("LongLogTag")
        protected void onPostExecute(Address addresss) {
            if (addresss == null) {
                new GetGeoCodeAPIAsynchTask().execute(myLocation.getLatitude(), myLocation.getLongitude());
                Log.d(TAG, "onPostExecute: *****");
            } else {
//                progressBar.setVisibility(View.GONE);
                address = addresss.getAddressLine(0);
                String City = addresss.getLocality();
                Log.d(TAG, "onPostExecute: **************************" + City);
                String city = addresss.getLocality();
                String state = addresss.getAdminArea();
                //create your custom title
                String title = city + "-" + state;
                placeVisitedAddress.setText(address);
                Geocoder geocoder = new Geocoder(VisitReport.this);
                try {
                    ArrayList<Address> addresses = (ArrayList<Address>) geocoder.getFromLocationName("karur", 50);
                    for (Address address3 : addresses) {
                        double lat = address3.getLatitude();
                        double lon = address3.getLongitude();
//                        address2.setText(lat +
//                                "\n"
//                                + lon);

                    }
                } catch (IOException e) {
                    e.printStackTrace();

                }
                //create your custom title
//                String title = city + "-" + state;
//                Alertbox alertbox=new Alertbox(MainActivity.this);
//                alertbox.showAlertboxwithback("Your Current location is "+city);


            }
        }
    }

    private class GetGeoCodeAPIAsynchTask extends AsyncTask<Double, Void, String[]> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String[] doInBackground(Double... latlang) {
            String response;
            try {
                String URL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latlang[0] + "," + latlang[1] + "&key=" + APIKEY;
                Log.v("URL", URL);
                response = getLatLongByURL(URL);
                return new String[]{response};
            } catch (Exception e) {
                return new String[]{"error"};
            }
        }

        @SuppressLint("LongLogTag")
        @Override
        protected void onPostExecute(String... result) {
            try {
                JSONObject jsonObject = new JSONObject(result[0]);

                address = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(0).getString("long_name");


                String City = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(2).getString("long_name");

                String state = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(4).getString("long_name");

                Log.d(TAG, "onPostExecute: " + City);

                placeVisitedAddress.setText(address);

//                String title = city + "-" + state;

//                Alertbox alertbox=new Alertbox(MainActivity.this);
//                alertbox.showAlertbox("Your Current location is "+city);

//                if (fetchType == FROMADDRESS) {
//
//                    mPrimaryAddress.setText(address);
//                    mSecondaryAddress.setText(title);
//                    mDropText.requestFocus();
//                    fromaddress = address;
//                    mPickupText.setText(address);
//
//                    editor.putString("FromAddress", fromaddress);
//                    editor.apply();
//                    editor.commit();
//
//                    Log.i("FromAddress1", address);
//                    Log.i("FromAddress2", title);
//                } else {
//                    mPrimaryAddress.setText(address);
//                    mSecondaryAddress.setText(title);
//                    mDropText.setText(address);
//                    toaddress = address;
//                    editor.putString("ToAddress", toaddress).commit();
//
//                    Log.i("ToAddress1", address);
//                    Log.i("ToAddress2", title);
//
//                }


                Log.d("Address", "" + address);
            } catch (JSONException e) {
                e.printStackTrace();
            }
//            progressBar.setVisibility(View.GONE);

        }
    }

    public String getLatLongByURL(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    private void askIgnoreOptimization() {

//        PowerManager powerManager = (PowerManager) getApplicationContext().getSystemService(POWER_SERVICE);
//        String packageName = getPackageName();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            Intent i = new Intent();
//            if (!powerManager.isIgnoringBatteryOptimizations(packageName)) {
//                i.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
//                i.setData(Uri.parse("package:" + packageName));
//                startActivity(i);
//            } else if (manufacturer.equalsIgnoreCase("xiaomi") || manufacturer.equalsIgnoreCase("oppo") || manufacturer.equalsIgnoreCase("vivo") || manufacturer.equalsIgnoreCase("Letv") || manufacturer.equalsIgnoreCase("Honor")) {
//                if (autostart.equals("notstarted")) {
//                    getautostartalert("Please enable the autostart option to the Lucas App to use the fully");
//                }
//            }

//        if(Build.VERSION.SDK_INT>=23){
//            int permissionCheck= ContextCompat
//                    .checkSelfPermission(this, Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
//
//            if(permissionCheck == PackageManager.PERMISSION_DENIED){
//
//                //Should we show an explanation
//                if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS)){
//                    //Show an explanation
//                    final String message = "";
//                    Intent intent = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
//                     intent.setData(Uri.parse("package:" + getPackageName()));
//                     startActivityForResult(intent, IGNORE_BATTERY_OPTIMIZATION_REQUEST);
////                    Snackbar.make(coordinatorLayoutView,message,Snackbar.LENGTH_LONG)
////                            .setAction("GRANT", new View.OnClickListener() {
////                                @Override
////                                public void onClick(View v) {
////                                    ActivityCompat.requestPermissions(NewHomeLTVS.this, new String[]{ Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS }, IGNORE_BATTERY_OPTIMIZATION_REQUEST);
////                                }
////                            })
////                            .show();
//
//                }else{
//                    //No explanation need,we can request the permission
//                    ActivityCompat.requestPermissions(this, new String[]{ Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS }, IGNORE_BATTERY_OPTIMIZATION_REQUEST);
//                }
//            }
//        }
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
//            Intent intent = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
//            intent.setData(Uri.parse("package:" + getPackageName()));
//            startActivityForResult(intent, IGNORE_BATTERY_OPTIMIZATION_REQUEST);
//        }
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            String packageName = getPackageName();
//            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
//            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
//                try {
//                    //some device doesn't has activity to handle this intent
//                    //so add try catch
//                    Intent intent = new Intent();
//                    intent.setAction(android.provider.Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
//                    intent.setData(Uri.parse("package:" + packageName));
//                    startActivity(intent);
//                } catch (Exception e) {
//
//                }
//            }
//        }
//
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
            intent.setData(Uri.parse("package:" + getPackageName()));
            startActivity(intent);
            startActivityForResult(intent, IGNORE_BATTERY_OPTIMIZATION_REQUEST);
        }
    }


    private void getAutoStartforApp() {

        try {

            Intent intent = new Intent();

            if ("xiaomi".equalsIgnoreCase(manufacturer)) {

                intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));

            } else if ("oppo".equalsIgnoreCase(manufacturer)) {

                intent.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity"));

            } else if ("vivo".equalsIgnoreCase(manufacturer)) {

                intent.setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"));

            } else if ("Letv".equalsIgnoreCase(manufacturer)) {

                intent.setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity"));

            } else if ("Honor".equalsIgnoreCase(manufacturer)) {

                intent.setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity"));

            } else {


            }

            List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

            if (list.size() > 0) {

                startActivity(intent);

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    private void init() {
        if (getServicesAvailable()) {
            if (checkPermission()) {

//                if(googleApiClient==null)
                createGoogleApi();
//                if(mLocationRequest==null)
                createLocationRequest();
                if (lastLocation == null)
                    lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

                enableGPS();


            } else {
                initPermission();
            }

//                getLastKnownLocation();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        progress.show();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
        }
        if (lastLocation == null)
            lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
//        createLocationRequest();
        if (isGPSEnabled()) {
            if (lastLocation != null) {
                Log.i(TAG, "LasKnown location. " +
                        "Long: " + lastLocation.getLongitude() +
                        " | Lat: " + lastLocation.getLatitude());
//                writeLastLocation();
//                createLocationRequest();//no need location gps
                setFusedLocationUpdate();
//                        animateCameraInMap(new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude()));
            }
//            else {
//                Log.w(TAG, "No location retrieved yet");
//                if (mLocationRequest != null) {
//                    createLocationRequest();
//                    setFusedLocationUpdate();
//                }
//            }

//            if(isGPSEnabled())
////                if (!geofenceStatus)
//                {
//                    startGeofence();
//                    geofenceStatus = true;
//                }
//        }

        }

    }


    @Override
    protected void onStart() {
        super.onStart();

    }


    @Override
    protected void onResume() {
        super.onResume();
//        getLastKnownLocation();
//        initPermission();

//        resetPreferredLauncherAndOpenChooser(AttendanceActivitySales.this);

        // Call GoogleApiClient connection when starting the Activity
        if (googleApiClient != null) {
//            if(!googleApiClient.isConnected())
            googleApiClient.connect();
//            else {
//                googleApiClient.reconnect();
//            }

//            if (googleApiClient.isConnected()) {
//                if (checkPermission())
//                    LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, mLocationRequest, new LocationListener() {
//                        @Override
//                        public void onLocationChanged(Location location) {
//                            animateCameraInMap(new LatLng(location.getLatitude(), location.getLongitude()));
//                        }
//                    });
//            }
        }

//        if (getIntent().getStringExtra("Geofence") != null) {
//            if (getIntent().getStringExtra("Geofence").equals(geofenceEnter)) {
//                if (!isMyLauncherDefault()) {
////                resetPreferredLauncherAndOpenChooser(AttendanceActivitySales.this);
////                clearActionMain();
//
//                    final AlertDialog alertDialog=box.customView();
//                    View dialogView= LayoutInflater.from(AttendanceActivitySales.this).inflate(R.layout.demo_launcher_screen,null);
//                    TextView msg = (TextView) dialogView.findViewById(R.id.txt_msg);
//                    Button okay = (Button) dialogView.findViewById(R.id.okay);
//                    Button exit = (Button) dialogView.findViewById(R.id.exit);
//                    exit.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            alertDialog.dismiss();
//                            finish();
//                        }
//                    });
//                    msg.setText(getString(R.string.launcher_details));
//                    okay.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            alertDialog.dismiss();
//                            startActivity(new Intent(Settings.ACTION_HOME_SETTINGS));
//                        }
//                    });
//
//
//                } else {
////                new CheckRunningActivity(KioskActivity.this).start();
//
//                }
//            } else {
//                finish();
//                startActivity(new Intent(Settings.ACTION_HOME_SETTINGS));
//            }
//        }
    }



    @Override
    protected void onPause() {
        super.onPause();
        if (googleApiClient != null)
            if (googleApiClient.isConnected())
                googleApiClient.disconnect();

//        ActivityManager activityManager = (ActivityManager) getApplicationContext()
//                .getSystemService(Context.ACTIVITY_SERVICE);
//
//        activityManager.moveTaskToFront(getTaskId(), ActivityManager.MOVE_TASK_WITH_HOME);


//        List<ActivityManager.AppTask> tasks= activityManager.getAppTasks();
//        Log.d(TAG, "onPause: "+tasks);

    }

    public boolean getServicesAvailable() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (api.isUserResolvableError(isAvailable)) {

            Dialog dialog = api.getErrorDialog(this, isAvailable, 0);
            dialog.show();
            return false;
        } else {
            Toast.makeText(this, "Cannot Connect To Play Services", Toast.LENGTH_SHORT).show();
            return false;
        }

    }

    private void initPermission() {
        if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(VisitReport.this, locationAccess[0]) ||
                PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(VisitReport.this, locationAccess[1]) ||
                PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(VisitReport.this, locationAccess[2]) ||
                PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(VisitReport.this, locationAccess[3])
        ) {
            ActivityCompat.requestPermissions(VisitReport.this, locationAccess, REQUEST_CHECK_SETTINGS);
        } else {
            permissionGranted = true;
            init();
//            setMDM();
//            startLocationUpdate();
        }
    }



    private boolean isGPSEnabled() {
//        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        return gps_enabled;
    }

    private void enableGPS() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
//                        setMDM();
                        setFusedLocationUpdate();
//                        if (!geofenceStatus) {
//                            startGeofence();
//                            geofenceStatus = true;
//                        }
//                        drawGeofence();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(
                                    VisitReport.this,
                                    REQUEST_LOCATION_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            Log.e(TAG, "Exception : " + e.getMessage());
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.e(TAG, "Location settings are not satisfied.");
                        break;
                }
            }
        });
    }

    private synchronized void createGoogleApi() {
        Log.d(TAG, "createGoogleApi()");
        {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(VisitReport.this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();

        }
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(100);
        mLocationRequest.setFastestInterval(100);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(10);

    }


    @Override
    public void onRequestPermissionsResult(final int requestCode, final String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            int len = permissions.length;
            int permissionIndex=-1;
            for (int i = 0; i < len; i++) {
                if (ActivityCompat.checkSelfPermission(VisitReport.this, permissions[i]) != PackageManager.PERMISSION_GRANTED) {
                    permissionGranted = false;
                    permissionIndex=i;
                }
            }

            if (permissionGranted) {
                permissionGranted = true;

//                setMDM();
                init();
//                if(isLocationSuccess)
                //                {
                //                    startLocationUpdate();
                //                }
            } else {
                if(permissionIndex==0 || permissionIndex==1)
                    invokeAlert("You did not give permission to access your Location. Do want to exit", permissions, requestCode);
                else invokeAlert("You did not give permission to access your Storage. Do want to exit", permissions, requestCode);
            }
        }

    }

    private void requestManualPermission(String[] permissions, int requestCode) {
        ActivityCompat.requestPermissions(this, permissions, requestCode);
    }

    // Check for permission to access Location
    private boolean checkPermission() {
        Log.d(TAG, "checkPermission()");
        // Ask for permission if it wasn't granted yet
        return (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged: ");

        if(progress!=null)
            if(progress.isShowing())
                progress.dismiss();
        // check_button.setVisibility(View.VISIBLE);
        if (location != null) {
            myLocation.setLatitude(location.getLatitude());
            myLocation.setLongitude(location.getLongitude());
            editor.putString("latitude", myLocation.getLatitude()+"");
            editor.putString("longitude", myLocation.getLongitude()+"");
            Log.d(TAG, "onLocationChanged: " + myLocation.getLatitude() + " long " + myLocation.getLongitude());
            editor.commit();
            new GeocodeAsyncTask().execute(myLocation.getLatitude(), myLocation.getLongitude());
            //Or Do whatever you want with your location
        } else if (myLocation != null) {
            editor.putString("latitude", myLocation.getLatitude()+"");
            editor.putString("longitude", myLocation.getLongitude()+"");
            Log.d(TAG, "onLocationChanged: " + myLocation.getLatitude() + " long " + myLocation.getLongitude());
            editor.commit();
            new GeocodeAsyncTask().execute(myLocation.getLatitude(), myLocation.getLongitude());
        }

//        animateCameraInMap(new LatLng(location.getLatitude(), location.getLongitude()));

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }


    private void setFusedLocationUpdate() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
        }
        locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER,
                TWO_MINUTES,
                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
        if (googleApiClient.isConnected())
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void invokeAlert(String msg, final String[] permissions, final int requestCode) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(VisitReport.this);
        dialog.setTitle("Location");
        dialog.setMessage(msg);
        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                requestManualPermission(permissions, requestCode);
                permissionGranted = true;
            }
        });
        dialog.show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: ");
        if (googleApiClient != null)
            if (googleApiClient.isConnected()) {
                googleApiClient.disconnect();
            }
//        enableLauncher();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_LOCATION_SETTINGS) {
            if (resultCode == RESULT_LOCATION_CODE_CANCELLED) {

                Alert alert=new Alert(VisitReport.this);
                alert.showAlertBoxWithListener("You cannot use this Application without turning ON your GPS",View.VISIBLE);
                alert.setOnPositiveClickListener(new Alert.onPositiveClickListener() {
                    @Override
                    public void onPositiveClick() {
                        enableGPS();
                    }
                });
                alert.setOnNegativeClickListener(new Alert.onNegativeClickListener() {
                    @Override
                    public void onPositiveClick() {
                        finish();
                    }
                });

//                AlertDialog.Builder builder = box.showAlertBox("You cannot use this Application without turning ON your GPS location");
//                builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
////                        askIgnoreOptimization();
//                        enableGPS();
//                    }
//                });
//
//                builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        finish();
//                    }
//                });
//                builder.show();
            } else {
//                setMDM();
                setFusedLocationUpdate();
//                if (!geofenceStatus) {
//                    startGeofence();
//                    geofenceStatus = true;
//                }


//                if(!isAdminActive())
//                     ActivateAdmin();
//                else {
////                    startContextService();
//                }
            }
        }

    }
























    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: ");


//        enableLauncher();



    }
}
