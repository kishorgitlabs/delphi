package com.brainmagic.delphitvs.service;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.StrictMode;

import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import com.brainmagic.delphitvs.R;
import com.brainmagic.delphitvs.regionalmanager.AttendanceActiveRegional;
import com.google.android.gms.location.LocationRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.net.ssl.HttpsURLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import model.api.APIService;
import model.api.RetroClient;
import model.regional.salesnamelist.ResponseRegionaTravel;
import model.sales.travelhistory.travelwithhistoryresult.UploadCoordinatesRegional;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import room.AppDatabase;
public class BGServicenormalRegional extends Service {

    public static final String APIKEY = "AIzaSyB9kdMz4eGNnXSMSMQ0cGLG7tHq6bNLr18";
    private static final String TAG = "BGServicenormal";
    private static final int TWENTY_MINUTES = 0* 60 * 1000;
    static final int NOTIFICATION_ID = 543;
    private boolean isServiceRunning = false;
    public LocationManager locationManager;
    public MyLocationListener listener;
    private Location mLastLocation, mCurrentLocation;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private String address, city, state, description, locate, emaildi, phonenumber, formatdate, formatedtime,regid;
    int ntwCount=0;
    private double mLatitude;
    private double mLongtitude;
    long trackId;
    String name = "noName",empId;
    private float distance=0f;
    public static final String NOTIFICATION_CHANNEL_ID = "10001";
    private Date previousTime=null;
    private int onLocationCount=0;
    private AppDatabase appDatabase;

    private boolean isConnected()
    {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        //boolean isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
        return isConnected;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            startForeground(12345678, startNotification());
            Log.wtf(TAG, "onCreate: Rm" + isServiceRunning);
           // startServiceWithNotification();
            getLocationDetails();
            appDatabase=AppDatabase.getAppDatabase(this);
            myshare = getSharedPreferences("Delphi", MODE_PRIVATE);
            editor = myshare.edit();
            name = myshare.getString("empName", "");
             //  empid=myshare.getString("sid","");
            empId = myshare.getString("empId", "");
            regid = myshare.getString("empId", "");
            mLatitude = Double.parseDouble(myshare.getString("latitude", ""));
            mLongtitude = Double.parseDouble(myshare.getString("longitude", ""));
            trackId = myshare.getLong("trackId", 0);
            mLastLocation = new Location("");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    Notification startNotification()
    {
        Notification notification=null;
        try {
            Log.d(TAG, "startServiceWithNotification: first");
            Intent notificationIntent = new Intent(this, AttendanceActiveRegional.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
            Bitmap icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
            Log.d(TAG, "startServiceWithNotification: second");
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                    .setContentTitle(getResources().getString(R.string.app_name))
                    .setTicker(getResources().getString(R.string.app_name))
                    .setSmallIcon(R.drawable.route)
                    .setContentText("Travel in Progress")
//                    .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                    .setContentIntent(pendingIntent)
                    .setOngoing(true);

            Log.d(TAG, "startServiceWithNotification: third");
            notification = builder.build();
            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notification.flags = notification.flags | Notification.FLAG_NO_CLEAR;// NO_CLEAR makes the notification stay when the user performs a "delete all" command
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, getResources().getString(R.string.app_name), NotificationManager.IMPORTANCE_DEFAULT);
                channel.setDescription("Travel in Progress");
                builder.setChannelId(NOTIFICATION_CHANNEL_ID);
                Log.d(TAG, "startServiceWithNotification: four");
//                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.createNotificationChannel(channel);
            }

            Log.d(TAG, "startServiceWithNotification: five");

            startForeground(1, notification);

            Log.d(TAG, "startServiceWithNotification: six");
        }catch (Exception e)
        {
            e.printStackTrace();
            Log.d(TAG, "startServiceWithNotification: error");
        }
        return notification;
    }
    public void getLocationDetails() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        listener = new MyLocationListener();
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, TWENTY_MINUTES, 0, listener);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, TWENTY_MINUTES, 0, listener);

            Log.d(TAG, "onStartCommand: id*********");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    @Override
    public void onTaskRemoved(final Intent rootIntent) {
//        startActivity(getPopupIntent());

    }

    @Override
    public void onDestroy() {
        try {
            //date
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            final String date = df.format(c.getTime());

            Calendar cal = Calendar.getInstance();
            final Date timeFormat = cal.getTime();
            SimpleDateFormat simpleDateFormats = new SimpleDateFormat("HH:mm a");
            String attendanceTime =simpleDateFormats.format(timeFormat);

            if(previousTime==null)
            {
                Date time = cal.getTime();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm aa");
                previousTime =simpleDateFormat.parse(simpleDateFormat.format(time));
                Log.d(TAG, "onDestroy: lastTime "+previousTime.getTime());

//            if (CheckDeviceIsMoved(loc, mLastLocation))
                Log.d(TAG, "onDestroy: lat 1 " + mLatitude + " n lon 1 " + mLongtitude);
                if(isConnected()) {
                    getDistance((mLastLocation.getLatitude()), mLastLocation.getLongitude(), mLatitude, mLatitude);
                    new GeocodeAsyncTask().execute(mLatitude, mLongtitude);
                }

            }
            else if(diffTime(previousTime)>10)
            {
                Date time = cal.getTime();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm aa");
                previousTime =simpleDateFormat.parse (simpleDateFormat.format(time));
                Log.d(TAG, "onDestroy: lastTime "+previousTime.getTime());
                Log.d(TAG, "onDestroy: lat 1 " + mLatitude + " n lon 1 " + mLatitude);
                if(isConnected()) {
                    getDistance((mLastLocation.getLatitude()), mLastLocation.getLongitude(), mLatitude, mLatitude);
                    new GeocodeAsyncTask().execute(mLatitude, mLongtitude);
                }
            }
            else {
                Log.d(TAG, "onDestroy: time diff "+diffTime(previousTime));
                if(onLocationCount>=3)
                {
                    if(diffTime(previousTime)>5) {
                        onLocationCount = 0;
                        Date time = cal.getTime();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm aa");
                        previousTime = simpleDateFormat.parse(simpleDateFormat.format(time));
                        if (isConnected()) {
                            getDistance((mLastLocation.getLatitude()), mLastLocation.getLongitude(), mLatitude, mLongtitude);
                            new GeocodeAsyncTask().execute(mLatitude, mLongtitude);
                        }
                    }
                }
            }
            Log.d(TAG, "onDestroyBGService: ");
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        stopForeground(true);
        super.onDestroy();
//        stopMyService();
    }
    void stopMyService() {
        stopForeground(true);
        stopSelf();
        isServiceRunning = false;
    }
    private void setTravelCoordinates(double latitude, double longitude)
    {
        try {

            //date
            Calendar c = Calendar.getInstance();
//            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            final String date = df.format(c.getTime());

            Calendar cal = Calendar.getInstance();
            final Date timeFormat = cal.getTime();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
            String attendanceTime =simpleDateFormat.format(timeFormat);

            APIService service = RetroClient.getApiService();
            Call<ResponseRegionaTravel> coordinatesCall = service.setTravelCoordinate(Integer.parseInt(empId), name, trackId, attendanceTime,
                    date, latitude, longitude, address, distance,regid);
            coordinatesCall.enqueue(new Callback<ResponseRegionaTravel>() {
                @Override
                public void onResponse(Call<ResponseRegionaTravel> call, Response<ResponseRegionaTravel> response) {
                    try{
                        if(response.isSuccessful())
                        {
                            distance=0f;
                            onLocationCount=0;
                        }
                        else {
                            distance=0f;
                            UploadCoordinatesRegional dataList=new UploadCoordinatesRegional();
                            dataList.setEmpId(Long.valueOf(empId));
                            dataList.setRegid(empId);
                            dataList.setEmpName(name);
                            dataList.setTrackId(Long.valueOf(trackId));
                            dataList.setDatetime(date);
                            dataList.setTimedate(String.valueOf(timeFormat));
                            dataList.setLatitude(mLatitude);
                            dataList.setLangtitude(mLongtitude);
                            dataList.setDistance(Double.valueOf(distance));
                            dataList.setAddress(address);
                            appDatabase.ProductsDAO().insertAttendances(dataList);
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
                @Override
                public void onFailure(Call<ResponseRegionaTravel> call, Throwable t) {

                }
            });
        }catch (Exception e)
        {
            Log.d(TAG, "setTravelCoordinates: "+e);
            e.printStackTrace();
        }
    }

    private boolean CheckDeviceIsMoved(Location current, Location last) {
        try {
            double dist = current.distanceTo(last) / 1000;
//            Log.v("distance Calculate ", Double.toString(dist));

            NumberFormat df = DecimalFormat.getInstance();
            df.setMaximumFractionDigits(3);
            df.setGroupingUsed(false);

            String distanc = df.format(dist).toString().replaceAll(",", ".");
            dist = Double.parseDouble(distanc);

            if (dist <= 0.900) { //(in km, you can use 0.1 for metres etc.)
                //If it's within 1km, we assume we're not moving
//                Log.v("Device is = ", "Not  Moved");
//                return false;
                return true;
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    public long diffTime(Date lastTime) {
        long min = 0;
        long differences,difference,minutes=0 ;
        Log.d(TAG, "onLocationChanged diffTime: one");
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm aa"); // for 12-hour system, hh should be used instead of HH
//            // There is no minute different between the two, only 8 hours difference. We are not considering Date, So minute will always remain 0
//            Date date1 = simpleDateFormat.parse("09:40 AM");
//            Date date2 = simpleDateFormat.parse("10:20 AM");
            Calendar cal = Calendar.getInstance();
            Date time = cal.getTime();
            Log.d(TAG, "onLocationChanged diffTime: format "+simpleDateFormat.format(time));
            Date currentTime = simpleDateFormat.parse(simpleDateFormat.format(time));

            Log.d(TAG, "onLocationChanged diffTime: two");

            differences = (currentTime.getTime() - lastTime.getTime()) / 1000;
            Log.d(TAG, "diffTime: difs "+differences);
//            difference = (currentTime - lastTime) / 1000;
//            Log.d(TAG, "diffTime: diff "+difference);
//            long hours = difference % (24 * 3600) / 3600; // Calculating Hours
            minutes = differences % 3600 / 60; // Calculating minutes if there is any minutes difference
//            long minute = difference % 3600 / 60; // Calculating minutes if there is any minutes difference
//            min = minute + (hours * 60); // This will be our final minutes. Multiplying by 60 as 1 hour contains 60 mins
            Log.d(TAG, "minutes: "+minutes);
//            Log.d(TAG, "minute: "+minute);
            Log.d(TAG, "min: "+min);
        } catch (Throwable e) {
            Log.d(TAG, " error diffTime: "+e.getMessage());
            e.printStackTrace();
        }
        return minutes;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand: ");
        try {
            String regid = myshare.getString("regid", "");
            if(regid!=null) {
                startForeground(12345678,startNotification());
            }
            else
                stopMyService();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return START_NOT_STICKY ;
    }

    public String getDistance(double lat1, double lon1, double lat2, double lon2) {
        distance = 0f;
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        String url = "https://maps.googleapis.com/maps/api/directions/xml?origin=" + lat1 + "," + lon1 + "&destination=" + lat2 + "," + lon2 + "&key=" + APIKEY + "";

        String tag[] = {"value"};  //will give distance as string e.g 1.2 km
        // or tag[] = {"value"} if you want to get distance in metre e.g. 1234
        Log.i("URL display == ", url);
        String address[] = {"end_address"};
        HttpResponse response = null;
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();
            HttpPost httpPost = new HttpPost(url);
            response = httpClient.execute(httpPost, localContext);
            InputStream is = response.getEntity().getContent();
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(is);

            if (doc != null) {
                NodeList nl, n2;
                ArrayList args = new ArrayList();
                ArrayList args1 = new ArrayList();
                for (String s : tag) {
                    nl = doc.getElementsByTagName(s);
                    if (nl.getLength() > 0) {
                        Node node = nl.item(nl.getLength() - 1);
                        args.add(node.getTextContent());
                    } else {
                        args.add("0");
                    }
                    for (String s1 : address) {
                        n2 = doc.getElementsByTagName(s1);
                        if (n2.getLength() > 0) {
                            Node node = n2.item(n2.getLength() - 1);
                            args1.add(node.getTextContent());
                        } else {
                            args1.add("no address");
                        }
                    }
                }
                String dis = String.format("%s", args.get(0));
                distance = Float.parseFloat(dis) / 1000;
                Log.d(TAG, "getDistance: Distance "+distance);

            } else {
                System.out.print("Doc is null");
                Log.d(TAG, "getDistance: Doc is null ");

                if (lat1 != 0.0) {
                    Location locationA = new Location("point A");
                    locationA.setLatitude(lat1);
                    locationA.setLongitude(lon1);
                    Location locationB = new Location("point B");
                    locationB.setLatitude(lat2);
                    locationB.setLongitude(lon2);
                    distance = locationA.distanceTo(locationB) / 1000;// in km
                    Log.d(TAG, "getDistance: small distance "+distance);
                }
            }
            if (distance == 0) {
                Log.d(TAG, "getDistance: inside distance is zero");
                if (lat1 != 0.0) {
                    Log.d(TAG, "getDistance: inside lat not one");
                    Location locationA = new Location("point A");
                    locationA.setLatitude(lat1);
                    locationA.setLongitude(lon1);
                    Location locationB = new Location("point B");
                    locationB.setLatitude(lat2);
                    locationB.setLongitude(lon2);
                    distance = locationA.distanceTo(locationB) / 1000;
                }
            }
        } catch (Exception e) {
            Log.d(TAG, "getDistance: inside exception in getDistance");
            e.printStackTrace();
        }
        try {
            DecimalFormat df = new DecimalFormat();
            df.setMaximumFractionDigits(3);
            Log.d("distance from google **", Float.toString(distance));
            distance = Float.parseFloat(df.format(distance));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return "";
    }
    private class GeocodeAsyncTask extends AsyncTask<Double, Void, Address> {
        String errorMessage = "";
        double lat=0, lon=0;
        @Override
        protected Address doInBackground(Double... latlang) {
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            lat=latlang[0];
            lon=latlang[1];
            List<Address> addresses = null;
            if (geocoder.isPresent()) {
                try {
                    addresses = geocoder.getFromLocation(latlang[0], latlang[1], 1);
//                    Log.d(TAG, "doInBackground: ************");
                } catch (IOException ioException) {
                    errorMessage = "Service Not Available";
//                    Log.e(TAG, errorMessage, ioException);
                } catch (IllegalArgumentException illegalArgumentException) {
                    errorMessage = "Invalid Latitude or Longitude Used";
                }
                if (addresses != null && addresses.size() > 0)
                    return addresses.get(0);
            } else {
                new GetGeoCodeAPIAsynchTask().execute(latlang[0], latlang[1]);
            }
            return null;
        }
        protected void onPostExecute(Address addresss) {
            if (addresss == null) {
                new GetGeoCodeAPIAsynchTask().execute(lat, lon);
            } else {
                address = addresss.getAddressLine(0); //0 to obtain first possible address
                city = addresss.getLocality();
                Log.d(TAG, "onPostExecute: **************************"+city);
                state = addresss.getAdminArea();
                setTravelCoordinates(lat,lon);
            }
        }
    }
    private class GetGeoCodeAPIAsynchTask extends AsyncTask<Double, Void, String[]> {
        double lat,lon;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected String[] doInBackground(Double... latlang) {
            lat=latlang[0];
            lon=latlang[1];
            String response;
            try {
                String URL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latlang[0] + "," + latlang[1] + "&key=" + APIKEY;
                response = getLatLongByURL(URL);
                return new String[]{response};
            } catch (Exception e) {
                return new String[]{"error"};
            }
        }
        @Override
        protected void onPostExecute(String... result) {
            try {
                JSONObject jsonObject = new JSONObject(result[0]);
                address = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(0).getString("long_name");
                setTravelCoordinates(lat,lon);
                Log.d(TAG, "onPostExecute: "+city);
            } catch (JSONException e) {
                e.printStackTrace();
                if(TextUtils.isEmpty(address)||address.equals(null))
                {
                    address="Address Not found";
                }
            }
        }
        public String getLatLongByURL(String requestURL) {
            URL url;
            String response = "";
            try {
                url = new URL(requestURL);

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");
                conn.setDoOutput(true);
                int responseCode = conn.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        response += line;
                    }
                } else {
                    response = "";
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
    public String getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return "Wifi";

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return "Mobile";
        }
        return "Not Connected";
    }

    public class MyLocationListener implements android.location.LocationListener {

        public void onLocationChanged(final Location loc) {
            try {
                Log.d(TAG, "Location changed");
                //date
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                final String date = df.format(c.getTime());

                Calendar cal = Calendar.getInstance();
                final Date timeFormat = cal.getTime();
                SimpleDateFormat simpleDateFormats = new SimpleDateFormat("HH:mm a");
                String attendanceTime =simpleDateFormats.format(timeFormat);

                Log.d(TAG, "onLocationChanged: "+mLatitude);
                mLastLocation.setLatitude(mLatitude);
                mLastLocation.setLongitude(mLongtitude);

                //to find how many time it calls onLocationListener
                onLocationCount++;
                Log.d(TAG, "onLocationChanged: preTime " + (previousTime));
                if(previousTime==null)
                {
                    Date time = cal.getTime();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm aa");
                    previousTime =simpleDateFormat.parse (simpleDateFormat.format(time));
                    Log.d(TAG, "onLocationChanged: previousTime=null ");

                    onLocationCount=0;
//            if (CheckDeviceIsMoved(loc, mLastLocation))
                    mLatitude = loc.getLatitude();
                    mLongtitude = loc.getLongitude();
                    Log.d(TAG, "onLocationChanged: lat 1 " + loc.getLatitude() + " n lon 1 " + loc.getLongitude());

                    if (isConnected()) {
                        getDistance((mLastLocation.getLatitude()), mLastLocation.getLongitude(), loc.getLatitude(), loc.getLongitude());
                        new GeocodeAsyncTask().execute(loc.getLatitude(), loc.getLongitude());

                    }else {
                        ntwCount++;
                        distance=0f;
                        UploadCoordinatesRegional dataList=new UploadCoordinatesRegional();
                        dataList.setEmpId(Long.valueOf(empId));
                        dataList.setRegid(empId);
                        dataList.setEmpName(name);
                        dataList.setTrackId(Long.valueOf(trackId));
                        dataList.setDatetime(date);
                        dataList.setTimedate(String.valueOf(attendanceTime));
                        dataList.setLatitude(mLatitude);
                        dataList.setLangtitude(mLongtitude);
                        dataList.setDistance(Double.valueOf(distance));
                        dataList.setAddress(address);
                        appDatabase.ProductsDAO().insertAttendances(dataList);
                    }

                }
                else if(diffTime(previousTime)>10)
                {
                    onLocationCount=0;
                    Log.d(TAG, "onLocationChanged: " + (previousTime.getTime()));
                    Log.d(TAG, "onLocationChanged: diff " + (diffTime(previousTime)));
                    Date time = cal.getTime();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm aa");
                    previousTime =simpleDateFormat.parse (simpleDateFormat.format(time));
                    Log.d(TAG, "onLocationChanged: lastTime "+previousTime.getTime());
                    mLatitude = loc.getLatitude();
                    mLongtitude = loc.getLongitude();
                    Log.d(TAG, "onLocationChanged: lat 1 " + loc.getLatitude() + " n lon 1 " + loc.getLongitude());
                    if (isConnected()) {
                        getDistance((mLastLocation.getLatitude()), mLastLocation.getLongitude(), loc.getLatitude(), loc.getLongitude());
                        new GeocodeAsyncTask().execute(loc.getLatitude(), loc.getLongitude());

                    }else {
                        ntwCount++;
                        distance=0f;
                        UploadCoordinatesRegional dataList=new UploadCoordinatesRegional();
                        dataList.setEmpId(Long.valueOf(empId));
                        dataList.setRegid(empId);
                        dataList.setEmpName(name);
                        dataList.setTrackId(Long.valueOf(trackId));
                        dataList.setDatetime(date);
                        dataList.setTimedate(String.valueOf(attendanceTime));
                        dataList.setLatitude(mLatitude);
                        dataList.setLangtitude(mLongtitude);
                        dataList.setDistance(Double.valueOf(distance));
                        dataList.setAddress(address);
                        appDatabase.ProductsDAO().insertAttendances(dataList);
                    }
                }
                else {
                    Log.d(TAG, "onLocationChanged: time diff "+diffTime(previousTime));
                    Log.d(TAG, "onLocationChanged: onLocationCount "+onLocationCount);

                    if(onLocationCount>=3)
                    {
                        if(diffTime(previousTime)>5) {
                            onLocationCount = 0;
                            Date time = cal.getTime();
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm aa");
                            previousTime = simpleDateFormat.parse(simpleDateFormat.format(time));
                            if (isConnected()) {
                                getDistance((mLastLocation.getLatitude()), mLastLocation.getLongitude(), loc.getLatitude(), loc.getLongitude());
                                new GeocodeAsyncTask().execute(loc.getLatitude(), loc.getLongitude());

                            } else {
                                ntwCount++;
                                distance = 0f;
                                UploadCoordinatesRegional dataList = new UploadCoordinatesRegional();
                                dataList.setEmpId(Long.valueOf(empId));
                                dataList.setRegid(empId);
                                dataList.setEmpName(name);
                                dataList.setTrackId(Long.valueOf(trackId));
                                dataList.setDatetime(date);
                                dataList.setTimedate(String.valueOf(attendanceTime));
                                dataList.setLatitude(mLatitude);
                                dataList.setLangtitude(mLongtitude);
                                dataList.setDistance(Double.valueOf(distance));
                                dataList.setAddress(address);
                                appDatabase.ProductsDAO().insertAttendances(dataList);
                            }
                        }

                    }
                }
            }catch (Exception e)
            {
                e.printStackTrace();
                Log.d(TAG, "onLocationChanged: "+e.getCause());
                Log.d(TAG, "onLocationChanged: "+e.getMessage());
                Log.d(TAG, "onLocationChanged: "+e);
            }
        }
        public void  onProviderDisabled(String provider) {
            // Toast.makeText(getApplicationContext(), "Gps Disabled", Toast.LENGTH_SHORT).show();
        }

        public void onProviderEnabled(String provider) {
            // Toast.makeText(getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT).show();
        }
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }
    }
    public class AddItems extends AsyncTask<Double, Void, String>
    {
        private String datetime,updateddate,timedate,latitude,longitude;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(TAG, "onPreExecute: Additems");
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            datetime = df.format(c.getTime());
            SimpleDateFormat mdformat = new SimpleDateFormat("h:mm a");
            timedate = mdformat.format(c.getTime());
//        distance="0";
            updateddate="";
        }
        @Override
        protected String doInBackground(Double... doubles) {

            return "success";
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s.equals("success"))
            {
                Log.d(TAG, "onPostExecute: success");
                distance=0f;
                onLocationCount=0;
            }

        }
    }
}
