package com.brainmagic.delphitvs.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.widget.Toast;

public class NetworkCheckReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if(LocationManager.PROVIDERS_CHANGED_ACTION.matches(intent.getAction()))
        {
            Toast.makeText(context, "Started", Toast.LENGTH_SHORT).show();
        }

    }
}
