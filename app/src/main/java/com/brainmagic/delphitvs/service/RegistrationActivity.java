package com.brainmagic.delphitvs.service;

import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;


import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.delphitvs.MainActivity;
import com.brainmagic.delphitvs.R;
import com.goodiebag.pinview.Pinview;
import com.google.firebase.FirebaseApp;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import model.network.dealer.dealerstatecity.DealerModel;
import model.registeration.Registration;
import model.registeration.RegistrationPostValues;
import model.registeration.ShopType;
import model.registeration.checkregistration.CheckRegistration;
import model.resendotp.ResendOtpStatus;
import model.validateotp.ValidatedMobileOtp;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import spinner.MultiSelectionSpinner;
import alert.Alert;
import fcm.MyFirebaseInstanceId;
import model.api.APIService;
import model.api.RetroClient;
import model.registeration.RegistrationData;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toaster.Toasts;

import static android.content.ContentValues.TAG;

public class RegistrationActivity extends AppCompatActivity {
    private EditText editText_name, editText_email, editText_phone, editText_city, editText_shopname, editText_address, editShopType, editPincode, editSubAddress;
    private MaterialSpinner spinner_state, spinner_usertype;
    private Button btn_register;
    private String[] states = {"Select State", "Andhra Pradesh", "Assam", "Bihar", "Chhattisgarh", "Delhi", "Goa", "Gujarat", "Haryana", "Himachal Pradesh",
            "Jammu & Kashmir", "Jharkhand", "Karnataka", "Kerala", "Lakshadweep", "Madhya Pradesh", "Maharashtra", "Manipur", "Megalaya", "Mizoram", "Nagaland",
            "Nepal", "New Delhi", "Odisha", "Puducherry", "Punjab", "Rajasthan", "Sikkim", "Tamil Nadu", "Telangana", "Tripura", "Uttar Pradesh", "Uttarakhand",
            "Visakhapatnam", "West Bengal"};
    private String[] types = {"Select Usertype", "DTVS Executive", "Dist Executive", "ASD", "ASP", "NWS", "Retailer", "Whole seller"};
    private String S_username, S_password, App_ID, Device_ID;
    Long Reg_ID;
    //    private ArrayAdapter<CharSequence> adapter;
    private ArrayAdapter<String> adapter;
    private View shopTypeView;
    private Alert alert = new Alert(this);
    private Toasts toasts = new Toasts(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private NetworkConnection networkConnection = new NetworkConnection(this);
    private static RegistrationActivity instance;
    private LinearLayout shopNameLayout, shopTypeLayout;
    private String imei1, Imei2,Msg,otpStr,strrr;
    private String str_name, str_email, str_phone, str_state, str_city, str_usertype, str_address, str_sub_address, str_shopname, strPincode;
    private Context context;
    private MultiSelectionSpinner multiSelectionSpinner;
    private List<ShopType> shopTypeLists;
    private AlertDialog otpalertbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        instance = this;
        super.onCreate(savedInstanceState);

        setContentView(R.layout.registration);

        FirebaseApp.initializeApp(this);
        myshare = getSharedPreferences("Delphi", MODE_PRIVATE);
        editor = myshare.edit();
        //findviewbyid

        final List<String> shopTypeList = new ArrayList<>();
        shopTypeList.add("3W");
        shopTypeList.add("Car");
        shopTypeList.add("LCV");
        shopTypeList.add("HCV");
        shopTypeList.add("Tractor");
        shopTypeList.add("Van");
        shopTypeList.add("ASD");
        shopTypeList.add("ASP");
        shopTypeList.add("LAD/Whole seller");
//        shopTypeList.add("Retailer");
        multiSelectionSpinner = findViewById(R.id.shop_type_spinner);
        editPincode = (EditText) findViewById(R.id.pincode);
        editShopType = (EditText) findViewById(R.id.shop_type);
        editText_name = (EditText) findViewById(R.id.nameedit);
        editText_email = (EditText) findViewById(R.id.emailedit);
        editText_phone = (EditText) findViewById(R.id.phoneedit);
        editText_city = (EditText) findViewById(R.id.cityedit);
        editText_shopname = (EditText) findViewById(R.id.shopnameedit);
        editSubAddress = (EditText) findViewById(R.id.sub_address);
        editText_address = (EditText) findViewById(R.id.addressedit);
        spinner_state = (MaterialSpinner) findViewById(R.id.stateedit);
        spinner_usertype = (MaterialSpinner) findViewById(R.id.usertypeedit);
        btn_register = (Button) findViewById(R.id.btn_register);
        shopNameLayout = findViewById(R.id.shop_name_layout);
        shopTypeLayout = findViewById(R.id.shop_type_layout);
        shopTypeView = findViewById(R.id.shop_type_view);

//        adapter = ArrayAdapter.createFromResource(this, R.array.state_array, R.layout.support_simple_spinner_dropdown_item);
        //  adapter = new ArrayAdapter<String>(RegistrationActivity.this,R.layout.simple_spinner_item,R.array.state_array);
        //adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        editText_phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String mobileNo = s.toString();
                if (mobileNo.length() == 10) {
                    NetworkConnection connection = new NetworkConnection(RegistrationActivity.this);
                    if (connection.CheckInternet()) {
                        getDataByMobileNo(mobileNo);
                    }
                } else if (mobileNo.length() == 0) {
                    editText_name.getText().clear();
                    editText_email.getText().clear();
                    editText_city.getText().clear();
                    editText_address.getText().clear();
                    editSubAddress.getText().clear();
                    editPincode.getText().clear();
                    editShopType.getText().clear();
                    editText_shopname.getText().clear();

                    spinner_usertype.setItems(types);


                }

            }
        });
        editShopType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        multiSelectionSpinner.setItems(shopTypeList);
        multiSelectionSpinner.setSelection(shopTypeList.size());

        multiSelectionSpinner.setListener(new MultiSelectionSpinner.OnMultipleItemsSelectedListener() {
            @Override
            public void selectedIndices(List<Integer> indices) {

            }

            @Override
            public void selectedStrings(List<ShopType> strings) {

                shopTypeLists = strings;
            }
        });

        spinner_state.setItems(states);
        spinner_usertype.setItems(types);

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInternet();
            }
        });

        editShopType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        spinner_usertype.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                String userType = (String) item.toString();
                if (userType.equals("DTVS Executive")) {
                    shopNameLayout.setVisibility(View.GONE);
                    shopTypeLayout.setVisibility(View.GONE);
                    shopTypeView.setVisibility(View.GONE);

                } else {
                    shopNameLayout.setVisibility(View.VISIBLE);
                    shopTypeLayout.setVisibility(View.VISIBLE);
                    shopTypeView.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    private void checkInternet() {
        if (networkConnection.CheckInternet()) {
            validateFileds();
        } else {
            Toast.makeText(getApplicationContext(), "Please check your network connection and try again!", Toast.LENGTH_SHORT).show();
        }
    }

    private void getDataByMobileNo(String mobileNo) {
        try {
            APIService service = RetroClient.getApiService();

            Call<CheckRegistration> call = service.getDataByMobileNo(mobileNo);

            call.enqueue(new Callback<CheckRegistration>() {

                @Override
                public void onResponse(Call<CheckRegistration> call, Response<CheckRegistration> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getResult().equals("Success")) {
                            editText_email.setText(response.body().getData().getEmail());
                            editText_shopname.setText(response.body().getData().getShopName());
                            editText_city.setText(response.body().getData().getCity());
                            spinner_state.setText(response.body().getData().getState());
                            editText_address.setText(response.body().getData().getAddress1());
                            editSubAddress.setText(response.body().getData().getAddress2());
                            editPincode.setText(response.body().getData().getPincode());
                            spinner_usertype.setText(response.body().getData().getUsertype());
                            editText_name.setText(response.body().getData().getName());
                            if (response.body().getData().getUsertype() != null)
                                if (response.body().getData().getUsertype().equals("DTVS Executive")) {
                                    shopNameLayout.setVisibility(View.GONE);
                                    shopTypeLayout.setVisibility(View.GONE);
                                    shopTypeView.setVisibility(View.GONE);
                                }
                        }
                    }

                }

                @Override
                public void onFailure(Call<CheckRegistration> call, Throwable t) {

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static Context getContext() {
        return instance;
    }

    private void validateFileds() {

        str_name = editText_name.getText().toString();
        str_email = editText_email.getText().toString();
        str_phone = editText_phone.getText().toString();
        str_state = spinner_state.getText().toString();
        str_city = editText_city.getText().toString();
        str_usertype = spinner_usertype.getText().toString();
        str_shopname = editText_shopname.getText().toString();
        strPincode = editPincode.getText().toString();
        str_address = editText_address.getText().toString();
        str_sub_address = editSubAddress.getText().toString();
        if (spinner_usertype.getText().toString().equals("Select Usertype"))
            toasts.ShowErrorToast("Select Usertype");
        else if (TextUtils.isEmpty(editText_name.getText().toString()))
            toasts.ShowErrorToast("Enter your name");
        else if (TextUtils.isEmpty(editText_phone.getText().toString()))
            toasts.ShowErrorToast("Enter your phonenumber");

        else if(TextUtils.isEmpty(editText_email.getText().toString()))
            toasts.ShowErrorToast("Enter your email-id");

        else if (spinner_usertype.getText().toString().equals("DTVS Executive")) {
            if (TextUtils.isEmpty(editText_city.getText().toString()))
                toasts.ShowErrorToast("Enter your city");
            else if (spinner_state.getText().toString().equals("Select State"))
                toasts.ShowErrorToast("Select your state");
            else if (TextUtils.isEmpty(editText_address.getText().toString()))
                toasts.ShowErrorToast("Enter Your Address1");
            else if (TextUtils.isEmpty(strPincode))
                toasts.ShowErrorToast("Enter Your Pin code");

            else
                GenerateFirebaseId();
        }
        else if (TextUtils.isEmpty(editText_shopname.getText().toString()))
            toasts.ShowErrorToast("Enter Your Shopname");
        else if (shopTypeLists == null)
            toasts.ShowErrorToast("Please choose Shop Type");
        else if (shopTypeLists.size() == 0)
            toasts.ShowErrorToast("Please choose Shop Type");
        else if (TextUtils.isEmpty(editText_city.getText().toString()))
            toasts.ShowErrorToast("Enter your city");
        else if (spinner_state.getText().toString().equals("Select State"))
            toasts.ShowErrorToast("Select your state");
        else if (TextUtils.isEmpty(editText_address.getText().toString()))
            toasts.ShowErrorToast("Enter Your Address1");
        else if (TextUtils.isEmpty(strPincode))
            toasts.ShowErrorToast("Enter Your Pin code");

        else
            GenerateFirebaseId();
//        userSignIn();

    }

    private void GenerateFirebaseId() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        try {

            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            new MyFirebaseInstanceId().onTokenRefresh();
            App_ID = myshare.getString("Appid", "");
            Log.e(TAG, "Register token: " + App_ID);
            progressDialog.dismiss();
            if (App_ID != null && !App_ID.equals("")) {
                if (networkConnection.CheckInternet())
                    userSignIn();
                else
                    alert.showAlertbox(getString(R.string.no_network));
            } else {
                toasts.ShowErrorToast("Error in generating Instance ID !\nPlease try again !");
            }
        }
        catch (Exception ex) {
            progressDialog.dismiss();
            Log.e(TAG, "Error GenerateFirebaseId : " + ex.getMessage());
            toasts.ShowErrorToast("Error in generating Instance ID !\nPlease try again !");
        }
    }

    private void userSignIn() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Validating credentials...\nPlease wait....");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
//            List<ShopType> shopTypes=new ArrayList<>();
//            for(String type:shopTypeLists)
//            {
//                ShopType type1=new ShopType();
//                type1.setShopType(type);
//                shopTypes.add(type1);
//            }
            APIService service = RetroClient.getApiService();
            // Calling JSON
            RegistrationPostValues postValues = new RegistrationPostValues();
            postValues.setName(str_name);
            postValues.setEmailId(str_email);
            postValues.setPhoneNumber(str_phone);
            postValues.setCity(str_city);
            postValues.setState(str_state);
            postValues.setAddress1(str_address);
            postValues.setAddress2(str_sub_address);
            postValues.setShopName(str_shopname);
            postValues.setUserType(str_usertype);
            postValues.setAppId(App_ID);
            postValues.setPincode(strPincode);
            if (shopTypeLists == null)
                shopTypeLists = new ArrayList<>();
            postValues.setShopTypeList(shopTypeLists);

//            Call<RegistrationData> call = service.REGISTRATION_DATA_CALL(str_name,str_email,str_phone,str_city,str_state,str_address,str_shopname,str_usertype,App_ID);
            Call<RegistrationData> call = service.REGISTRATION_DATA_CALL(postValues);
            call.enqueue(new Callback<RegistrationData>() {
                @Override
                public void onResponse(Call<RegistrationData> call, Response<RegistrationData> response) {
                    progressDialog.dismiss();
                    if ("Success".equals(response.body().getResult())) {
                        myshare = getSharedPreferences("Dephi", MODE_PRIVATE);
                        editor = myshare.edit();
                        editor.putBoolean("isLogin", true);
                        editor.putString("AccName", editText_name.getText().toString());
                        editor.putString("mobileNo", "");
                        editor.apply();
                        Intent intent = new Intent(RegistrationActivity.this, MainActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.left_in, R.anim.left_out);
                    }
                    else if ("UnRegistered".equals(response.body().getResult())) {

                        editor.putString("mobileNo", str_phone);
                        editor.putString("AccName", editText_name.getText().toString());
                        editor.commit();
                        otpalertbox = new AlertDialog.Builder(RegistrationActivity.this).create();
                        View otpalert = LayoutInflater.from(RegistrationActivity.this).inflate(R.layout.otpalert, null);

                        final Pinview otp = otpalert.findViewById(R.id.otp);
                        Button submitotp = otpalert.findViewById(R.id.otpsubmit);
                        final Button backotp = otpalert.findViewById(R.id.otpback);
                        TextView resendotp = otpalert.findViewById(R.id.resendotp);


                        backotp.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                otpalertbox.dismiss();
                            }
                        });

                        resendotp.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                resendOtptoMobile();
                            }
                        });
                        submitotp.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String otppost = otp.getValue();
                                if (otppost.length() != 4) {
                                    toasts.ShowErrorToast("Enter 4 Digit Otp");
                                } else {
                                    otpverify(otppost);
                                }
                            }
                        });
                        otpalertbox.setView(otpalert);
                        otpalertbox.setCanceledOnTouchOutside(false);
                        otpalertbox.setCancelable(false);
                        otpalertbox.show();


                    }

                    else if ("NotSuccess".equals(response.body().getResult())) {
                        alert.showAlertbox(getString(R.string.server_error));
                    } else {
                        alert.showAlertbox(getResources().getString(R.string.server_error));
                    }
                }

                @Override
                public void onFailure(Call<RegistrationData> call, Throwable t) {
                    progressDialog.dismiss();
                    alert.showAlertbox("Something went wrong . Please try again later .");
                }

            });
        } catch (Exception ex) {
            ex.getMessage();
            Log.v("Error", ex.getMessage());
        }
    }

    private void resendOtptoMobile() {
        final ProgressDialog progressDialog = new ProgressDialog(RegistrationActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {
            APIService service = RetroClient.getApiService();
            Call<ResendOtpStatus> call = service.resendOtp(str_phone);
            call.enqueue(new Callback<ResendOtpStatus>() {
                @Override
                public void onResponse(Call<ResendOtpStatus> call, Response<ResendOtpStatus> response) {
                    progressDialog.dismiss();
                    try {

                        if (response.isSuccessful()) {
                            if (response.body().getResult().equals("Success")) {
                                toasts.ShowErrorToast("OTP Resend to your registered mobile number");
                            }
                        } else {
                            alert.showAlertbox("Please try again later");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        alert.showAlertbox("Exception error. Please try again later");
                    }
                }

                @Override
                public void onFailure(Call<ResendOtpStatus> call, Throwable t) {
                    progressDialog.dismiss();
                    alert.showAlertbox("Something went wrong . Please try again later .");
                }
            });
        } catch (Exception e) {
            progressDialog.dismiss();
            e.printStackTrace();
            alert.showAlertbox("Exception error. Please try again later");
        }

    }


    private void otpverify(String otppost) {
        final ProgressDialog progressDialog = new ProgressDialog(RegistrationActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {
            APIService service = RetroClient.getApiService();
            Call<ValidatedMobileOtp> call = service.validateotp(str_phone, otppost);
            call.enqueue(new Callback<ValidatedMobileOtp>() {
                @Override
                public void onResponse(Call<ValidatedMobileOtp> call, Response<ValidatedMobileOtp> response) {
                    progressDialog.dismiss();
                    try {

                        if (response.isSuccessful()) {
                            if (response.body().getResult().equals("Verified")) {
                                myshare = getSharedPreferences("Dephi", MODE_PRIVATE);
                                editor = myshare.edit();
                                editor.putBoolean("isLogin", true);
                                editor.putString("AccName", editText_name.getText().toString());
                                editor.putString("mobileNo", "");
                                editor.apply();
                                Intent intent = new Intent(RegistrationActivity.this, MainActivity.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.left_in, R.anim.left_out);
                            } else if (response.body().getResult().equals("NotVerified")) {
//                                toasts.ShowErrorToast("Invalid OTP");
                                Toast.makeText(getApplicationContext(), "Please Enter Valid OTP ", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            alert.showAlertbox("Please try again later");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        alert.showAlertbox("Exception error. Please try again later");
                    }
                }

                @Override
                public void onFailure(Call<ValidatedMobileOtp> call, Throwable t) {
                    progressDialog.dismiss();
                    alert.showAlertbox("Something went wrong . Please try again later .");
                }
            });
        } catch (Exception e) {
            progressDialog.dismiss();
            e.printStackTrace();
            alert.showAlertbox("Exception error. Please try again later");
        }

    }
  /*  @SuppressLint("NewApi")
    private void openCamera1() {
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

                // TODO: Consider calling

                //    ActivityCompat#requestPermissions

                // here to request the missing permissions, and then overriding

                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,

                //                                          int[] grantResults)

                // to handle the case where the user grants the permission. See the documentation

                // for ActivityCompat#requestPermissions for more details.

                return;

            }

            imei1 = tm.getDeviceId(2);

            Imei2 = tm.getDeviceId(1);



        }else

        {

            try {

                Class<?> telephonyClass = Class.forName(tm.getClass().getName());

                Class<?>[] parameter = new Class[1];

                parameter[0] = int.class;

                Method getFirstMethod = telephonyClass.getMethod("getDeviceId", parameter);

                Log.d("SimData", getFirstMethod.toString());

                Object[] obParameter = new Object[1];

                obParameter[0] = 0;

                TelephonyManager manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);

                imei1 = (String) getFirstMethod.invoke(tm, obParameter);

                Log.d("SimData", "first :" + imei1);

                obParameter[0] = 1;

                Imei2 = (String) getFirstMethod.invoke(tm, obParameter);

                Log.d("SimData", "Second :" + Imei2);

            }catch (Exception e)

            {

                e.printStackTrace();

            }

        }




    }*/

}
