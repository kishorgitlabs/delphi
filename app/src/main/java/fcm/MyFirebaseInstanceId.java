package fcm;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.brainmagic.delphitvs.service.RegistrationActivity;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import static android.content.ContentValues.TAG;


public class MyFirebaseInstanceId extends FirebaseInstanceIdService {


    SharedPreferences myshare;
    SharedPreferences.Editor editor;

    Context context;


    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        String uniqueID = FirebaseInstanceId.getInstance().getId();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        Log.d(TAG, "Unique Id: " + uniqueID);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String refreshedToken) {
        try {
            myshare = RegistrationActivity.getContext().getSharedPreferences("Delphi", 0);
            editor = myshare.edit();
            editor.putString("Appid", refreshedToken);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
