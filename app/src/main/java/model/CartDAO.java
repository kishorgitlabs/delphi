package model;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "cart")

public class CartDAO  {
    @PrimaryKey(autoGenerate = true)
    private int pId;

    @ColumnInfo(name = "partnumber")
    private String partnumber;

    @ColumnInfo(name = "mrp")
    private String mrp;

    @ColumnInfo(name ="qty")
    private String qty;

    @ColumnInfo(name = "totalamount")
    private String totalamount;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "segment")
    private String segment;

    @ColumnInfo(name = "customermobilenumber")
    private String customermobilenumber;

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public String getCustomermobilenumber() {
        return customermobilenumber;
    }

    public void setCustomermobilenumber(String customermobilenumber) {
        this.customermobilenumber = customermobilenumber;
    }

    public String getCustomerusertype() {
        return customerusertype;
    }

    public void setCustomerusertype(String customerusertype) {
        this.customerusertype = customerusertype;
    }

    @ColumnInfo(name = "customerusertype")
    private String customerusertype;

    public int getPId() {
        return pId;
    }

    public void setPId(int pId) {
        this.pId = pId;
    }

    public String getPartnumber() {
        return partnumber;
    }

    public void setPartnumber(String partnumber) {
        this.partnumber = partnumber;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(String totalamount) {
        this.totalamount = totalamount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
