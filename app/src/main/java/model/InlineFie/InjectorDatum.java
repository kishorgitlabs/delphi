
package model.InlineFie;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class InjectorDatum {

    @SerializedName("expoledview")
    private String mExpoledview;
    @SerializedName("ILLUSTRATION_NO")
    private String mILLUSTRATIONNO;
    @SerializedName("id")
    private Long mId;
    @SerializedName("LINE_SEQ_NO")
    private String mLINESEQNO;
    @SerializedName("PART_DESCRIPTION")
    private String mPARTDESCRIPTION;
    @SerializedName("PartNo")
    private String mPartNo;
    @SerializedName("Pro_image")
    private String mProImage;
    @SerializedName("QTY_FIP")
    private String mQTYFIP;
    @SerializedName("SerPART_NO")
    private String mSerPARTNO;

    public String getExpoledview() {
        return mExpoledview;
    }

    public void setExpoledview(String expoledview) {
        mExpoledview = expoledview;
    }

    public String getILLUSTRATIONNO() {
        return mILLUSTRATIONNO;
    }

    public void setILLUSTRATIONNO(String iLLUSTRATIONNO) {
        mILLUSTRATIONNO = iLLUSTRATIONNO;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getLINESEQNO() {
        return mLINESEQNO;
    }

    public void setLINESEQNO(String lINESEQNO) {
        mLINESEQNO = lINESEQNO;
    }

    public String getPARTDESCRIPTION() {
        return mPARTDESCRIPTION;
    }

    public void setPARTDESCRIPTION(String pARTDESCRIPTION) {
        mPARTDESCRIPTION = pARTDESCRIPTION;
    }

    public String getPartNo() {
        return mPartNo;
    }

    public void setPartNo(String partNo) {
        mPartNo = partNo;
    }

    public String getProImage() {
        return mProImage;
    }

    public void setProImage(String proImage) {
        mProImage = proImage;
    }

    public String getQTYFIP() {
        return mQTYFIP;
    }

    public void setQTYFIP(String qTYFIP) {
        mQTYFIP = qTYFIP;
    }

    public String getSerPARTNO() {
        return mSerPARTNO;
    }

    public void setSerPARTNO(String serPARTNO) {
        mSerPARTNO = serPARTNO;
    }

}
