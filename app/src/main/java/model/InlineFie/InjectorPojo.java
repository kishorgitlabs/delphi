
package model.InlineFie;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class InjectorPojo {

    @SerializedName("data")
    private List<InjectorDatum> mData;
    @SerializedName("result")
    private String mResult;

    public List<InjectorDatum> getData() {
        return mData;
    }

    public void setData(List<InjectorDatum> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
