
package model.InlineFie;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class InlineGovernorPojo {

    @SerializedName("data")
    private List<GovenorDatum> mData;
    @SerializedName("result")
    private String mResult;

    public List<GovenorDatum> getData() {
        return mData;
    }

    public void setData(List<GovenorDatum> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
