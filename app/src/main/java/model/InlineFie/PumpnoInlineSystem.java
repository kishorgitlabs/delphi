
package model.InlineFie;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PumpnoInlineSystem {

    @SerializedName("data")
    private List<PumpnoData> mData;
    @SerializedName("result")
    private String mResult;

    public List<PumpnoData> getData() {
        return mData;
    }

    public void setData(List<PumpnoData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
