
package model.allmakeproducts;

import java.util.List;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class AllMakeProductsSpares {

    @SerializedName("data")
    private List<AllMakeSparesData> mData;
    @SerializedName("result")
    private String mResult;

    public List<AllMakeSparesData> getData() {
        return mData;
    }

    public void setData(List<AllMakeSparesData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
