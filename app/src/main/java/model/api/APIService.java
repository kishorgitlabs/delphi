package model.api;

import com.brainmagic.delphitvs.InlineGovernor;
import com.brainmagic.delphitvs.regionalmanager.AttendanceInOutRegional;
import com.brainmagic.delphitvs.regionalmanager.RegionalManagerPojo;
import com.brainmagic.delphitvs.regionalmanager.RegionsalesexecutivenamePojo;

import model.InlineFie.GovernorExplodedView;
import model.InlineFie.InjectorExplodedView;
import model.InlineFie.InjectorPojo;
import model.InlineFie.InlineGovernorPojo;
import model.InlineFie.PumpnoInlineSystem;
import model.InlineFie.PumpnoPdf;
import model.InlineFie.Pumpnoexplodedview;
import model.PartnumberSearchData;
import model.allmakeproducts.AllMakeProductsSpares;
import model.attendance.AttendanceData;
import model.changepassword.ChangePasswordModel;
import model.changepassword.CommonExpanDInlineFieSystem;
import model.customerdetailslist.CustomerDetails;
import model.customerlist.CustomerList;

import model.distributorcitylist.DistributorCityList;
import model.distributorlist.DistributorList;
import model.distributorregion.DistributorRegionList;
import model.forgotpassword.ForgotPasswordModel;
import model.general.Categorydata.CategoryData;
import model.general.categorylist.CategoryList;
import model.commonexpandablemodel.CommonRotaryExpandableList;
import model.filters.details.FilterDetail;
import model.filters.main.FiltersMain;
import model.filters.oemmodel.OEModelList;
import model.general.feedback.FeedBackModel;
import model.general.video.VideoCategory;
import model.getpartnumber.GetPartNumber;
import model.network.dealer.dealerdetails.DealerList;
import model.network.dealer.dealerstatecity.DealerModel;
import model.orderdetails.OrderDetails;
import model.orderdetails.SalesOrdersPojo;
import model.partdetails.PartDetails;
import model.partdetails.SalesOrderDatePojo;
import model.partnumbersearch.PartNumberSearch;
import model.placeorder.PlaceOrder;
import model.pricelist.PriceDetails;
import model.pump.PumpMainSub;
import model.regional.CurrentOrderViewRegionalPojo;
import model.regional.DateWiseOrdersPojo;
import model.regional.Extra;
import model.regional.ViewVisitReportPojo;
import model.regional.salesnamelist.CurrentOrderDetailsRegionalPojo;
import model.regional.salesnamelist.CurrentOrderHistoryPojo;
import model.regional.salesnamelist.RegionalAttendanceView;
import model.regional.salesnamelist.RegionalInOutTime;
import model.regional.salesnamelist.ResponseRegionaTravel;
import model.regional.salesnamelist.ResponseRegionalTimeHistoryPojo;
import model.regional.salesnamelist.SalesNameList;
import model.registeration.RegistrationData;
import model.registeration.RegistrationPostValues;
import model.registeration.checkregistration.CheckRegistration;
import model.resendotp.ResendOtpStatus;
import model.rotaryexplodview.RotaryExplodeView;
import model.rotaryfiepdf.PDFDownload;
import model.rotaryservicelist.RotaryServiceData;
import model.sales.createcustomer.CreateCustomerModel;
import model.sales.createcustomer.imageupload.ImageUpload;
import model.sales.customerdetails.GetCustomerDetails;
import model.sales.customerdetails.search.CustomerNameList;
import model.sales.login.SalesLoginModel;
import model.sales.order.viewfullhistory.OrderMoreDetails;
import model.sales.travelcoordinates.InsertTravelCordinates;
import model.sales.travelhistory.AttendanceInOut;
import model.sales.travelhistory.MarkAttendanceModel;
import model.sales.viewattendance.attendance.SalesAttendance;
import model.sales.viewattendance.attendancehistory.SalesAttendanceHistory;
import model.sales.viewattendance.salescoordinate.SalesCoordinates;
import model.sales.visitreport.ViewVisitReportModel;
import model.sales.visitreport.VisitReportModel;
import model.servicepart.ServicePartlistData;
import model.validateotp.ValidatedMobileOtp;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface APIService {

    //Post Partnumber search
    @FormUrlEncoded
    @POST("api/values/Partsearch")
    public Call<PartnumberSearchData> SearchPartnumber(
            @Field("PartNumber") String partnumber); //Post Partnumber search

    @FormUrlEncoded
    @POST("api/values/Partsearch")
    public Call<PartNumberSearch> SearchPartnumbers(
            @Field("PartNumber") String partnumber); //Post Partnumber search

    @FormUrlEncoded
    @POST("api/values/ProductDetail")
    public Call<FilterDetail> PRODUCT_DETAILS_DATA_CALL(
            @Field("Main_Category") String maincatagory,
            @Field("Sub_Category") String subcatagory); //Post Partnumber search

    @FormUrlEncoded
    @POST("api/values/getApplicationExpandable2")
    public Call<CommonExpanDInlineFieSystem> commonExpandableLists(
            @Field("Main_Category") String maincatagory,
            @Field("Sub_Category") String subcatagory);

    @FormUrlEncoded
    @POST("api/values/getApplicationExpandable1")
    public Call<CommonRotaryExpandableList> commonExpandableList(
            @Field("Main_Category") String maincatagory,
            @Field("Sub_Category") String subcatagory);


    @FormUrlEncoded
    @POST("api/values/ProductDetail")
    public Call<PumpMainSub> PRODUCT_DETAILS_DATA(
            @Field("Main_Category") String maincatagory,
            @Field("Sub_Category") String subcatagory); //Post Partnumber search
//    @FormUrlEncoded
//    @POST("api/values/RotaryProductDetail")
//    public Call<NewServiceParts> rotaryOldNewServiceList(
//            @Field("Main_Category") String maincatagory,
//            @Field("Sub_Category") String subcatagory);

    @FormUrlEncoded
    @POST("api/values/RotaryExpandProductDetail")
    public Call<CommonRotaryExpandableList> rotaryOldNewServiceList(
            @Field("Main_Category") String maincatagory,
            @Field("Sub_Category") String subcatagory);

    @FormUrlEncoded
    @POST("api/values/serviceDetails")
    public Call<RotaryServiceData> rotaryServiceList(
            @Field("Part_Number") String maincatagory);
    @FormUrlEncoded
    @POST("api/values/serviceDetails")
    public Call<PumpnoInlineSystem> rotaryServiceLists(
            @Field("Part_Number") String maincatagory);

    @FormUrlEncoded
    @POST("api/values/serviceDetails")
    public Call<InjectorPojo> injectorservicelist(
            @Field("Part_Number") String maincatagory);

    @FormUrlEncoded
    @POST("api/values/serviceDetails")
    public Call<InlineGovernorPojo> injectorservicelists(
            @Field("Part_Number") String maincatagory);

    @FormUrlEncoded
    @POST("api/values/Inline")
    public Call<AllMakeProductsSpares> INLINE_DATA_CALL(
            @Field("inline") String inlinenumber);

    @FormUrlEncoded
    @POST("api/values/ServicePartDetail")
    public Call<ServicePartlistData> SERVICE_PARTLIST_DATA_CALL(
            @Field("DTVS_Part_No") String partnodtvs);


    @FormUrlEncoded
    @POST("api/values/mobileregister")
    public Call<RegistrationData> REGISTRATION_DATA_CALL(
            @Field("name") String inlinenumber,
            @Field("email") String email,
            @Field("mobileno") String mobileno,
            @Field("city") String city,
            @Field("state") String state,
            @Field("Address") String address,
            @Field("ShopName") String shopname,
            @Field("usertype") String usertype,
            @Field("Appid") String appid);

    @POST("api/values/mobileregister")
    public Call<RegistrationData> REGISTRATION_DATA_CALL(
            @Body RegistrationPostValues postValues);

    @FormUrlEncoded
    @POST("api/values/ProductMainCat")
    public Call<FiltersMain> filtersMain(
            @Field("Type") String segmentList);

    @GET("api/values/OEM")
    public Call<OEModelList> oeList();

    @FormUrlEncoded
    @POST("api/values/Mastercheck")
    public Call<FilterDetail> filterList(
            @Field("Sub_Category") String subCategory,
            @Field("Segment") String segment,
            @Field("Type") String type);

//    @FormUrlEncoded
//    @POST("api/values/PartNoandDescription")
//    public Call<FilterDetail> commonFilterList(
//            @Field("Part_Number") String part,
//            @Field("Description") String dsc);


    @FormUrlEncoded
    @POST("api/values/PartNoandDescription")
    public Call<FilterDetail> commonFilterList(
            @Field("Part_Number") String part,
            @Field("VechileAppType") String vehiclAppType
            );

    @FormUrlEncoded
    @POST("api/values/GetPDF")
    public Call<PDFDownload> pdfDownload(
            @Field("partno") String part);

    @FormUrlEncoded
    @POST("api/values/GetPDF")
    public Call<PumpnoPdf> pdfDownloads(
            @Field("partno") String part);
    @FormUrlEncoded
    @POST("api/values/Exploadview")
    public Call<RotaryExplodeView> explodedView(
            @Field("PartNumber") String part);

    @FormUrlEncoded
    @POST("api/values/Exploadview")
    public Call<InjectorExplodedView> injectorexplodedView(
            @Field("PartNumber") String part);

    @FormUrlEncoded
    @POST("api/values/Exploadview")
    public Call<GovernorExplodedView> injectorexplodedViews(
            @Field("PartNumber") String part);

    @FormUrlEncoded
    @POST("api/values/Exploadview")
    public Call<Pumpnoexplodedview> explodedViews(
            @Field("PartNumber") String part);


    @FormUrlEncoded
    @POST("api/values/OEMbasedModel")
    public Call<OEModelList> oEBasedModel(
            @Field("OEM") String oe);

    @FormUrlEncoded
    @POST("api/values/OEMbasedModelList")
    public Call<FilterDetail> filtereOeBasedList(
            @Field("OEM") String oe,
            @Field("Model") String model
    );

    @FormUrlEncoded
    @POST("api/values/getRotaryExpandable1")
    public Call<CommonRotaryExpandableList> commonRotaryList(
            @Field("Main_Category") String maincatagory,
            @Field("Sub_Category") String subcatagory);

    @GET("api/values/Statelist")
    public Call<DealerModel> getDealerStateList();

    @FormUrlEncoded
    @POST("api/values/GetStatewiseCity_Usertype")
    public Call<DealerModel> getDelphiDistributorCityList(
            @Field("State") String state,
            @Field("UserType") String userType
    );

    @FormUrlEncoded
//    @POST("api/values/GetStatewiseCity")
    @POST("api/values/ServiceDealerCitylist")
    public Call<DealerModel> getDealerCityList(
            @Field("State") String state
    );

    @FormUrlEncoded
    @POST("api/values/ServiceDealerlist")
    public Call<DealerList> getDealerList(
            @Field("State") String state,
            @Field("City") String city
    );

    @FormUrlEncoded
    @POST("api/values/ServiceDistributorlist")
    public Call<DealerList> getDelphiDistributorlist(
            @Field("State") String state,
            @Field("City") String city,
            @Field("UserType") String userType
    );

    @GET("api/values/Pricelist")
    public Call<PriceDetails> getPriceList();

    @GET("api/values/BrochureCategory")
    public Call<CategoryList> getMarketCategory();

    @FormUrlEncoded
    @POST("api/values/BrochureCategoryBasedlist")
    public Call<CategoryData> getMarketCategoryData(
            @Field("Category") String category
    );

//    @FormUrlEncoded
//    @POST("api/values/EnquiryFormdetail")
//    public Call<CategoryData> sendFeedBack(
//            @Field("Partno") String partNo,
//            @Field("name") String name,
//            @Field("emailid") String mail,
//            @Field("mobileno") String mobileNo,
//            @Field("message") String msg,
//            @Field("contacttype ") String contactType,
//            @Field("city") String city,
//            @Field("company") String company
//    );

    @FormUrlEncoded
    @POST("api/values/VideoCategoryBasedlist")
    public Call<VideoCategory> getVideoList(
            @Field("Category") String category
    );

    @FormUrlEncoded
    @POST("api/values/EnquiryFormdetail")
    public Call<FeedBackModel> sendFeedBack(
            @Field("Partno") String partNo,
            @Field("name") String name,
            @Field("emailid") String emailId,
            @Field("mobileno") String mobileNO,
            @Field("message") String message,
            @Field("contacttype") String contactType,
            @Field("city") String city,
            @Field("company") String company
    );

    @GET("api/values/DistributorRegionlist")
    public Call<DistributorRegionList> getDistributorRegion();

    @FormUrlEncoded
    @POST("api/values/DistributorRegionBasedCitylist")
    public Call<DistributorCityList> getDistributorList(
            @Field("Region") String category
    );

    @FormUrlEncoded
    @POST("api/values/Distributorlist")
    public Call<DistributorList> getDistributorList(
            @Field("Region") String region,
            @Field("City") String city
    );

    @FormUrlEncoded
    @POST("api/values/ResendSendOTP")
    public Call<ResendOtpStatus> resendOtp(
            @Field("MobileNo") String mobile
    );

    @FormUrlEncoded
    @POST("api/values/ValidateOTP")
    public Call<ValidatedMobileOtp> validateotp(
            @Field("MobileNo") String mobile,
            @Field("OTP") String otp
    );

    @FormUrlEncoded
    @POST("api/Values/AttendanceNew")
    public Call<AttendanceData> attendnace(
            @Field("EmpId") int id,
            @Field("Name") String Name,
            @Field("Date") String date,
            @Field("attendanceTime") String attendancetime,
            @Field("Address") String address,
            @Field("Designation") String designation,
            @Field("OutAddress") String outaddress,
            @Field("InLatitude") String inlat,
            @Field("InLongitude") String inlong,
            @Field("OutLatitude") String outlat,
            @Field("OutLongitude") String outlong,
            @Field("EmpCode") String code,
            @Field("AttendDay") String Day,
            @Field("Distance") Double distance
    );

    @FormUrlEncoded
    @POST("api/values/login")
    public Call<SalesLoginModel> setSalesLogin(
            @Field("UserName") String uName,
            @Field("Password") String uPass,
            @Field("DeviceName") String deviceName,
            @Field("sdkInt") String sdkInt
    );

    @FormUrlEncoded
    @POST("api/values/forgot")
    public Call<ForgotPasswordModel> forgotPassword(
            @Field("EmailId") String emailId,
            @Field("UserType") String userType
    );

    @FormUrlEncoded
    @POST("api/values/Attendanceinandout")
    public Call<MarkAttendanceModel> markAttendance(
            @Field("EmpId") String empId,
            @Field("EmpCode") String empCode,
            @Field("Name") String name,
            @Field("Date") String date,
            @Field("InTime") String inTime,
            @Field("InLatitude") String inLatitude,
            @Field("InLongitude") String inLongitude,
            @Field("AttendDay") String attendanceDay,
            @Field("OutTime") String outTime,
            @Field("TotalDuration") String totalDuration,
            @Field("OutAddress") String outAddress,
            @Field("OutLongitude") String outLongitude,
            @Field("OutLatitude") String outLatitude,
            @Field("Address") String address,
            @Field("Designation") String destination
//            @Field("setCoordinateList") List<UploadCoordinates> uploadCoordinates
    );

    @POST("api/values/Attendanceinandout")
    public Call<MarkAttendanceModel> markAttendances(
            @Body AttendanceInOut attendanceInOut
//            @Field("setCoordinateList") List<UploadCoordinates> uploadCoordinates
    );

    @POST("api/Values/RegAttendanceinandout")
    Call<RegionalManagerPojo> regionalattendance(
            @Body AttendanceInOutRegional attendanceInOut

    );

    @FormUrlEncoded
    @POST("api/values/CoodinateInsert")
    public Call<InsertTravelCordinates> setTravelCoordinates(
            @Field("empId") int empId,
            @Field("empName") String name,
            @Field("trackId") long trackId,
            @Field("time") String time,
            @Field("Date") String date,
            @Field("latitude") double latitude,
            @Field("longitude") double longitude,
            @Field("Address") String address,
            @Field("distance") float distance
    );

    @FormUrlEncoded
    @POST("api/values/regionalCoodinateInsert")
    public Call<ResponseRegionaTravel> setTravelCoordinate(
            @Field("R_id") int empId,
            @Field("RegName") String name,
            @Field("TrackId") long trackId,
            @Field("timedate") String time,
            @Field("datetime") String date,
            @Field("latitude") double latitude,
            @Field("langtitude") double longitude,
            @Field("address") String address,
            @Field("distance") float distance,
            @Field("Regid")String regid
    );
    @FormUrlEncoded
    @POST("api/values/salesAttendanceview")
    public Call<SalesAttendance> salesAttendanceView(
            @Field("EmpId") String empId
    );

    @FormUrlEncoded
    @POST("api/values/salesAttendanceHistory")
    public Call<SalesAttendanceHistory> salesAttendanceHistoryView(
            @Field("EmpId") String empId,
            @Field("Date") String date
    );

    @FormUrlEncoded
    @POST("api/values/viewSalesCoordinationAttendance")
    public Call<SalesCoordinates> viewSalesCoordinate(
            @Field("EmpId") String empId,
            @Field("TrackId") String trackId,
            @Field("datetime") String date
    );

    @FormUrlEncoded
    @POST("api/values/viewRMCoordinationAttendance")
    public Call<ResponseRegionalTimeHistoryPojo> viewregionmap(
            @Field("R_id") String empId,
            @Field("datetime") String trackId,
            @Field("TrackId") String date
    );


    @FormUrlEncoded
        @POST("api/values/viewSalesCoordinationAttendance")
    public Call<SalesCoordinates> viewregionalcordinates(
            @Field("EmpId") String empId,
            @Field("TrackId") String trackId,
            @Field("datetime") String date
    );
    @FormUrlEncoded
    @POST("api/values/salesAttendanceSearchByDate")
    public Call<SalesAttendance> viewSalesByDate(
            @Field("EmpId") String empId,
            @Field("EmpCode") String empCode,
            @Field("fromdate") String fromDate,
            @Field("todate") String toDate
    );
    @FormUrlEncoded
    @POST("api/values/salesAttendanceSearchByDate")
    public Call<SalesAttendance> viewsalesbyregional(
            @Field("EmpId") String empId,
            @Field("EmpCode") String empCode,
            @Field("fromdate") String fromDate,
            @Field("todate") String toDate
    );
    @FormUrlEncoded
    @POST("api/values/Regionsalesexecutivename")
    public Call<SalesNameList> getSalesNameList(
            @Field("Regid") String regId
    );

    @FormUrlEncoded
    @POST("api/values/RegionalAttendanceSearchviewNew")

    public Call<RegionalInOutTime> getinandouttime(
            @Field("EmpId") String regId,
            @Field("Date") String date
    );

    @FormUrlEncoded
    @POST("api/values/RegionAttendenceview")

    public Call<RegionalAttendanceView> getattendancelist(
            @Field("Regid") String regId
    );
    @FormUrlEncoded
    @POST("api/values/RegionAttendenceSearchview1")
    public Call<RegionalAttendanceView> getdatewiselist(
                 @Field("Regid") String regId,
                 @Field("EmpCode")String empcode,
                 @Field("fromdate")String fromdate,
                 @Field("todate")String todate
                 );


    @FormUrlEncoded
    @POST("api/values/VisitReport")
    public Call<VisitReportModel> addVisitReport(
            @Field("SalesPersonName") String salesPersonName,
            @Field("ExeId") String empId,
            @Field("CustMobileNo") String customerMobileNo,
            @Field("PlaceOfVisit") String placeOfVisit,
            @Field("VisitedDate") String visitedDate,
//          @Field("Time") String regId,
            @Field("CustomerName") String customerName,
            @Field("CustomerType") String customerType,
            @Field("Remark") String remark
    );

    @FormUrlEncoded
    @POST("api/values/viewCustomerData")
    public Call<GetCustomerDetails> getCustomerDetails(
            @Field("ExecutId") String regId
    );

    @FormUrlEncoded
    @POST("api/values/viewVisitReport")
    public Call<ViewVisitReportModel> viewVisitReport(
            @Field("ExeId") String empId
    );


    @FormUrlEncoded
    @POST("api/values/viewVisitReportDatewise")
    Call<ViewVisitReportModel> viewVisitReportDateWise(
            @Field("fromdate") String fromDate,
            @Field("todate") String toDate,
            @Field("ExeId") String salesPerson,
            @Field("UserType") String userType
    );
    @FormUrlEncoded
    @POST("api/values/viewVisitReportDatewise")
    Call<Extra> visitreportregionaldate(
            @Field("fromdate") String fromDate,
            @Field("todate") String toDate,
            @Field("ExeId") String salesPerson,
            @Field("UserType") String userType
    );

    @FormUrlEncoded
    @POST("api/values/viewCustomerName")
    public Call<CustomerNameList> getCustomerName(
            @Field("ExecutId") String empId
    );

    @FormUrlEncoded
    @POST("api/values/viewCustomerList")
    public Call<GetCustomerDetails> viewCustomerList(
            @Field("ExecutId") String empId,
            @Field("DealerName") String dealerName
    );

    @FormUrlEncoded
    @POST("api/values/GetRegionwiseState")
    public Call<CustomerNameList> getStateList(
            @Field("DaelerRegion") String region
    );

    @FormUrlEncoded
    @POST("api/values/GetStatewiseCity")
    public Call<CustomerNameList> getCityList(
            @Field("DealerState") String city
    );

    @FormUrlEncoded
    @POST("api/values/CreateCustomer")
    public Call<CreateCustomerModel> createCustomer(
            @Field("ExecutId") String empId,
            @Field("DaelerRegion") String empRegion,
            @Field("Regid") String regId,
            @Field("UserType") String userType,
            @Field("ContactPersonname") String contactPerson,
            @Field("DealerMobile") String mobile,
            @Field("ShopName") String shopName,
            @Field("DealerName") String dealerName,
            @Field("DealerEmail") String mail,
            @Field("DealerState") String state,
            @Field("DealerCity") String city,
            @Field("DealerAddress") String address,
            @Field("Pincode") String pincode,
            @Field("GST") String gst,
            @Field("Image") String image
    );

    @FormUrlEncoded
    @POST("api/values/orderview")
    public Call<SalesOrdersPojo> getOrderHistory(
            @Field("ExeId") String exeId
    );

    @FormUrlEncoded
    @POST("api/values/searchorderviewdate")
    Call<SalesOrderDatePojo> getSalesOrderHistoryDateWise(
            @Field("excuteid") String exeId,
            @Field("fromdate") String fromDate,
            @Field("todate") String toDate
    );

    @FormUrlEncoded
    @POST("api/values/orderviewDetail")
    Call<OrderMoreDetails> getSalesOrderMoreDetails(
            @Field("orderid") String fromdate

    );



    @Multipart
    @POST("api/Upload/PostUserImage")
    public Call<ImageUpload> imageUpload(
            @Part MultipartBody.Part file
    );

    @GET("api/values/OverallPartnumber")
    public Call<GetPartNumber> getpartnumber();

    @FormUrlEncoded
    @POST("api/values/ServicePartDetail")
    public Call<PartDetails> getpartdetails(
            @Field("Part_Number") String partnumber
    );

    @FormUrlEncoded
    @POST("api/values/CityBasedCustomerList")
    public Call<CustomerList> getCustomerList(
            @Field("ExecutId") String exeid
    );

    @FormUrlEncoded
    @POST("api/values/CustomerList")
    public Call<CustomerDetails> getCustomerDetail(
            @Field("DealerMobile") String dealermob
    );

    @POST("api/values/SaveOrders")
    public Call<PlaceOrder> placeOrder(
            @Body OrderDetails orderDetails
    );

    @FormUrlEncoded
    @POST("api/values/CheckReg")
    public Call<CheckRegistration> checkRegistration(
            @Field("mobileno") String mobileNo
    );

    @FormUrlEncoded
    @POST("api/values/GetMobileNoWiseData")
    public Call<CheckRegistration> getDataByMobileNo(
            @Field("mobileno") String mobileNo
    );

    @FormUrlEncoded
    @POST("api/values/ChangePassword")
    public Call<ChangePasswordModel> changePassword(
            @Field("MobileNo") String mobileNo,
            @Field("Password") String password,
            @Field("NewPassword") String newPassword
    );

    @FormUrlEncoded
    @POST("api/values/GetPDFInline")
    public Call<PDFDownload> getInlinePdf(
            @Field("pdfvalue") String category
    );


@FormUrlEncoded
@POST("api/Values/RMorderview")
public Call<CurrentOrderViewRegionalPojo>getsalesname(@Field("RegId")String regid);


@FormUrlEncoded
    @POST("api/Values/Regionsalesexecutivename")
    public Call<RegionsalesexecutivenamePojo>getexename(@Field("Regid") String regid);

@FormUrlEncoded
    @POST("api/Values/orderview1")
    public Call<CurrentOrderDetailsRegionalPojo>salesdetails(

            @Field("ExeId")String salesid,
            @Field("OrderDate")String date

);
@FormUrlEncoded
    @POST("api/Values/orderviewDetail")
    public Call<CurrentOrderHistoryPojo> getorderdetils(@Field("OrderId")String orderid);

@FormUrlEncoded
    @POST("api/Values/viewRegionalVisitReport1")
    public Call<ViewVisitReportPojo>getsaleslist(@Field("RegId")String reg);

    @FormUrlEncoded
    @POST("api/Values/viewRegionalVisitReport1")
    public Call<ViewVisitReportPojo>getsaleslists
            (@Field("RegId")String reg);


@FormUrlEncoded
    @POST("api/Values/viewRegionalVisitReport1")
public Call<ViewVisitReportPojo>getexecutivenames(@Field("regId")String regid);
    @FormUrlEncoded
    @POST("Api/values/viewVisitReportDatewise")
    Call<ViewVisitReportModel> viewVisitReportDateWises(
            @Field("fromdate") String fromDate,
            @Field("todate") String toDate,
            @Field("ExeId") String salesPerson,
            @Field("UserType") String userType

    );


    @FormUrlEncoded
    @POST("api/values/RMorderview")

    Call<DateWiseOrdersPojo> allreports(

            @Field("Regid")String regid,
            @Field("fromdate")String fromdate,
            @Field("todate")String todate

    );

}