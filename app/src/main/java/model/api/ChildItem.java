package model.api;

public class ChildItem{
	private String mainfilter;
	private String ecu;
	private String rail;
	private String injector;
	private String pumpNo;
	private String prefilter;

	public void setMainfilter(String mainfilter){
		this.mainfilter = mainfilter;
	}

	public String getMainfilter(){
		return mainfilter;
	}

	public void setEcu(String ecu){
		this.ecu = ecu;
	}

	public String getEcu(){
		return ecu;
	}

	public void setRail(String rail){
		this.rail = rail;
	}

	public String getRail(){
		return rail;
	}

	public void setInjector(String injector){
		this.injector = injector;
	}

	public String getInjector(){
		return injector;
	}

	public void setPumpNo(String pumpNo){
		this.pumpNo = pumpNo;
	}

	public String getPumpNo(){
		return pumpNo;
	}

	public void setPrefilter(String prefilter){
		this.prefilter = prefilter;
	}

	public String getPrefilter(){
		return prefilter;
	}

	@Override
 	public String toString(){
		return 
			"ChildItem{" + 
			"mainfilter = '" + mainfilter + '\'' + 
			",ecu = '" + ecu + '\'' + 
			",rail = '" + rail + '\'' + 
			",injector = '" + injector + '\'' + 
			",pumpNo = '" + pumpNo + '\'' + 
			",prefilter = '" + prefilter + '\'' + 
			"}";
		}
}
