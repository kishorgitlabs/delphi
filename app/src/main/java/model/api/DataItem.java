package model.api;

import java.util.List;

public class DataItem{
	private Object partTableData;
	private String application;
	private Object oldServiceList;
	private List<ChildItem> child;
	private String partNo;

	public void setPartTableData(Object partTableData){
		this.partTableData = partTableData;
	}

	public Object getPartTableData(){
		return partTableData;
	}

	public void setApplication(String application){
		this.application = application;
	}

	public String getApplication(){
		return application;
	}

	public void setOldServiceList(Object oldServiceList){
		this.oldServiceList = oldServiceList;
	}

	public Object getOldServiceList(){
		return oldServiceList;
	}

	public void setChild(List<ChildItem> child){
		this.child = child;
	}

	public List<ChildItem> getChild(){
		return child;
	}

	public void setPartNo(String partNo){
		this.partNo = partNo;
	}

	public String getPartNo(){
		return partNo;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"partTableData = '" + partTableData + '\'' + 
			",application = '" + application + '\'' + 
			",oldServiceList = '" + oldServiceList + '\'' + 
			",child = '" + child + '\'' + 
			",partNo = '" + partNo + '\'' + 
			"}";
		}
}