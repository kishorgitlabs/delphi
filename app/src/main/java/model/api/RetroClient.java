package model.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroClient {
//    private static final String ROOT_URL = "http://delphitvsjson.brainmagicllc.com/";
    private static final String ROOT_URL = "http://json.delphitvs-catalog.in/";
    public static final String APIKEY = "AIzaSyBRmYkOOy9QhrI53Fp3h_Tt8t7amWNa4Q0";
    // private static final String ROOT_URL = "http://10.0.2.2:54358/"
    /**
     * Get Retrofit Instance
     */
    private static Retrofit getRetrofitInstance() {



        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okClient())
                .build();
    }


    private static OkHttpClient okClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(45, TimeUnit.MINUTES)
                .writeTimeout(45, TimeUnit.MINUTES)
                .readTimeout(45, TimeUnit.MINUTES)
                .build();
    }


    /**
     * Get API Service
     *
     * @return API Service
     */
    public static APIService getApiService() {
        return getRetrofitInstance().create(APIService.class);
    }

}
