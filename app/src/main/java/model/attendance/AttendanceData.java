package model.attendance;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Awesome Pojo Generator
 * */
public class AttendanceData{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private AttendanceDataList data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(AttendanceDataList data){
   this.data=data;
  }
  public AttendanceDataList getData(){
   return data;
  }
}