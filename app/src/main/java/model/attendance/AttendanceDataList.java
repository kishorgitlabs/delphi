package model.attendance;

import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Awesome Pojo Generator
 * */

public class AttendanceDataList {
    @PrimaryKey(autoGenerate = true)
    private int pId;

    @ColumnInfo(name="OutLongitude")
    @SerializedName("OutLongitude")
    @Expose
    private String OutLongitude;
    @ColumnInfo(name="Designation")
    @SerializedName("Designation")
    @Expose
    private String Designation;
    @ColumnInfo(name="flag")
    @SerializedName("flag")
    @Expose
    private int flag;
    @ColumnInfo(name="Address")
    @SerializedName("Address")
    @Expose
    private String Address;
    @ColumnInfo(name="attendanceTime")
    @SerializedName("attendanceTime")
    @Expose
    private String attendanceTime;
    @ColumnInfo(name="OutTime")
    @SerializedName("OutTime")
    @Expose
    private String OutTime;
    @ColumnInfo(name="Disid")
    @SerializedName("Disid")
    @Expose
    private String Disid;
    @ColumnInfo(name="EmpCode")
    @SerializedName("EmpCode")
    @Expose
    private String EmpCode;
    @ColumnInfo(name="InLongitude")
    @SerializedName("InLongitude")
    @Expose
    private Double InLongitude;
    @ColumnInfo(name="Date")
    @SerializedName("Date")
    @Expose
    private String Date;
    @ColumnInfo(name="InLatitude")
    @SerializedName("InLatitude")
    @Expose
    private Double InLatitude;
    @ColumnInfo(name="Name")
    @SerializedName("Name")
    @Expose
    private String Name;
    @ColumnInfo(name="TotalDuration")
    @SerializedName("TotalDuration")
    @Expose
    private String TotalDuration;
    @ColumnInfo(name="OutAddress")
    @SerializedName("OutAddress")
    @Expose
    private String OutAddress;
    @ColumnInfo(name="AttendDay")
    @SerializedName("AttendDay")
    @Expose
    private String AttendDay;
    @ColumnInfo(name="InTime")
    @SerializedName("InTime")
    @Expose
    private String InTime;
    @ColumnInfo(name="OutLatitude")
    @SerializedName("OutLatitude")
    @Expose
    private String OutLatitude;
    @ColumnInfo(name="CreatedDate")
    @SerializedName("CreatedDate")
    @Expose
    private String CreatedDate;
    @ColumnInfo(name="id")
    @SerializedName("id")
    @Expose
    private int id;
    @ColumnInfo(name="EmpId")
    @SerializedName("EmpId")
    @Expose
    private int EmpId;
    @ColumnInfo(name="Regid")
    @SerializedName("Regid")
    @Expose
    private String Regid;

    public int getPId() {
        return pId;
    }

    public void setPId(int pId) {
        this.pId = pId;
    }
    public void setOutLongitude(String OutLongitude){
        this.OutLongitude=OutLongitude;
    }
    public String getOutLongitude(){
        return OutLongitude;
    }
    public void setDesignation(String Designation){
        this.Designation=Designation;
    }
    public String getDesignation(){
        return Designation;
    }
    public void setFlag(int flag){
        this.flag=flag;
    }
    public int getFlag(){
        return flag;
    }
    public void setAddress(String Address){
        this.Address=Address;
    }
    public String getAddress(){
        return Address;
    }
    public void setAttendanceTime(String attendanceTime){
        this.attendanceTime=attendanceTime;
    }
    public String getAttendanceTime(){
        return attendanceTime;
    }
    public void setOutTime(String OutTime){
        this.OutTime=OutTime;
    }
    public String getOutTime(){
        return OutTime;
    }
    public void setDisid(String Disid){
        this.Disid=Disid;
    }
    public String getDisid(){
        return Disid;
    }
    public void setEmpCode(String EmpCode){
        this.EmpCode=EmpCode;
    }
    public String getEmpCode(){
        return EmpCode;
    }
    public void setInLongitude(Double InLongitude){
        this.InLongitude=InLongitude;
    }
    public Double getInLongitude(){
        return InLongitude;
    }
    public void setDate(String Date){
        this.Date=Date;
    }
    public String getDate(){
        return Date;
    }
    public void setInLatitude(Double InLatitude){
        this.InLatitude=InLatitude;
    }
    public Double getInLatitude(){
        return InLatitude;
    }
    public void setName(String Name){
        this.Name=Name;
    }
    public String getName(){
        return Name;
    }
    public void setTotalDuration(String TotalDuration){
        this.TotalDuration=TotalDuration;
    }
    public String getTotalDuration(){
        return TotalDuration;
    }
    public void setOutAddress(String OutAddress){
        this.OutAddress=OutAddress;
    }
    public String getOutAddress(){
        return OutAddress;
    }
    public void setAttendDay(String AttendDay){
        this.AttendDay=AttendDay;
    }
    public String getAttendDay(){
        return AttendDay;
    }
    public void setInTime(String InTime){
        this.InTime=InTime;
    }
    public String getInTime(){
        return InTime;
    }
    public void setOutLatitude(String OutLatitude){
        this.OutLatitude=OutLatitude;
    }
    public String getOutLatitude(){
        return OutLatitude;
    }
    public void setCreatedDate(String CreatedDate){
        this.CreatedDate=CreatedDate;
    }
    public String getCreatedDate(){
        return CreatedDate;
    }
    public void setId(int id){
        this.id=id;
    }
    public int getId(){
        return id;
    }
    public void setEmpId(int EmpId){
        this.EmpId=EmpId;
    }
    public int getEmpId(){
        return EmpId;
    }
    public void setRegid(String Regid){
        this.Regid=Regid;
    }
    public String getRegid(){
        return Regid;
    }
}