
package model.changepassword;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ChangePasswordModel {

    @SerializedName("data")
    private ChangePasswordResult mData;
    @SerializedName("result")
    private String mResult;

    public ChangePasswordResult getData() {
        return mData;
    }

    public void setData(ChangePasswordResult data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
