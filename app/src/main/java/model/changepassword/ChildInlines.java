
package model.changepassword;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ChildInlines {

    @SerializedName("ecu")
    private String mEcu;
    @SerializedName("Governor")
    private String mGovernor;
    @SerializedName("injector")
    private String mInjector;
    @SerializedName("mainfilter")
    private String mMainfilter;
    @SerializedName("prefilter")
    private String mPrefilter;
    @SerializedName("PumpNo")
    private String mPumpNo;
    @SerializedName("rail")
    private String mRail;

    public String getEcu() {
        return mEcu;
    }

    public void setEcu(String ecu) {
        mEcu = ecu;
    }

    public String getGovernor() {
        return mGovernor;
    }

    public void setGovernor(String governor) {
        mGovernor = governor;
    }

    public String getInjector() {
        return mInjector;
    }

    public void setInjector(String injector) {
        mInjector = injector;
    }

    public String getMainfilter() {
        return mMainfilter;
    }

    public void setMainfilter(String mainfilter) {
        mMainfilter = mainfilter;
    }

    public String getPrefilter() {
        return mPrefilter;
    }

    public void setPrefilter(String prefilter) {
        mPrefilter = prefilter;
    }

    public String getPumpNo() {
        return mPumpNo;
    }

    public void setPumpNo(String pumpNo) {
        mPumpNo = pumpNo;
    }

    public Object getRail() {
        return mRail;
    }

    public void setRail(String rail) {
        mRail = rail;
    }

}
