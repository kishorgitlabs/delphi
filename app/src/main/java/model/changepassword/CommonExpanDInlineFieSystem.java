
package model.changepassword;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CommonExpanDInlineFieSystem {

    @SerializedName("data")
    private List<DatumInline> mData;
    @SerializedName("result")
    private String mResult;

    public List<DatumInline> getData() {
        return mData;
    }

    public void setData(List<DatumInline> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
