
package model.changepassword;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class DatumInline {

    @SerializedName("AppType")
    private Object mAppType;
    @SerializedName("application")
    private String mApplication;
    @SerializedName("Child")
    private List<ChildInlines> mChild;
    @SerializedName("oldServiceList")
    private Object mOldServiceList;
    @SerializedName("partNo")
    private String mPartNo;
    @SerializedName("partTableData")
    private Object mPartTableData;

    public Object getAppType() {
        return mAppType;
    }

    public void setAppType(Object appType) {
        mAppType = appType;
    }

    public String getApplication() {
        return mApplication;
    }

    public void setApplication(String application) {
        mApplication = application;
    }

    public List<ChildInlines> getChild() {
        return mChild;
    }

    public void setChild(List<ChildInlines> child) {
        mChild = child;
    }

    public Object getOldServiceList() {
        return mOldServiceList;
    }

    public void setOldServiceList(Object oldServiceList) {
        mOldServiceList = oldServiceList;
    }

    public String getPartNo() {
        return mPartNo;
    }

    public void setPartNo(String partNo) {
        mPartNo = partNo;
    }

    public Object getPartTableData() {
        return mPartTableData;
    }

    public void setPartTableData(Object partTableData) {
        mPartTableData = partTableData;
    }

}
