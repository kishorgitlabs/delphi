
package model.commonexpandablemodel;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Child {

    @SerializedName("ecu")
    private String mEcu;
    @SerializedName("injector")
    private String mInjector;
    @SerializedName("mainfilter")
    private String mMainfilter;
    @SerializedName("prefilter")
    private String mPrefilter;
    @SerializedName("rail")
    private String mRail;
    @SerializedName("PumpNo")
    private String mPumpNo;

    public String getEcu() {
        return mEcu;
    }

    public void setEcu(String ecu) {
        mEcu = ecu;
    }

    public String getInjector() {
        return mInjector;
    }

    public void setInjector(String injector) {
        mInjector = injector;
    }

    public String getMainfilter() {
        return mMainfilter;
    }

    public void setMainfilter(String mainfilter) {
        mMainfilter = mainfilter;
    }

    public String getmPumpNo() {
        return mPumpNo;
    }

    public void setmPumpNo(String mPumpNo) {
        this.mPumpNo = mPumpNo;
    }

    public String getPrefilter() {

        return mPrefilter;
    }

    public void setPrefilter(String prefilter) {
        mPrefilter = prefilter;
    }

    public String getRail() {
        return mRail;
    }

    public void setRail(String rail) {
        mRail = rail;
    }

}
