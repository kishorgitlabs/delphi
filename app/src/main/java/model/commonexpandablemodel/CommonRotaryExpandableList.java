
package model.commonexpandablemodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CommonRotaryExpandableList {

    @SerializedName("data")
    private List<ParentClass> mData;
    @SerializedName("result")
    private String mResult;

    public List<ParentClass> getData() {

        return mData;
    }

    public void setData(List<ParentClass> data)
    {
        mData = data;
    }

    public String getResult() {

        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
