
package model.commonexpandablemodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ParentClass {

    @SerializedName("application")
    private String mApplication;
    @SerializedName("Child")
    private List<Child> mChild;
    @SerializedName("oldServiceList")
    private List<OldServiceList> mOldServiceList;
    @SerializedName("partNo")
    private String mPartNo;
    @SerializedName("AppType")
    private String appType;
    @SerializedName("partTableData")
    private PartTableData mPartTableData;

    public String getAppType() {
        return appType;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }

    public String getApplication() {
        return mApplication;
    }

    public void setApplication(String application) {
        mApplication = application;
    }

    public List<Child> getChild() {
        return mChild;
    }

    public void setChild(List<Child> child) {
        mChild = child;
    }

    public List<OldServiceList> getOldServiceList() {
        return mOldServiceList;
    }

    public void setOldServiceList(List<OldServiceList> oldServiceList) {
        mOldServiceList = oldServiceList;
    }

    public String getPartNo() {
        return mPartNo;
    }

    public void setPartNo(String partNo) {
        mPartNo = partNo;
    }

    public PartTableData getPartTableData() {
        return mPartTableData;
    }

    public void setPartTableData(PartTableData partTableData) {
        mPartTableData = partTableData;
    }

}
