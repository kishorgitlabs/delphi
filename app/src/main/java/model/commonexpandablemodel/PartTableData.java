
package model.commonexpandablemodel;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PartTableData {

    @SerializedName("AlphaNumerical_Number")
    private Object mAlphaNumericalNumber;
    @SerializedName("Description")
    private Object mDescription;
    @SerializedName("ECU")
    private Object mECU;
    @SerializedName("Engine")
    private String mEngine;
    @SerializedName("HSN_Code")
    private Object mHSNCode;
    @SerializedName("id")
    private Long mId;
    @SerializedName("Image1")
    private String mImage1;
    @SerializedName("Injector")
    private String mInjector;
    @SerializedName("MRP")
    private Double mMRP;
    @SerializedName("Main_Category")
    private String mMainCategory;
    @SerializedName("Main_Filter")
    private String mMainFilter;
    @SerializedName("Model")
    private Object mModel;
    @SerializedName("OEM")
    private Object mOEM;
    @SerializedName("OE_Part_Number")
    private Object mOEPartNumber;
    @SerializedName("Part_Number")
    private String mPartNumber;
    @SerializedName("pdfname")
    private Object mPdfname;
    @SerializedName("Pre_Filter")
    private Object mPreFilter;
    @SerializedName("Product_Image")
    private Object mProductImage;
    @SerializedName("Product_Type")
    private Object mProductType;
    @SerializedName("Rail")
    private Object mRail;
    @SerializedName("Segment")
    private Object mSegment;
    @SerializedName("Sub_Category")
    private String mSubCategory;
    @SerializedName("Type")
    private String mType;
    @SerializedName("Vehicle_Application")
    private String mVehicleApplication;

    public Object getAlphaNumericalNumber() {
        return mAlphaNumericalNumber;
    }

    public void setAlphaNumericalNumber(Object alphaNumericalNumber) {
        mAlphaNumericalNumber = alphaNumericalNumber;
    }

    public Object getDescription() {
        return mDescription;
    }

    public void setDescription(Object description) {
        mDescription = description;
    }

    public Object getECU() {
        return mECU;
    }

    public void setECU(Object eCU) {
        mECU = eCU;
    }

    public String getEngine() {
        return mEngine;
    }

    public void setEngine(String engine) {
        mEngine = engine;
    }

    public Object getHSNCode() {
        return mHSNCode;
    }

    public void setHSNCode(Object hSNCode) {
        mHSNCode = hSNCode;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getImage1() {
        return mImage1;
    }

    public void setImage1(String image1) {
        mImage1 = image1;
    }

    public String getInjector() {
        return mInjector;
    }

    public void setInjector(String injector) {
        mInjector = injector;
    }

    public Double getMRP() {
        return mMRP;
    }

    public void setMRP(Double mRP) {
        mMRP = mRP;
    }

    public String getMainCategory() {
        return mMainCategory;
    }

    public void setMainCategory(String mainCategory) {
        mMainCategory = mainCategory;
    }

    public String getMainFilter() {
        return mMainFilter;
    }

    public void setMainFilter(String mainFilter) {
        mMainFilter = mainFilter;
    }

    public Object getModel() {
        return mModel;
    }

    public void setModel(Object model) {
        mModel = model;
    }

    public Object getOEM() {
        return mOEM;
    }

    public void setOEM(Object oEM) {
        mOEM = oEM;
    }

    public Object getOEPartNumber() {
        return mOEPartNumber;
    }

    public void setOEPartNumber(Object oEPartNumber) {
        mOEPartNumber = oEPartNumber;
    }

    public String getPartNumber() {
        return mPartNumber;
    }

    public void setPartNumber(String partNumber) {
        mPartNumber = partNumber;
    }

    public Object getPdfname() {
        return mPdfname;
    }

    public void setPdfname(Object pdfname) {
        mPdfname = pdfname;
    }

    public Object getPreFilter() {
        return mPreFilter;
    }

    public void setPreFilter(Object preFilter) {
        mPreFilter = preFilter;
    }

    public Object getProductImage() {
        return mProductImage;
    }

    public void setProductImage(Object productImage) {
        mProductImage = productImage;
    }

    public Object getProductType() {
        return mProductType;
    }

    public void setProductType(Object productType) {
        mProductType = productType;
    }

    public Object getRail() {
        return mRail;
    }

    public void setRail(Object rail) {
        mRail = rail;
    }

    public Object getSegment() {
        return mSegment;
    }

    public void setSegment(Object segment) {
        mSegment = segment;
    }

    public String getSubCategory() {
        return mSubCategory;
    }

    public void setSubCategory(String subCategory) {
        mSubCategory = subCategory;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getVehicleApplication() {
        return mVehicleApplication;
    }

    public void setVehicleApplication(String vehicleApplication) {
        mVehicleApplication = vehicleApplication;
    }

}
