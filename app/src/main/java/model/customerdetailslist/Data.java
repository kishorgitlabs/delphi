
package model.customerdetailslist;


import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class Data {

    @SerializedName("Approve")
    private String mApprove;
    @SerializedName("CompanyName")
    private String mCompanyName;
    @SerializedName("ContactPersonname")
    private String mContactPersonname;
    @SerializedName("DaelerRegion")
    private String mDaelerRegion;
    @SerializedName("DealerAddress")
    private String mDealerAddress;
    @SerializedName("DealerCity")
    private String mDealerCity;
    @SerializedName("DealerEmail")
    private String mDealerEmail;
    @SerializedName("DealerMobile")
    private String mDealerMobile;
    @SerializedName("DealerName")
    private String mDealerName;
    @SerializedName("DealerState")
    private String mDealerState;
    @SerializedName("dealercode")
    private String mDealercode;
    @SerializedName("deleteflag")
    private String mDeleteflag;
    @SerializedName("ExecutId")
    private String mExecutId;
    @SerializedName("Executecode")
    private String mExecutecode;
    @SerializedName("GST")
    private String mGST;
    @SerializedName("id")
    private String mId;
    @SerializedName("Image")
    private String mImage;
    @SerializedName("Insertdate")
    private String mInsertdate;
    @SerializedName("Pincode")
    private String mPincode;
    @SerializedName("reg_code")
    private String mRegCode;
    @SerializedName("RegName")
    private String mRegName;
    @SerializedName("Regid")
    private String mRegid;
    @SerializedName("ShopName")
    private String mShopName;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("UpdateDate")
    private String mUpdateDate;
    @SerializedName("UserType")
    private String mUserType;

    public String getApprove() {
        return mApprove;
    }

    public void setApprove(String approve) {
        mApprove = approve;
    }

    public String getCompanyName() {
        return mCompanyName;
    }

    public void setCompanyName(String companyName) {
        mCompanyName = companyName;
    }

    public String getContactPersonname() {
        return mContactPersonname;
    }

    public void setContactPersonname(String contactPersonname) {
        mContactPersonname = contactPersonname;
    }

    public String getDaelerRegion() {
        return mDaelerRegion;
    }

    public void setDaelerRegion(String daelerRegion) {
        mDaelerRegion = daelerRegion;
    }

    public String getDealerAddress() {
        return mDealerAddress;
    }

    public void setDealerAddress(String dealerAddress) {
        mDealerAddress = dealerAddress;
    }

    public String getDealerCity() {
        return mDealerCity;
    }

    public void setDealerCity(String dealerCity) {
        mDealerCity = dealerCity;
    }

    public String getDealerEmail() {
        return mDealerEmail;
    }

    public void setDealerEmail(String dealerEmail) {
        mDealerEmail = dealerEmail;
    }

    public String getDealerMobile() {
        return mDealerMobile;
    }

    public void setDealerMobile(String dealerMobile) {
        mDealerMobile = dealerMobile;
    }

    public String getDealerName() {
        return mDealerName;
    }

    public void setDealerName(String dealerName) {
        mDealerName = dealerName;
    }

    public String getDealerState() {
        return mDealerState;
    }

    public void setDealerState(String dealerState) {
        mDealerState = dealerState;
    }

    public String getDealercode() {
        return mDealercode;
    }

    public void setDealercode(String dealercode) {
        mDealercode = dealercode;
    }

    public String getDeleteflag() {
        return mDeleteflag;
    }

    public void setDeleteflag(String deleteflag) {
        mDeleteflag = deleteflag;
    }

    public String getExecutId() {
        return mExecutId;
    }

    public void setExecutId(String executId) {
        mExecutId = executId;
    }

    public String getExecutecode() {
        return mExecutecode;
    }

    public void setExecutecode(String executecode) {
        mExecutecode = executecode;
    }

    public String getGST() {
        return mGST;
    }

    public void setGST(String gST) {
        mGST = gST;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public String getInsertdate() {
        return mInsertdate;
    }

    public void setInsertdate(String insertdate) {
        mInsertdate = insertdate;
    }

    public String getPincode() {
        return mPincode;
    }

    public void setPincode(String pincode) {
        mPincode = pincode;
    }

    public String getRegCode() {
        return mRegCode;
    }

    public void setRegCode(String regCode) {
        mRegCode = regCode;
    }

    public String getRegName() {
        return mRegName;
    }

    public void setRegName(String regName) {
        mRegName = regName;
    }

    public String getRegid() {
        return mRegid;
    }

    public void setRegid(String regid) {
        mRegid = regid;
    }

    public String getShopName() {
        return mShopName;
    }

    public void setShopName(String shopName) {
        mShopName = shopName;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getUpdateDate() {
        return mUpdateDate;
    }

    public void setUpdateDate(String updateDate) {
        mUpdateDate = updateDate;
    }

    public String getUserType() {
        return mUserType;
    }

    public void setUserType(String userType) {
        mUserType = userType;
    }

}
