
package model.customerlist;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class Datas {

    @SerializedName("ExecutiveCode")
    private String mExecutiveCode;
    @SerializedName("id")
    private String mId;
    @SerializedName("Name")
    private String mName;

    public String getExecutiveCode() {
        return mExecutiveCode;
    }

    public void setExecutiveCode(String executiveCode) {
        mExecutiveCode = executiveCode;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getId() {
        return mId;
    }


    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

}
