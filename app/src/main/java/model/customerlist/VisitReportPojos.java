
package model.customerlist;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class VisitReportPojos {

    @SerializedName("data")
    private List<Datas> mData;
    @SerializedName("result")
    private String mResult;

    public List<Datas> getData() {
        return mData;
    }

    public void setData(List<Datas> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
