
package model.distributorlist;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


@SuppressWarnings("unused")
public class DistributorData implements Serializable {

    @SerializedName("City")
    private String mCity;
    @SerializedName("CustomerCode")
    private String mCustomerCode;
    @SerializedName("id")
    private String mId;
    @SerializedName("InsertDate")
    private Object mInsertDate;
    @SerializedName("Name")
    private String mName;
    @SerializedName("Pincode")
    private String mPincode;
    @SerializedName("Region")
    private String mRegion;
    @SerializedName("SalesOffice")
    private String mSalesOffice;
    @SerializedName("Street")
    private String mStreet;
    @SerializedName("Street2")
    private String mStreet2;
    @SerializedName("Street3")
    private String mStreet3;
    @SerializedName("Type")
    private Object mType;

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getCustomerCode() {
        return mCustomerCode;
    }

    public void setCustomerCode(String customerCode) {
        mCustomerCode = customerCode;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public Object getInsertDate() {
        return mInsertDate;
    }

    public void setInsertDate(Object insertDate) {
        mInsertDate = insertDate;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPincode() {
        return mPincode;
    }

    public void setPincode(String pincode) {
        mPincode = pincode;
    }

    public String getRegion() {
        return mRegion;
    }

    public void setRegion(String region) {
        mRegion = region;
    }

    public String getSalesOffice() {
        return mSalesOffice;
    }

    public void setSalesOffice(String salesOffice) {
        mSalesOffice = salesOffice;
    }

    public String getStreet() {
        return mStreet;
    }

    public void setStreet(String street) {
        mStreet = street;
    }

    public String getStreet2() {
        return mStreet2;
    }

    public void setStreet2(String street2) {
        mStreet2 = street2;
    }

    public String getStreet3() {
        return mStreet3;
    }

    public void setStreet3(String street3) {
        mStreet3 = street3;
    }

    public Object getType() {
        return mType;
    }

    public void setType(Object type) {
        mType = type;
    }

}
