
package model.distributorlist;

import java.util.List;
import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class DistributorList {

    @SerializedName("data")
    private List<DistributorData> mData;
    @SerializedName("result")
    private String mResult;

    public List<DistributorData> getData() {
        return mData;
    }

    public void setData(List<DistributorData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
