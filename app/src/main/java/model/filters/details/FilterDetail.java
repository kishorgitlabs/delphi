
package model.filters.details;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class FilterDetail{

    @SerializedName("data")
    private List<FilterDetailList> mData;
    @SerializedName("result")
    private String mResult;

    public List<FilterDetailList> getData() {
        return mData;
    }

    public void setData(List<FilterDetailList> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
