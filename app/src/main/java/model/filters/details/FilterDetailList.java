
package model.filters.details;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class FilterDetailList{

    //Rail,Injector,Main Filter, Pre Filter, ECU, Engine

    @SerializedName("AlphaNumerical_Number")
    private String mAlphaNumericalNumber;
    @SerializedName("Image1")
    private String mImage;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("HSN_Code")
    private String mHSNCode;
    @SerializedName("id")
    private Long mId;
    @SerializedName("MRP")
    private String mMRP;
    @SerializedName("Main_Category")
    private String mMainCategory;
    @SerializedName("OE_Part_Number")
    private String mOEPartNumber;
    @SerializedName("Part_Number")
    private String mPartNumber;
    @SerializedName("Product_Image")
    private String mProductImage;
    @SerializedName("Product_Type")
    private String mProductType;
    @SerializedName("Segment")
    private String mSegment;
    @SerializedName("Sub_Category")
    private String mSubCategory;
    @SerializedName("Type")
    private String mType;
    @SerializedName("Vehicle_Application")
    private String mVehicleApplication;


    public String getmImage() {
        return mImage;
    }

    public void setmImage(String mImage) {
        this.mImage = mImage;
    }


    public String getAlphaNumericalNumber() {
        return mAlphaNumericalNumber;
    }

    public void setAlphaNumericalNumber(String alphaNumericalNumber) {
        mAlphaNumericalNumber = alphaNumericalNumber;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getHSNCode() {
        return mHSNCode;
    }

    public void setHSNCode(String hSNCode) {
        mHSNCode = hSNCode;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getMRP() {
        return mMRP;
    }

    public void setMRP(String mRP) {
        mMRP = mRP;
    }

    public String getMainCategory() {
        return mMainCategory;
    }

    public void setMainCategory(String mainCategory) {
        mMainCategory = mainCategory;
    }

    public String getOEPartNumber() {
        return mOEPartNumber;
    }

    public void setOEPartNumber(String oEPartNumber) {
        mOEPartNumber = oEPartNumber;
    }

    public String getPartNumber() {
        return mPartNumber;
    }

    public void setPartNumber(String partNumber) {
        mPartNumber = partNumber;
    }

    public String getProductImage() {
        return mProductImage;
    }

    public void setProductImage(String productImage) {
        mProductImage = productImage;
    }

    public String getProductType() {
        return mProductType;
    }

    public void setProductType(String productType) {
        mProductType = productType;
    }

    public String getSegment() {
        return mSegment;
    }

    public void setSegment(String segment) {
        mSegment = segment;
    }

    public String getSubCategory() {
        return mSubCategory;
    }

    public void setSubCategory(String subCategory) {
        mSubCategory = subCategory;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getVehicleApplication() {
        return mVehicleApplication;
    }

    public void setVehicleApplication(String vehicleApplication) {
        mVehicleApplication = vehicleApplication;
    }

}
