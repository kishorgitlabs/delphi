
package model.general.Categorydata;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CategoryData {

    @SerializedName("data")
    private List<CategoryItems> mData;
    @SerializedName("result")
    private String mResult;

    public List<CategoryItems> getData() {
        return mData;
    }

    public void setData(List<CategoryItems> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
