
package model.general.Categorydata;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CategoryItems {

    @SerializedName("BrochureName")
    private String mBrochureName;
    @SerializedName("Category")
    private String mCategory;
    @SerializedName("id")
    private Long mId;
    @SerializedName("InsertDate")
    private String mInsertDate;
    @SerializedName("UploadFile")
    private String mUploadFile;
    @SerializedName("uploadbrochureFile")
    private String mUploadbrochureFile;

    public String getBrochureName() {
        return mBrochureName;
    }

    public void setBrochureName(String brochureName) {
        mBrochureName = brochureName;
    }

    public String getCategory() {
        return mCategory;
    }

    public void setCategory(String category) {
        mCategory = category;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getInsertDate() {
        return mInsertDate;
    }

    public void setInsertDate(String insertDate) {
        mInsertDate = insertDate;
    }

    public String getUploadFile() {
        return mUploadFile;
    }

    public void setUploadFile(String uploadFile) {
        mUploadFile = uploadFile;
    }

    public String getUploadbrochureFile() {
        return mUploadbrochureFile;
    }

    public void setUploadbrochureFile(String uploadbrochureFile) {
        mUploadbrochureFile = uploadbrochureFile;
    }

}
