
package model.general.categorylist;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CategoryData {

    @SerializedName("Category")
    private String mCategory;
    @SerializedName("id")
    private Long mId;
    @SerializedName("InsertDate")
    private Object mInsertDate;

    public String getCategory() {
        return mCategory;
    }

    public void setCategory(String category) {
        mCategory = category;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Object getInsertDate() {
        return mInsertDate;
    }

    public void setInsertDate(Object insertDate) {
        mInsertDate = insertDate;
    }

}
