
package model.general.categorylist;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CategoryList {

    @SerializedName("data")
    private List<CategoryData> mData;
    @SerializedName("result")
    private String mResult;

    public List<CategoryData> getData() {
        return mData;
    }

    public void setData(List<CategoryData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
