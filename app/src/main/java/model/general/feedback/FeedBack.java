
package model.general.feedback;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class FeedBack {

    @SerializedName("city")
    private String mCity;
    @SerializedName("company")
    private String mCompany;
    @SerializedName("contacttype")
    private String mContacttype;
    @SerializedName("Date")
    private String mDate;
    @SerializedName("emailid")
    private String mEmailid;
    @SerializedName("Enquiry_id")
    private Long mEnquiryId;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("mobileno")
    private String mMobileno;
    @SerializedName("name")
    private String mName;
    @SerializedName("Partno")
    private String mPartno;

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getCompany() {
        return mCompany;
    }

    public void setCompany(String company) {
        mCompany = company;
    }

    public String getContacttype() {
        return mContacttype;
    }

    public void setContacttype(String contacttype) {
        mContacttype = contacttype;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getEmailid() {
        return mEmailid;
    }

    public void setEmailid(String emailid) {
        mEmailid = emailid;
    }

    public Long getEnquiryId() {
        return mEnquiryId;
    }

    public void setEnquiryId(Long enquiryId) {
        mEnquiryId = enquiryId;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getMobileno() {
        return mMobileno;
    }

    public void setMobileno(String mobileno) {
        mMobileno = mobileno;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPartno() {
        return mPartno;
    }

    public void setPartno(String partno) {
        mPartno = partno;
    }

}
