
package model.general.feedback;

import com.google.gson.annotations.SerializedName;
@SuppressWarnings("unused")
public class FeedBackModel {

    @SerializedName("data")
    private FeedBack mData;
    @SerializedName("result")
    private String mResult;

    public FeedBack getData() {
        return mData;
    }

    public void setData(FeedBack data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
