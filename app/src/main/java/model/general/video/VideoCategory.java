
package model.general.video;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class VideoCategory {

    @SerializedName("data")
    private List<VideoList> mData;
    @SerializedName("result")
    private String mResult;

    public List<VideoList> getData() {
        return mData;
    }

    public void setData(List<VideoList> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
