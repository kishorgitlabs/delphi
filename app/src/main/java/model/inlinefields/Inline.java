package model.inlinefields;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Inline {
  @SerializedName("DTVSPartno")
  @Expose
  private String DTVSPartno;
  @SerializedName("EquivalentPartNo")
  @Expose
  private String EquivalentPartNo;
  @SerializedName("UOM")
  @Expose
  private String UOM;
  @SerializedName("Description")
  @Expose
  private String Description;
  @SerializedName("Category")
  @Expose
  private String Category;
  @SerializedName("CustomerApplication")
  @Expose
  private String CustomerApplication;
  @SerializedName("UOS")
  @Expose
  private String UOS;
  @SerializedName("GST")
  @Expose
  private String GST;
  @SerializedName("MRP")
  @Expose
  private String MRP;
  @SerializedName("id")
  @Expose
  private String id;
  @SerializedName("Supply_from")
  @Expose
  private String Supply_from;
  @SerializedName("HSNCODE")
  @Expose
  private String HSNCODE;
  public void setDTVSPartno(String DTVSPartno){
   this.DTVSPartno=DTVSPartno;
  }
  public String getDTVSPartno(){
   return DTVSPartno;
  }
  public void setEquivalentPartNo(String EquivalentPartNo){
   this.EquivalentPartNo=EquivalentPartNo;
  }
  public String getEquivalentPartNo(){
   return EquivalentPartNo;
  }
  public void setUOM(String UOM){
   this.UOM=UOM;
  }
  public String getUOM(){
   return UOM;
  }
  public void setDescription(String Description){
   this.Description=Description;
  }
  public String getDescription(){
   return Description;
  }
  public void setCategory(String Category){
   this.Category=Category;
  }
  public String getCategory(){
   return Category;
  }
  public void setCustomerApplication(String CustomerApplication){
   this.CustomerApplication=CustomerApplication;
  }
  public String getCustomerApplication(){
   return CustomerApplication;
  }
  public void setUOS(String UOS){
   this.UOS=UOS;
  }
  public String getUOS(){
   return UOS;
  }
  public void setGST(String GST){
   this.GST=GST;
  }
  public String getGST(){
   return GST;
  }
  public void setMRP(String MRP){
   this.MRP=MRP;
  }
  public String getMRP(){
   return MRP;
  }
  public void setId(String id){
   this.id=id;
  }
  public String getId(){
   return id;
  }
  public void setSupply_from(String Supply_from){
   this.Supply_from=Supply_from;
  }
  public String getSupply_from(){
   return Supply_from;
  }
  public void setHSNCODE(String HSNCODE){
   this.HSNCODE=HSNCODE;
  }
  public String getHSNCODE(){
   return HSNCODE;
  }
}