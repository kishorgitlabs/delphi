package model.inlinefields;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class InlineData{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private List<Inline> data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(List<Inline> data){
   this.data=data;
  }
  public List<Inline> getData(){
   return data;
  }
}