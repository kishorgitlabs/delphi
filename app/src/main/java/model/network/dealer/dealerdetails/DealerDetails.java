
package model.network.dealer.dealerdetails;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class DealerDetails implements Serializable {

    @SerializedName("Addressline1")
    private String mAddressline1;
    @SerializedName("Addressline2")
    private String mAddressline2;
    @SerializedName("Addressline3")
    private String mAddressline3;
    @SerializedName("Category")
    private String mCategory;
    @SerializedName("City")
    private String mCity;
    @SerializedName("ContactNumber")
    private String mContactNumber;
    @SerializedName("Contactperson")
    private String mContactperson;
    @SerializedName("CustomerCode")
    private String mCustomerCode;
    @SerializedName("DIAGNOSTICKITAVAILABILITY")
    private String mDIAGNOSTICKITAVAILABILITY;
    @SerializedName("District")
    private String mDistrict;
    @SerializedName("Email")
    private String mEmail;
    @SerializedName("id")
    private Long mId;
    @SerializedName("JcbSetup")
    private String mJcbSetup;
    @SerializedName("NameOfServiceProvider")
    private String mNameOfServiceProvider;
    @SerializedName("Pincode")
    private String mPincode;
    @SerializedName("Region")
    private String mRegion;
    @SerializedName("State")
    private String mState;
    @SerializedName("Status")
    private String mStatus;

    public String getAddressline1() {
        return mAddressline1;
    }

    public void setAddressline1(String addressline1) {
        mAddressline1 = addressline1;
    }

    public String getAddressline2() {
        return mAddressline2;
    }

    public void setAddressline2(String addressline2) {
        mAddressline2 = addressline2;
    }

    public String getAddressline3() {
        return mAddressline3;
    }

    public void setAddressline3(String addressline3) {
        mAddressline3 = addressline3;
    }

    public String getCategory() {
        return mCategory;
    }

    public void setCategory(String category) {
        mCategory = category;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getContactNumber() {
        return mContactNumber;
    }

    public void setContactNumber(String contactNumber) {
        mContactNumber = contactNumber;
    }

    public String getContactperson() {
        return mContactperson;
    }

    public void setContactperson(String contactperson) {
        mContactperson = contactperson;
    }

    public String getCustomerCode() {
        return mCustomerCode;
    }

    public void setCustomerCode(String customerCode) {
        mCustomerCode = customerCode;
    }

    public String getDIAGNOSTICKITAVAILABILITY() {
        return mDIAGNOSTICKITAVAILABILITY;
    }

    public void setDIAGNOSTICKITAVAILABILITY(String dIAGNOSTICKITAVAILABILITY) {
        mDIAGNOSTICKITAVAILABILITY = dIAGNOSTICKITAVAILABILITY;
    }

    public String getDistrict() {
        return mDistrict;
    }

    public void setDistrict(String district) {
        mDistrict = district;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getJcbSetup() {
        return mJcbSetup;
    }

    public void setJcbSetup(String jcbSetup) {
        mJcbSetup = jcbSetup;
    }

    public String getNameOfServiceProvider() {
        return mNameOfServiceProvider;
    }

    public void setNameOfServiceProvider(String nameOfServiceProvider) {
        mNameOfServiceProvider = nameOfServiceProvider;
    }

    public String getPincode() {
        return mPincode;
    }

    public void setPincode(String pincode) {
        mPincode = pincode;
    }

    public String getRegion() {
        return mRegion;
    }

    public void setRegion(String region) {
        mRegion = region;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

}
