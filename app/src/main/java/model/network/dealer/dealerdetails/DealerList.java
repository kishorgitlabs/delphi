
package model.network.dealer.dealerdetails;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class DealerList implements Serializable {

    @SerializedName("data")
    private List<DealerDetails> mData;
    @SerializedName("result")
    private String mResult;

    public List<DealerDetails> getData() {
        return mData;
    }

    public void setData(List<DealerDetails> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
