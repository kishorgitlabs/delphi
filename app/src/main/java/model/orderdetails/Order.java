
package model.orderdetails;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Order {

    @SerializedName("ContactPerson")
    private String mContactPerson;
    @SerializedName("CusId")
    private String mCusId;
    @SerializedName("CustomerType")
    private String mCustomerType;
    @SerializedName("DisId")
    private String mDisId;
    @SerializedName("ExeId")
    private String mExeId;
    @SerializedName("OrderDate")
    private String mOrderDate;
    @SerializedName("RegId")
    private String mRegId;
    @SerializedName("ShopName")
    private String mShopName;
    @SerializedName("Total")
    private String mTotal;

    public String getContactPerson() {
        return mContactPerson;
    }

    public void setContactPerson(String contactPerson) {
        mContactPerson = contactPerson;
    }

    public String getCusId() {
        return mCusId;
    }

    public void setCusId(String cusId) {
        mCusId = cusId;
    }

    public String getCustomerType() {
        return mCustomerType;
    }

    public void setCustomerType(String customerType) {
        mCustomerType = customerType;
    }

    public String getDisId() {
        return mDisId;
    }

    public void setDisId(String disId) {
        mDisId = disId;
    }

    public String getExeId() {
        return mExeId;
    }

    public void setExeId(String exeId) {
        mExeId = exeId;
    }

    public String getOrderDate() {
        return mOrderDate;
    }

    public void setOrderDate(String orderDate) {
        mOrderDate = orderDate;
    }

    public String getRegId() {
        return mRegId;
    }

    public void setRegId(String regId) {
        mRegId = regId;
    }

    public String getShopName() {
        return mShopName;
    }

    public void setShopName(String shopName) {
        mShopName = shopName;
    }

    public String getTotal() {
        return mTotal;
    }

    public void setTotal(String total) {
        mTotal = total;
    }

}
