
package model.orderdetails;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class OrderDetail {

    @SerializedName("date")
    private String mDate;
    @SerializedName("ExeName")
    private String mExeName;
    @SerializedName("Exeid")
    private Long mExeid;
    @SerializedName("total")
    private String mTotal;

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getExeName() {
        return mExeName;
    }

    public void setExeName(String exeName) {
        mExeName = exeName;
    }

    public Long getExeid() {
        return mExeid;
    }

    public void setExeid(Long exeid) {
        mExeid = exeid;
    }

    public String getTotal() {
        return mTotal;
    }

    public void setTotal(String total) {
        mTotal = total;
    }

}
