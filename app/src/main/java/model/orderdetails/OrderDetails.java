
package model.orderdetails;

import java.util.List;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class OrderDetails {

    @SerializedName("Order")
    private Order mOrder;
    @SerializedName("Order_Parts")
    private List<OrderPart> mOrderParts;


    public Order getOrder() {
        return mOrder;
    }

    public void setOrder(Order order) {
        mOrder = order;
    }

    public List<OrderPart> getOrderParts() {
        return mOrderParts;
    }

    public void setOrderParts(List<OrderPart> orderParts) {
        mOrderParts = orderParts;
    }

}
