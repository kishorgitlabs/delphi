
package model.orderdetails;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class OrderPart {

    @SerializedName("AmountPerUnit")
    private String mAmountPerUnit;
    @SerializedName("GrandTotal")
    private String mGrandTotal;
    @SerializedName("PartNo")
    private String mPartNo;
    @SerializedName("ProductName")
    private String mProductName;
    @SerializedName("Quantity")
    private String mQuantity;
    @SerializedName("Segment")
    private String mSegment;
    @SerializedName("ExeId")
    private String mExeId;

    public String getExeId() {
        return mExeId;
    }

    public void setExeId(String mExeId) {
        this.mExeId = mExeId;
    }

    public String getAmountPerUnit() {
        return mAmountPerUnit;
    }

    public void setAmountPerUnit(String amountPerUnit) {
        mAmountPerUnit = amountPerUnit;
    }

    public String getGrandTotal() {
        return mGrandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        mGrandTotal = grandTotal;
    }

    public String getPartNo() {
        return mPartNo;
    }

    public void setPartNo(String partNo) {
        mPartNo = partNo;
    }

    public String getProductName() {
        return mProductName;
    }

    public void setProductName(String productName) {
        mProductName = productName;
    }

    public String getQuantity() {
        return mQuantity;
    }

    public void setQuantity(String quantity) {
        mQuantity = quantity;
    }

    public String getSegment() {
        return mSegment;
    }

    public void setSegment(String segment) {
        mSegment = segment;
    }

}
