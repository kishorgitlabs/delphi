
package model.partdetails;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class OrderList {

    @SerializedName("ContactPerson")
    private String mContactPerson;
    @SerializedName("CusId")
    private Object mCusId;
    @SerializedName("CustomerType")
    private String mCustomerType;
    @SerializedName("DisId")
    private Long mDisId;
    @SerializedName("EXid")
    private String mEXid;
    @SerializedName("ExeId")
    private String mExeId;
    @SerializedName("fromdate")
    private String mFromdate;
    @SerializedName("id")
    private Long mId;
    @SerializedName("InsertDate")
    private String mInsertDate;
    @SerializedName("OrderDate")
    private String mOrderDate;
    @SerializedName("OrderType")
    private Object mOrderType;
    @SerializedName("Orderid")
    private String mOrderid;
    @SerializedName("RegId")
    private Long mRegId;
    @SerializedName("ShopName")
    private Object mShopName;
    @SerializedName("todate")
    private String mTodate;
    @SerializedName("Total")
    private String mTotal;
    @SerializedName("ExeName")
    private String ExeName;

    public String getExeName() {
        return ExeName;
    }

    public void setExeName(String exeName) {
        ExeName = exeName;
    }

    public String getDiscode() {
        return discode;
    }

    public void setDiscode(String discode) {
        this.discode = discode;
    }

    public String getDisname() {
        return disname;
    }

    public void setDisname(String disname) {
        this.disname = disname;
    }

    @SerializedName("dis")
    private String discode;
    @SerializedName("disname")
    private String disname;

    public String getContactPerson() {
        return mContactPerson;
    }

    public void setContactPerson(String contactPerson) {
        mContactPerson = contactPerson;
    }

    public Object getCusId() {
        return mCusId;
    }

    public void setCusId(Object cusId) {
        mCusId = cusId;
    }

    public String getCustomerType() {
        return mCustomerType;
    }

    public void setCustomerType(String customerType) {
        mCustomerType = customerType;
    }

    public Long getDisId() {
        return mDisId;
    }

    public void setDisId(Long disId) {
        mDisId = disId;
    }

    public String getEXid() {
        return mEXid;
    }

    public void setEXid(String eXid) {
        mEXid = eXid;
    }

    public String getExeId() {
        return mExeId;
    }

    public void setExeId(String exeId) {
        mExeId = exeId;
    }

    public String getFromdate() {
        return mFromdate;
    }

    public void setFromdate(String fromdate) {
        mFromdate = fromdate;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getInsertDate() {
        return mInsertDate;
    }

    public void setInsertDate(String insertDate) {
        mInsertDate = insertDate;
    }

    public String getOrderDate() {
        return mOrderDate;
    }

    public void setOrderDate(String orderDate) {
        mOrderDate = orderDate;
    }

    public Object getOrderType() {
        return mOrderType;
    }

    public void setOrderType(Object orderType) {
        mOrderType = orderType;
    }

    public String getOrderid() {
        return mOrderid;
    }

    public void setOrderid(String orderid) {
        mOrderid = orderid;
    }

    public Long getRegId() {
        return mRegId;
    }

    public void setRegId(Long regId) {
        mRegId = regId;
    }

    public Object getShopName() {
        return mShopName;
    }

    public void setShopName(Object shopName) {
        mShopName = shopName;
    }

    public String getTodate() {
        return mTodate;
    }

    public void setTodate(String todate) {
        mTodate = todate;
    }

    public String getTotal() {
        return mTotal;
    }

    public void setTotal(String total) {
        mTotal = total;
    }

}
