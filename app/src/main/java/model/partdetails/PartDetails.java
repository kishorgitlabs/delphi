
package model.partdetails;

import java.util.List;
import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class PartDetails {

    @SerializedName("data")
    private List<PartDetailsData> mData;
    @SerializedName("result")
    private String mResult;

    public List<PartDetailsData> getData() {
        return mData;
    }

    public void setData(List<PartDetailsData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
