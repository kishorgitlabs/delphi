
package model.partdetails;


import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class PartDetailsData {

    @SerializedName("AlphaNumerical_Number")
    private String mAlphaNumericalNumber;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("ECU")
    private String mECU;
    @SerializedName("Engine")
    private String mEngine;
    @SerializedName("HSN_Code")
    private String mHSNCode;
    @SerializedName("id")
    private String mId;
    @SerializedName("Image1")
    private String mImage1;
    @SerializedName("Injector")
    private String mInjector;
    @SerializedName("MRP")
    private String mMRP;
    @SerializedName("Main_Category")
    private String mMainCategory;
    @SerializedName("Main_Filter")
    private String mMainFilter;
    @SerializedName("Model")
    private String mModel;
    @SerializedName("OEM")
    private String mOEM;
    @SerializedName("OE_Part_Number")
    private String mOEPartNumber;
    @SerializedName("Part_Number")
    private String mPartNumber;
    @SerializedName("pdfname")
    private String mPdfname;
    @SerializedName("Pre_Filter")
    private String mPreFilter;
    @SerializedName("Product_Image")
    private String mProductImage;
    @SerializedName("Product_Type")
    private String mProductType;
    @SerializedName("Rail")
    private String mRail;
    @SerializedName("Segment")
    private String mSegment;
    @SerializedName("Sub_Category")
    private String mSubCategory;
    @SerializedName("Type")
    private String mType;
    @SerializedName("Vehicle_Application")
    private String mVehicleApplication;
    @SerializedName("PartNo")
    private String partNo;

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public String getAlphaNumericalNumber() {
        return mAlphaNumericalNumber;
    }

    public void setAlphaNumericalNumber(String alphaNumericalNumber) {
        mAlphaNumericalNumber = alphaNumericalNumber;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getECU() {
        return mECU;
    }

    public void setECU(String eCU) {
        mECU = eCU;
    }

    public String getEngine() {
        return mEngine;
    }

    public void setEngine(String engine) {
        mEngine = engine;
    }

    public String getHSNCode() {
        return mHSNCode;
    }

    public void setHSNCode(String hSNCode) {
        mHSNCode = hSNCode;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getImage1() {
        return mImage1;
    }

    public void setImage1(String image1) {
        mImage1 = image1;
    }

    public String getInjector() {
        return mInjector;
    }

    public void setInjector(String injector) {
        mInjector = injector;
    }

    public String getMRP() {
        return mMRP;
    }

    public void setMRP(String mRP) {
        mMRP = mRP;
    }

    public String getMainCategory() {
        return mMainCategory;
    }

    public void setMainCategory(String mainCategory) {
        mMainCategory = mainCategory;
    }

    public String getMainFilter() {
        return mMainFilter;
    }

    public void setMainFilter(String mainFilter) {
        mMainFilter = mainFilter;
    }

    public String getModel() {
        return mModel;
    }

    public void setModel(String model) {
        mModel = model;
    }

    public String getOEM() {
        return mOEM;
    }

    public void setOEM(String oEM) {
        mOEM = oEM;
    }

    public String getOEPartNumber() {
        return mOEPartNumber;
    }

    public void setOEPartNumber(String oEPartNumber) {
        mOEPartNumber = oEPartNumber;
    }

    public String getPartNumber() {
        return mPartNumber;
    }

    public void setPartNumber(String partNumber) {
        mPartNumber = partNumber;
    }

    public String getPdfname() {
        return mPdfname;
    }

    public void setPdfname(String pdfname) {
        mPdfname = pdfname;
    }

    public String getPreFilter() {
        return mPreFilter;
    }

    public void setPreFilter(String preFilter) {
        mPreFilter = preFilter;
    }

    public String getProductImage() {
        return mProductImage;
    }

    public void setProductImage(String productImage) {
        mProductImage = productImage;
    }

    public String getProductType() {
        return mProductType;
    }

    public void setProductType(String productType) {
        mProductType = productType;
    }

    public String getRail() {
        return mRail;
    }

    public void setRail(String rail) {
        mRail = rail;
    }

    public String getSegment() {
        return mSegment;
    }

    public void setSegment(String segment) {
        mSegment = segment;
    }

    public String getSubCategory() {
        return mSubCategory;
    }

    public void setSubCategory(String subCategory) {
        mSubCategory = subCategory;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getVehicleApplication() {
        return mVehicleApplication;
    }

    public void setVehicleApplication(String vehicleApplication) {
        mVehicleApplication = vehicleApplication;
    }

}
