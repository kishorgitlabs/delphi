
package model.partnumbersearch;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PartNumberData {

    @SerializedName("AlphaNumerical_Number")
    private String mAlphaNumericalNumber;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("ECU")
    private String mECU;
    @SerializedName("Engine")
    private Object mEngine;
    @SerializedName("HSN_Code")
    private String mHSNCode;
    @SerializedName("id")
    private Long mId;
    @SerializedName("Injector")
    private String mInjector;
    @SerializedName("MRP")
    private Object mMRP;
    @SerializedName("Main_Category")
    private String mMainCategory;
    @SerializedName("Main_Filter")
    private String mMainFilter;
    @SerializedName("OE_Part_Number")
    private String mOEPartNumber;
    @SerializedName("Part_Number")
    private String mPartNumber;
    @SerializedName("Pre_Filter")
    private String mPreFilter;
    @SerializedName("Product_Image")
    private String mProductImage;
    @SerializedName("Product_Type")
    private String mProductType;
    @SerializedName("Rail")
    private String mRail;
    @SerializedName("Segment")
    private String mSegment;
    @SerializedName("Sub_Category")
    private String mSubCategory;
    @SerializedName("Type")
    private String mType;
    @SerializedName("Vehicle_Application")
    private String mVehicleApplication;

    public String getAlphaNumericalNumber() {
        return mAlphaNumericalNumber;
    }

    public void setAlphaNumericalNumber(String alphaNumericalNumber) {
        mAlphaNumericalNumber = alphaNumericalNumber;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getECU() {
        return mECU;
    }

    public void setECU(String eCU) {
        mECU = eCU;
    }

    public Object getEngine() {
        return mEngine;
    }

    public void setEngine(Object engine) {
        mEngine = engine;
    }

    public String getHSNCode() {
        return mHSNCode;
    }

    public void setHSNCode(String hSNCode) {
        mHSNCode = hSNCode;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getInjector() {
        return mInjector;
    }

    public void setInjector(String injector) {
        mInjector = injector;
    }

    public Object getMRP() {
        return mMRP;
    }

    public void setMRP(Object mRP) {
        mMRP = mRP;
    }

    public String getMainCategory() {
        return mMainCategory;
    }

    public void setMainCategory(String mainCategory) {
        mMainCategory = mainCategory;
    }

    public String getMainFilter() {
        return mMainFilter;
    }

    public void setMainFilter(String mainFilter) {
        mMainFilter = mainFilter;
    }

    public String getOEPartNumber() {
        return mOEPartNumber;
    }

    public void setOEPartNumber(String oEPartNumber) {
        mOEPartNumber = oEPartNumber;
    }

    public String getPartNumber() {
        return mPartNumber;
    }

    public void setPartNumber(String partNumber) {
        mPartNumber = partNumber;
    }

    public String getPreFilter() {
        return mPreFilter;
    }

    public void setPreFilter(String preFilter) {
        mPreFilter = preFilter;
    }

    public String getProductImage() {
        return mProductImage;
    }

    public void setProductImage(String productImage) {
        mProductImage = productImage;
    }

    public String getProductType() {
        return mProductType;
    }

    public void setProductType(String productType) {
        mProductType = productType;
    }

    public String getRail() {
        return mRail;
    }

    public void setRail(String rail) {
        mRail = rail;
    }

    public String getSegment() {
        return mSegment;
    }

    public void setSegment(String segment) {
        mSegment = segment;
    }

    public String getSubCategory() {
        return mSubCategory;
    }

    public void setSubCategory(String subCategory) {
        mSubCategory = subCategory;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getVehicleApplication() {
        return mVehicleApplication;
    }

    public void setVehicleApplication(String vehicleApplication) {
        mVehicleApplication = vehicleApplication;
    }

}
