
package model.partnumbersearch;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PartNumberSearch {

    @SerializedName("data")
    private List<PartNumberData> mData;
    @SerializedName("result")
    private String mResult;

    public List<PartNumberData> getData() {
        return mData;
    }

    public void setData(List<PartNumberData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
