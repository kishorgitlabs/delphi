
package model.pricelist;

import java.util.List;
import com.google.gson.annotations.SerializedName;
@SuppressWarnings("unused")
public class PriceDetails {

    @SerializedName("data")
    private List<List<PriceListData>> mData;
    @SerializedName("result")
    private String mResult;

    public List<List<PriceListData>> getData() {
        return mData;
    }

    public void setData(List<List<PriceListData>> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
