
package model.pricelist;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PriceListData {

    @SerializedName("Description")
    private String mDescription;
    @SerializedName("id")
    private Long mId;
    @SerializedName("insertDate")
    private String mInsertDate;
    @SerializedName("MRP")
    private Double mMRP;
    @SerializedName("PartNo")
    private String mPartNo;

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getInsertDate() {
        return mInsertDate;
    }

    public void setInsertDate(String insertDate) {
        mInsertDate = insertDate;
    }

    public Double getMRP() {
        return mMRP;
    }

    public void setMRP(Double mRP) {
        mMRP = mRP;
    }

    public String getPartNo() {
        return mPartNo;
    }

    public void setPartNo(String partNo) {
        mPartNo = partNo;
    }

}
