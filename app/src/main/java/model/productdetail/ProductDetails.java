
package model.productdetail;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ProductDetails {

    @SerializedName("Brand")
    private String mBrand;
    @SerializedName("CreateDate")
    private String mCreateDate;
    @SerializedName("DTVS_Part_No")
    private String mDTVSPartNo;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("id")
    private String mId;
    @SerializedName("Image_Name")
    private String mImageName;
    @SerializedName("Main_Category")
    private String mMainCategory;
    @SerializedName("OE_Part_No")
    private String mOEPartNo;
    @SerializedName("Product_Type")
    private String mProductType;
    @SerializedName("Sub_Category")
    private String mSubCategory;
    @SerializedName("Vehicle_Application")
    private String mVehicleApplication;

    public String getBrand() {
        return mBrand;
    }

    public void setBrand(String brand) {
        mBrand = brand;
    }

    public String getCreateDate() {
        return mCreateDate;
    }

    public void setCreateDate(String createDate) {
        mCreateDate = createDate;
    }

    public String getDTVSPartNo() {
        return mDTVSPartNo;
    }

    public void setDTVSPartNo(String dTVSPartNo) {
        mDTVSPartNo = dTVSPartNo;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getImageName() {
        return mImageName;
    }

    public void setImageName(String imageName) {
        mImageName = imageName;
    }

    public String getMainCategory() {
        return mMainCategory;
    }

    public void setMainCategory(String mainCategory) {
        mMainCategory = mainCategory;
    }

    public String getOEPartNo() {
        return mOEPartNo;
    }

    public void setOEPartNo(String oEPartNo) {
        mOEPartNo = oEPartNo;
    }

    public String getProductType() {
        return mProductType;
    }

    public void setProductType(String productType) {
        mProductType = productType;
    }

    public String getSubCategory() {
        return mSubCategory;
    }

    public void setSubCategory(String subCategory) {
        mSubCategory = subCategory;
    }

    public String getVehicleApplication() {
        return mVehicleApplication;
    }

    public void setVehicleApplication(String vehicleApplication) {
        mVehicleApplication = vehicleApplication;
    }

}
