
package model.productdetail;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ProductDetailsData {

    @SerializedName("data")
    private List<ProductDetails> mData;
    @SerializedName("result")
    private String mResult;

    public List<ProductDetails> getData() {
        return mData;
    }

    public void setData(List<ProductDetails> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
