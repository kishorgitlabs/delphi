
package model.pump;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ProductData {

    @SerializedName("AlphaNumerical_Number")
    private Object mAlphaNumericalNumber;
    @SerializedName("Description")
    private Object mDescription;
    @SerializedName("ECU")
    private Object mECU;
    @SerializedName("Engine")
    private String mEngine;
    @SerializedName("HSN_Code")
    private Object mHSNCode;
    @SerializedName("id")
    private Long mId;
    @SerializedName("Injector")
    private Object mInjector;
    @SerializedName("MRP")
    private Object mMRP;
    @SerializedName("Main_Category")
    private String mMainCategory;
    @SerializedName("Main_Filter")
    private Object mMainFilter;
    @SerializedName("OE_Part_Number")
    private Object mOEPartNumber;
    @SerializedName("Part_Number")
    private String mPartNumber;
    @SerializedName("Pre_Filter")
    private Object mPreFilter;
    @SerializedName("Product_Image")
    private Object mProductImage;
    @SerializedName("Product_Type")
    private Object mProductType;
    @SerializedName("Rail")
    private Object mRail;
    @SerializedName("Segment")
    private Object mSegment;
    @SerializedName("Sub_Category")
    private String mSubCategory;
    @SerializedName("Type")
    private String mType;
    @SerializedName("Vehicle_Application")
    private Object mVehicleApplication;

    public Object getAlphaNumericalNumber() {
        return mAlphaNumericalNumber;
    }

    public void setAlphaNumericalNumber(Object alphaNumericalNumber) {
        mAlphaNumericalNumber = alphaNumericalNumber;
    }

    public Object getDescription() {
        return mDescription;
    }

    public void setDescription(Object description) {
        mDescription = description;
    }

    public Object getECU() {
        return mECU;
    }

    public void setECU(Object eCU) {
        mECU = eCU;
    }

    public String getEngine() {
        return mEngine;
    }

    public void setEngine(String engine) {
        mEngine = engine;
    }

    public Object getHSNCode() {
        return mHSNCode;
    }

    public void setHSNCode(Object hSNCode) {
        mHSNCode = hSNCode;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Object getInjector() {
        return mInjector;
    }

    public void setInjector(Object injector) {
        mInjector = injector;
    }

    public Object getMRP() {
        return mMRP;
    }

    public void setMRP(Object mRP) {
        mMRP = mRP;
    }

    public String getMainCategory() {
        return mMainCategory;
    }

    public void setMainCategory(String mainCategory) {
        mMainCategory = mainCategory;
    }

    public Object getMainFilter() {
        return mMainFilter;
    }

    public void setMainFilter(Object mainFilter) {
        mMainFilter = mainFilter;
    }

    public Object getOEPartNumber() {
        return mOEPartNumber;
    }

    public void setOEPartNumber(Object oEPartNumber) {
        mOEPartNumber = oEPartNumber;
    }

    public String getPartNumber() {
        return mPartNumber;
    }

    public void setPartNumber(String partNumber) {
        mPartNumber = partNumber;
    }

    public Object getPreFilter() {
        return mPreFilter;
    }

    public void setPreFilter(Object preFilter) {
        mPreFilter = preFilter;
    }

    public Object getProductImage() {
        return mProductImage;
    }

    public void setProductImage(Object productImage) {
        mProductImage = productImage;
    }

    public Object getProductType() {
        return mProductType;
    }

    public void setProductType(Object productType) {
        mProductType = productType;
    }

    public Object getRail() {
        return mRail;
    }

    public void setRail(Object rail) {
        mRail = rail;
    }

    public Object getSegment() {
        return mSegment;
    }

    public void setSegment(Object segment) {
        mSegment = segment;
    }

    public String getSubCategory() {
        return mSubCategory;
    }

    public void setSubCategory(String subCategory) {
        mSubCategory = subCategory;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public Object getVehicleApplication() {
        return mVehicleApplication;
    }

    public void setVehicleApplication(Object vehicleApplication) {
        mVehicleApplication = vehicleApplication;
    }

}
