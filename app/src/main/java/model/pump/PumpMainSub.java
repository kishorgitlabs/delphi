
package model.pump;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PumpMainSub {

    @SerializedName("data")
    private List<ProductData> mData;
    @SerializedName("result")
    private String mResult;

    public List<ProductData> getData() {
        return mData;
    }

    public void setData(List<ProductData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
