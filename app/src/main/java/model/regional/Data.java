
package model.regional;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Data {

    @SerializedName("grandTotal")
    private String mGrandTotal;
    @SerializedName("orderDetail")
    private List<OrderDetail> mOrderDetail;

    public String getGrandTotal() {
        return mGrandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        mGrandTotal = grandTotal;
    }

    public List<OrderDetail> getOrderDetail() {
        return mOrderDetail;
    }

    public void setOrderDetail(List<OrderDetail> orderDetail) {
        mOrderDetail = orderDetail;
    }

}
