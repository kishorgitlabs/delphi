
package model.regional;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class OrderDetail {

    @SerializedName("date")
    private String mDate;
    @SerializedName("ExeName")
    private String mExeName;
    @SerializedName("Exeid")
    private String mExeid;
    @SerializedName("total")
    private String mTotal;

    @SerializedName("dis")
    private String customercode;
    @SerializedName("disname")
    private String customername;

    public String getCustomercode() {
        return customercode;
    }

    public void setCustomercode(String customercode) {
        this.customercode = customercode;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getDate() {

        return mDate;
    }

    public void setDate(String date) {

        mDate = date;
    }

    public String getExeName() {
        return mExeName;
    }

    public void setExeName(String exeName) {

        mExeName = exeName;
    }

    public String getExeid() {
        return mExeid;
    }

    public void setExeid(String exeid) {
        mExeid = exeid;
    }

    public String getTotal() {
        return mTotal;
    }

    public void setTotal(String total) {
        mTotal = total;
    }

}
