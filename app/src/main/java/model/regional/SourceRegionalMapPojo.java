package model.regional;

import java.io.Serializable;

public class SourceRegionalMapPojo implements Serializable {
	private String inLatitude;
	private String inLongitude;

	public void setInLatitude(String inLatitude){
		this.inLatitude = inLatitude;
	}

	public String getInLatitude(){
		return inLatitude;
	}

	public void setInLongitude(String inLongitude){
		this.inLongitude = inLongitude;
	}

	public String getInLongitude(){
		return inLongitude;
	}

	@Override
 	public String toString(){
		return 
			"SourceRegionalMapPojo{" + 
			"inLatitude = '" + inLatitude + '\'' + 
			",inLongitude = '" + inLongitude + '\'' + 
			"}";
		}
}