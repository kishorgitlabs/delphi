package model.regional;

import java.util.List;
import java.io.Serializable;

public class ViewVisitDatePojoDTO implements Serializable {
	private String result;
	private List<DataDTO> data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(List<DataDTO> data){
		this.data = data;
	}

	public List<DataDTO> getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"ViewVisitDatePojoDTO{" + 
			"result = '" + result + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}