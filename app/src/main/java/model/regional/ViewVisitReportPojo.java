
package model.regional;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class ViewVisitReportPojo {

    @SerializedName("data")
    private List<ViewVisitResult> mData;
    @SerializedName("result")
    private String mResult;

    public List<ViewVisitResult> getData() {
        return mData;
    }

    public void setData(List<ViewVisitResult> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
