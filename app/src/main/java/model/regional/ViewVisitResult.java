
package model.regional;



import com.google.gson.annotations.SerializedName;

import io.reactivex.annotations.NonNull;

@SuppressWarnings("unused")
public class ViewVisitResult {

    @SerializedName("ExecutiveCode")
    private String mExecutiveCode;
    @SerializedName("id")
    private String mId;
    @SerializedName("Name")
    private String mName;
    @SerializedName("All")
    private Object All;


    @NonNull
    @Override
    public String toString() {
        return getName();
    }

    public Object getAll() {
        return All;
    }

    public void setAll(Object all) {
        All = all;
    }

    public String getExecutiveCode() {
        return mExecutiveCode;
    }

    public void setExecutiveCode(String executiveCode) {
        mExecutiveCode = executiveCode;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

}
