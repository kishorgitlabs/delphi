
package model.regional.salesnamelist;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Data {

    @SerializedName("Order_list")
    private List<OrderList> mOrderList;
    @SerializedName("tot")
    private String mTot;

    public List<OrderList> getOrderList() {
        return mOrderList;
    }

    public void setOrderList(List<OrderList> orderList) {
        mOrderList = orderList;
    }

    public String getTot() {
        return mTot;
    }

    public void setTot(String tot) {
        mTot = tot;
    }

}
