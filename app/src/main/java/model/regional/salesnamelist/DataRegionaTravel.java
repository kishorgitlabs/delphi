package model.regional.salesnamelist;

import java.io.Serializable;

public class DataRegionaTravel implements Serializable {
	private int Id;
	private Object TrackId;
	private int R_id;
	private String RegName;
	private Object latitude;
	private Object langtitude;
	private String address;
	private String datetime;
	private Object timedate;
	private Object updateddate;
	private Object distance;
	private Object Disid;
	private Object Regid;
	private Object Message;

	public void setId(int id){
		this.Id = id;
	}

	public int getId(){
		return Id;
	}

	public void setTrackId(Object trackId){
		this.TrackId = trackId;
	}

	public Object getTrackId(){
		return TrackId;
	}

	public void setRId(int rId){
		this.R_id = rId;
	}

	public int getRId(){
		return R_id;
	}

	public void setRegName(String regName){
		this.RegName = regName;
	}

	public String getRegName(){
		return RegName;
	}

	public void setLatitude(Object latitude){
		this.latitude = latitude;
	}

	public Object getLatitude(){
		return latitude;
	}

	public void setLangtitude(Object langtitude){
		this.langtitude = langtitude;
	}

	public Object getLangtitude(){
		return langtitude;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setDatetime(String datetime){
		this.datetime = datetime;
	}

	public String getDatetime(){
		return datetime;
	}

	public void setTimedate(Object timedate){
		this.timedate = timedate;
	}

	public Object getTimedate(){
		return timedate;
	}

	public void setUpdateddate(Object updateddate){
		this.updateddate = updateddate;
	}

	public Object getUpdateddate(){
		return updateddate;
	}

	public void setDistance(Object distance){
		this.distance = distance;
	}

	public Object getDistance(){
		return distance;
	}

	public void setDisid(Object disid){
		this.Disid = disid;
	}

	public Object getDisid(){
		return Disid;
	}

	public void setRegid(Object regid){
		this.Regid = regid;
	}

	public Object getRegid(){
		return Regid;
	}

	public void setMessage(Object message){
		this.Message = message;
	}

	public Object getMessage(){
		return Message;
	}

	@Override
 	public String toString(){
		return 
			"DataRegionaTravel{" + 
			"id = '" + Id + '\'' +
			",trackId = '" + TrackId + '\'' +
			",r_id = '" + R_id + '\'' +
			",regName = '" + RegName + '\'' +
			",latitude = '" + latitude + '\'' + 
			",langtitude = '" + langtitude + '\'' + 
			",address = '" + address + '\'' + 
			",datetime = '" + datetime + '\'' + 
			",timedate = '" + timedate + '\'' + 
			",updateddate = '" + updateddate + '\'' + 
			",distance = '" + distance + '\'' + 
			",disid = '" + Disid + '\'' +
			",regid = '" +Regid + '\'' +
			",message = '" + Message + '\'' +
			"}";
		}
}