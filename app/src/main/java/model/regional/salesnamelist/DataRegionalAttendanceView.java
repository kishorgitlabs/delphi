package model.regional.salesnamelist;

import java.io.Serializable;

public class DataRegionalAttendanceView implements Serializable {
	private String attendanceTime;
	private Object setCoordinateList;
	private int id;
	private String designation;
	private int empId;
	private String empCode;
	private String name;
	private String Date;
	private String inTime;
	private String outTime;
	private Object totalDuration;
	private String outAddress;
	private String inLatitude;
	private String inLongitude;
	private String outLongitude;
	private String outLatitude;
	private String address;
	private String createdDate;
	private int flag;
	private Object disid;
	private int regid;
	private String AttendDay;
	private String regName;
	private String distance;

	public void setAttendanceTime(String attendanceTime){
		this.attendanceTime = attendanceTime;
	}

	public String getAttendanceTime(){
		return attendanceTime;
	}

	public void setSetCoordinateList(Object setCoordinateList){
		this.setCoordinateList = setCoordinateList;
	}

	public Object getSetCoordinateList(){
		return setCoordinateList;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setDesignation(String designation){
		this.designation = designation;
	}

	public String getDesignation(){
		return designation;
	}

	public void setEmpId(int empId){
		this.empId = empId;
	}

	public int getEmpId(){
		return empId;
	}

	public void setEmpCode(String empCode){
		this.empCode = empCode;
	}

	public String getEmpCode(){
		return empCode;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setDate(String date){
		this.Date = date;
	}

	public String getDate(){
		return Date;
	}

	public void setInTime(String inTime){
		this.inTime = inTime;
	}

	public String getInTime(){
		return inTime;
	}

	public void setOutTime(String outTime){
		this.outTime = outTime;
	}

	public String getOutTime(){
		return outTime;
	}

	public void setTotalDuration(Object totalDuration){
		this.totalDuration = totalDuration;
	}

	public Object getTotalDuration(){
		return totalDuration;
	}

	public void setOutAddress(String outAddress){
		this.outAddress = outAddress;
	}

	public String getOutAddress(){
		return outAddress;
	}

	public void setInLatitude(String inLatitude){
		this.inLatitude = inLatitude;
	}

	public String getInLatitude(){
		return inLatitude;
	}

	public void setInLongitude(String inLongitude){
		this.inLongitude = inLongitude;
	}

	public String getInLongitude(){
		return inLongitude;
	}

	public void setOutLongitude(String outLongitude){
		this.outLongitude = outLongitude;
	}

	public String getOutLongitude(){
		return outLongitude;
	}

	public void setOutLatitude(String outLatitude){
		this.outLatitude = outLatitude;
	}

	public String getOutLatitude(){
		return outLatitude;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setCreatedDate(String createdDate){
		this.createdDate = createdDate;
	}

	public String getCreatedDate(){
		return createdDate;
	}

	public void setFlag(int flag){
		this.flag = flag;
	}

	public int getFlag(){
		return flag;
	}

	public void setDisid(Object disid){
		this.disid = disid;
	}

	public Object getDisid(){
		return disid;
	}

	public void setRegid(int regid){
		this.regid = regid;
	}

	public int getRegid(){
		return regid;
	}

	public void setAttendDay(String attendDay){
		this.AttendDay = attendDay;
	}

	public String getAttendDay(){
		return AttendDay;
	}

	public void setRegName(String regName){
		this.regName = regName;
	}

	public String getRegName(){
		return regName;
	}

	public void setDistance(String distance){
		this.distance = distance;
	}

	public String getDistance(){
		return distance;
	}
}