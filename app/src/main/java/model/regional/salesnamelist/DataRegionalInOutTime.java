package model.regional.salesnamelist;

import java.io.Serializable;

public class DataRegionalInOutTime implements Serializable {
	private String id;
	private String Designation;
	private String EmpId;
	private String EmpCode;
	private String Name;
	private String Date;
	private String InTime;
	private String OutTime;
	private Object TotalDuration;
	private String OutAddress;
	private String InLatitude;
	private String InLongitude;
	private String OutLongitude;
	private String OutLatitude;
	private String Address;
	private String CreatedDate;
	private Object flag;
	private Object Disid;
	private String Regid;
	private String AttendDay;
	private String RegName;
	private String Distance;

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setDesignation(String designation){
		this.Designation = designation;
	}

	public String getDesignation(){
		return Designation;
	}

	public void setEmpId(String empId){
		this.EmpId = empId;
	}

	public String getEmpId(){
		return EmpId;
	}

	public void setEmpCode(String empCode){
		this.EmpCode = empCode;
	}

	public String getEmpCode(){
		return EmpCode;
	}

	public void setName(String name){
		this.Name = name;
	}

	public String getName(){
		return Name;
	}

	public void setDate(String date){
		this.Date = date;
	}

	public String getDate(){
		return Date;
	}

	public String getInTime() {
		return InTime;
	}

	public void setInTime(String inTime) {
		InTime = inTime;
	}

	public String getOutTime() {
		return OutTime;
	}

	public void setOutTime(String outTime) {
		OutTime = outTime;
	}

	public void setTotalDuration(Object totalDuration){
		this.TotalDuration = totalDuration;
	}

	public Object getTotalDuration(){
		return TotalDuration;
	}

	public void setOutAddress(String outAddress){
		this.OutAddress = outAddress;
	}

	public String getOutAddress(){
		return OutAddress;
	}

	public void setInLatitude(String inLatitude){
		this.InLatitude = inLatitude;
	}

	public String getInLatitude(){
		return InLatitude;
	}

	public void setInLongitude(String inLongitude){
		this.InLongitude = inLongitude;
	}

	public String getInLongitude(){
		return InLongitude;
	}

	public void setOutLongitude(String outLongitude){
		this.OutLongitude = outLongitude;
	}

	public String getOutLongitude(){
		return OutLongitude;
	}

	public void setOutLatitude(String outLatitude){
		this.OutLatitude = outLatitude;
	}

	public String getOutLatitude(){
		return OutLatitude;
	}

	public void setAddress(String address){
		this.Address = address;
	}

	public String getAddress(){
		return Address;
	}

	public void setCreatedDate(String createdDate){
		this.CreatedDate = createdDate;
	}

	public String getCreatedDate(){
		return CreatedDate;
	}

	public void setFlag(Object flag){
		this.flag = flag;
	}

	public Object getFlag(){
		return flag;
	}

	public void setDisid(Object disid){
		this.Disid = disid;
	}

	public Object getDisid(){
		return Disid;
	}

	public void setRegid(String regid){
		this.Regid = regid;
	}

	public String getRegid(){
		return Regid;
	}

	public void setAttendDay(String attendDay){
		this.AttendDay = attendDay;
	}

	public String getAttendDay(){
		return AttendDay;
	}

	public void setRegName(String regName){
		this.RegName = regName;
	}

	public String getRegName(){
		return RegName;
	}

	public void setDistance(String distance){
		this.Distance = distance;
	}

	public String getDistance(){
		return Distance;
	}

	@Override
 	public String toString(){
		return 
			"DataRegionalInOutTime{" + 
			"id = '" + id + '\'' + 
			",designation = '" + Designation + '\'' +
			",empId = '" + EmpId + '\'' +
			",empCode = '" + EmpCode + '\'' +
			",name = '" + Name + '\'' +
			",date = '" + Date + '\'' +
			",inTime = '" + InTime + '\'' +
			",outTime = '" + OutTime + '\'' +
			",totalDuration = '" + TotalDuration + '\'' +
			",outAddress = '" + OutAddress + '\'' +
			",inLatitude = '" + InLatitude + '\'' +
			",inLongitude = '" + InLongitude + '\'' +
			",outLongitude = '" + OutLongitude + '\'' +
			",outLatitude = '" + OutLatitude + '\'' +
			",address = '" + Address + '\'' +
			",createdDate = '" + CreatedDate + '\'' +
			",flag = '" + flag + '\'' + 
			",disid = '" + Disid + '\'' +
			",regid = '" + Regid + '\'' +
			",attendDay = '" + AttendDay + '\'' +
			",regName = '" + RegName + '\'' +
			",distance = '" + Distance + '\'' +
			"}";
		}
}