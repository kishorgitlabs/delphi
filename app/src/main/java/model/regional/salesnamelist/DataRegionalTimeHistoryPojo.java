package model.regional.salesnamelist;

import java.io.Serializable;

public class DataRegionalTimeHistoryPojo implements Serializable {
	private String Id;
	private String TrackId;
	private String R_id;
	private String RegName;
	private Double latitude;
	private Double langtitude;
	private String address;
	private String datetime;
	private String timedate;
	private String updateddate;
	private String distance;
	private String Disid;
	private String Regid;
	private String Message;

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getTrackId() {
		return TrackId;
	}

	public void setTrackId(String trackId) {
		TrackId = trackId;
	}

	public String getR_id() {
		return R_id;
	}

	public void setR_id(String r_id) {
		R_id = r_id;
	}

	public String getRegName() {
		return RegName;
	}

	public void setRegName(String regName) {
		RegName = regName;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLangtitude() {
		return langtitude;
	}

	public void setLangtitude(Double langtitude) {
		this.langtitude = langtitude;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getTimedate() {
		return timedate;
	}

	public void setTimedate(String timedate) {
		this.timedate = timedate;
	}

	public String getUpdateddate() {
		return updateddate;
	}

	public void setUpdateddate(String updateddate) {
		this.updateddate = updateddate;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getDisid() {
		return Disid;
	}

	public void setDisid(String disid) {
		Disid = disid;
	}

	public String getRegid() {
		return Regid;
	}

	public void setRegid(String regid) {
		Regid = regid;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	@Override
 	public String toString(){
		return 
			"DataRegionalTimeHistoryPojo{" + 
			"id = '" + Id + '\'' +
			",trackId = '" + TrackId + '\'' +
			",r_id = '" + R_id + '\'' +
			",regName = '" + RegName + '\'' +
			",latitude = '" + latitude + '\'' + 
			",langtitude = '" + langtitude + '\'' + 
			",address = '" + address + '\'' + 
			",datetime = '" + datetime + '\'' + 
			",timedate = '" + timedate + '\'' + 
			",updateddate = '" + updateddate + '\'' + 
			",distance = '" + distance + '\'' + 
			",disid = '" + Disid + '\'' +
			",regid = '" + Regid + '\'' +
			",message = '" + Message + '\'' +
			"}";
		}
}