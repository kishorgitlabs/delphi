
package model.regional.salesnamelist;


import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class OrderList {

    @SerializedName("ContactPerson")
    private String mContactPerson;
    @SerializedName("Orderid")
    private String mOrderid;
    @SerializedName("ShopName")
    private Object mShopName;
    @SerializedName("Total")
    private String mTotal;

    public String getContactPerson() {
        return mContactPerson;
    }

    public void setContactPerson(String contactPerson) {
        mContactPerson = contactPerson;
    }

    public String getOrderid() {
        return mOrderid;
    }

    public void setOrderid(String orderid) {
        mOrderid = orderid;
    }

    public Object getShopName() {
        return mShopName;
    }

    public void setShopName(Object shopName) {
        mShopName = shopName;
    }

    public String getTotal() {
        return mTotal;
    }

    public void setTotal(String total) {
        mTotal = total;
    }

}
