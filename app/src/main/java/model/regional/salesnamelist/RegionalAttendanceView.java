package model.regional.salesnamelist;

import java.util.List;
import java.io.Serializable;

public class RegionalAttendanceView implements Serializable {
	private String result;
	private List<DataRegionalAttendanceView> data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(List<DataRegionalAttendanceView> data){
		this.data = data;
	}

	public List<DataRegionalAttendanceView> getData(){
		return data;
	}
}