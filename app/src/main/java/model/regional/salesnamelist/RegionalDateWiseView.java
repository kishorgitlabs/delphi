package model.regional.salesnamelist;

import java.util.List;
import java.io.Serializable;

public class RegionalDateWiseView implements Serializable {
	private String result;
	private List<DataRegionalDateWiseView> data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(List<DataRegionalDateWiseView> data){
		this.data = data;
	}

	public List<DataRegionalDateWiseView> getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"ResponseRegionalDateWiseView{" + 
			"result = '" + result + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}