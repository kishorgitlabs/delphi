package model.regional.salesnamelist;

import java.util.List;
import java.io.Serializable;

public class RegionalInOutTime implements Serializable {
	private String result;
	private List<DataRegionalInOutTime> data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setdata(List<DataRegionalInOutTime> data){
		this.data = data;
	}

	public List<DataRegionalInOutTime> getdata(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"ResponseRegionalInOutTime{" + 
			"result = '" + result + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}