package model.regional.salesnamelist;

import java.io.Serializable;

public class ResponseRegionaTravel implements Serializable {
	private String result;
	private DataRegionaTravel data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(DataRegionaTravel data){
		this.data = data;
	}

	public DataRegionaTravel getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"ResponseRegionaTravel{" + 
			"result = '" + result + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}