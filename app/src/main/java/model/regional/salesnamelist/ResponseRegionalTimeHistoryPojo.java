package model.regional.salesnamelist;

import java.util.List;
import java.io.Serializable;

public class ResponseRegionalTimeHistoryPojo implements Serializable {
	private String result;
	private List<DataRegionalTimeHistoryPojo> data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(List<DataRegionalTimeHistoryPojo> data){
		this.data = data;
	}

	public List<DataRegionalTimeHistoryPojo> getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"ResponseRegionalTimeHistoryPojo{" + 
			"result = '" + result + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}