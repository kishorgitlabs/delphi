package model.regional.salesnamelist;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SalesNameList {


    @SerializedName("result")
    String result;
    @SerializedName("data")
    List<String > date;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public List<String> getDate() {
        return date;
    }

    public void setDate(List<String> date) {
        this.date = date;
    }
}
