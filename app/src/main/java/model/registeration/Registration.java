package model.registeration;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Registration {
  @SerializedName("date")
  @Expose
  private String date;
  @SerializedName("Status")
  @Expose
  private String Status;
  @SerializedName("country")
  @Expose
  private String country;
  @SerializedName("ShopName")
  @Expose
  private String ShopName;
  @SerializedName("city")
  @Expose
  private String city;
  @SerializedName("usertype")
  @Expose
  private String usertype;
  @SerializedName("mobileno")
  @Expose
  private String mobileno;
  @SerializedName("type")
  @Expose
  private String type;
  @SerializedName("password")
  @Expose
  private String password;
  @SerializedName("name")
  @Expose
  private String name;
  @SerializedName("id")
  @Expose
  private String id;
  @SerializedName("state")
  @Expose
  private String state;
  @SerializedName("email")
  @Expose
  private String email;
  @SerializedName("Location")
  @Expose
  private String Location;
  public void setDate(String date){
   this.date=date;
  }
  public String getDate(){
   return date;
  }
  public void setStatus(String Status){
   this.Status=Status;
  }
  public String getStatus(){
   return Status;
  }
  public void setCountry(String country){
   this.country=country;
  }
  public String getCountry(){
   return country;
  }
  public void setShopName(String ShopName){
   this.ShopName=ShopName;
  }
  public String getShopName(){
   return ShopName;
  }
  public void setCity(String city){
   this.city=city;
  }
  public String getCity(){
   return city;
  }
  public void setUsertype(String usertype){
   this.usertype=usertype;
  }
  public String getUsertype(){
   return usertype;
  }
  public void setMobileno(String mobileno){
   this.mobileno=mobileno;
  }
  public String getMobileno(){
   return mobileno;
  }
  public void setType(String type){
   this.type=type;
  }
  public String getType(){
   return type;
  }
  public void setPassword(String password){
   this.password=password;
  }
  public String getPassword(){
   return password;
  }
  public void setName(String name){
   this.name=name;
  }
  public String getName(){
   return name;
  }
  public void setId(String id){
   this.id=id;
  }
  public String getId(){
   return id;
  }
  public void setState(String state){
   this.state=state;
  }
  public String getState(){
   return state;
  }
  public void setEmail(String email){
   this.email=email;
  }
  public String getEmail(){
   return email;
  }
  public void setLocation(String Location){
   this.Location=Location;
  }
  public String getLocation(){
   return Location;
  }
}