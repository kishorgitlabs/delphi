package model.registeration;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class RegistrationData {
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private Registration data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(Registration data){
   this.data=data;
  }
  public Registration getData(){
   return data;
  }
}