package model.registeration;

import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Expose;

import java.util.List;

public class RegistrationPostValues {

    @SerializedName("name")
    public String name;

        @SerializedName("mobileno")
    public String phoneNumber;

    @SerializedName("email")
    public String emailId;

    @SerializedName("ShopName")
    public String shopName;

    @SerializedName("state")
    public String state;

    @SerializedName("city")
    public String city;

    @SerializedName("Address1")
    public String address1;

    @SerializedName("Address2")
    public String address2;

    @SerializedName("pincode")
    public String pincode;

    @SerializedName("usertype")
    public String userType;

    @SerializedName("type")
    public String appId;

    @SerializedName("shopTypeList")
    public List<ShopType> shopTypeList;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public List<ShopType> getShopTypeList() {
        return shopTypeList;
    }

    public void setShopTypeList(List<ShopType> shopTypeList) {
        this.shopTypeList = shopTypeList;
    }
}
