package model.registeration;

import com.google.gson.annotations.SerializedName;

public class ShopType {
    @SerializedName("ShopType")
    String shopType;

    public String getShopType() {
        return shopType;
    }

    public void setShopType(String shopType) {
        this.shopType = shopType;
    }
}
