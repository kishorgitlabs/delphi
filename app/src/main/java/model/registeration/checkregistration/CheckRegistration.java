
package model.registeration.checkregistration;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CheckRegistration {

    @SerializedName("data")
    private CheckRegistrationResult mData;
    @SerializedName("result")
    private String mResult;

    public CheckRegistrationResult getData() {
        return mData;
    }

    public void setData(CheckRegistrationResult data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
