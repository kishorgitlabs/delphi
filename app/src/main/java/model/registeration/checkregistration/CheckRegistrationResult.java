
package model.registeration.checkregistration;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CheckRegistrationResult {

    @SerializedName("Address1")
    private String mAddress1;
    @SerializedName("Address2")
    private String mAddress2;
    @SerializedName("city")
    private String mCity;
    @SerializedName("country")
    private Object mCountry;
    @SerializedName("date")
    private String mDate;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("id")
    private Long mId;
    @SerializedName("Location")
    private Object mLocation;
    @SerializedName("mobileno")
    private String mMobileno;
    @SerializedName("name")
    private String mName;
    @SerializedName("OTPStatus")
    private String mOTPStatus;
    @SerializedName("otp")
    private String mOtp;
    @SerializedName("password")
    private Object mPassword;
    @SerializedName("Pincode")
    private String mPincode;
    @SerializedName("ShopName")
    private String mShopName;
    @SerializedName("ShopType")
    private String mShopType;
    @SerializedName("shopTypeList")
    private Object mShopTypeList;
    @SerializedName("state")
    private String mState;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("type")
    private String mType;
    @SerializedName("usertype")
    private String mUsertype;

    public String getAddress1() {
        return mAddress1;
    }

    public void setAddress1(String address1) {
        mAddress1 = address1;
    }

    public String getAddress2() {
        return mAddress2;
    }

    public void setAddress2(String address2) {
        mAddress2 = address2;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public Object getCountry() {
        return mCountry;
    }

    public void setCountry(Object country) {
        mCountry = country;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Object getLocation() {
        return mLocation;
    }

    public void setLocation(Object location) {
        mLocation = location;
    }

    public String getMobileno() {
        return mMobileno;
    }

    public void setMobileno(String mobileno) {
        mMobileno = mobileno;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getOTPStatus() {
        return mOTPStatus;
    }

    public void setOTPStatus(String oTPStatus) {
        mOTPStatus = oTPStatus;
    }

    public String getOtp() {
        return mOtp;
    }

    public void setOtp(String otp) {
        mOtp = otp;
    }

    public Object getPassword() {
        return mPassword;
    }

    public void setPassword(Object password) {
        mPassword = password;
    }

    public String getPincode() {
        return mPincode;
    }

    public void setPincode(String pincode) {
        mPincode = pincode;
    }

    public String getShopName() {
        return mShopName;
    }

    public void setShopName(String shopName) {
        mShopName = shopName;
    }

    public String getShopType() {
        return mShopType;
    }

    public void setShopType(String shopType) {
        mShopType = shopType;
    }

    public Object getShopTypeList() {
        return mShopTypeList;
    }

    public void setShopTypeList(Object shopTypeList) {
        mShopTypeList = shopTypeList;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getUsertype() {
        return mUsertype;
    }

    public void setUsertype(String usertype) {
        mUsertype = usertype;
    }

}
