
package model.rotary;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class OldServiceData {

    @SerializedName("oldServiceList")
    private List<OldServiceList> mOldServiceList;
    @SerializedName("partTableData")
    private PartTableData mPartTableData;

    public List<OldServiceList> getOldServiceList() {
        return mOldServiceList;
    }

    public void setOldServiceList(List<OldServiceList> oldServiceList) {
        mOldServiceList = oldServiceList;
    }

    public PartTableData getPartTableData() {
        return mPartTableData;
    }

    public void setPartTableData(PartTableData partTableData) {
        mPartTableData = partTableData;
    }

}
