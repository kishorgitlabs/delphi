
package model.rotary;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class OldServiceList {

    @SerializedName("id")
    private Long mId;
    @SerializedName("Part_Number")
    private String mPartNumber;
    @SerializedName("Pdf_Name")
    private String mPdfName;
    @SerializedName("Superseded_No")
    private String mSupersededNo;

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getPartNumber() {
        return mPartNumber;
    }

    public void setPartNumber(String partNumber) {
        mPartNumber = partNumber;
    }

    public String getPdfName() {
        return mPdfName;
    }

    public void setPdfName(String pdfName) {
        mPdfName = pdfName;
    }

    public String getSupersededNo() {
        return mSupersededNo;
    }

    public void setSupersededNo(String supersededNo) {
        mSupersededNo = supersededNo;
    }

}
