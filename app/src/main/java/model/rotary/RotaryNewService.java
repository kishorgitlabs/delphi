
package model.rotary;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class RotaryNewService {

    @SerializedName("data")
    private List<OldServiceData> mData;
    @SerializedName("result")
    private String mResult;

    public List<OldServiceData> getData() {
        return mData;
    }

    public void setData(List<OldServiceData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
