
package model.rotaryoldparts;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class NewServiceParts {

    @SerializedName("data")
    private List<OldNewServiceList> mData;
    @SerializedName("result")
    private String mResult;

    public List<OldNewServiceList> getData() {
        return mData;
    }

    public void setData(List<OldNewServiceList> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
