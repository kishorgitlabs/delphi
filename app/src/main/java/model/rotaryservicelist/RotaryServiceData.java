
package model.rotaryservicelist;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class RotaryServiceData {

    @SerializedName("data")
    private List<RotaryServiceList> mData;
    @SerializedName("result")
    private String mResult;

    public List<RotaryServiceList> getData() {
        return mData;
    }

    public void setData(List<RotaryServiceList> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
