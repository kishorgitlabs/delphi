
package model.rotaryservicelist;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class RotaryServiceList {

    @SerializedName("ILLUSTRATION_NO")
    private String mILLUSTRATIONNO;
    @SerializedName("id")
    private Long mId;
    @SerializedName("LINE_SEQ_NO")
    private String mLINESEQNO;
    @SerializedName("PART_DESCRIPTION")
    private String mPARTDESCRIPTION;
    @SerializedName("PartNo")
    private String mPartNo;
    @SerializedName("QTY_FIP")
    private String mQTYFIP;
    @SerializedName("SerPART_NO")
    private String mSerPARTNO;

    public String getILLUSTRATIONNO() {
        return mILLUSTRATIONNO;
    }

    public void setILLUSTRATIONNO(String iLLUSTRATIONNO) {
        mILLUSTRATIONNO = iLLUSTRATIONNO;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getLINESEQNO() {
        return mLINESEQNO;
    }

    public void setLINESEQNO(String lINESEQNO) {
        mLINESEQNO = lINESEQNO;
    }

    public String getPARTDESCRIPTION() {
        return mPARTDESCRIPTION;
    }

    public void setPARTDESCRIPTION(String pARTDESCRIPTION) {
        mPARTDESCRIPTION = pARTDESCRIPTION;
    }

    public String getPartNo() {
        return mPartNo;
    }

    public void setPartNo(String partNo) {
        mPartNo = partNo;
    }

    public String getQTYFIP() {
        return mQTYFIP;
    }

    public void setQTYFIP(String qTYFIP) {
        mQTYFIP = qTYFIP;
    }

    public String getSerPARTNO() {
        return mSerPARTNO;
    }

    public void setSerPARTNO(String serPARTNO) {
        mSerPARTNO = serPARTNO;
    }

}
