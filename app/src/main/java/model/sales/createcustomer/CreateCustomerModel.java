
package model.sales.createcustomer;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CreateCustomerModel {

    @SerializedName("data")
    private Object mData;
    @SerializedName("result")
    private String mResult;

    public Object getData() {
        return mData;
    }

    public void setData(Object data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
