
package model.sales.createcustomer.imageupload;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ImageUpload {

    @SerializedName("Message")
    private String mMessage;

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

}
