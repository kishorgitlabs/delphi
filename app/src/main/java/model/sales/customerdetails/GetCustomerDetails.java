
package model.sales.customerdetails;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class GetCustomerDetails {

    @SerializedName("data")
    private List<GetCustomerDetailsResult> mData;
    @SerializedName("result")
    private String mResult;

    public List<GetCustomerDetailsResult> getData() {
        return mData;
    }

    public void setData(List<GetCustomerDetailsResult> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
