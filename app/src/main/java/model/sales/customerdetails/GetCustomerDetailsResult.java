
package model.sales.customerdetails;



import com.google.gson.annotations.SerializedName;

import io.reactivex.annotations.NonNull;

@SuppressWarnings("unused")
public class GetCustomerDetailsResult {

    @SerializedName("Approve")
    private String mApprove;
    @SerializedName("CompanyName")
    private String mCompanyName;
    @SerializedName("ContactPersonname")
    private String mContactPersonname;
    @SerializedName("DaelerRegion")
    private String mDaelerRegion;
    @SerializedName("DealerAddress")
    private String mDealerAddress;
    @SerializedName("DealerCity")
    private String mDealerCity;
    @SerializedName("DealerEmail")
    private String mDealerEmail;
    @SerializedName("DealerMobile")
    private String mDealerMobile;
    @SerializedName("DealerName")
    private String mDealerName;
    @SerializedName("DealerState")
    private String mDealerState;
    @SerializedName("dealercode")
    private Object mDealercode;
    @SerializedName("deleteflag")
    private String mDeleteflag;
    @SerializedName("ExecutId")
    private Long mExecutId;
    @SerializedName("Executecode")
    private Object mExecutecode;
    @SerializedName("GST")
    private String mGST;
    @SerializedName("id")
    private Long mId;
    @SerializedName("Insertdate")
    private String mInsertdate;
    @SerializedName("reg_code")
    private Object mRegCode;
    @SerializedName("RegName")
    private String mRegName;
    @SerializedName("Regid")
    private Long mRegid;
    @SerializedName("ShopName")
    private String mShopName;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("UpdateDate")
    private Object mUpdateDate;
    @SerializedName("UserType")
    private String mUserType;
    @SerializedName("Pincode")
    private String mPinCode;
    @SerializedName("UploadImage")
    private String mUploadImage;

    public String getmUploadImage() {
        return mUploadImage;
    }

    public void setmUploadImage(String mUploadImage) {
        this.mUploadImage = mUploadImage;
    }

    public String getmPinCode() {
        return mPinCode;
    }

    public void setmPinCode(String mPinCode) {
        this.mPinCode = mPinCode;
    }

    public String getApprove() {
        return mApprove;
    }

    public void setApprove(String approve) {
        mApprove = approve;
    }

    public String getCompanyName() {
        return mCompanyName;
    }

    public void setCompanyName(String companyName) {
        mCompanyName = companyName;
    }

    public String getContactPersonname() {
        return mContactPersonname;
    }

    public void setContactPersonname(String contactPersonname) {
        mContactPersonname = contactPersonname;
    }

    public String getDaelerRegion() {
        return mDaelerRegion;
    }

    public void setDaelerRegion(String daelerRegion) {
        mDaelerRegion = daelerRegion;
    }

    public String getDealerAddress() {
        return mDealerAddress;
    }

    public void setDealerAddress(String dealerAddress) {
        mDealerAddress = dealerAddress;
    }

    public String getDealerCity() {
        return mDealerCity;
    }

    public void setDealerCity(String dealerCity) {
        mDealerCity = dealerCity;
    }

    public String getDealerEmail() {
        return mDealerEmail;
    }

    public void setDealerEmail(String dealerEmail) {
        mDealerEmail = dealerEmail;
    }

    public String getDealerMobile() {
        return mDealerMobile;
    }

    public void setDealerMobile(String dealerMobile) {
        mDealerMobile = dealerMobile;
    }

    public String getDealerName() {
        return mDealerName;
    }

    public void setDealerName(String dealerName) {
        mDealerName = dealerName;
    }

    public String getDealerState() {
        return mDealerState;
    }

    public void setDealerState(String dealerState) {
        mDealerState = dealerState;
    }

    public Object getDealercode() {
        return mDealercode;
    }

    public void setDealercode(Object dealercode) {
        mDealercode = dealercode;
    }

    public String getDeleteflag() {
        return mDeleteflag;
    }

    public void setDeleteflag(String deleteflag) {
        mDeleteflag = deleteflag;
    }

    public Long getExecutId() {
        return mExecutId;
    }

    public void setExecutId(Long executId) {
        mExecutId = executId;
    }

    public Object getExecutecode() {
        return mExecutecode;
    }

    public void setExecutecode(Object executecode) {
        mExecutecode = executecode;
    }

    public String getGST() {
        return mGST;
    }

    public void setGST(String gST) {
        mGST = gST;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getInsertdate() {
        return mInsertdate;
    }

    public void setInsertdate(String insertdate) {
        mInsertdate = insertdate;
    }

    public Object getRegCode() {
        return mRegCode;
    }

    public void setRegCode(Object regCode) {
        mRegCode = regCode;
    }

    public String getRegName() {
        return mRegName;
    }

    public void setRegName(String regName) {
        mRegName = regName;
    }

    public Long getRegid() {
        return mRegid;
    }

    public void setRegid(Long regid) {
        mRegid = regid;
    }

    public String getShopName() {
        return mShopName;
    }

    public void setShopName(String shopName) {
        mShopName = shopName;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public Object getUpdateDate() {
        return mUpdateDate;
    }

    public void setUpdateDate(Object updateDate) {
        mUpdateDate = updateDate;
    }

    public String getUserType() {
        return mUserType;
    }

    public void setUserType(String userType) {
        mUserType = userType;
    }

    @NonNull
    @Override
    public String toString() {
        return mDealerMobile;
    }
}
