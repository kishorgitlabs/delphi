
package model.sales.login;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SalesLoginModel {

    @SerializedName("data")
    private SalesLoginResult mData;
    @SerializedName("result")
    private String mResult;

    public SalesLoginResult getData() {

        return mData;
    }

    public void setData(SalesLoginResult data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
