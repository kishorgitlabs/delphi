
package model.sales.login;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SalesLoginResult {


    public String getMobno() {
        return mobno;
    }

    public void setMobno(String mobno) {
        this.mobno = mobno;
    }

    @SerializedName("MobileNumber")
    private String mobno;
    @SerializedName("Createddate")
    private String mCreateddate;
    @SerializedName("DeviceName")
    private String mDeviceName;
    @SerializedName("EmailId")
    private String mEmailId;
    @SerializedName("EmailPassword")
    private String mEmailPassword;
    @SerializedName("EmailSignature")
    private Object mEmailSignature;
    @SerializedName("ExecutiveCode")
    private String mExecutiveCode;
    @SerializedName("id")
    private Long mId;
    @SerializedName("LastLogin")
    private Object mLastLogin;
    @SerializedName("MobileNo")
    private String mMobileNo;
    @SerializedName("Name")
    private String mName;
    @SerializedName("Password")
    private String mPassword;
    @SerializedName("paymentemail")
    private Object mPaymentemail;
    @SerializedName("paymentsms")
    private Object mPaymentsms;
    @SerializedName("Regid")
    private String mRegid;
    @SerializedName("Region")
    private String mRegion;
    @SerializedName("RegionalManagername")
    private String mRegionalManagername;
    @SerializedName("salesemail")
    private String mSalesemail;
    @SerializedName("salessms")
    private String mSalessms;
    @SerializedName("sdkInt")
    private String mSdkInt;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("updatedate")
    private String mUpdatedate;
    @SerializedName("UserName")
    private String mUserName;
    @SerializedName("UserType")
    private String mUserType;
@SerializedName("R_Id")
private String regionid;

    public String getManagercode() {
        return managercode;
    }

    public void setManagercode(String managercode) {
        this.managercode = managercode;
    }

    @SerializedName("Managercode")
    private String managercode;

//@SerializedName("id")
//private String exeid;


//    public String getexeid() {
////        return regionid;
////    }
////
////    public void setRegionid(String regionid) {
////        this.regionid = regionid;
////    }

    public String getRegionid() {
        return regionid;
    }

    public void setRegionid(String regionid) {
        this.regionid = regionid;
    }

    public String getCreateddate() {

        return mCreateddate;
    }

    public void setCreateddate(String createddate) {

        mCreateddate = createddate;
    }

    public String getDeviceName() {
        return mDeviceName;
    }

    public void setDeviceName(String deviceName) {
        mDeviceName = deviceName;
    }

    public String getEmailId() {
        return mEmailId;
    }

    public void setEmailId(String emailId) {
        mEmailId = emailId;
    }

    public String getEmailPassword() {
        return mEmailPassword;
    }

    public void setEmailPassword(String emailPassword) {
        mEmailPassword = emailPassword;
    }

    public Object getEmailSignature() {
        return mEmailSignature;
    }

    public void setEmailSignature(Object emailSignature) {
        mEmailSignature = emailSignature;
    }

    public String getExecutiveCode() {
        return mExecutiveCode;
    }

    public void setExecutiveCode(String executiveCode) {
        mExecutiveCode = executiveCode;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Object getLastLogin() {
        return mLastLogin;
    }

    public void setLastLogin(Object lastLogin) {
        mLastLogin = lastLogin;
    }

    public String getMobileNo() {
        return mMobileNo;
    }

    public void setMobileNo(String mobileNo) {
        mMobileNo = mobileNo;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public Object getPaymentemail() {
        return mPaymentemail;
    }

    public void setPaymentemail(Object paymentemail) {
        mPaymentemail = paymentemail;
    }

    public Object getPaymentsms() {
        return mPaymentsms;
    }

    public void setPaymentsms(Object paymentsms) {
        mPaymentsms = paymentsms;
    }

    public String getRegid() {
        return mRegid;
    }

    public void setRegid(String regid) {
        mRegid = regid;
    }

    public String getRegion() {
        return mRegion;
    }

    public void setRegion(String region) {
        mRegion = region;
    }

    public String getRegionalManagername() {
        return mRegionalManagername;
    }

    public void setRegionalManagername(String regionalManagername) {
        mRegionalManagername = regionalManagername;
    }

    public String getSalesemail() {
        return mSalesemail;
    }

    public void setSalesemail(String salesemail) {
        mSalesemail = salesemail;
    }

    public String getSalessms() {
        return mSalessms;
    }

    public void setSalessms(String salessms) {
        mSalessms = salessms;
    }

    public String getSdkInt() {
        return mSdkInt;
    }

    public void setSdkInt(String sdkInt) {
        mSdkInt = sdkInt;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getUpdatedate() {
        return mUpdatedate;
    }

    public void setUpdatedate(String updatedate) {
        mUpdatedate = updatedate;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    public String getUserType() {
        return mUserType;
    }

    public void setUserType(String userType) {
        mUserType = userType;
    }

}
