
package model.sales.order.orderhistory;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class OrderHistoryModel {

    @SerializedName("data")
    private OrderResult mData;
    @SerializedName("result")
    private String mResult;

    public OrderResult getData() {
        return mData;
    }

    public void setData(OrderResult data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
