
package model.sales.order.orderhistory;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class OrderList {

    @SerializedName("ContactPerson")
    private String mContactPerson;
    @SerializedName("CusId")
    private Long mCusId;
    @SerializedName("CustomerType")
    private String mCustomerType;
    @SerializedName("DisId")
    private Long mDisId;
    @SerializedName("ExeId")
    private String mExeId;
    @SerializedName("id")
    private Long mId;
    @SerializedName("InsertDate")
    private String mInsertDate;
    @SerializedName("OrderDate")
    private String mOrderDate;
    @SerializedName("OrderType")
    private String mOrderType;
    @SerializedName("Orderid")
    private String mOrderid;
    @SerializedName("RegId")
    private Long mRegId;
    @SerializedName("ShopName")
    private String mShopName;
    @SerializedName("Total")
    private String mTotal;
    @SerializedName("EXid")
    private String mExid;

    public String getmExid() {
        return mExid;
    }

    public void setmExid(String mExid) {
        this.mExid = mExid;
    }

    public String getContactPerson() {
        return mContactPerson;
    }

    public void setContactPerson(String contactPerson) {
        mContactPerson = contactPerson;
    }

    public Long getCusId() {
        return mCusId;
    }

    public void setCusId(Long cusId) {
        mCusId = cusId;
    }

    public String getCustomerType() {
        return mCustomerType;
    }

    public void setCustomerType(String customerType) {
        mCustomerType = customerType;
    }

    public Long getDisId() {
        return mDisId;
    }

    public void setDisId(Long disId) {
        mDisId = disId;
    }

    public String getExeId() {
        return mExeId;
    }

    public void setExeId(String exeId) {
        mExeId = exeId;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getInsertDate() {
        return mInsertDate;
    }

    public void setInsertDate(String insertDate) {
        mInsertDate = insertDate;
    }

    public String getOrderDate() {
        return mOrderDate;
    }

    public void setOrderDate(String orderDate) {
        mOrderDate = orderDate;
    }

    public String getOrderType() {
        return mOrderType;
    }

    public void setOrderType(String orderType) {
        mOrderType = orderType;
    }

    public String getOrderid() {
        return mOrderid;
    }

    public void setOrderid(String orderid) {
        mOrderid = orderid;
    }

    public Long getRegId() {
        return mRegId;
    }

    public void setRegId(Long regId) {
        mRegId = regId;
    }

    public String getShopName() {
        return mShopName;
    }

    public void setShopName(String shopName) {
        mShopName = shopName;
    }

    public String getTotal() {
        return mTotal;
    }

    public void setTotal(String total) {
        mTotal = total;
    }

}
