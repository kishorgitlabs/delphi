
package model.sales.order.viewfullhistory;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class OrderMoreDetails {

    @SerializedName("data")
    private List<OrderMoreDetailsResult> mData;
    @SerializedName("result")
    private String mResult;

    public List<OrderMoreDetailsResult> getData() {
        return mData;
    }

    public void setData(List<OrderMoreDetailsResult> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
