
package model.sales.order.viewfullhistory;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class OrderMoreDetailsResult {

    @SerializedName("AmountPerUnit")
    private Double mAmountPerUnit;
    @SerializedName("GrandTotal")
    private Double mGrandTotal;
    @SerializedName("id")
    private Long mId;
    @SerializedName("InsertDate")
    private Object mInsertDate;
    @SerializedName("OrderId")
    private String mOrderId;
    @SerializedName("PartNo")
    private String mPartNo;
    @SerializedName("ProductName")
    private String mProductName;
    @SerializedName("Quantity")
    private String mQuantity;
    @SerializedName("Segment")
    private String mSegment;

    public Double getAmountPerUnit() {
        return mAmountPerUnit;
    }

    public void setAmountPerUnit(Double amountPerUnit) {
        mAmountPerUnit = amountPerUnit;
    }

    public Double getGrandTotal() {
        return mGrandTotal;
    }

    public void setGrandTotal(Double grandTotal) {
        mGrandTotal = grandTotal;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Object getInsertDate() {
        return mInsertDate;
    }

    public void setInsertDate(Object insertDate) {
        mInsertDate = insertDate;
    }

    public String getOrderId() {
        return mOrderId;
    }

    public void setOrderId(String orderId) {
        mOrderId = orderId;
    }

    public String getPartNo() {
        return mPartNo;
    }

    public void setPartNo(String partNo) {
        mPartNo = partNo;
    }

    public String getProductName() {
        return mProductName;
    }

    public void setProductName(String productName) {
        mProductName = productName;
    }

    public String getQuantity() {
        return mQuantity;
    }

    public void setQuantity(String quantity) {
        mQuantity = quantity;
    }

    public String getSegment() {
        return mSegment;
    }

    public void setSegment(String segment) {
        mSegment = segment;
    }

}
