
package model.sales.travelcoordinates;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")

public class InsertTravelCordinatesRegional {

    @SerializedName("data")
    private TravelCoordinateResultRegional mData;
    @SerializedName("result")
    private String mResult;

    public TravelCoordinateResultRegional getData() {
        return mData;
    }

    public void setData(TravelCoordinateResultRegional data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
