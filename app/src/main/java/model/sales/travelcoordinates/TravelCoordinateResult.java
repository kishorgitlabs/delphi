
package model.sales.travelcoordinates;



import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
@Entity(tableName = "attendance")
public class TravelCoordinateResult {

    @PrimaryKey(autoGenerate = true)
    private int pId;
    @ColumnInfo(name="Address")
    @SerializedName("address")
    private String mAddress;
    @ColumnInfo(name="Datetime")
    @SerializedName("datetime")
    private String mDatetime;
    @ColumnInfo(name="Distance")
    @SerializedName("distance")
    private Double mDistance;
    @ColumnInfo(name="EmpId")
    @SerializedName("EmpId")
    private Long mEmpId;
    @ColumnInfo(name="EmpName")
    @SerializedName("EmpName")
    private String mEmpName;
    @ColumnInfo(name="Id")
    @SerializedName("Id")
    private Long mId;
    @ColumnInfo(name="langtitude")
    @SerializedName("langtitude")
    private Double mLangtitude;
    @ColumnInfo(name="latitude")
    @SerializedName("latitude")
    private Double mLatitude;
    @ColumnInfo(name="timedate")
    @SerializedName("timedate")
    private String mTimedate;
    @ColumnInfo(name="TrackId")
    @SerializedName("TrackId")
    private Long mTrackId;
    @ColumnInfo(name="updateddate")
    @SerializedName("updateddate")
    private String mUpdateddate;

    public int getpId() {
        return pId;
    }

    public void setpId(int pId) {
        this.pId = pId;
    }

    public String getmAddress() {
        return mAddress;
    }

    public void setmAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getmDatetime() {
        return mDatetime;
    }

    public void setmDatetime(String mDatetime) {
        this.mDatetime = mDatetime;
    }

    public Double getmDistance() {
        return mDistance;
    }

    public void setmDistance(Double mDistance) {
        this.mDistance = mDistance;
    }

    public Long getmEmpId() {
        return mEmpId;
    }

    public void setmEmpId(Long mEmpId) {
        this.mEmpId = mEmpId;
    }

    public String getmEmpName() {
        return mEmpName;
    }

    public void setmEmpName(String mEmpName) {
        this.mEmpName = mEmpName;
    }

    public Long getmId() {
        return mId;
    }

    public void setmId(Long mId) {
        this.mId = mId;
    }

    public Double getmLangtitude() {
        return mLangtitude;
    }

    public void setmLangtitude(Double mLangtitude) {
        this.mLangtitude = mLangtitude;
    }

    public Double getmLatitude() {
        return mLatitude;
    }

    public void setmLatitude(Double mLatitude) {
        this.mLatitude = mLatitude;
    }

    public String getmTimedate() {
        return mTimedate;
    }

    public void setmTimedate(String mTimedate) {
        this.mTimedate = mTimedate;
    }

    public Long getmTrackId() {
        return mTrackId;
    }

    public void setmTrackId(Long mTrackId) {
        this.mTrackId = mTrackId;
    }

    public String getmUpdateddate() {
        return mUpdateddate;
    }

    public void setmUpdateddate(String mUpdateddate) {
        this.mUpdateddate = mUpdateddate;
    }
}
