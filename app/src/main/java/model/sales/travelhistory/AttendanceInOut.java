package model.sales.travelhistory;

import java.util.List;

public class AttendanceInOut {
    String EmpId;
    String EmpCode;
    String Name;
    String Date;
    String InTime;
    String attendanceTime;
    String InLatitude;
    String InLongitude;
    String AttendDay;
    String OutTime;
    String TotalDuration;
    String OutAddress;
    String OutLongitude;
    String OutLatitude;
    String Address;
    String Designation;
    List<UploadCoordinates> setCoordinateList;

    public String getAttendanceTime() {

        return attendanceTime;
    }

    public void setAttendanceTime(String attendanceTime) {
        this.attendanceTime = attendanceTime;
    }

    public String getEmpId() {
        return EmpId;
    }

    public void setEmpId(String empId) {
        EmpId = empId;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getInTime() {
        return InTime;
    }

    public void setInTime(String inTime) {
        InTime = inTime;
    }

    public String getInLatitude() {
        return InLatitude;
    }

    public void setInLatitude(String inLatitude) {
        InLatitude = inLatitude;
    }

    public String getInLongitude() {
        return InLongitude;
    }

    public void setInLongitude(String inLongitude) {
        InLongitude = inLongitude;
    }

    public String getAttendDay() {
        return AttendDay;
    }

    public void setAttendDay(String attendDay) {
        AttendDay = attendDay;
    }

    public String getOutTime() {
        return OutTime;
    }

    public void setOutTime(String outTime) {
        OutTime = outTime;
    }

    public String getTotalDuration() {
        return TotalDuration;
    }

    public void setTotalDuration(String totalDuration) {
        TotalDuration = totalDuration;
    }

    public String getOutAddress() {
        return OutAddress;
    }

    public void setOutAddress(String outAddress) {
        OutAddress = outAddress;
    }

    public String getOutLongitude() {
        return OutLongitude;
    }

    public void setOutLongitude(String outLongitude) {
        OutLongitude = outLongitude;
    }

    public String getOutLatitude() {
        return OutLatitude;
    }

    public void setOutLatitude(String outLatitude) {
        OutLatitude = outLatitude;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getDesignation() {
        return Designation;
    }

    public void setDesignation(String designation) {
        Designation = designation;
    }

    public List<UploadCoordinates> getSetCoordinateList() {
        return setCoordinateList;
    }

    public void setSetCoordinateList(List<UploadCoordinates> setCoordinateList) {
        this.setCoordinateList = setCoordinateList;
    }

    public String getEmpCode() {
        return EmpCode;
    }

    public void setEmpCode(String empCode) {
        EmpCode = empCode;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
