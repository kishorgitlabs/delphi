
package model.sales.travelhistory;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class MarkAttendanceModel {

    @SerializedName("data")
    private MarkAttendanceResult mData;
    @SerializedName("result")
    private String mResult;

    public MarkAttendanceResult getData() {
        return mData;
    }

    public void setData(MarkAttendanceResult data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
