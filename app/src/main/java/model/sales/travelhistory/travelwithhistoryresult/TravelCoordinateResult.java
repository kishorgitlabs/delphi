
package model.sales.travelhistory.travelwithhistoryresult;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class TravelCoordinateResult {

    @SerializedName("attendancehistorytable")
    private Attendancehistorytable mAttendancehistorytable;
    @SerializedName("attendancetable")
    private Attendancetable mAttendancetable;

    public Attendancehistorytable getAttendancehistorytable() {
        return mAttendancehistorytable;
    }

    public void setAttendancehistorytable(Attendancehistorytable attendancehistorytable) {
        mAttendancehistorytable = attendancehistorytable;
    }

    public Attendancetable getAttendancetable() {
        return mAttendancetable;
    }

    public void setAttendancetable(Attendancetable attendancetable) {
        mAttendancetable = attendancetable;
    }

}
