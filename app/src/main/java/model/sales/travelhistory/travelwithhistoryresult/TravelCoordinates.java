
package model.sales.travelhistory.travelwithhistoryresult;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class TravelCoordinates {

    @SerializedName("data")
    private TravelCoordinateResult mData;
    @SerializedName("result")
    private String mResult;

    public TravelCoordinateResult getData() {
        return mData;
    }

    public void setData(TravelCoordinateResult data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
