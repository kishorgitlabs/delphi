
package model.sales.viewattendance.attendance;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SalesAttendance {

    @SerializedName("data")
    private List<SalesAttendanceResult> mData;
    @SerializedName("result")
    private String mResult;

    public List<SalesAttendanceResult> getData() {
        return mData;
    }

    public void setData(List<SalesAttendanceResult> data) {

        mData = data;
    }

    public String getResult() {

        return mResult;
    }

    public void setResult(String result) {

        mResult = result;
    }

}
