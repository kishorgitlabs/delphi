
package model.sales.viewattendance.attendancehistory;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SalesAttendanceHistory {

    @SerializedName("data")
    private List<SalesAttendanceHistoryResult> mData;
    @SerializedName("result")
    private String mResult;

    public List<SalesAttendanceHistoryResult> getData() {
        return mData;
    }

    public void setData(List<SalesAttendanceHistoryResult> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
