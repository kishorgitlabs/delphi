
package model.sales.viewattendance.attendancesearchbydate;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SalesAttendanceByDate {

    @SerializedName("data")
    private List<SalesAttendanceByDateResult> mData;
    @SerializedName("result")
    private String mResult;

    public List<SalesAttendanceByDateResult> getData() {
        return mData;
    }

    public void setData(List<SalesAttendanceByDateResult> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
