
package model.sales.viewattendance.salescoordinate;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SalesCoordinates {

    @SerializedName("data")
    private List<SalesDataList> mData;
    @SerializedName("result")
    private String mResult;

    public List<SalesDataList> getData() {
        return mData;
    }

    public void setData(List<SalesDataList> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
