
package model.sales.viewattendance.salescoordinate;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SalesDataList {

    @SerializedName("address")
    private String mAddress;
    @SerializedName("datetime")
    private String mDatetime;
    @SerializedName("distance")
    private Double mDistance;
    @SerializedName("EmpId")
    private Long mEmpId;
    @SerializedName("EmpName")
    private String mEmpName;
    @SerializedName("Id")
    private Long mId;
    @SerializedName("langtitude")
    private Double mLangtitude;
    @SerializedName("latitude")
    private Double mLatitude;
    @SerializedName("timedate")
    private String mTimedate;
    @SerializedName("TrackId")
    private Long mTrackId;
    @SerializedName("updateddate")
    private Object mUpdateddate;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getDatetime() {
        return mDatetime;
    }

    public void setDatetime(String datetime) {
        mDatetime = datetime;
    }

    public Double getDistance() {
        return mDistance;
    }

    public void setDistance(Double distance) {
        mDistance = distance;
    }

    public Long getEmpId() {
        return mEmpId;
    }

    public void setEmpId(Long empId) {
        mEmpId = empId;
    }

    public String getEmpName() {
        return mEmpName;
    }

    public void setEmpName(String empName) {
        mEmpName = empName;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Double getLangtitude() {
        return mLangtitude;
    }

    public void setLangtitude(Double langtitude) {
        mLangtitude = langtitude;
    }

    public Double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(Double latitude) {
        mLatitude = latitude;
    }

    public String getTimedate() {
        return mTimedate;
    }

    public void setTimedate(String timedate) {
        mTimedate = timedate;
    }

    public Long getTrackId() {
        return mTrackId;
    }

    public void setTrackId(Long trackId) {
        mTrackId = trackId;
    }

    public Object getUpdateddate() {
        return mUpdateddate;
    }

    public void setUpdateddate(Object updateddate) {
        mUpdateddate = updateddate;
    }

}
