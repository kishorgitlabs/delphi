
package model.sales.visitreport;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Datum {

    @SerializedName("CustMobileNo")
    private String mCustMobileNo;
    @SerializedName("CustomerName")
    private String mCustomerName;
    @SerializedName("CustomerType")
    private String mCustomerType;
    @SerializedName("ExeId")
    private Long mExeId;
    @SerializedName("id")
    private String mId;
    @SerializedName("NextVisit")
    private Object mNextVisit;
    @SerializedName("PlaceOfVisit")
    private String mPlaceOfVisit;
    @SerializedName("PurposeOfVisit")
    private Object mPurposeOfVisit;
    @SerializedName("Remark")
    private String mRemark;
    @SerializedName("SalesPersonName")
    private String mSalesPersonName;
    @SerializedName("Time")
    private Object mTime;
    @SerializedName("VisitedDate")
    private String mVisitedDate;

    public String getCustMobileNo() {
        return mCustMobileNo;
    }

    public void setCustMobileNo(String custMobileNo) {
        mCustMobileNo = custMobileNo;
    }

    public String getCustomerName() {
        return mCustomerName;
    }

    public void setCustomerName(String customerName) {
        mCustomerName = customerName;
    }

    public String getCustomerType() {
        return mCustomerType;
    }

    public void setCustomerType(String customerType) {
        mCustomerType = customerType;
    }

    public Long getExeId() {
        return mExeId;
    }

    public void setExeId(Long exeId) {
        mExeId = exeId;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public Object getNextVisit() {
        return mNextVisit;
    }

    public void setNextVisit(Object nextVisit) {
        mNextVisit = nextVisit;
    }

    public String getPlaceOfVisit() {
        return mPlaceOfVisit;
    }

    public void setPlaceOfVisit(String placeOfVisit) {
        mPlaceOfVisit = placeOfVisit;
    }

    public Object getPurposeOfVisit() {
        return mPurposeOfVisit;
    }

    public void setPurposeOfVisit(Object purposeOfVisit) {
        mPurposeOfVisit = purposeOfVisit;
    }

    public String getRemark() {
        return mRemark;
    }

    public void setRemark(String remark) {
        mRemark = remark;
    }

    public String getSalesPersonName() {
        return mSalesPersonName;
    }

    public void setSalesPersonName(String salesPersonName) {
        mSalesPersonName = salesPersonName;
    }

    public Object getTime() {
        return mTime;
    }

    public void setTime(Object time) {
        mTime = time;
    }

    public String getVisitedDate() {
        return mVisitedDate;
    }

    public void setVisitedDate(String visitedDate) {
        mVisitedDate = visitedDate;
    }

}
