
package model.sales.visitreport;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class VisitReportModel {

    @SerializedName("data")
    private Object mData;
    @SerializedName("result")
    private String mResult;

    public Object getData() {
        return mData;
    }

    public void setData(Object data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
