
package model.servicepart;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ServicePartlist {

    @SerializedName("DTVS_Part_No")
    private String mDTVSPartNo;
    @SerializedName("id")
    private String mId;
    @SerializedName("illustrationNo")
    private String mIllustrationNo;
    @SerializedName("Image")
    private String mImage;
    @SerializedName("InsertDate")
    private String mInsertDate;
    @SerializedName("LineSeqNo")
    private String mLineSeqNo;
    @SerializedName("PartDescription")
    private String mPartDescription;
    @SerializedName("PartNo")
    private String mPartNo;
    @SerializedName("QtyFip")
    private String mQtyFip;

    public String getDTVSPartNo() {
        return mDTVSPartNo;
    }

    public void setDTVSPartNo(String dTVSPartNo) {
        mDTVSPartNo = dTVSPartNo;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getIllustrationNo() {
        return mIllustrationNo;
    }

    public void setIllustrationNo(String illustrationNo) {
        mIllustrationNo = illustrationNo;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public String getInsertDate() {
        return mInsertDate;
    }

    public void setInsertDate(String insertDate) {
        mInsertDate = insertDate;
    }

    public String getLineSeqNo() {
        return mLineSeqNo;
    }

    public void setLineSeqNo(String lineSeqNo) {
        mLineSeqNo = lineSeqNo;
    }

    public String getPartDescription() {
        return mPartDescription;
    }

    public void setPartDescription(String partDescription) {
        mPartDescription = partDescription;
    }

    public String getPartNo() {
        return mPartNo;
    }

    public void setPartNo(String partNo) {
        mPartNo = partNo;
    }

    public String getQtyFip() {
        return mQtyFip;
    }

    public void setQtyFip(String qtyFip) {
        mQtyFip = qtyFip;
    }

}
