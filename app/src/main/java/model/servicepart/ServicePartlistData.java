
package model.servicepart;

import java.util.List;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ServicePartlistData {

    @SerializedName("data")
    private List<ServicePartlist> mData;
    @SerializedName("result")
    private String mResult;

    public List<ServicePartlist> getData() {
        return mData;
    }

    public void setData(List<ServicePartlist> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
