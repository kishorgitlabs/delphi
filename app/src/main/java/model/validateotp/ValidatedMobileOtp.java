
package model.validateotp;


import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class ValidatedMobileOtp {

    @SerializedName("data")
    private ValidatedOtpData mData;
    @SerializedName("result")
    private String mResult;

    public ValidatedOtpData getData() {
        return mData;
    }

    public void setData(ValidatedOtpData data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
