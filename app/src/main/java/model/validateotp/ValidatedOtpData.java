
package model.validateotp;


import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class ValidatedOtpData {

    @SerializedName("Address")
    private Object mAddress;
    @SerializedName("BussinessName")
    private Object mBussinessName;
    @SerializedName("Emailid")
    private String mEmailid;
    @SerializedName("Flag")
    private String mFlag;
    @SerializedName("id")
    private Long mId;
    @SerializedName("InsertDate")
    private String mInsertDate;
    @SerializedName("MobileNo")
    private String mMobileNo;
    @SerializedName("Name")
    private String mName;
    @SerializedName("OTP")
    private String mOTP;
    @SerializedName("OTPStatus")
    private Object mOTPStatus;
    @SerializedName("UserType")
    private String mUserType;

    public Object getAddress() {
        return mAddress;
    }

    public void setAddress(Object address) {
        mAddress = address;
    }

    public Object getBussinessName() {
        return mBussinessName;
    }

    public void setBussinessName(Object bussinessName) {
        mBussinessName = bussinessName;
    }

    public String getEmailid() {
        return mEmailid;
    }

    public void setEmailid(String emailid) {
        mEmailid = emailid;
    }

    public String getFlag() {
        return mFlag;
    }

    public void setFlag(String flag) {
        mFlag = flag;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getInsertDate() {
        return mInsertDate;
    }

    public void setInsertDate(String insertDate) {
        mInsertDate = insertDate;
    }

    public String getMobileNo() {
        return mMobileNo;
    }

    public void setMobileNo(String mobileNo) {
        mMobileNo = mobileNo;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getOTP() {
        return mOTP;
    }

    public void setOTP(String oTP) {
        mOTP = oTP;
    }

    public Object getOTPStatus() {
        return mOTPStatus;
    }

    public void setOTPStatus(Object oTPStatus) {
        mOTPStatus = oTPStatus;
    }

    public String getUserType() {
        return mUserType;
    }

    public void setUserType(String userType) {
        mUserType = userType;
    }

}
