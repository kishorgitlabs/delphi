package room;



import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import model.CartDAO;
import model.sales.travelhistory.UploadCoordinates;
import model.sales.travelhistory.travelwithhistoryresult.UploadCoordinatesRegional;

@Dao
public interface ProductDAO {

    @Query("DELETE FROM attendance")
    void deleteAttendance();

    @Query("DELETE FROM regionalattendance")
    void deleteAttendances();

    @Insert
    void insertAttendance(UploadCoordinates products);
    @Insert
    void insertAttendances(UploadCoordinatesRegional products);

    @Query("SELECT * FROM attendance")
    List<UploadCoordinates> getCoordinates();

    @Query("SELECT * FROM regionalattendance")
    List<UploadCoordinatesRegional> getCoordinate();

    @Insert
    void insertCartdao(CartDAO cartDAO);

    @Query("SELECT * FROM cart")
    List<CartDAO> getCartDao();

    @Query("SELECT customermobilenumber FROM cart")
    String getCustomerMobileNo();

    @Query("UPDATE cart SET totalamount=:totalamount,qty=:qty WHERE partnumber=:partnumber")
    void updatecart(String totalamount,String qty,String partnumber);

    @Query("DELETE FROM cart WHERE partnumber=:partnumber")
    void deletepartnumber(String partnumber);


    @Query("SELECT partnumber  FROM cart")
    List<String> getpartnumber();


    @Query("DELETE FROM cart")
    void deleteccart();

}
