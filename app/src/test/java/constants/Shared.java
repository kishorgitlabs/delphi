package constants;

public class Shared {
    public static final String MySharedPreferences = "Shared_Delphi";
    public static final String Shared_isregister = "isregister";
    public static final String Shared_Update = "isupdated";

    public static final String Shared_Device_ID = "device_id";
    public static final String Shared_App_ID = "app_id";
    public static final String Shared_reg_ID = "reg_id";
    public static final String Shared_islogin = "islogin";
    public static final String Shared_User_type= "user_type";


    //public static final String Shared_type = "type";


}
